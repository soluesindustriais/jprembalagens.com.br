<?
	$h1    		= 'Produtos';
	$title 		= 'Produtos';
	$desc  		= 'Conheça os Produtos que a JPR Embalagens trabalha, como, envelopes, sacos e sacolas sob medida, fabricamos produtos personalizados de acordo com a necessidade da sua empresa.';
	$key   		= 'envelopes, sacos, sacolas, sob, medida, personalizados';
	$var 		= 'Produtos';
	
	include('inc/head.php');
?>

</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
    
                  
                	<?=$caminho?>	
                   
                	<article>
                    
                    <h1><?=$h1?></h1>   
        	            
            		<h2>Conheça os Produtos da JPR Embalagens nas categorias abaixo:</h2>
            
                        <ul class="thumbnails">
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>envelopes" title="Envelopes"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/envelopes-01.jpg" alt="Envelopes" /></a>
                                  <h2><a href="<?=$url;?>envelopes" title="Envelopes">Envelopes</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>sacos" title="Sacos"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/sacos-01.jpg" alt="Sacos" /></a>
                                  <h2><a href="<?=$url;?>sacos" title="Sacos">Sacos</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>sacolas" title="Sacolas"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/sacolas-01.jpg" alt="Sacolas" /></a>
                                  <h2><a href="<?=$url;?>sacolas" title="Sacolas">Sacolas</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>bobinas" title="Bobinas"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/bobinas-01.jpg" alt="Bobinas" /></a>
                                  <h2><a href="<?=$url;?>bobinas" title="Bobinas">Bobinas</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>capas" title="Capas"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/capas-01.jpg" alt="Capas" /></a>
                                  <h2><a href="<?=$url;?>capas" title="Capas">Capas</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>cobertura" title="Cobertura"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/cobertura-01.jpg" alt="Cobertura" /></a>
                                  <h2><a href="<?=$url;?>cobertura" title="Cobertura">Cobertura</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>filme" title="Filme"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/filme-01.jpg" alt="Filme" /></a>
                                  <h2><a href="<?=$url;?>filme" title="Filme">Filme</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>plastico" title="Plástico"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/plastico-01.jpg" alt="Plástico" /></a>
                                  <h2><a href="<?=$url;?>plastico" title="Plástico">Plástico</a></h2>
                             </li>
                             <li>
                                  <a rel="nofollow" href="<?=$url;?>rolo" title="Rolo de Plástico"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/thumb/rolo-01.jpg" alt="Rolo de Plástico" /></a>
                                  <h2><a href="<?=$url;?>rolo" title="Rolo de Plástico">Rolo de Plástico</a></h2>
                             </li>
                        </ul>
                        
                        </article>    
                        
                        <? include('inc/coluna-lateral.php');?>       	
    
    		<br class="clear" />  
        
            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>