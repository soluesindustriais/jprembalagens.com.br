<?php
	$h1    			= 'Bobina tubular';
	$title 			= 'Bobina tubular';
	$desc  			= 'A bobina tubular feita em polipropileno é bastante usada em indústrias alimentícias, em gráficas e também em confecções, por se caracterizar pelo brilho, transparência';
	$key   			= 'bobina, tubular, bobinas tubulares';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas tubulares';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina tubular</strong> é uma embalagem bastante diversa e altamente recomendada para a sua empresa. Confira maiores informações.</p>

            <p>A embalagem é um aspecto fundamental para que o produto esteja bem protegido e sua empresa possa vender mais. Por isso, uma ótima opção para a sua empresa é a <strong>bobina tubular</strong>.</p>
            
            <p>A <strong>bobina tubular</strong> é um tipo de embalagem bastante versátil, que pode ser fabricado conforme a necessidade de cada cliente. A produção pode ser feita em polietileno de alta ou de baixa densidade, em polipropileno, BOPP, laminado, entre outras opções.</p>
            
            <p>A <strong>bobina tubular</strong> feita em polipropileno é bastante usada em indústrias alimentícias, em gráficas e também em confecções, por se caracterizar pelo brilho, transparência. A opção em polietileno é um plástico resistente e atóxico e, por isso, pode ser usado par aa embalagem de qualquer tipo de produto. Outra opção é em BOPP, usado bastante no mercado de varejo como embalagem de presente.</p>
            
        	<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>             

            <h2>Bobina tubular também pode ser feita com material reciclado</h2>
            
            <p>A <strong>bobina tubular</strong> também pode ser feita usando material reciclado. Na opção com reciclado cristal, a produção é feita a partir de aparas de material virgem, dando um aspecto amarelado à embalagem, mas mantendo a transparência.</p>
            
            <p>Outra opção é a <strong>bobina tubular</strong> reciclado canela, feita a partir de aparas de reciclado cristal, com cor alterada para canela, mas ainda mantendo a transparência da embalagem, resultando em possibilidade de visualizar o que está sendo embalado.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina tubular</strong> feita com material reciclado é uma maneira de melhorar a imagem da sua empresa no mercado, por indicar a preocupação da sua marca com as questões ambientais. Além disso, as embalagens recicladas representam uma economia de custos para a sua empresa.</p>
            
            <p>E para adquirir a <strong>bobina tubular</strong>, conte com a JPR Embalagens. A empresa está no mercado há mais de 15 anos e leva até o consumidor as melhores opções de embalagem, identificando oportunidades de melhoria para reduzir perdas e custos.</p>
            
            <p>O atendimento na JPR Embalagens pode ser personalizado, com possibilidade de fabricação de material liso ou impresso em até seis cores, e também com personalização de medidas. A empresa tem preços em conta e ótimas condições de pagamento, resultando em ótima relação custo-benefício. Por isso, aproveite e solicite já o seu orçamento.</p>
            
                        
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>