<?php
$h1    			= 'Saco valvulado';
$title 			= 'Saco valvulado';
$desc  			= 'O saco valvulado geralmente é utilizado por indústrias de alimentos, adubos, produtos químicos, plásticos e minérios. É uma opção para reduzir seus custos com embalagens.';
$key   			= 'Sacos valvulados, Saco, sacos, valvulado, Saco com valvula';
$var 			= 'Sacos valvulados';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <p>Fabricado em polietileno natural ou pigmentado em diversas cores, o <strong>saco valvulado</strong> pode ser liso ou impresso em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>É uma embalagem que possui um sistema de fechamento prático e eficiente. A válvula se fecha automaticamente através da compressão do produto ensacado.</p>
             <p>Por ser uma embalagem versátil, são fabricados com um sistema avançado de filamentos selados, além dos nossos produtos serem isentos de costuras e facilitam os processos de enchimento automático, paletização, transporte e armazenagem.</p>
             <p>O <strong>saco valvulado</strong> geralmente é utilizado por indústrias de alimentos, adubos, produtos químicos, plásticos e minérios.</p>
             <p>Também podem ser produzidos com matéria-prima reciclada. É uma opção para reduzir seus custos com embalagens.</p>
             <p>Este produtos possuem alta resistência à tração, são flexíveis, são resistentes ao frio e inodoro. O polímero utilizado é qualificado para produtos alimentares.</p>
             <h2>Há vários formatos que o saco valvulado pode ser produzido, veja alguns modelos:</h2>
             <ul class="list">
                <li><strong>Saco valvulado com válvula topo e fundo quadrado</strong>.</li>
                <li><strong>Saco valvulado com válvula topo e fundo sanfonado</strong>.</li>
                <li><strong>Saco valvulado com válvula topo com fundo reto</strong>.</li>
                <li><strong>Saco valvulado com válvula traseira</strong>, próprio para argamassa. (Este modelo não fabricamos) </li>
            </ul>  
            <p>Nossa quantidade mínima de produção são de 200kg para <strong>sacos valvulados lisos</strong> e 300kg impressos.</p>
            <p>Para receber um orçamento de <strong>saco valvulado</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>