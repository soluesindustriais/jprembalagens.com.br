<?php
$h1    			= 'Saco plástico PE';
$title 			= 'Saco plástico PE';
$desc  			= 'O saco plástico PE (polietileno) pode ser fabricado em polietileno de alta ou baixa densidade. Uma infinidade de produtos que podem ser embalados com ele.';
$key   			= 'Sacos plásticos PE, Saco, sacos, plástico, PE';
$var 			= 'Sacos plásticos PE';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>
                       <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     

             <br>  

             <p>O <strong>saco plástico PE</strong> (polietileno) pode ser fabricado em polietileno de alta ou baixa densidade.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>
             <p>É uma infinidade de produtos que podem ser embalados com o <strong>saco plástico PE</strong>, por ser uma embalagem super versátil e protetora, e podem ser lisos ou impresso em até 6 cores.</p>
             <p>Além disso, na linha embalagens flexíveis, o <strong>PE (polietileno)</strong>, é uma das embalagens que podem ser 100% recicladas e reutilizadas. Neste caso, se você deseja reduzir custos com embalagem, utiliza o <strong>saco plástico PE reciclado</strong>. </p>
             <h2>Veja abaixo algumas opções do saco plástico PE:</h2>
             <ul class="list">
                <li><strong>Saco plástico PE reciclado cristal</strong>: é produzido a partir da sobra ou descarte do plástico virgem, e a embalagem fica com um aspecto amarelado e com pontos, devido ao processo da reciclagem.</li>
                <li><strong>Saco plástico PE reciclado canela</strong>: é produzido a partir das sobras ou descartes do plástico reciclado cristal e outros. Nesta opção, o aspecto visual da embalagem ficará na cor canela, porem é transparente, sendo possível visualizar perfeitamente o que há dentro da embalagem.</li>
                <li><strong>Saco plástico PE reciclado colorido</strong> : é produzido a partir da mistura de várias embalagens, como <strong>sacos</strong>, sacolas, <a href="<?=$url;?>saco-lixo" title="Saco de Lixo"><strong>sacos de lixo</strong></a>,  embalagens de alimentos, entre muitos outros. Neste formato, a embalagem fica sem padrão de cor, geralmente cinza escura, verde escuro ou até mesmo preto, perdendo completamente a transparência, não sendo possível visualizar o que há dentro da embalagem. </li>
            </ul>
            <p>Este tipo de embalagem é amplamente utilizado por indústrias alimentícias, têxteis, confecções, gráficas, editoras, laboratórios, entre muitos outros.</p>
            <p>Nossa quantidade mínima de produção de <strong>saco plástico PE</strong> são de 150kg liso e 250kg impresso.</p>
            <p>Para receber um orçamento de <strong>saco plástico PE</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            

        </article>

        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  

        <br class="clear" />  
        


        <?php include('inc/regioes.php');?>

        <?php include('inc/copyright.php');?>


    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>