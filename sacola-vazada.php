<?php
$h1    			= 'Sacola vazada';
$title 			= 'Sacola vazada';
$desc  			= 'A sacola vazada é produzida com uma porcentagem de matéria-prima reciclada, porem mantem a mesma resistência e segurança que o plástico virgem.';
$key   			= 'Sacola, vazada, Sacolas vazadas, sacola vazada personalizada, sacola vazada impressa';
$var 			= 'Sacolas vazadas';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <h2>Sacolas vazadas em diversos tipos e cores</h2>
             <p>Fabricamos <strong>sacola vazada</strong> em polietileno de alta (PEAD) ou baixa densidade (PEBD).</p>
             <p>Podem ser lisas ou impressas em até 6 cores, além da <strong>sacola vazada</strong> ser produzida na cor natural ou pigmentada em diversas cores.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <h2>Versatilidade</h2>
             <p>A <strong>sacola vazada</strong> é amplamente utilizada por farmácias, laboratórios, lojas, gráficas, em eventos, entre outros. </p>
             <p>É um produto super versátil, podendo ser produzida com aditivo oxibiodegradavel, e nesta opção, a <strong>sacola vazada</strong> em contato com o meio ambiente, se degrada em curto espaço de tempo. A natureza agradece.</p>
             <p>Para reduzir custos com embalagem, utilize a <strong>sacola vazada reciclada</strong>. Neste formato, a <strong>sacola vazada</strong> é produzida com uma porcentagem de matéria-prima reciclada, porem mantem a mesma resistência e segurança que o plástico virgem.</p>
             <p>São produzidas sob medida, de acordo com a necessidade de cada cliente. </p>
             <p>Fabricamos a partir de 250kg para a <strong>sacola vazada</strong> com impressão, e sem impressão mínimo de 150kg.</p>
             <p>Para receber um orçamento de <strong>sacola vazada</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>