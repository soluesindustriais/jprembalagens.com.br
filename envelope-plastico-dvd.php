<?php
	$h1    			= 'Envelope plástico DVD';
	$title 			= 'Envelope plástico DVD';
	$desc  			= 'O envelope plástico DVD é fabricado em polipropileno, pois este material é transparente e possui um aspecto brilhoso';
	$key   			= 'Envelopes plástico DVD, Envelopes, Envelope, plástico, DVD, capa para dvd, envelope para dvd';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes  plástico DVD';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br>  
                    
                                                
            <p>O <strong>envelope plástico DVD</strong> é fabricado em polipropileno, pois este material é transparente e possui um aspecto brilhoso, deixando a mostra o produto.</p>
            
			<? $pasta = "imagens/produtos/envelopes/"; $quantia = 3; include('inc/gallery.php'); ?>
            
            <p>Fabricados sob medida, os <strong>envelopes plásticos DVD</strong> podem ser fabricados com diversos tipos de fecho, com aba adesiva, botões, ilhós, e também podem ser fechados em seladoras manuais.</p>

            <p>Você também pode optar por <strong>envelope plástico DVD</strong> liso ou personalizado em até 6 cores, assim é uma forma de divulgar a sua marca, além de seu produto causar boa impressão à primeira vista e conquistar consumidores Nosso lote mínimo de produção são de 300kg para envelope plástico DVD com impressão, e 200kg sem impressão.</p>
            
            <p>Outra opção que também temos, são os <strong>envelopes plásticos DVD</strong> com sopala e furo, pois assim seu produto pode ser colocado em displays, e você se diferencia dos seus concorrentes.</p>
            
            <p>A aba adesiva que é aplicada no <strong>envelope plástico DVD</strong>, pode ser permanente e assim o produto se torna inviolável, ou abre e fecha, neste formato o produto poderá ser acessado diversas vezes.</p>
            
            <p>Além de <strong>envelope plástico DVD</strong>, a JPR Embalagens fabrica outros tipos de embalagens, como sacos plásticos, sacolas com alça vazada, sacolas com alça camiseta, bobinas plásticas e envelopes plásticos.</p>
            
            <p>Todos esses produtos podem também ser lisos ou personalizados, e com outros tipos de acessórios como o fecho zip lock, tala, botão, ilhos, entre muitos outros.</p>
            
            <p>Com o foco em sustentabilidade e preocupação com nosso meio ambiente, estamos fabricando envelope plástico DVD com o aditivo oxibiodegradavel. O plástico comum leva mais de 100 anos para se decompor na natureza, e com o uso do aditivo oxibiodegradavel, o plástico em contato com o meio ambiente se degrada em um curto espaço de tempo, levando até seis meses para se decompor.</p>
            
            <p>Para receber um orçamento do <strong>envelope plástico DVD</strong>, basta possuir as medidas (largura x comprimento x espessura), e a quantidade que você deseja utilizar.</p>
			
			
			
            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>