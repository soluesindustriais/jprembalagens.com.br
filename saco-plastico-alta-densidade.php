<?php
$h1    			= 'Saco plástico de alta densidade';
$title 			= 'Saco plástico de alta densidade';
$desc  			= 'O saco plástico de alta densidade (PEAD), pode ser liso ou impresso em até 6 cores, além de pode ser produzido na cor transparente ou pigmentado em diversas cores.';
$key   			= 'Sacos plásticos de alta densidade, Saco, sacos, plástico, alta, densidade';
$var 			= 'Sacos plásticos de alta densidade';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             <p>Fabricamos <strong>saco plástico de alta densidade</strong> sob medida, de acordo com a necessidade de cada cliente.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>O <strong>saco plástico de alta densidade</strong> (PEAD), pode ser liso ou impresso em até 6 cores, além de pode ser produzido na cor transparente ou pigmentado em diversas cores.</p>
             <p>Geralmente produzido em espessuras finas, o <strong>saco plástico de alta densidade</strong> é amplamente utilizado para proteção de diversos produtos, como eletrodomésticos, envio de jornais e revistas, plásticos, entre muito outros.</p>
             <p>Também pode ser produzido com aba adesiva, assim facilitando o fechamento da embalagem, e é uma embalagem 100% reciclável.</p>
             <p>A principal característica do <strong>saco plástico de alta densidade</strong> é o seu aspecto visual fosco e opaco, além de ser uma das embalagens mais resistentes ao rasgo e a ruptura. A matéria-prima de polietileno de alta densidade é muito encontrada em <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plásticas</strong></a>, bobinas picotadas, por serem embalagens leves e finas, porem resistentes.</p>
             <p>Para contribuir com o nosso planeta, utilize <strong>saco plástico de alta densidade reciclado</strong>, pois é uma forma de reduzir seus custos com embalagem, além de ser uma embalagem ecologicamente correta.  </p> 
             <p>O <strong>saco plástico de alta densidade reciclado</strong> é produzido a partir de aparas do material virgem e também de outras embalagens que foram recicladas, como <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plásticos</strong></a> em geral, embalagens de alimentos, entre outros, e por ser reciclado, a cor da embalagem altera para amarelo bem claro, porem continua com o seu aspecto fosco e opaco, e continua sendo uma embalagem totalmente transparente, ou seja, é possível identificar perfeitamente o que há dentro da embalagem.</p>
             <p>Nossa quantidade mínima de produção são de 100kg para <strong>saco plástico alta densidade</strong> liso e 300kg impresso.</p>
             <p>Para receber um orçamento de <strong>saco plástico alta densidade</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>