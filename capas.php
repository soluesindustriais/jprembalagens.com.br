<?
$h1    		= 'Capas';
$title 		= 'Capas';
$desc  		= 'A JPR Embalagens traz para o mercado uma linha completa de Capas. Em nossa linha de produtos você encontra capas fabricadas.';
$key   		= 'Capas, Capas plásticas, Capas para pallet';
$var 		= 'Capas';

include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

  <div class="wrapper-topo">

    <? include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

      <section>

          
          <?=$caminhoCategoria?>
          <article>

          <h1><?=$h1?></h1>   

          <h2>A JPR Embalagens traz para o mercado uma linha completa de capas.</h2>

          <p>Em nossa linha de produtos você encontra <strong>capas fabricados sob medida</strong>, de acordo com a necessidade da sua empresa.</p>

          <p>Conheça nossa linha de <strong>envelopes</strong>:</p>

          <ul class="thumbnails">
           <li>
            <a rel="nofollow" href="<?=$url;?>capa-polietileno" title="Capa em Polietileno"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/capas/thumb/capa-polietileno-01.jpg" alt="Capa em Polietileno" /></a>
            <h2><a href="<?=$url;?>capa-polietileno" title="Capa em Polietileno">Capa em Polietileno</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>capa-pallet" title="Capa Pallet"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/capas/thumb/capa-pallet-01.jpg" alt="Capa Pallet" /></a>
            <h2><a href="<?=$url;?>capa-pallet" title="Capa Pallet">Capa Pallet</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>capa-plastica" title="Capa Plástica"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/capas/thumb/capa-plastica-01.jpg" alt="Capa Plástica" /></a>
            <h2><a href="<?=$url;?>capa-plastica" title="Capa Plástica">Capa Plástica</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>capa-plastica-palete" title="Capa Plástica para Palete"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/capas/thumb/capa-plastica-palete-01.jpg" alt="Capa Plástica para Palete" /></a>
            <h2><a href="<?=$url;?>capa-plastica-palete" title="Capa Plástica para Palete">Capa Plástica para Palete</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>capa-plastica-pallet" title="Capa Plástica para Pallet"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/capas/thumb/capa-plastica-pallet-01.jpg" alt="Capa Plástica para Pallet" /></a>
            <h2><a href="<?=$url;?>capa-plastica-pallet" title="Capa Plástica para Pallet">Capa Plástica para Pallet</a></h2>
          </li>
        </ul>

        <? include('inc/saiba-mais.php');?>

      </article>

      <? include('inc/coluna-lateral-paginas.php');?>   

      <br class="clear" />  

      <? include('inc/regioes.php');?>

    </section>

  </main>



</div><!-- .wrapper -->



<? include('inc/footer.php');?>


</body>
</html>