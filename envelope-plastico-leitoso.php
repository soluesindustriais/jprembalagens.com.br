<?php
	$h1    			= 'Envelope plástico leitoso';
	$title 			= 'Envelope plástico leitoso';
	$desc  			= 'Nosso envelope plástico leitoso é amplamente utilizado por laboratórios, gráficas, editoras e empresas em geral. Pode ser fabricado com diversos acessórios.';
	$key   			= 'Envelopes plásticos leitosos, Envelopes, Envelope, plástico, leitoso';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos leitosos';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
     
            		<? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>                       
                    <p>Produzidos em polietileno ou polipropileno, o <strong>envelope plástico leitoso</strong> pode ser liso ou impresso em até 6 cores. </p>
                    
                    
                    
                    <p>Nosso <strong>envelope plástico leitoso</strong> é amplamente utilizado por laboratórios, gráficas, editoras e empresas em geral. Por ser uma embalagem versátil, e também pode ser fabricado com diversos acessórios como:</p>
                    
                    <ul class="list">
                        <li><strong>envelope plástico leitoso com aba adesiva</strong>;</li>
                        <li><strong>envelope plástico leitoso com fecho zip</strong>;</li>
                        <li><strong>envelope plástico leitoso com ilho</strong>;</li>
                        <li><strong>envelope plástico leitoso com botão</strong>;</li>
                        <li>Entre outros.</li>
                    </ul>	
                    <p>Se você deseja contribuir com o meio ambiente, utilize <strong>envelope plástico leitoso reciclado</strong> ou oxi-biodegradavel.</p>
                    
                    <h2>Envelope plástico leitoso reciclado</h2>
                    
                    <p>O <strong>envelope plástico leitoso</strong> reciclado é fabricado a partir de matérias-primas recicladas, ou seja, utilizamos o descarte de outras embalagens, como sacos de arroz, feijão, <a href="<?=$url;?>saco-leitoso" title="Saco Leitoso"><strong>saco leitoso</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, e outros tipos de embalagens, reciclamos e assim fabricamos uma nova embalagem. </p>
                    <p>Outra forma de você obter uma embalagem sustentável é com o <strong>envelope plástico leitoso oxi-biodegradavel</strong>, este é um aditivo adicionado no processo de fabricação da embalagem, e que proporciona rápida degradação da embalagem em contato com a natureza. Enquanto o plástico comum levam mais de 100 anos para se decompor, o plástico com o aditivo oxi-biodegradavel pode levar até 6 meses para se degradar.</p>
                    <p>Além do <strong>envelope plástico leitoso</strong>, a JPR Embalagens fabrica outros tipos de embalagem, como bobinas plásticas, <a href="<?=$url;?>sacos" title="Sacos"><strong>Sacos</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>Sacolas</strong></a>, <a href="<?=$url;?>envelope-plastico-personalizado" title="Envelope Plástico Personalizado">Envelope Plástico Personalizado</a>, <a href="<?=$url;?>envelope-plastico-impresso" title="Envelope Plástico Impresso">Envelope Plástico Impresso</a> e laminados.</p>
                    <p>Produzimos a partir de 250kg para <strong>envelope plástico leitoso impresso</strong> e 150kg para envelopes lisos.</p>
                    <p>Para receber um orçamento desse produto, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade que deseja utilizar.</p>

            
			<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>