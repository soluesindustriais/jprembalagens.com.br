<?php
	$h1    			= 'Bobina de plástico';
	$title 			= 'Bobina de plástico';
	$desc  			= 'A bobina de plástico pode ser feita em diversos tamanhos, transparente ou pigmentada, lisa ou impressa, com impressão de ótima qualidade';
	$key   			= 'bobina, plástico, bobinas de plásticos';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de plásticos';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>   
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Embalagem bastante versátil e empregada em diversos segmentos, a <strong>bobina de plástico</strong> proporciona ótima relação custo-benefício. Confira as vantagens.</p>

            <p>Além da qualidade do produto, uma empresa deve lembrar-se sempre da importância de uma embalagem que seja atraente, prática para o consumidor e que garanta a proteção do seu produto em todas as etapas. Por isso, a <strong>bobina de plástico</strong> é uma opção vantajosa.</p>
            
            <p>A <strong>bobina de plástico</strong> pode ser confeccionada em PEAD, PEBD, PEBDL ou PP. No caso de bobinas PP (polipropileno), a embalagem se caracteriza pelo brilho e pela transparência, com grande utilização em gráficas, confecções e indústrias alimentícias.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O segmento alimentício também utiliza bastante a <strong>bobina de plástico</strong> feita em polietileno, que é um plástico atóxico e resistente, o que possibilita a embalagem de qualquer tipo de produto. Por isso, envolve alimentos, bebidas, farináceos e leites, além de produtos da área de automotrizes e metalurgia. Neste caso, a embalagem não causa nenhum tipo de alteração nas propriedades do produto que está sendo embalado.</p>
            <p>Você pode se interessar também por <a target='_blank' title='Bobina de saco plástico transparente' href='https://www.jprembalagens.com.br/bobina-de-saco-plastico-transparente'>Bobina de saco plástico transparente</a>. Veja mais detalhes ou solicite um <b>orçamento gratuito</b> com um dos fornecedores disponíveis!</p>
            <h2>Bobina de plástico pode ser personalizada de diversas formas</h2>
            
            <p>Uma grande vantagem da <strong>bobina de plástico</strong> é a possibilidade de personalização. A impressão pode ser feita em até oito cores no caso de flexografia ou em até nove cores para rotogravura, com laminação com e sem solvente.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina de plástico</strong> pode ser feita em diversos tamanhos, transparente ou pigmentada, lisa ou impressa, com impressão de ótima qualidade. Para isso, conte com o profissionalismo da JPR Embalagens.</p>
            
            <p>A JPR Embalagens é uma empresa que atua no segmento de embalagens plásticas flexíveis já mais de 15 anos, sempre levando até os clientes o que já de mais moderno em tecnologia e com a melhor relação custo-benefício do mercado.</p>
            
            <p>A equipe da empresa proporciona um atendimento totalmente personalizado, que é voltado para as necessidades e demandas de cada cliente. Na JPR Embalagens é possível adquirir <strong>bobina de plástico</strong> conforme suas preferências, com preço em conta e ótimas condições de pagamento.</p>
            
            <p>Saiba mais sobre o produto e as vantagens da empresa entrando em contato com um dos consultores. Aproveite as ótimas condições e solicite já o seu orçamento, informando as medidas e quantidade de <strong>bobina de plástico</strong> que você necessita.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>