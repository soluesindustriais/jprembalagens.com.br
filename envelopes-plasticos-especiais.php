<?php
	$h1    			= 'Envelopes plásticos especiais';
	$title 			= 'Envelopes plásticos especiais';
	$desc  			= 'Nosso envelopes plásticos especiais, podem ser lisos ou impresso em até 6 cores, além de serem coloridos ou transparentes.';
	$key   			= 'Envelope plástico especial, Envelopes, Envelope, plásticos, especiais, Envelopes plásticos sob medida';
	$var 			= 'Envelope plástico especial';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
               
            <h2>Envelopes plásticos especiais fabricados sob medida</h2>   
                                
            <p>A JPR Embalagens trabalha com uma linha de <strong>envelopes plásticos especiais</strong>, fabricados sob medida, de acordo com a necessidade de cada cliente.</p>
            
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>

            
            <p>Nosso <strong>envelopes plásticos especiais</strong>, podem ser lisos ou impresso em até 6 cores, além de serem coloridos ou transparentes.</p>
            <p>A modernidade da embalagem fala muito do produto que ela contém, assim, nossos <strong>envelopes plásticos especiais</strong> são fabricados com excelente qualidade, pois a embalagem impressa se torna o seu passaporte para novos clientes. </p>
            <h2>Versatilidade</h2>
            <p>Os <strong>envelopes plásticos especiais</strong> são super versáteis, assim podem receber diversos tipos de acabamentos e fechos, como aba adesiva, fecho zip lock, <a href="<?=$url;?>envelope-plastico-ilhos" title="Envelope Plástico Com Ilhós">ilhós</a>, botão, além de poderem ser reciclados.</p>
            <p>Com o intuito em contribuir com a sustentabilidade, estamos produzindo <strong>envelopes plásticos especiais</strong> a partir de matérias-primas recicladas, assim compramos as resinas plásticas feitas a partir de embalagens de alimentos, vestuários, <strong>envelopes</strong>, entre várias outras, e as transformamos em novas embalagens. A natureza agradece.</p>
            <p>Outra forma de embalagens sustentáveis que produzimos, são os <strong>envelopes plásticos especiais</strong> oxi-biodegradaveis. O plástico comum, leva em média 100 anos para se decompor, e quando é adicionado durante o processo de extrusão o aditivo oxi-biodegradavel, ele faz com que a embalagem se degrade em curto espaço de tempo, em media 6 meses. </p>
            <p>Se você deseja contribuir com o meio ambiente, utiliza <strong>envelopes plásticos especiais</strong> reciclados ou oxi-biodegradavel. Pequenas ações podem contribuir para o futuro da humanidade.</p>
            <p>Nosso lote mínimo de produção para <strong>envelopes plásticos especiais</strong>, são de 250kg impressos e 150kg sem personalização.
            Para receber um orçamento de <strong>envelopes plásticos especiais</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento + aba x espessura) e a quantidade que deseja utilizar.</p>

            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>