<?php
	$h1    			= 'Envelope plástico';
	$title 			= 'Envelope plástico';
	$desc  			= 'Trabalhamos com uma diversificada linha de envelope plástico, fabricamos em PE ou PP e podem ser lisos ou impressos em até 6 cores.';
	$key   			= 'Envelopes plásticos, Envelopes, Envelope, plástico, envelope plastificado';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos';
	
	include('inc/head.php');
?>
<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
            
                  
                   
                	 <article>
                    
                    <h1><?=$h1?></h1>   
                    
            <p>Trabalhamos com uma diversificada linha de <strong>envelope plástico</strong>, fabricamos em PE ou PP e podem ser lisos ou impressos em até 6 cores. </p>
                        
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>

            
            <h2>Versatilidade dos envelopes plásticos</h2>
            <p>Nosso <strong>envelope plástico</strong> podem ser feitos com diversos tipos de fecho, dentre eles o fecho zip lock, aba adesiva, botão, ilhós, talas, entre outros.</p>
            <p>Este tipo de embalagem pode ser utilizado para diversas aplicações, como envio mala direta, brindes, embalar roupas, objetos pequenos em geral.</p>
            <p>Além de impressos, os <strong>envelopes plásticos</strong> podem ser transparentes ou pigmentado em diversas cores, como branco, amarelo, preto, entre outras cores.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para produtos que requerem segurança durante o transporte, sugerimos trabalhar com o <strong>envelope plástico</strong> com adesivo permanente, e para violá-lo é necessário danificar a embalagem. Em outras aplicações em que o produto precisa ser acessado varias vezes, sugerimos utilizar este produto com adesivo abre e fecha. Na maioria das vezes, o <strong>envelope plástico</strong> é utilizado por correios, courriers, editoras, gráficas, laboratórios e em empresas em geral.</p>
            
            <p>Com o foco em sustentabilidade, fabricamos <strong>envelope plástico</strong> com aditivo oxi-biodegradavel, este aditivo proporciona a rápida degradação da embalagem em contato com o meio ambiente, podendo levar até 6 meses para se decompor, ao contrário do plástico comum que demora mais de 100 anos para se decompor.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para você que deseja reduzir custos com embalagens, utilize os envelopes fabricados a partir de matéria-prima reciclada. Optar pelo <a href="<?=$url;?>envelope-reciclado" title="Envelope Reciclado">envelope Reciclado</a> é uma forma moderna e econômica para reduzir custos, além de você contribuir com a natureza. Em outras palavras, o <strong>envelope plástico reciclado</strong> é produzido através de diversas embalagens que passaram por um processo de reciclagem, como embalagens de alimentos, vestuários, industriais, entre muitas outras.</p>
            <p>Para receber um orçamento do <strong>envelope plástico</strong>, basta obter as medidas (largura x comprimento + aba x espessura), e a quantidade que você deseja utilizar.</p>

        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>