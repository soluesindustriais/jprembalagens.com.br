<?php
$h1    			= 'Sacola plástica preta';
$title 			= 'Sacola plástica preta';
$desc  			= 'Para reduzir custos com sacola plástica preta, utilize a sacola produzida com uma porcentagem de matéria-prima reciclada.';
$key   			= 'Sacola, plástica, preta, Sacolas plásticas pretas';
$var 			= 'Sacolas plásticas pretas';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacolas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>                     
            <p>A <strong>sacola plástica preta</strong>, geralmente é fabricada com polietileno de alta ou baixa densidade.</p>
            <p>Podem ser lisas ou impressas em até 6 cores, além da <strong>sacola plástica preta</strong> pode ser pigmentada em outras cores.</p>
            <p>Devido ao pigmento que é adicionado durante o processo de produção da <strong>sacola plástica preta</strong>, ela protege o produto embalado contra a luz, e a embalagem veda completamente o produto, não sendo possível visualizar o que há dentro da <strong>sacola</strong>.</p>
            <h2>Modelos</h2>
            <p>São infinitos os cortes, modelos e soldas que a <strong>sacola plástica preta</strong> poder ser produzida. Veja alguns dos nossos modelos:</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacolas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 

            <ul class="list">
                <li><strong>Com alça vazada</strong>;</li>
                <li><strong>Com alça camiseta</strong>;</li>
                <li><strong>Com alça boca de palhaço</strong>;</li>
                <li><strong>Com alça fita</strong>;</li>
                <li><strong>Sacola plástica branca com ilhós</strong>;</li>
                <li><strong>Com zíper</strong>;</li>
                <li><strong>Com cordão</strong>;</li>
                <li>Entre outras.</li>
            </ul>  
            <h2>Como reduzir custos</h2>
            <p>Para reduzir custos com <strong>sacola plástica preta</strong>, utilize a <strong>sacola</strong> produzida com uma porcentagem de matéria-prima reciclada. Neste formato, a <strong>sacola</strong> mantem a mesma resistência, além de continuar protegendo o produto embalado.</p>
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacolas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 
            <p>Além de <strong>sacolas plásticas preta</strong>, a JPR Embalagens possui uma ampla linha de embalagens como, <a href="<?=$url;?>saco-leitoso" title="Saco Leitoso"><strong>saco leitoso</strong></a>, <a href="<?=$url;?>envelope-reciclado" title="Envelope Reciclado"><strong>envelope reciclado</strong></a>, <a href="<?=$url;?>saco-bolha" title="Saco Bolha"><strong>saco bolha</strong></a>, filme e bobinas, entre outros.</p>
            <p>Para <strong>sacola plástica preta personalizada</strong>, nosso lote mínimo de produção são de 250kg e sem impressão 150kg.</p>
            <p>Para receber um orçamento de <strong>sacola plástica preta</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>