<?php
	$h1    			= 'Bobinas plásticas técnicas';
	$title 			= 'Bobinas plásticas técnicas';
	$desc  			= 'As bobinas plásticas técnicas podem ter o aspecto visual conforme a sua preferência. Existe a possibilidade de fabricação com material liso ou impressa em até seis cores';
	$key   			= 'bobinas, plásticas, técnicas, bobina plástica técnica';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina plástica técnica';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p><strong>Bobinas plásticas técnicas</strong> são um tipo de embalagem versátil e usado em vários segmentos. Confira maiores informações.</p>
            
            <p>Para vender mais no mercado, todo e qualquer detalhe pode fazer a diferença, inclusive a embalagem. Isso porque muitas vezes o primeiro contato que a empresa tem com a sua marca acontece justamente através de uma embalagem.</p>
            
            <p>Por isso, ela deve ser de qualidade, apresentando resistência e ótimo aspecto visual. Neste caso, a recomendação é utilizar as <strong>bobinas plásticas técnicas</strong>.</p>

            <p>As <strong>bobinas plásticas técnicas</strong> podem ser fabricadas em diversos materiais: polietileno de alta densidade (PEAD), polietileno de baixa densidade (PEBD), polipropileno (PP), polipropileno biorientado (BOPP), poliéster (PET), estruturas laminadas, entre outros.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Além da variedade de materiais para fabricação das bobinas plasticas técnicas, também existe a possibilidade de fabricá-las com matéria-prima virgem ou recicladas cristal, canela e colorido. Neste caso, você contribui com o meio ambiente e ainda reduz custos com embalagem, em até 30%, além de melhorar a imagem da sua empresa no mercado justamente por indicar o alinhamento da sua empresa com as causas ambientais.</p>
            
            <p>As <strong>bobinas plásticas técnicas</strong> podem ter o aspecto visual conforme a sua preferência. Existe a possibilidade de fabricação com material liso ou impressa em até seis cores, o que facilita a divulgação das cores da sua marca.</p>
            
            <h2>Bobinas plásticas técnicas com preço em conta e ótimas condições de pagamento</h2>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir as <strong>bobinas plásticas técnicas</strong>, aproveite os benefícios da JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa é especializada no segmento de embalagens plásticas flexíveis.</p>
            
            <p>A empresa dispõe de uma equipe que está constantemente atualizada, de modo a levar até o cliente o que há de mais moderno na área de embalagens, identificando oportunidades de melhoria e reduzindo perdas e custos. O objetivo é fornecer uma embalagem resistente, que protege o produto em todas as etapas, com preços em conta, proporcionando excelente relação custo-benefício.</p>
            
            <p>O atendimento da JPR Embalagens é personalizado e voltado para as necessidades e demandas de cada tipo de cliente. Por isso, aproveite.</p>
            
            <p>Entre em contato com a equipe para saber mais sobre os produtos e sobre as ótimas condições da empresa e solicite já o seu orçamento, informando quantidade e medidas que você precisa.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>