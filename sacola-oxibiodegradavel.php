<?php
$h1    			= 'Sacola oxibiodegradavel';
$title 			= 'Sacola oxibiodegradavel';
$desc  			= 'A sacola oxibiodegradavel se degrada em curto espaço de tempo, levando até 6 meses para de decompor, ajudando o meio ambiente';
$key   			= 'Sacola, oxibiodegradavel, Sacolas oxibiodegradaveis';
$var 			= 'Sacolas oxibiodegradaveis';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  

             <p>Enquanto a <strong>sacola comum</strong> pode levar mais de 100 anos para se decompor, a <strong>sacola oxibiodegradavel</strong> se degrada em curto espaço de tempo, levando até 6 meses para de decompor, ajudando o meio ambiente.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>A <strong>sacola oxibiodegradavel</strong> é fabricada em polietileno de alta densidade ou polietileno de baixa densidade. Durante o processo de fabricação da <strong>sacola</strong>, é adicionado o aditivo oxibiodegradavel. O aditivo não altera cor, cheiro ou resistência da <strong>sacola</strong>, apenas contribui para que ela se degrade em um tempo relativamente menor que a <strong>sacola</strong> comum, sem deixar resíduos nocivos ao meio ambiente. </p>
             <p>Pode ser lisa ou impressa em até 6 cores, além de poder ser transparente ou pigmentada em diversas cores.</p>
             <h2>Veja alguns modelos de sacolas oxibiodegradaveis que trabalhamos:</h2>
             <ul class="list">
                <li><strong>Sacola oxibiodegradavel com alça vazada;</strong></li>
                <li><strong>Sacola oxibiodegradavel com alça camiseta</strong>;</li>
                <li><strong>Sacola oxibiodegradavel com alça fita</strong>;</li>
                <li><strong>Sacola oxibiodegradavel com ilhós</strong>;</li>
                <li><strong>Sacola oxibiodegradavel com cordão</strong>;</li>
                <li>Entre outras.</li>
            </ul>
            <p>A JPR Embalagens possui uma ampla linha de embalagens como, <a href="<?=$url;?>sacola-plastica-tipo-camiseta" title="Sacola Plastica Tipo Camiseta"><strong>sacola plastica tipo camiseta</strong></a>, <a href="<?=$url;?>sacola-vazada" title="Sacola Vazada"><strong>sacola vazada</strong></a>, <a href="<?=$url;?>saco-valvulado" title="Saco Valvulado"><strong>saco valvulado</strong></a>, <a href="<?=$url;?>envelope-inviolavel" title="Envelope Inviolável"><strong>envelope inviolável</strong></a>, entre outras.</p>
            <p>Nossa quantidade mínima de produção de <strong>sacola oxibiodegradavel impressa</strong> são de 250kg e lisa 150 kg.</p>
            <p>Para receber um orçamento de qualquer um de nossos modelos de produtos, comuns ou da linha ecológica, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>