<?php
	$h1    		= 'Solicite um Orçamento';
	$title 		= 'Solicite um Orçamento';
	$desc  		= 'Solicite um Orçamento pelo formulário e logo entraremos em contato.';
	$key   		= '';
	$var   		= 'Solicite um Orçamento';
	 	
	include('inc/head.php');
	
?>
<script src="<?=$url?>js/validation.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready( function() {

    $("#formContato").validate({
        // Define as regras
        rules:{
            nome:{
                required: true, minlength: 2
            },
            email:{
                required: true, email: true
            },
			ddd:{
                required: true, minlength: 2
            },
			telefone:{
                required: true, minlength: 8
            },
			como_conheceu:{
                required: true
            },
            mensagem:{
                required: true, minlength: 5
            }
        },
        // Define as mensagens de erro para cada regra
        messages:{
            nome:{
                required: "Digite o seu nome",
                minLength: "O seu nome deve conter, no mínimo, 2 caracteres"
            },
            email:{
                required: "Digite o seu e-mail para contato",
                email: "Digite um e-mail válido"
            },
			ddd:{
                required: "Digite o seu DDD",
                minLength: "O seu DDD deve conter, no mínimo, 2 caracteres"
            },
			telefone:{
                required: "Digite o seu Telefone",
                minLength: "O seu telefone deve conter, no mínimo, 8 caracteres",
            },
			como_conheceu:{
                required: "Como nos conheceu?"
            },
            mensagem:{
                required: "Digite a sua mensagem",
                minLength: "A sua mensagem deve conter, no mínimo, 2 caracteres"
            }
        }
    });
});
</script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section class="full">
                

		<?=$caminho?>
        <h1><?=$h1?></h1>
 
<iframe class="form-iframe" src="http://www.embalagensflexiveis.com.br/formulario/pre-orcamento.php" style="width:100%;height:880px"></iframe>
    		
        </section><!-- section full-->

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>