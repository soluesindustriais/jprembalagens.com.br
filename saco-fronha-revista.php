<?php
$h1    			= 'Saco fronha para revista';
$title 			= 'Saco fronha para revista';
$desc  			= 'Fabricamos saco fronha para revista em diversos tamanhos e cores. Seu sistema de fácil abertura facilita a colocação do produto, desta forma, ganhando tempo.';
$key   			= 'Sacos fronha para revistas, Saco, sacos, fronha, revista, revistas';
$var 			= 'Sacos fronha para revistas';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section> 
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             

             <p>Fabricamos <strong>saco fronha para revista</strong> em diversos tamanhos e cores. Seu sistema de fácil abertura facilita a colocação do produto, desta forma é uma excelente opção para ganhar tempo e embalar mais produtos por minuto. </p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>Podem ser confeccionados em PEBD, PP, PEAD, entres outras opções, além do <strong>saco fronha para revista</strong> poder ser produzido a partir de matérias-primas recicladas.</p>
             <p>A vantagem em se utilizar a embalagem reciclada, é que ela pode proporcionar até 30% de redução de custos, além de você contribuir com o nosso meio ambiente.</p>
             <p>Por ser um produto 100% reciclável, o <strong>saco fronha para revista</strong> pode ser fabricado em dois tipos de matéria-prima reciclada, sendo o primeiro em reciclado cristal (PEAD ou PEBD) e o segundo em reciclado canela (PEBD).</p>
             <p>O <strong>saco fronha para revista reciclado cristal</strong>, é feito a partir das aparas do material virgem. Devido ao processo de produção que o material passa pela segunda vez, o aspecto visual da embalagem muda, a cor do plástico altera para amarelo claro, porem á embalagem é  totalmente transparente, ou seja, é possível ver perfeitamente o produto dentro da embalagem.</p>
             <p>Já o <strong>saco fronha para revista reciclado canela</strong>, é produzido a partir das aparas do material reciclado cristal. Neste caso, a embalagem passa pela terceira vez no processo produção, e desta forma o aspecto visual do <strong>saco fronha para revista</strong> fica na própria cor de canela, inclusive este é um dos motivos da embalagem se chamar reciclado canela. Neste reciclado também é possível ver o que há dentro da embalagem. </p>
             <p>O <strong>saco fronha para revista</strong> pode ser liso ou impresso em até 6 cores.</p> 
             <p>Para <strong>saco fronha para revista impresso</strong>, nosso lote mínimo de produção são de 250kg e para sem impressão são de 150kg.</p>
             <p>Para receber nosso orçamento de <strong>saco fronha para revista</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade que estima utilizar. </p>


             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>