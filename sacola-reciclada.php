<?php
$h1    			= 'Sacola reciclada';
$title 			= 'Sacola reciclada';
$desc  			= 'A sacola reciclada é uma forma econômica e segura para transportar produtos de diversos formatos e tamanho. São reforçadas na alça, assim aumentam a segurança.';
$key   			= 'Sacola, reciclada, Sacolas recicladas, sacola reciclada personalizada, sacola reciclada impressa';
$var 			= 'Sacolas recicladas';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             
             <p>Trabalhamos com uma ampla linha de <strong>sacola reciclada</strong>. São produzidas em polietileno de alta (PEAD) ou baixa densidade (PEBD). Podem ser lisas ou impressas em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>São amplamente utilizadas por lojas e comércios, supermercados, padarias, entre muitos outros.</p>
             <p>A <strong>sacola reciclada</strong> é uma forma econômica e segura para transportar produtos de diversos formatos e tamanho. São reforçadas na alça, assim aumentam a segurança até a chegada do produto em seu destino final.</p>
             <p>É um produto ecologicamente correto, pois a <strong>sacola reciclada</strong> é produzida a partir de outras embalagens que foram reprocessadas e recicladas, como embalagens de alimentos, outros modelos de <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacola plastica</strong></a>, <a href="<?=$url;?>envelope-plastico" title="Envelope Plástico"><strong>envelopes plásticos</strong></a>, <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>saco plastico</strong></a>, entre muitos outros.</p>
             <p>São muito amplos os formatos e modelos que a <strong>sacola reciclada</strong> pode ser produzida.</p>
             <h2>Modelos de <strong>sacolas recicladas</strong> mais utilizadas</h2>
             <ul class="list">
                <li><strong>Com alça vazada</strong>;</li>
                <li><strong>Com alça camiseta</strong>;</li>
                <li><strong>Com alça boca de palhaço</strong>;</li>
                <li><strong>Com alça fita</strong>;</li>
                <li><strong>Com cordão</strong>;</li>
                <li>Entre muitas outras.</li>
            </ul>
            <p>A <strong>sacola reciclada</strong> é fabricada sob medida, de acordo com necessidade de cada cliente, e podem ser feitas na cor transparente ou pigmentada em diversas cores. </p>
            <p>Nossa quantidade mínima de produção de <strong>sacola reciclada impressa</strong> são de 250kg e lisa 150 kg.</p>
            <p>Para receber um orçamento de <strong>sacola reciclada</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>