<?php
$h1    			= 'Saco metalizado';
$title 			= 'Saco metalizado';
$desc  			= 'Geralmente o saco metalizado é confeccionado com espessuras finas, indicados para transportar produto e objetos com leve peso.';
$key   			= 'Sacos metalizados, Saco, sacos, metalizado, metalizados, saco metalizado para presente';
$var 			= 'Sacos metalizados';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <p>Fabricados com BOPP, o <strong>saco metalizado</strong> poder ser liso ou impresso em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>Este produto é amplamente utilizado por lojas, supermercados, shoppings, como embalagem para presente, devido a seu aspecto metalizado.</p>
             <p>Geralmente os <strong>sacos metalizados</strong> são confeccionados com espessuras finas, indicados para transportar produto e objetos com leve peso. Para facilitar o fechamento da embalagem, esta embalagem pode ser confeccionado com aba adesiva, assim você dispensa o uso de seladoras.</p>
             <p>Além do <strong>saco metalizado</strong>, também fabricamos outros modelos de <strong>embalagem para presentes</strong>, como <strong>saco BOPP perolizado</strong> e <a href="<?=$url;?>saco-polietileno" title="Saco de Polietileno"><strong>saco de polipropileno transparente</strong></a>.</p>
             <p>Também são muito utilizados em indústrias alimentícias, pois o <strong>saco metalizado</strong> dá barreira à luz, além de ajudar da conservação do alimento. </p>
             <p>Este produto é uma embalagem prática e segura, e a metalização ajuda a manter o produto em sigilo, pois não é possível ver o há dentro da embalagem.</p>
             <p>Por sermos fábrica, não possuímos medida padrão de <strong>saco metalizado</strong>, sendo assim, as embalagens são fabricadas de acordo com a necessidade de cada cliente.</p>
             <p>Para <strong>saco metalizado impresso</strong>, nosso mínimo de produção são de 300kg e sem impressão 150kg.</p>
             <p>Para receber um orçamento de <strong>saco metalizado</strong>, entre em contato com um de nossos consultores, e informe as medidas e quantidades que você deseja utilizar.</p>

             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>