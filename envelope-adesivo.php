<?php
	$h1    			= 'Envelope com adesivo';
	$title 			= 'Envelope com adesivo';
	$desc  			= 'Fabricamos envelope com adesivo em polietileno e polipropileno, além de serem produzidos diversos formatos e cores';
	$key   			= 'Envelopes com adesivo, Envelopes, Envelope, adesivo';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes com adesivo';
	
	include('inc/head.php');
?>
<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 

                    
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>   
                                    
            <p>Fabricamos <strong>envelope com adesivo</strong> em polietileno e polipropileno, além de serem produzidos diversos formatos e cores. Este tipo de embalagem possui sistemas de fechamento permanente ou abre-e-fecha, adequando-se a necessidade de cada cliente.</p>
            
            <p>Devido à praticidade que o <strong>envelope com adesivo</strong> possui, ele é amplamente utilizado em diversos segmentos, como gráficas, editoras, livrarias e em outros segmentos de uma forma geral.</p>
            
            <p>Reduza custos utilizando <strong>envelopes com adesivo reciclados</strong>. Trabalhamos com matéria-prima selecionada pela qual garantimos a qualidade e resistência que você busca em nossos <strong>envelopes com adesivo</strong>.</p> 
            <p>O <strong>envelope com adesivo</strong> pode ser liso ou impresso em até 6 cores.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>   
            
            
            
            <h2>Vantagens do envelope com adesivo</h2>
            
            <p>A vantagem em se utilizar o <strong>envelope com adesivo</strong>, é que você aumenta o seu processo de produção, pois o <strong>envelope</strong> é pratico e eficaz, basta retirar o liner e dobrar a aba para lacrar a embalagem, assim você ganha em tempo e poderá embalar mais produtos por minuto. </p>
            
            <p>A modernidade do <strong>envelope com adesivo</strong> dá uma melhor apresentação ao seu produto, além de você pode reutilizar a embalagem diversas vezes.</p>
            
            <p>Muitas vezes a embalagem é o seu passaporte para o mercado, pois uma embalagem moderna e inovadora atrai a atenção de seus clientes, e mostra quanto você esta preocupado em sempre fazer o melhor. Por isso, utilize um de nossos <strong>envelopes com adesivo</strong>, e tenha a segurança de trabalhar com a embalagem ideal adequado para o seu produto.</p>
            
            <p>Além de poder ser impresso, nossos <strong>envelopes com adesivo</strong> podem ser transparentes ou pigmentados.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>   
            
            <p>Confeccionamos outras linhas de <strong>envelopes</strong> como <a href="<?=$url;?>envelope-plastico-ilhos" title="Envelope Plástico Com Ilhós"><strong>envelope plástico com ilhós</strong></a>, <strong>envelope com botão</strong>, <strong>envelope com fecho zip lock</strong>, entre muitos outros. Consulte-nos.</p>
            
            <p>Nosso mínimo da produção é 300kg para <strong>envelope com adesivo impresso</strong> e 200kg sem impressão.</p>
            
            <p>Para receber um orçamento do <strong>envelope com adesivo</strong>, basta obter as medidas (largura x comprimento + aba x espessura), e a quantidade que você deseja utilizar.</p>

        	 
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>