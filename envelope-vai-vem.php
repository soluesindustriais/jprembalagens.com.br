<?php
	$h1    			= 'Envelope vai e vem';
	$title 			= 'Envelope vai e vem';
	$desc  			= 'Nossos envelope vai e vem, quando pigmentado ou colorido, possuem barreira a luz e a umidade, assim você consegue trabalhar com uma embalagem douradora.';
	$key   			= 'Envelopes, Envelope, vai, vem, Envelopes vai e vem, envelope para correspondência interna';
	$var 			= 'Envelopes vai e vem';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>


<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                    
            <h2>Nosso envelope vai e vem é produzido em plástico leitoso ou colorido</h2>
             
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
             <p>Produzido em polietileno de baixa densidade, o <strong>envelope vai e vem</strong> pode ser liso ou impresso em diversas cores. É geralmente utilizado para transportar documentos e correspondências entre setores. </p>
            <p>Nosso lote mínimo de produção de <strong>envelope vai e vem</strong> é de 250kg, com prazo de entrega de 20 dias (corridos).</p>
            <p>O fechamento do <strong>envelope vai e vem</strong>, é realizado através de duas talas (macho/fêmea) que possui na parte superior da embalagem. Este modelo de embalagem pode ser aberto por diversas vezes, e não perderá a aderência.</p>
            <p>Outra opção que fabricamos também é o fechamento do <strong>envelope vai e vem</strong> com fecho zip, pois este ocupa uma área menor da embalagem, possui a mesma funcionalidade da tala, em termos de praticidade e custos, são iguais, portanto, fica a critério do cliente qual tipo de fecho será utilizado.</p>  
            <p>É uma embalagem segura e super versátil, e o grande destaque do <strong>envelope vai e vem</strong>, é que ele pode ser reutilizado varias vezes, até todas as linhas para preenchimento estar completas.</p>
            <p>Nossos <strong>envelopes vai e vem</strong>, quando pigmentado ou colorido, possuem barreira a luz e a umidade, assim você consegue trabalhar com uma embalagem douradora sem se preocupar com a qualidade.</p>
            <p>O <strong>envelope vai e vem</strong>, também conhecido como <strong>envelope para correspondência interna</strong>, é muito utilizado em empresas como:</p>
            <ul class="list">
                <li>Empresas de courier.</li>
                <li>Empresas aéreas.</li>
                <li>Transportadoras.</li>
                <li>Bancos</li>
                <li>Correspondência interna (Malote vai e vem)</li>
                <li>Documentação do Produto – CQ</li>
                <li>Identificador</li>
                <li>Protetores de fichas de produção/manutenção</li>
                <li>E etc.</li>
            </ul>
            <p>Estes <strong>envelopes</strong> são próprios para escritas com canetas esferográficas. Fabricamos <strong>envelopes vai e vem</strong> de acordo com a necessidade de cada cliente. Basta ter as medidas (largura x comprimento x espessura) e a quantidade que será utilizada.</p>

            
			<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>