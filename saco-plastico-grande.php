<?php
$h1    			= 'Saco plástico grande';
$title 			= 'Saco plástico grande';
$desc  			= 'Somos especialistas na produção de saco plástico grande, trabalhamos com máquinas próprias para a produção destas embalagens';
$key   			= 'Sacos plásticos grandes, Saco, sacos, plástico, grande';
$var 			= 'Sacos plásticos grandes';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 
            
            <p>Somos especialistas na produção de <strong>saco plástico grande</strong>, trabalhamos com máquinas próprias para a produção destas embalagens.</p>


            <p>O <strong>saco plástico grande</strong> é produzido em polietileno de baixa densidade (PEBD), podendo ser transparente ou pigmentado em diversas cores.</p>
            <p>São indicados para produtos que possuem grandes formatos, assim o <strong>saco plástico grande</strong> é fabricado sob medida, de acordo com a necessidade de cada cliente.</p>
            <h2>Saco plástico grande reciclado</h2>
            <p>Para reduzir seus custos com embalagem, utilize <strong>saco plástico grande reciclado</strong>. São produzidos a partir de aparas do plástico virgem e de outras embalagens que foram reprocessadas, como embalagens alimentícias, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, <a href="<?=$url;?>sacos" title="Sacos"><strong>sacos</strong></a>, dentre outros, e neste formado, o <strong>saco plástico grande</strong> fica com o aspecto visual amarelado, porem transparente, sendo possível ver perfeitamente o que há dentro da embalagem. </p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>             
            <p>Além de você contribuir com meio ambiente e utilizar uma embalagem ecologicamente correta, terá uma grande economia, pois a matéria-prima reciclada é bem mais em conta que a virgem. O <strong>saco plástico grande</strong> é muito utilizado por indústrias automotivas, moveleiras, colchões, maquinas, entre outros. </p> 
            <p>Além de <strong>saco plástico grande</strong>, a JPR Embalagens trabalha com capas para pallet, bobinas plásticas, sacos em geral, embalagens especiais, entre outras. </p>
            <p>Nossa quantidade mínima de produção são de 200kg para <strong>saco plástico grande</strong> liso e 300kg impresso. Para receber um orçamento de <strong>saco plástico grande</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>