<?php
	$h1    			= 'Envelope inviolável';
	$title 			= 'Envelope inviolável';
	$desc  			= 'O envelope inviolável possui um sistema de fechamento permanente, e isso o torna inviolável e para ter acesso ao produto embalado, é necessário danificar a embalagem';
	$key   			= 'Envelopes invioláveis, Envelopes, Envelope, inviolável';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes invioláveis';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                    
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>            
                                    
            <p>A JPR Embalagens trabalha com diversos modelos de <strong>envelopes invioláveis</strong>. São fabricados com as resinas de polietileno e polipropileno.</p>
            

            
            <h2>Sistema de segurança do envelope inviolável</h2>
            <p>O <strong>envelope inviolável</strong> possui um sistema de fechamento permanente, e isso o torna inviolável e para ter acesso ao produto embalado, é necessário danificar a embalagem.</p>
            <p>Além disso, o <strong>envelope inviolável</strong> tem impressão personalizada em flexografia com opção de numeração sequencial e ou código de barras.</p>

            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>            
            
            <p>Estes modelos de embalagens não são reutilizáveis, após aberto, se danificam completamente, e o sistema de fechamento perde a aderência.</p>

            <p>O <strong>envelope inviolável</strong> pode ser produzido a partir de matéria-prima virgem ou reciclado, dependendo da aplicação de cada cliente.</p>
            
            <p>Fabricados com a resina de polietileno coextrusado (duas camadas de filme), em 2 cores diferentes, nas partes interna e externa, ou monocamada (uma só cor) são opções do <strong>envelope inviolável</strong>.</p>  
            <p>Trabalhamos com o desenvolvimento de <strong>envelope inviolável</strong>, basta fornecer aos nossos consultores as medidas (largura x comprimento + aba x espessura) e a quantidade que você irá utilizar.</p> 
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>    
                    
            <p>O <strong>envelope inviolável</strong> pode ser fabricado com aditivo oxi-biodegradável, e neste formato, a embalagem se degrada em um período de até 6 meses. Esta uma opção para você contribuir com o meio ambiente. A natureza agradece.</p>

            
            
            <p>Quando falamos em produtos que requerem uma segurança a violações e durante o seu transporte, nos preocupamos em propor o melhor aos nossos clientes, esta é a nossa especialidade. Temos uma completa linha de produtos como, <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>Envelopes de Segurança</strong></a>, <a href="<?=$url;?>sacola-impressa" title="Sacola Impressa"><strong>Sacolas Impressas</strong></a>, <a href="<?=$url;?>sacola-plastica-reciclada" title="Sacola Plastica Reciclada"><strong>Sacolas Plasticas Recicladas</strong></a>, entre muitos outros.</p>
            <p>Para receber um orçamento do <strong>envelope inviolável</strong>, basta obter as medidas (largura x comprimento + aba x espessura), e a quantidade que você deseja utilizar.</p>
 
            
        	
             
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>