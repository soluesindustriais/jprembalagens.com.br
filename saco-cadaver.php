<?php
$h1    			= 'Saco para cadáver';
$title 			= 'Saco para cadáver';
$desc  			= 'O saco para cadáver possui etiqueta de identificação, e são fabricados somente na cor cinza. São acondicionados em pacotes.';
$key   			= 'Sacos para cadáveres, Saco, sacos, cadáver, cadáveres';
$var 			= 'Sacos para cadáveres';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>  
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             
             <p>O <strong>saco para cadáver</strong> é fabricado em polietileno de baixa densidade (PEBD), dentro dos padrões da ABNT.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>A embalagem não possui soldas, no entanto, é reforçada, além da superfície frontal ser composta por 75% com zíper, para facilitar a abertura e transporte do cadáver. O <strong>saco para cadáver</strong> veda completamente o corpo no interior da embalagem, e assim evita que o corpo tenha contato com o meio externo.</p>
             <h2>Nossas medidas padrões são:</h2>
             <ul class="list">
                <li><strong>Saco para cadaver recém nascido</strong> (RN) – Medida:  30 cm x 60 cm</li>
                <li><strong>Saco para cadaver Pequeno</strong> (P) – Medida: 50 cm x 100 cm</li>
                <li><strong>Saco para cadaver Médio</strong> (M) – Medida: 60 cm x 150 cm</li>
                <li><strong>Saco para cadaver Grade</strong> (G) – Medida: 90 cm x 220 cm</li>
            </ul>	
            <p>O <strong>saco para cadáver</strong> possui etiqueta de identificação, e são fabricados somente na cor cinza.</p>
            <p>São acondicionados em pacotes, e cada pacote possui 25 peças. Nosso lote mínimo para faturamento é de R$ 500,00.</p>
            <p>Além de <strong>saco para cadáver</strong>, a JPR Embalagens trabalha com outras linhas de <a href="<?=$url;?>saco-hospitalar" title="Saco Hospitalar"><strong>sacos hospitalares</strong></a>, como <a href="<?=$url;?>saco-infectante" title="Saco Infectante"><strong>saco para lixo infectante</strong></a>, <a href="<?=$url;?>saco-hamper" title="Saco Hamper"><strong>saco hamper</strong></a> e <a href="<?=$url;?>saco-lixo" title="Saco de Lixo"><strong>sacos para lixo comum</strong></a>.</p>
            <p>Para receber um orçamento de <strong>saco para cadáver</strong>, entre em contato com um de nossos consultores, e informe o modelo e quantidade que você deseja. </p>


            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>