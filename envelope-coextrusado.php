<?php
	$h1    			= 'Envelope coextrusado';
	$title 			= 'Envelope coextrusado';
	$desc  			= 'Dependendo da aplicação, o fechamento do envelope coextrusado pode ser permanente, e assim a embalagem se torna inviolável, ou tipo abre e fecha';
	$key   			= 'Envelopes coextrusados, Envelopes, Envelope, coextrusado';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes coextrusados';
	
	include('inc/head.php');
?>
<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
           
                    
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 3; include('inc/gallery.php'); ?>
                                    
            <p>Cada vez que uma encomenda é enviada por correio, courrier, transportadora de valores e outros meios, quase sempre ela segue protegida por um de nossos <strong>envelopes coextrusados</strong>. </p>
            
            
            <p>Dependendo da aplicação, o fechamento do <strong>envelope coextrusado</strong> pode ser permanente, e assim a embalagem se torna inviolável, ou tipo “abre e fecha” (como no caso de roupas, por exemplo). </p>
            
            <p><strong>envelope coextrusado</strong>, em 2 cores diferentes, nas partes interna e externa, ou monocamada (uma só cor) são alternativas do material do <strong>envelope</strong>. </p>
            
            <h2>O sistema de segurança do envelope coextrusado</h2>
            

            <p>Para produtos que requerem extrema segurança durante o seu transporte, indicamos trabalhar com o <strong>envelope coextrusado</strong>. Devido à dupla camada de filmes que o <strong>envelope coextrusado</strong> possui, a embalagem se torna mais resistente ao rasgo e à ruptura.</p>
            
            <p>O <strong>envelope coextrusado personalizado</strong>, expressa a sua preocupação em inovar, em oferecer sempre o melhor. Seu produto causa boa impressão à primeira vista e conquista consumidores. É seu passaporte para novos mercados. O <strong>envelope coextrusado</strong> é utilizado por empresas que estão preocupadas em inovar e proteger as informações e produtos que circulam dentro e fora da empresa, e assim transmitem a imagem para seus clientes e fornecedores que valorizam e se preocupam com estas questões.</p>
            
            <p>Desenvolvemos <strong>envelopes coextrusados</strong> de acordo com a necessidade de cada cliente, podendo ser liso ou impresso em até 6 cores. Com o foco em sustentabilidade, a JPR Embalagens passou a produzir <strong>envelopes plásticos</strong> partir com matéria-prima reciclada, e nesta opção, a resistência e segurança do <a href="<?=$url;?>envelope-reciclado" title="Envelope Reciclado"><strong>envelope reciclado</strong></a> é as mesma que a do material virgem, e além de você reduzir seus custos com embalagens, você também estará contribuindo para um planeta mais sustentável.</p>
            
            <p>Nossos lotes mínimo de produção são de 200kg para envelopes coextrusado liso e 300kg para os impressos.</p>
            <p>Para receber um orçamento de <strong>envelopes coextrusados</strong>, basta possuir as medidas (largura x comprimento + aba x espessura), e a quantidade que você deseja utilizar.</p>

        	 
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>