<?php
	$h1    			= 'Bobina impressa';
	$title 			= 'Bobina impressa';
	$desc  			= 'A bobina impressa, se destaca pela sua qualidade e resistência, fazendo a proteção adequada do que está sendo embalado no transporte e no armazenamento dos produtos';
	$key   			= 'bobina, impressa, bobinas impressas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas impressas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
           	<p>A <strong>bobina impressa</strong> é uma das maneiras de divulgar melhor a sua marca. Saiba mais sobre este tipo de embalagem.</p>

            <p>Constantemente empresários buscam maneiras de vender mais. Uma destas formas é a divulgação da marca, inclusive em embalagens. Por isso, uma ótima opção para a sua empresa é a <strong>bobina impressa</strong>.</p>
            
            <p>A <strong>bobina impressa</strong> pode ser fabricada em polipropileno sob medida, conforme necessidade de cada cliente, com possibilidade de personalização em até seis cores diferentes. Por causa desta versatilidade, a bobina é usada em vários segmentos.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Os tipos de empresa que mais usam a <strong>bobina impressa</strong> são as que enviam produtos promocionais a clientes, malas diretas, convites ou outros tipos de materiais. Isso porque esta embalagem proporciona uma apresentação impecável do seu produto. A embalagem se destaca pelo brilho intenso e pela transparência.</p>
                        
            <p>Além da questão visual, a <strong>bobina impressa</strong> também se destaca pela sua qualidade e resistência, fazendo a proteção adequada do que está sendo embalado no transporte e no armazenamento dos produtos.</p>
            
            <p>A resistência à tração da <strong>bobina impressa</strong> é um ponto a se destacar. O rompimento da embalagem só começa a acontecer após o estiramento máximo da bobina.</p>
            
            <p>Outra vantagem é a possibilidade da fabricação com aditivo oxibiodegradável, o que faz com que a embalagem se degrade em um curto espaço de tempo ao entrar em contato com o meio ambiente. Esta alternativa sustentável também é uma maneira de melhorar a imagem da sua empresa frente aos consumidores, justamente por indicar a sua preocupação com as causas ambientais.</p>
            
            <h2>Conheça bobina impressa com preços reduzidos</h2>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir a <strong>bobina impressa</strong>, aproveite os benefícios da JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa é especializada em embalagens plásticas flexíveis, buscando levar até o cliente sempre opções econômicas e de qualidade comprovada. O objetivo é reduzir perdas, identificar oportunidades de melhoria e reduzir custos.</p>
            
            <p>Na JPR Embalagens, o atendimento é personalizado, voltado às necessidades e preferências de cada cliente. A empresa disponibiliza preços em conta e ótimas condições de pagamento, resultando em excelente relação custo-benefício.</p>
            
            <p>Aproveite. Entre em contato com um dos consultores para conhecer as condições e esclarecer eventuais dúvidas e faça já o seu pedido.</p>
            
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>