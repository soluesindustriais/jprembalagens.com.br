<?php
$h1    			= 'Capa pallet';
$title 			= 'Capa pallet';
$desc  			= 'A capa pallet é uma embalagem que se caracteriza pela confecção em polietileno transparente, um material que possui durabilidade e grande resistências mecânicas';
$key   			= 'capa pallet, capas pallet';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Capas pallet';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            

             <?=$caminhoProdutoCapas?>
              <article>
             <h1><?=$h1?></h1>     
             <br>  
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            <p>Embalagem que se destaca pela resistência e pela durabilidade. Confira maiores informações e saiba quais são as utilizações.</p>
            
            <p>A embalagem é um aspecto funda mental na hora de manter a qualidade dos produtos. Isso porque ela deve ser confeccionada em material resistente e que não altere as características originais do que está sendo embalado. Neste caso, uma ótima opção é a <strong>Capa pallet</strong>.</p>

            <p>A <strong>Capa pallet</strong> é uma embalagem que se caracteriza pela confecção em polietileno transparente, um material que possui durabilidade e grande resistências mecânicas, graças à exposição às intempéries. </p>
            
            <p>Além disso, a <strong>Capa pallet</strong> se destaca por oferecer mais segurança e rigidez na gora de movimentação de cargas, fazendo com que as mercadorias não sejam danificadas e estejam totalmente impermeabilizadas contra sujeiras, exposições térmicas e outros tipos de danos.</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>Capa pallet</strong> facilita a inspeção e a identificação da carga e também é altamente indicada para embalagem de produtos unitizados. Outra vantagem é a flexibilidade deste material, permitindo o acondicionamento de cargas com diferentes disposições e tamanhos.</p>
            
            <p>A <strong>Capa pallet</strong> é a opção ideal para uma série de indústrias, tais como: de alimentos, de fundição, metalúrgica, calçadista, fumageiras, químicas, entre outras.</p>
            
            <h2>Saiba onde encontrar Capa pallet com preços em conta</h2>
            
            <p>Na hora de adquirir a <strong>Capa pallet</strong>, aproveite os benefícios da JPR Embalagens. A empresa atua no mercado há mais de 15 anos e é altamente especializada no segmento de embalagens plásticas flexíveis.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O principal objetivo da JPR Embalagens é a redução de perdas e de custos a partir da identificação de oportunidades de melhoria. Para isso, a empresa dispõe de equipamentos de última geração e de equipe que está constantemente atualizada. Além de <strong>Capa pallet</strong>, a empresa produz sacos, sacolas, envelopes, bobinas, filmes e outros tipos de materiais.</p>
            
            <p>Na JPR Embalagens, é possível personalizar a capa conforme suas necessidades. O atendimento é feito para atender os diferentes tipos de demanda dos clientes. E tudo isso com preços reduzidos e condições de pagamento vantajosas, o que resulta em ótima relação custo-benefício para os clientes.</p>
            
            <p>Saiba mais sobre as vantagens da <strong>Capa pallet</strong> e conheça os valores entrando em contato com um dos consultores e solicitando agora mesmo o seu orçamento.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>           
        <br class="clear" />                                                                                                    
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>