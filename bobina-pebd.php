<?php
	$h1    			= 'Bobina pebd';
	$title 			= 'Bobina pebd';
	$desc  			= 'A bobina pebd, ou seja, a bobina de polietileno e baixa densidade, é usada em uma série de segmentos para embalar produtos, já que é uma embalagem versátil, com grande resistência a rasgo';
	$key   			= 'bobina, pebd, bobinas pebd';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas pebd';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
			<p>Conte com uma embalagem resistente na hora de embalar os seus produtos. Saiba mais sobre a <strong>bobina pebd</strong>.</p>

            <p>A embalagem é um aspecto fundamental na hora de vender um produto. Ela precisa ter qualidade elevada e boa aparência, como é o caso da <strong>bobina pebd</strong>.</p>
            
            <p>A <strong>bobina pebd</strong>, ou seja, a bobina de polietileno e baixa densidade, é usada em uma série de segmentos para embalar produtos, já que é uma embalagem versátil, com grande resistência a rasgo ou tração e que tem uma ótima selagem. Para aumentar a segurança e/ou a praticidade, a <strong>bobina pebd</strong> pode ter acessórios como abas, fechos, talas, alças ou ainda adesivos.</p>

            
            <h2>Bobina pebd pode ser sustentável: saiba mais!</h2>
            
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Para contribuir com a sustentabilidade e a manutenção da vida e ainda melhorar a imagem da sua marca no mercado justamente pelo alinhamento com as causas ambientais, uma opção é a <strong>bobina pebd</strong> reciclada.</p>
            
            <p>Neste caso, a fabricação pode ser feita de três formas diferentes; Uma delas é a fabricação a partir de aparas de material virgem, que seria a fabricação cristal. Outra opção é o reciclado canela, que é feito a partir de aparas do material reciclado cristal. Há ainda o reciclado colorido, que é a fabricação a partir de plásticos transparentes e coloridos. Neste último caso, a bobina fica sem transparência e sem padrão de cor.</p>
            

            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Outra vantagem da <strong>bobina pebd</strong> feita com material reciclável é que ela resulta em redução de custos para a empresa: uma economia de até 30% nos custos com embalagem.</p>
                        
            <p>E para economizar ainda mais, conte com a JPR Embalagens. Atuando há mais de 15 anos no mercado de embalagens plásticas flexíveis, a JPR Embalagens disponibiliza <strong>bobina pebd</strong> com qualidade elevada e preço em conta, resultando em ótima relação custo-benefício para o cliente. Além disso, a equipe técnica da empresa está constantemente atualizada para proporcionar o que há de mais moderno neste segmento.<br />
            <br />
            O atendimento na JPR Embalagens é personalizado, completamente voltado para as necessidades e demandas dos clientes. Saiba mais sobre as bobinas, suas aplicações e confira as ótimas condições que a JPR Embalagens oferece entrando em contato com um dos consultores. Solicite já o seu orçamento.</p>

            
            
            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>