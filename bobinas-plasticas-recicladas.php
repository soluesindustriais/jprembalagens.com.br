<?php
	$h1    			= 'Bobinas plásticas recicladas';
	$title 			= 'Bobinas plásticas recicladas';
	$desc  			= 'As bobinas plásticas recicladas são uma excelente maneira de melhorar a sua imagem, justamente por indicar a sua preocupação com questões ambientais';
	$key   			= 'bobinas, plásticas, recicladas, bobinas plásticas, bobinas recicladas, bobina plástica reciclada';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina plástica reciclada';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                 
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>As <strong>bobinas plásticas recicladas</strong> protegem o seu produto e ainda colaboram com o meio ambiente. Saiba mais sobre esta embalagem.</p>

            <p>Empresários estão constantemente pensando em maneiras de melhorar a imagem da empresa no mercado. E isso pode começar pelas embalagens, já que muitas vezes ela é o primeiro contato que o consumidor tem com a sua marca. Por isso, uma ótima opção são as <strong>bobinas plásticas recicladas</strong>.</p>
            
            <p>As <strong>bobinas plásticas recicladas</strong> são uma excelente maneira de melhorar a sua imagem, justamente por indicar a sua preocupação com questões ambientais. Mas, além disso, há outra vantagem: a redução dos custos com embalagem em até 30% investindo em uma embalagem que mantem a qualidade.</p>
            
            <p>As <strong>bobinas plásticas recicladas</strong> são uma das formas mais econômicas de embalar produtos, garantindo integridade do produto, já que elas são resistentes a rasgos, rupturas e quedas.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A fabricação desta embalagem pode ser feita de três maneiras diferentes: reciclado cristal, canela ou colorido. No caso de <strong>bobinas plásticas recicladas</strong> cristal, a produção é feita a partir de aparas de material virgem e embalagens que já foram recicladas. Por causa do processo de reciclagem, a embalagem fica com aspecto amarelo claro, com alguns pontos, mas a transparência é mantida.</p>
            
            <p>As <strong>bobinas plásticas recicladas</strong> canela são feitas a partir de plásticos reciclados e também tem alguns pontos, além de ter cor da embalagem alterada para marrom claro, mas ainda mantendo a transparência. Outra opção é o reciclado colorido, feito a partir de embalagens recicladas, que é ainda mais em conta, mas que resulta em uma embalagem sem padrão de cor e sem transparência.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>            
            
            <h2>Bobinas plásticas recicladas de qualidade e com ótimas condições</h2>
            

            
            <p>E para adquirir as <strong>bobinas plásticas recicladas</strong>, conte com os benefícios da JPR Embalagens. A empresa atua há mais de 15 anos na área de embalagens plásticas flexíveis, levando até o consumidor o que há de mais moderno neste segmento, sempre proporcionando redução de perdas e de custos. As <strong>bobinas plásticas recicladas</strong> podem ser personalizadas de acordo com a necessidade de cada cliente, e a JPR Embalagens atende este tipo de demanda, com valores em conta e ótimas condições de pagamento.</p>
            
            <p>Por isso, aproveite. Entre em contato com a equipe e solicite já o seu orçamento.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                   
             <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>