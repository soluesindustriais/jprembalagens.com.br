<?php
	$h1    			= 'Bobinas técnicas';
	$title 			= 'Bobinas técnicas';
	$desc  			= 'As bobinas técnicas se revelam como uma ótima opção porque são produzidas em BOPP laminado, metalizado ou transparente, com função de empacotamento automático ou rotulação.';
	$key   			= 'bobinas, técnicas, bobina técnica';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina técnica';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p><strong>Bobinas técnicas</strong> de qualidade e resistência e com preço em conta. Saiba mais sobre as vantagens desta embalagem.</p>

            <p>Empresários estão constantemente concentrados para definir estratégias que possam fazer com que a marca venda mais, alcance novos consumidores e chegue a novos mercados. Neste caso, a embalagem pode ser um aspecto fundamental e as <strong>bobinas técnicas</strong> são uma opção altamente recomendadas. A embalagem deve ser resistente e ter um aspecto visual atraente para o consumidor. Afinal, muitas vezes é através da embalagem que acontece o primeiro contato entre o cliente e a sua marca.</p>
            
            <p>E as <strong>bobinas técnicas</strong> se revelam como uma ótima opção porque são produzidas em BOPP laminado, metalizado ou transparente, tendo a função de empacotamento automático ou para rotulação. O material utilizado na fabricação da embalagem proporciona durabilidade prolongada ao produto embalado, além de proporcionar maior facilidade na hora de abrir a embalagem.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A personalização é o grande destaque desta embalagem, já que pode ser feita em até seis cores diferentes. Esta personalização de cores e também de medidas é essencial para atrair os olhos do cliente e fazer com que seu produto se destaque na gôndola do supermercado.</p>
            
            <h2>Bobinas técnicas com preço em conta e ótimas condições de pagamento</h2>
            
            <p>E para adquirir as <strong>bobinas técnicas</strong>, conte com as vantagens proporcionadas pela JPR Embalagens. A empresa tem mais de 15 anos de atuação na área de embalagens plásticas flexíveis, buscando reduzir perdas e custos através de oportunidades de melhorias.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>As <strong>bobinas técnicas</strong> e os outros produtos da JPR Embalagens se destacam pela fabricação com materiais de qualidade e aparelhos de última geração, resultando em embalagens resistentes e com ótimo aspecto visual. Além disso, a empresa tem preços em conta e ótimas condições de pagamento, o que faz com que o consumidor tenha a melhor relação custo-benefício do mercado.</p>
            
            <p>O atendimento da JPR Embalagens é totalmente personalizado conforme as preferências e necessidades dos clientes. Por isso, aproveite. Entre em contato com a equipe para saber mais sobre as <strong>bobinas técnicas</strong>, esclarecer dúvidas sobre o produto e conhecer as demais opções.</p>
            
            <p>Além de bobinas, a JPR Embalagens tem sacos, sacolas, envelopes, entre outros. Solicite já o seu orçamento, informando medidas e tamanhos dos produtos que você necessita.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>