<?
$h1        = 'Envelopes';
$title     = 'Envelopes';
$desc      = 'A JPR Embalagens traz para o mercado uma linha completa de envelopes. Em nossa linha de produtos você encontra envelopes fabricados sob medida.';
$key       = 'Envelope, Envelopes sob medida, fábrica de Envelopes';
$var     = 'Envelopes';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php'); ?>

<!-- Função Regiões -->
<script src="<?= $url; ?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>

<body>

  <div class="wrapper-topo">

    <? include('inc/topo.php'); ?>

  </div>

  <div class="wrapper">

    <main role="main">

      <section>


        <?= $caminhoCategoria ?>
        <article>

          <h1><?= $h1 ?></h1>

          <h2>A JPR Embalagens traz para o mercado uma linha completa de envelopes.</h2>

          <p>Em nossa linha de produtos você encontra <strong>envelopes fabricados sob medida</strong>, de acordo com a necessidade da sua empresa.</p>

          <p>Conheça nossa linha de <strong>envelopes</strong>:</p>
          <ul class="thumbnails">
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-adesivado" title="Envelope Adesivado"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-adesivado-01.jpg" alt="Envelope Adesivado" class="lazyload" title="Envelope Adesivado" /></a>
              <h4><a href="<?= $url; ?>envelope-adesivado" title="Envelope Adesivado">Envelope Adesivado</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-awb" title="Envelope AWB"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-awb-01.jpg" alt="Envelope AWB" class="lazyload" title="Envelope AWB" /></a>
              <h4><a href="<?= $url; ?>envelope-awb" title="Envelope AWB">Envelope AWB</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-bolha" title="Envelope Bolha"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-bolha-01.jpg" alt="Envelope Bolha" class="lazyload" title="Envelope Bolha" /></a>
              <h4><a href="<?= $url; ?>envelope-bolha" title="Envelope Bolha">Envelope Bolha</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-coextrusado" title="Envelope Coextrusado"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-coextrusado-01.jpg" alt="Envelope Coextrusado" class="lazyload" title="Envelope Coextrusado" /></a>
              <h4><a href="<?= $url; ?>envelope-coextrusado" title="Envelope Coextrusado">Envelope Coextrusado</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-adesivo" title="Envelope Com Adesivo"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-adesivo-01.jpg" alt="Envelope Com Adesivo" class="lazyload" title="Envelope Com Adesivo" /></a>
              <h4><a href="<?= $url; ?>envelope-adesivo" title="Envelope Com Adesivo">Envelope Com Adesivo</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-correios" title="Envelope Correios"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-correios-01.jpg" alt="Envelope Correios" class="lazyload" title="Envelope Correios" /></a>
              <h4><a href="<?= $url; ?>envelope-correios" title="Envelope Correios">Envelope Correios</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-seguranca" title="Envelope de Segurança"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-seguranca-01.jpg" alt="Envelope de Segurança" class="lazyload" title="Envelope de Segurança" /></a>
              <h4><a href="<?= $url; ?>envelope-seguranca" title="Envelope de Segurança">Envelope de Segurança</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-fronha" title="Envelope Fronha"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-fronha-01.jpg" alt="Envelope Fronha" class="lazyload" title="Envelope Fronha" /></a>
              <h4><a href="<?= $url; ?>envelope-fronha" title="Envelope Fronha">Envelope Fronha</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-inviolavel" title="Envelope Inviolável"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-inviolavel-01.jpg" alt="Envelope Inviolável" class="lazyload" title="Envelope Inviolável" /></a>
              <h4><a href="<?= $url; ?>envelope-inviolavel" title="Envelope Inviolável">Envelope Inviolável</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico" title="Envelope Plástico"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-01.jpg" alt="Envelope Plástico" class="lazyload" title="Envelope Plástico" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico" title="Envelope Plástico">Envelope Plástico</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-4-furos" title="Envelope Plástico 4 Furos"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-4-furos-01.jpg" alt="Envelope Plástico 4 Furos" class="lazyload" title="Envelope Plástico 4 Furos" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-4-furos" title="Envelope Plástico 4 Furos">Envelope Plástico 4 Furos</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-bolha" title="Envelope Plástico Bolha"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-bolha-01.jpg" alt="Envelope Plástico Bolha" class="lazyload" title="Envelope Plástico Bolha" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-bolha" title="Envelope Plástico Bolha">Envelope Plástico Bolha</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-ilhos" title="Envelope Plástico Com Ilhós"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-ilhos-01.jpg" alt="Envelope Plástico Com Ilhós" class="lazyload" title="Envelope Plástico Com Ilhós" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-ilhos" title="Envelope Plástico Com Ilhós">Envelope Plástico Com Ilhós</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-dvd" title="Envelope Plástico DVD"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-dvd-01.jpg" alt="Envelope Plástico DVD" class="lazyload" title="Envelope Plástico DVD" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-dvd" title="Envelope Plástico DVD">Envelope Plástico DVD</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-fronha" title="Envelope Plástico Fronha"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-fronha-01.jpg" alt="Envelope Plástico Fronha" class="lazyload" title="Envelope Plástico Fronha" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-fronha" title="Envelope Plástico Fronha">Envelope Plástico Fronha</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-impresso" title="Envelope Plástico Impresso"><img src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-impresso-01.jpg" alt="Envelope Plástico Impresso" class="lazyload" title="Envelope Plástico Impresso" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-impresso" title="Envelope Plástico Impresso">Envelope Plástico Impresso</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-inviolavel" title="Envelope Plástico Inviolável"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-inviolavel-01.jpg" alt="Envelope Plástico Inviolável" class="lazyload" title="Envelope Plástico Inviolável" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-inviolavel" title="Envelope Plástico Inviolável">Envelope Plástico Inviolável</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-janela" title="Envelope Plástico Janela"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-janela-01.jpg" alt="Envelope Plástico Janela" class="lazyload" title="Envelope Plástico Janela" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-janela" title="Envelope Plástico Janela">Envelope Plástico Janela</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-laboratorio" title="Envelope Plástico Laboratório"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-laboratorio-01.jpg" alt="Envelope Plástico Laboratório" class="lazyload" title="Envelope Plástico Laboratório" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-laboratorio" title="Envelope Plástico Laboratório">Envelope Plástico Laboratório</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-leitoso" title="Envelope Plástico Leitoso"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-leitoso-01.jpg" alt="Envelope Plástico Leitoso" class="lazyload" title="Envelope Plástico Leitoso" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-leitoso" title="Envelope Plástico Leitoso">Envelope Plástico Leitoso</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-mala-direta" title="Envelope Plástico Mala Direta"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-mala-direta-01.jpg" alt="Envelope Plástico Mala Direta" class="lazyload" title="Envelope Plástico Mala Direta" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-mala-direta" title="Envelope Plástico Mala Direta">Envelope Plástico Mala Direta</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-personalizado" title="Envelope Plástico Personalizado"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-personalizado-01.jpg" alt="Envelope Plástico Personalizado" class="lazyload" title="Envelope Plástico Personalizado" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-personalizado" title="Envelope Plástico Personalizado">Envelope Plástico Personalizado</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-revista" title="Envelope Plástico Revista"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-revista-01.jpg" alt="Envelope Plástico Revista" class="lazyload" title="Envelope Plástico Revista" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-revista" title="Envelope Plástico Revista">Envelope Plástico Revista</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-tipo-fronha" title="Envelope Plástico Tipo Fronha"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-tipo-fronha-01.jpg" alt="Envelope Plástico Tipo Fronha" class="lazyload" title="Envelope Plástico Tipo Fronha" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-tipo-fronha" title="Envelope Plástico Tipo Fronha">Envelope Plástico Tipo Fronha</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-vai-vem" title="Envelope Plástico Vai e Vem"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-plastico-vai-vem-01.jpg" alt="Envelope Plástico Vai e Vem" class="lazyload" title="Envelope Plástico Vai e Vem" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-vai-vem" title="Envelope Plástico Vai e Vem">Envelope Plástico Vai e Vem</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-reciclado" title="Envelope Reciclado"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-reciclado-01.jpg" alt="Envelope Reciclado" class="lazyload" title="Envelope Reciclado" /></a>
              <h4><a href="<?= $url; ?>envelope-reciclado" title="Envelope Reciclado">Envelope Reciclado</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-sedex" title="Envelope Sedex"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-sedex-01.jpg" alt="Envelope Sedex" class="lazyload" title="Envelope Sedex" /></a>
              <h4><a href="<?= $url; ?>envelope-sedex" title="Envelope Sedex">Envelope Sedex</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-transparente" title="Envelope Transparente"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-transparente-01.jpg" alt="Envelope Transparente" class="lazyload" title="Envelope Transparente" /></a>
              <h4><a href="<?= $url; ?>envelope-transparente" title="Envelope Transparente">Envelope Transparente</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-vai-vem" title="Envelope Vai e Vem"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelope-vai-vem-01.jpg" alt="Envelope Vai e Vem" class="lazyload" title="Envelope Vai e Vem" /></a>
              <h4><a href="<?= $url; ?>envelope-vai-vem" title="Envelope Vai e Vem">Envelope Vai e Vem</a></h4>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>envelopes-plasticos-especiais" title="Envelopes Plásticos Especiais"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-plasticos-especiais-01.jpg" alt="Envelopes Plásticos Especiais" class="lazyload" title="Envelopes Plásticos Especiais" /></a>
              <h4><a href="<?= $url; ?>envelopes-plasticos-especiais" title="Envelopes Plásticos Especiais">Envelopes Plásticos Especiais</a></h4>
            </li>







            <li>
              <a rel="nofollow" href="<?= $url; ?>cotacao-filme-stretch" title="cotação filme stretch"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-5.jpg" alt="cotação filme stretch" class="lazyload" title="cotação filme stretch" /></a>
              <h4><a href="<?= $url; ?>cotacao-filme-stretch" title="cotação filme stretch">cotação filme stretch/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>embalagem-filme-stretch-distribuidor" title="embalagem filme stretch distribuidor"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-0.jpg" alt="embalagem filme stretch distribuidor" class="lazyload" title="embalagem filme stretch distribuidor" /></a>
              <h4><a href="<?= $url; ?>embalagem-filme-stretch-distribuidor" title="embalagem filme stretch distribuidor">embalagem filme stretch distribuidor/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>embalagem-filme-stretch-preco" title="embalagem filme stretch preço"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-0.jpg" alt="embalagem filme stretch preço" class="lazyload" title="embalagem filme stretch preço" /></a>
              <h4><a href="<?= $url; ?>embalagem-filme-stretch-preco" title="embalagem filme stretch preço">embalagem filme stretch preço/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>fabrica-de-envelopes-de-seguranca" title="fábrica de envelopes de segurança"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-0.jpg" alt="fábrica de envelopes de segurança" class="lazyload" title="fábrica de envelopes de segurança" /></a>
              <h4><a href="<?= $url; ?>fabrica-de-envelopes-de-seguranca" title="fábrica de envelopes de segurança">fábrica de envelopes de segurança/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>fabrica-de-saco-de-lixo" title="fábrica de saco de lixo"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-1.jpg" alt="fábrica de saco de lixo" class="lazyload" title="fábrica de saco de lixo" /></a>
              <h4><a href="<?= $url; ?>fabrica-de-saco-de-lixo" title="fábrica de saco de lixo">fábrica de saco de lixo/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>fabrica-de-sacos-plasticos" title="fábrica de sacos plásticos"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-9.jpg" alt="fábrica de sacos plásticos" class="lazyload" title="fábrica de sacos plásticos" /></a>
              <h4><a href="<?= $url; ?>fabrica-de-sacos-plasticos" title="fábrica de sacos plásticos">fábrica de sacos plásticos/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>fabricante-de-filme-shrink" title="fabricante de filme shrink"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-8.jpg" alt="fabricante de filme shrink" class="lazyload" title="fabricante de filme shrink" /></a>
              <h4><a href="<?= $url; ?>fabricante-de-filme-shrink" title="fabricante de filme shrink">fabricante de filme shrink/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>filme-de-encolhimento" title="filme de encolhimento"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-2.jpg" alt="filme de encolhimento" class="lazyload" title="filme de encolhimento" /></a>
              <h4><a href="<?= $url; ?>filme-de-encolhimento" title="filme de encolhimento">filme de encolhimento/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>sacos-plasticos-para-embalagem" title="sacos plásticos para embalagem"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-2.jpg" alt="sacos plásticos para embalagem" class="lazyload" title="sacos plásticos para embalagem" /></a>
              <h4><a href="<?= $url; ?>sacos-plasticos-para-embalagem" title="sacos plásticos para embalagem">sacos plásticos para embalagem/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>filme-shrink" title="filme shrink"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-6.jpg" alt="filme shrink" class="lazyload" title="filme shrink" /></a>
              <h4><a href="<?= $url; ?>filme-shrink" title="filme shrink">filme shrink/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>filme-stretch" title="filme stretch"><img data-src="<?= $url; ?>imagens/produtos/envelopes/thumb/envelopes-7.jpg" alt="filme stretch" class="lazyload" title="filme stretch" /></a>
              <h4><a href="<?= $url; ?>filme-stretch" title="filme stretch">filme stretch/a></h4>
            </li>

          </ul>

          <? include('inc/saiba-mais.php'); ?>

        </article>

        <? include('inc/coluna-lateral-paginas.php'); ?>

        <br class="clear" />

        <? include('inc/regioes.php'); ?>

      </section>

    </main>



  </div><!-- .wrapper -->



  <? include('inc/footer.php'); ?>


</body>

</html>