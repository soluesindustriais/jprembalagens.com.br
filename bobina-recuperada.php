<?
	$h1    			= 'Bobina recuperada';
	$title 			= 'Bobina recuperada';
	$desc  			= 'A bobina recuperada é uma opção econômica, mas de qualidade e com grande resistência, para embalar os seus produtos. A produção é feita a partir de plásticos reciclados';
	$key   			= 'bobina, recuperada, bobinas recuperadas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas recuperadas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>A <strong>bobina recuperada</strong> é uma opção econômica, mas de qualidade e com grande resistência, para embalar os seus produtos. Confira maiores informações.</p>
            
            <p>Empresários estão constantemente pensando em estratégias de melhorar a imagem da empresa do mercado. E uma destas estratégias pode ser investindo numa embalagem de qualidade. Por isso, conheça a <strong>bobina recuperada</strong>.</p>

            <p>A <strong>bobina recuperada</strong> pode ser fabricada com grande diversidade de modelos, cores e materiais. Em todos os casos, o produto se destaca pela resistência e pela qualidade, proporcionando proteção durante transporte e armazenamento de produtos e sendo resistentes a rasgos, rupturas e quedas.</p>
            
            <p>A <strong>bobina recuperada</strong> cristal é um dos modelos que podem ser fabricados. Neste caso, a produção é feita com aparas de material virgem e o material fica com coloração amarelo claro e com alguns pontos, devido ao processo de reciclagem. Porém, a transparência da embalagem é mantida.</p>
            
  			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina recuperada</strong> canela também é outra opção. Neste caso, a produção é feita a partir de plásticos reciclados. A embalagem mantém a resistência e tem coloração marrom claro (cor de canela), porém continua com transparência, sendo possível visualizar o conteúdo interno da embalagem.</p>
            
            <p>Há ainda opção de <strong>bobina recuperada</strong> colorida, que é uma mistura de embalagens recicladas. Neste caso, a embalagem fica sem padrão e sem transparência. Esta é uma das embalagens plásticas flexíveis mais disponíveis no mercado.</p>
            
            <h2>Bobina recuperada com preço em conta</h2>

            
            <p>E para adquirir a <strong>bobina recuperada</strong>, conte com as vantagens da JPR Embalagens. Com mais de 15 anos de experiência no mercado, a empresa é especializada na venda de embalagens plásticas flexíveis e tem como objetivo identificar oportunidades de melhoria e reduzir perdas e custos.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>     
                   
            <p>Para isso, a JPR Embalagens conta com uma equipe técnica sempre atualizada, que busca as opções mais modernas do mercado, sempre garantindo embalagens resistentes e com preço em conta, resultando em ótima relação custo-benefício do mercado.</p>
            
            <p>O atendimento da empresa é totalmente personalizado e voltado às necessidades e preferências de cada cliente. Além de <strong>bobina recuperada</strong>, a empresa disponibiliza bobinas de outros tipos, além de filmes, envelopes, sacos e sacolas.</p>
            
            <p>Saiba mais entrando em contato com um dos consultores, aproveite as ótimas condições de pagamento da JPR Embalagens e solicite já o seu orçamento, informando medidas e quantidade que a sua empresa necessita.</p>

            
            <? include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<? include('inc/coluna-lateral-paginas.php')?>
        
			<? include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php')?>


</body>
</html>