<?php
$h1    			= 'Saco plástico transparente';
$title 			= 'Saco plástico transparente';
$desc  			= 'Com o saco plástico transparente são infinitas as aplicações e produtos que o pode embalar, ainda mais quando fabricados com matéria-prima virgem.';
$key   			= 'Sacos plásticos transparentes, Saco, sacos, plástico, transparente';
$var 			= 'Sacos plásticos transparentes';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>
                   <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     

             <br> 

             <p>Os <strong>sacos plásticos transparente</strong> podem ser produzidos em PEAD, PEBD, PP e BOPP.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>

             <p>São infinitas as aplicações e produtos que o <strong>saco plástico transparente</strong> pode embalar, ainda mais quando fabricados com matéria-prima virgem, o <strong>saco plástico transparente</strong> se torna atóxico, podendo embalar produtos alimentícios, sem agredir ou contaminar o produto.</p>
             <p>É um produto versátil, e pode ser liso ou impresso em até seis cores, podendo ser fabricado em diversas cores ou apenas transparente.</p>
             <p>Se você deseja reduzir custos com o <strong>saco plástico transparente</strong>, mas não armazena produtos alimentícios ou medicinais e deseja contribuir com o meio ambiente, utilize o <strong>saco plástico</strong> produzido com matéria-prima reciclada. Nesta opção, a embalagem fica com alguns pontos e com um aspecto amarelado, devido ao processo de reciclagem que o produto passa. Porem, a embalagem continuará transparente, podendo ver perfeitamente o produto embalado.</p>
             <p>Para facilitar o fechamento e manuseio do produto, o <strong>saco plástico transparente</strong> pode receber diversos acessórios, como aba adesiva, fecho zip, ilhós e botão, e como uma destas opções, você possuirá uma embalagem moderna e inovadora, além de dar um melhor acabamento em seu produto.</p>
             <p>Fabricamos também o <strong>saco plástico transparente com aditivo oxibiodegradavel</strong>. Nesta opção, a embalagem em contato com o meio ambiente se degrada em curto espaço de tempo, sem deixar resíduos nocivos ao meio ambiente.</p>
             <p>Nossa quantidade mínima de produção são de 100kg para <strong>saco plástico transparente</strong> liso e 250kg impresso.</p>
             <p>Para receber um orçamento de <strong>saco plástico transparente</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>



             <?php include('inc/saiba-mais.php');?>



         </article>

         <?php include('inc/coluna-lateral-paginas.php');?>

         <?php include('inc/paginas-relacionadas.php');?>  

         <br class="clear" />  



         <?php include('inc/regioes.php');?>

         <?php include('inc/copyright.php');?>


     </section>

 </main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>