<?php
$h1    			= 'Plástico filme';
$title 			= 'Plástico filme';
$desc  			= 'O plástico filme pode ser produzido conforme cada necessidade específica, e por isso é largamente utilizado em vários segmentos';
$key   			= 'plástico, filme, plásticos filme';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Plásticos filme';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoPlastico?>
              <article>
             <h1><?=$h1?></h1>     
             <br> 
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O <strong>plástico filme</strong> é a solução ideal para embalagens pela qualidade e versatilidade que proporciona. Confira maiores informações.</p>
            
            <p>A embalagem é um aspecto fundamental na hora de vender um produto. Afinal, é ela que garante a manutenção das características originais do que está sendo embalado e também é ela que torna um produto mais atraente, de acordo com o aspecto visual que possui. Por isso, uma ótima indicação é o <strong>plástico filme</strong>.</p>

            <p>O <strong>plástico filme</strong> pode ser produzido conforme cada necessidade específica, e por isso é largamente utilizado em vários segmentos. A fabricação pode ser em PEAD, PEBD, PP, BOPP, laminado, entre outras opções. Além disso, pode ser feito liso ou impresso em até seis cores.</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O <strong>plástico filme</strong> fabricado em polipropileno (PP) é mais usado por indústrias alimentícias, gráficas e confecções, caracterizando-se por brilho e transparência. Quando feito em BOPP, o plástico é usado no mercado de embalagem como embalagem de presente, tendo opção perolizado ou metalizado.</p>
            
            <p>O <strong>plástico filme</strong> ainda pode ser feito em polietileno, que é uma das embalagens mais usadas em alimentos, bebidas, leites e em farinhas. Isso porque é um plástico atóxico e resistente, que mantém as características originais do produto. Também é usado na área de automotivos e de metalurgia.</p>
            
            <h2>Plástico filme pode ser reciclado</h2>
            
            <p>Uma opção para reduzir os custos com embalagem e ainda contribuir com o meio ambiente é o <strong>plástico filme</strong> reciclado, que pode ser feito como reciclado cristal ou como reciclado canela.</p>
            
            <p>No caso de reciclado cristal, o <strong>plástico filme</strong> é feito a partir de aparas de material virgem, com coloração amarelo claro, mas ainda com transparência. O saco feito com reciclado canela é feito a partir de reciclado cristal, com embalagem com coloração canela, mas ainda com transparência.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir o <strong>plástico filme</strong>, aproveite os benefícios da JPR Embalagens. Com mais de 15 anos de presença no mercado, a empresa é especializada na área de embalagens plásticas flexíveis, levando até o consumidor as melhores soluções neste segmento.</p>
            
            <p>São produtos de qualidade comprovada e com preço em conta, resultando em ótima relação custo-benefício. O objetivo da JPR Embalagens é a identificação de oportunidades de melhoria, para reduzir perdas e custos.</p>
            
            <p>O atendimento é totalmente personalizado e as condições de pagamento vantajosas. Por isso, aproveite e solicite já o seu orçamento.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>