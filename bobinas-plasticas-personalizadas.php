<?php
	$h1    			= 'Bobinas plásticas personalizadas';
	$title 			= 'Bobinas plásticas personalizadas';
	$desc  			= 'As bobinas plásticas personalizadas podem ser fabricadas em diversos materiais: polietileno de baixa ou alta densidade, POPP ou polipropileno';
	$key   			= 'bobinas, plásticas, personalizadas, bobina plástica personalizada';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina plástica personalizada';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p><strong>bobinas plásticas personalizadas</strong> colaboram para a divulgação e fixação da sua marca. Saiba mais sobre este tipo de embalagem.</p>

            <p>A embalagem é um ponto essencial na hora de vender um produto, porque muitas vezes é ela o primeiro contato que o cliente tem com a sua marca. Por isso, invista na qualidade e na imagem da embalagem com as <strong>bobinas plásticas personalizadas</strong>.</p>
            
            <p>As <strong>bobinas plásticas personalizadas</strong> podem ser fabricadas em diversos materiais: polietileno de baixa ou alta densidade, POPP ou polipropileno. A produção é feita sob medida, conforme a necessidade de cada cliente. A personalização pode ser feita em até seis cores, e uma indicação é a utilização das cores da marca, para aumentar a fixação da sua empresa no mercado e para os consumidores.</p>
            
           <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A utilização das <strong>bobinas plásticas personalizadas</strong> é bastante diversa: para embalar roupas, exames, objetos, revistas, jornais, entre outros.</p>
            
            <p>Pode ser aplicada também para embalagem de presentes. Neste caso, a embalagem pode ser metalizada e perolizada, conforme layout que o cliente preferir.</p>
            
            <h2>bobinas plásticas personalizadas podem ser sustentáveis</h2>
            
            <p>As <strong>bobinas plásticas personalizadas</strong> também podem ter produção ecologicamente correta, com fabricação em reciclado cristal ou canela. No caso de bobinas com reciclado cristal, a produção é feita a partir de aparas de material virgem e outras embalagens, fazendo com que a embalagem tenha aspecto amarelo claro e com alguns pontos, devido ao processo de reciclagem.</p>

            <p>No caso de <strong>bobinas plásticas personalizadas</strong> com reciclado canela, o aspecto fica marrom claro e também com alguns pontos. Em ambos os casos, a transparência da embalagem é mantida, o que significa que é possível visualizar o conteúdo interno.</p>
            
           <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <p>E para adquirir as <strong>bobinas plásticas personalizadas</strong>, conte com a JPR Embalagens. A empresa atua há mais de 15 anos no mercado e leva até o consumidor as melhores soluções em embalagens plásticas flexíveis.</p>
            
            <p>A equipe técnica da JPR Embalagens está sempre atualizada em relação às últimas novidades do mercado, com o objetivo de reduzir perdas e custos. Além disso, a empresa disponibiliza atendimento totalmente personalizado, voltado para a preferência e necessidade dos clientes.</p>
            
            <p>Saiba mais sobre as ótimas condições e os preços em conta da empresa entrando em contato com um dos consultores e solicite já o seu orçamento.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>