<?php
	$h1    			= 'Bobina de pvc flexível';
	$title 			= 'Bobina de pvc flexível';
	$desc  			= 'A bobina de pvc flexível é uma embalagem ideal para embalagens promocionais do tipo unitárias ou de agrupamentos e é recomendada para embalagens unitárias.';
	$key   			= 'bobina, pvc, flexível, bobinas de pvc flexíveis';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de pvc flexíveis';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
          	<p>A bobina de PVC é a opção ideal para embalagens cujos produtos não possam ter contato com o ambiente externo. Saiba mais. A embalagem é um ponto fundamental na hora de vender mais e de ampliar a qualidade do seu produto. Isso porque a embalagem precisa ser resistente, manter as características originais do que está sendo embalado e ter um aspecto visual agradável. Por isso, conte com a <strong>bobina de pvc flexível</strong>.</p>
            
            <p>A <strong>bobina de pvc flexível</strong> é uma embalagem ideal para embalagens promocionais do tipo unitárias ou de agrupamentos. Também é uma opção altamente recomendada para embalagens unitárias que precisam estar invioláveis. Ou seja, para embalagens cujos produtos não podem ter nenhum tipo de contato com o meio, para que não tenha propriedades e características originais alteradas.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <p>A <strong>bobina de pvc flexível</strong> tem transparência e grande resistência, fazendo com que os produtos embalados tenham proteção contra violação, umidade e poeira.</p>

            <p>Um dos benefícios da <strong>bobina de pvc flexível</strong> é a aderência fácil, o que explica sua grande utilização em maquinários como túnel de encolhimento e seladora em L (automática, semiautomática ou manual).</p>
            
            <p>A <strong>bobina de pvc flexível</strong> também é usada para encartelamento a vácuo, produtos expostos em displays, cautelaria, materiais de construção, autopeças, utilidades domésticas, armarinhos, entre outros tipos de aplicação.</p>
            
            <h2>Bobina de pvc flexível da JPR Embalagens</h2>

            
            <p>Para obter a <strong>bobina de pvc flexível</strong>, aproveite as vantagens e benefícios disponibilizados pela JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa é especializada na área de embalagens plásticas flexíveis.</p>
            
                        
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>      
                  
            <p>A equipe técnica da JPR Embalagens está sempre atenta às novidades do mercado, para garantir para o consumidor uma embalagem de alta tecnologia e com custos reduzidos, que possam permitir um melhor controle de estoque, reduzir perdas e, consequentemente, reduzir custos.</p>
            
            <p>O atendimento da JPR Embalagens é totalmente personalizado e voltado às preferências e demandas do cliente. Isso significa que a <strong>bobina de pvc flexível</strong> pode ser personalizada de diferentes maneiras.</p>
            
            <p>Saiba mais sobre a personalização da <strong>bobina de pvc flexível</strong> e conheça as ótimas condições da JPR Embalagens entrando em contato com a equipe. Aproveite os benefícios e solicite já o seu orçamento.</p>
            
                        
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>