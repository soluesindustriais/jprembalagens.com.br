<?php
	$h1    			= 'Envelope plástico 4 furos';
	$title 			= 'Envelope plástico 4 furos';
	$desc  			= 'O envelope plástico 4 furos é muito utilizado para embalar documentos e depois são arquivados em pasta catálogo. Podem ser lisos ou impressos em até 6 cores.';
	$key   			= 'Envelopes plásticos 4 furos, Envelopes, Envelope, 4, furos, Envelope plástico quatro furos';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos quatro furos';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                   
            
                                    
            <p>O <strong>envelope plástico 4 furos</strong> é muito utilizado para embalar documentos e depois são arquivados em pasta catálogo.</p>
            
			<? $pasta = "imagens/produtos/envelopes/"; $quantia = 3; include('inc/gallery.php'); ?>
            
            <h2>Opções de envelope plásticos 4 furos</h2>
            
            <p>Fabricados em polietileno de baixa densidade, os <strong>envelope plásticos 4 furos</strong> podem ser lisos ou impressos em até 6 cores.</p>
            <p>Outras opções que temos deste produto, são os coloridos e transparentes, que ajudam na separação de documentos e se tornam se fácil localização.</p>
            <p>Também são fabricados a partir de matérias-primas recicladas, nesse formato o <strong>envelope plástico 4 furos</strong> pode ser liso ou impresso em até 6 cores, porem o aspecto visual da embalagem altera devido ao processo da reciclagem, assim a cor do plástico fica amarelo claro, porém transparente, ou seja, é possível ver perfeitamente o que há dentro da embalagem. </p>
            <p>Se você é uma empresa que se preocupa com o meio ambiente, e quer contribuir com o planeta, utilize <strong>envelope plástico 4 furos reciclado</strong>, além de resistentes e práticos, você reduz custos com embalagens, pois está matéria-prima é bem mais em conta que o  material virgem.</p>
             <p>Os <strong>envelopes plásticos 4 furos</strong>, também são utilizados em confecções, assim quando a roupa ou o tecido são embalados, os furos facilitam a vazão do ar que fica dentro da embalagem, e assim a embalagem fica bem justa as roupas ou tecidos, otimizando espaços no momento de guardar ou enviar aos seus clientes.</p>
            <p>Além de <strong>envelopes plásticos quatro furos</strong>, a JPR Embalagens fabrica um ampla linha de embalagens, como <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelopes de segurança</strong></a>, <a href="<?=$url;?>envelope-plastico-bolha" title="Envelope Plástico Bolha"><strong>envelope plástico bolha</strong></a>, <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plásticos</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, <a href="<?=$url;?>envelopes-plasticos-especiais" title="Envelopes Plásticos Especiais"><strong>envelopes plásticos especiais</strong></a> e muitas outras.</p>
            <p>Para receber um orçamento do <strong>envelope plástico quatro furos</strong>, basta possuir as medidas (largura x comprimento x espessura), e a quantidade que você deseja utilizar.</p>

        	 
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>