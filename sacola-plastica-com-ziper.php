<?
$h1    			= 'Sacola plástica com zíper';
$title 			= 'Sacola plástica com zíper';
$desc  			= 'Uma sacola plástica com zíper, expressa a sua preocupação em inovar. A sacola plástica com fecho zíper é utilizada por laboratórios, bancos, editoras, entre outros.';
$key   			= 'Sacola, plástica, zíper, Sacolas plásticas com zíper';
$var 			= 'Sacolas plásticas com zíper';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>
<!-- Função LightBox -->
<link rel="stylesheet" href="<?=$url?>css/lightbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$url?>js/lightbox.js"></script>

<!-- Função Regiões -->
<link rel="stylesheet" href="<?=$url;?>css/tabs.css" type="text/css" />
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
<script src="<?=$url;?>js/tabs.js" type="text/javascript"></script>

</head>
<body>
    <div class="wrapper">

        <? include('inc/topo.php');?>

        <div class="container">
          <?=$caminhoProdutoSacolas?>
          <? include('inc/coluna-lateral.php');?>
          <div class="content">

              <h1><?=$h1?></h1>
              
              <article>
              
              <p>A modernidade da embalagem fala muito do produto que ela contém. Uma <strong>sacola plástica com zíper</strong>, expressa a sua preocupação em inovar, em oferecer sempre o melhor. Seu produto causa boa impressão à primeira vista e conquista consumidores. É seu passaporte para novos mercados.</p>
              <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url;?><?=$pasta?>sacolas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            <p>O <strong>zíper</strong> facilita o fechamento da <strong>sacola</strong>, evitando que o produto migre para fora da embalagem, além de proteger contra a umidade e a poeira.</p>
            <p>A <strong>sacola plástica com fecho zíper</strong> é amplamente utilizada por laboratórios, bancos, editoras, hospitais, entre outros estabelecimentos e indústrias.</p>
            <p>São poucas as embalagens que protegem a integridade do produto como a <strong>sacola plástica com zíper</strong>, e diferente de qualquer outra embalagem, somente o <strong>fecho zíper</strong> continua protegendo o seu produto após a abertura, basta fechar novamente que a proteção continua.</p>
            <p>Podem ser lisas ou impressas em até 6 cores, além disso, a <strong>sacola plástica com zíper</strong> poder ser feita na cor transparente ou pigmentada em diversas cores.</p>
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url;?><?=$pasta?>sacolas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            <p>A <strong>sacola plástica com zíper</strong> possui uma alça na parte superior para facilitar o transporte do produto. É uma embalagem super moderna, e possui múltiplas funções. Além do <strong>fecho zíper</strong>, a <strong>sacola plástica</strong> também pode ser confeccionada com o fecho tipo tala, que apresenta a mesma função do <strong>zíper</strong>, e fica a critério do cliente o modelo e formato do fecho.</p>
            <p>Para <strong>sacola plástica com zíper personalizada</strong>, nosso lote mínimo de produção são de 250kg e sem impressão 150kg.</p>
            <p>Para receber um orçamento de <strong>sacola plástica com ziper</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <? include('inc/saiba-mais.php');?>
            
        </div>
        <div class="clear"></div>
        <? include('inc/thumb-mais-servicos.php');?>
        
        <? include('inc/regioes.php');?>
        
        
        <? include('inc/direitos-autorais.php');?>
    </div>
    <? include('inc/rodape.php');?>
</div>
</body>
</html>

