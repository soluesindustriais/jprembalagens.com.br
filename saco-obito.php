<?php
$h1    			= 'Saco de óbito';
$title 			= 'Saco de óbito';
$desc  			= 'Produzido conforme a norma ABNT, o saco de óbito é confeccionados em polietileno de baixa densidade, com zíper frontal, quando o assunto é qualidade, estamos em primeiro lugar';
$key   			= 'Sacos de óbito, Saco, sacos, óbito';
$var 			= 'Sacos de óbito';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <p>Produzido conforme a norma ABNT, o <strong>saco de óbito</strong> é confeccionados em polietileno de baixa densidade, com zíper frontal, e deve ter um rigoroso controle de qualidade, afim de assegurar sua funcionalidade e quando o assunto é qualidade, estamos em primeiro lugar.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>

             <h2>Fabricamos nas medidas:</h2>
             <ul class="list">
                <li><strong>Saco de óbito para recém nascido</strong> (RN) – Medida:  30 cm x 60 cm</li>
                <li><strong>Saco de óbito Pequeno</strong> (P) – Medida: 50 cm x 100 cm</li>
                <li><strong>Saco de óbito Médio</strong> (M) – Medida: 60 cm x 150 cm</li>
                <li><strong>Saco de óbito Grade</strong> (G) – Medida: 90 cm x 220 cm</li>
            </ul>	
            <p>Nossos <strong>sacos de óbito</strong> acompanham etiqueta de identificação, porem não possuem alça para transporte.</p>            
            
            <? $pasta = "imagens/produtos/";?>            
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>

            <p>Os <strong>sacos de óbito</strong> são fabricados somente na cor cinza, fazemos pacotes padrão com 25 <strong>sacos</strong> cada. Lote mínimo de faturamento de R$ 500,00. </p>
            <p>Os <strong>sacos de óbito</strong> indicados para transportar cadáver, e são muito utilizados por hospitais, laboratórios, entre outros.</p>
            <p>Além de <strong>saco de óbito</strong>, a JPR Embalagens trabalha com outras linhas de <a href="<?=$url;?>saco-hospitalar" title="Saco Hospitalar">sacos hospitalares</a> como, <a href="<?=$url;?>saco-infectante" title="Saco Infectante"><strong>saco para lixo infectante</strong></a>, <a href="<?=$url;?>saco-hamper" title="Saco Hamper"><strong>saco hamper</strong></a> e <a href="<?=$url;?>saco-lixo" title="Saco de Lixo"><strong>sacos para lixo comum</strong></a>.</p>
            <p>Para receber um orçamento de <strong>saco de óbito</strong>, entre em contato com um de nossos consultores, e informe o modelo e quantidade que você deseja. </p>

            
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>