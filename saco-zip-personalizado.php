<?php
$h1    			= 'Saco zip personalizado';
$title 			= 'Saco zip personalizado';
$desc  			= 'Crie e desenvolva o saco zip personalizado da maneira que desejar, possuímos equipamentos de ultima geração, e assim garantimos a qualidade que o seu produto merece.';
$key   			= 'Sacos zip personalizados, Saco, sacos, zip, personalizado';
$var 			= 'Sacos zip personalizados';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>

                       <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     

             <br> 

             <p>Fabricado em polietileno de baixa densidade, o <strong>saco zip personalizado</strong> pode ser impresso em até 6 cores, além de poder ser produzido nas opções transparente ou pigmentado.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>A modernidade da embalagem fala muito do produto que ela contém e o <strong>saco zip personalizado</strong> possui um sistema pratico de fechamento, dispensando o uso de seladoras. </p>
             <p>Uma embalagem inovadora faz com que o seu produto cause boa impressão à primeira vista e conquista novos consumidores. O <strong>saco zip personalizado</strong> é um dos canais para divulgar a sua marca, e se apresentar para novos clientes.</p>

             <h2>Segurança e durabilidade com o saco zip personalizado</h2>

             <p>Poucas embalagens protegem o produto como o <strong>saco zip personalizado</strong>. Mas, diferentemente de qualquer outra, só o <strong>zip</strong> continua protegendo após a abertura. Basta fechar novamente que a proteção continua, aumentando a vida útil do produto e facilitando a vida do consumidor. O <strong>saco zip personalizado</strong> é ótimo para embalar alimentos, produtos de higiene e muitos outros. E ótimo para você que ganha um novo argumento de venda, fabricamos também <a href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip"><strong>Sacola Plastica Zip</strong></a>.</p>
             <p>São produzidos sob medida, assim você escolhe as medidas e o formato que deseja receber o <strong>saco zip personalizado</strong>.</p>
             <p>Assim, crie e desenvolva o <strong>saco zip</strong> da maneira que desejar, possuímos equipamentos de ultima geração, e assim garantimos a qualidade que o seu produto merece.</p>
             <p>Nossa quantidade mínima de produção são de 250kg para <strong>sacos zip personalizado</strong> e sem personalização 150kg.</p>
             <p>Para receber um orçamento de <strong>sacos zip personalizado</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

             <?php include('inc/saiba-mais.php');?>



         </article>

         <?php include('inc/coluna-lateral-paginas.php');?>

         <?php include('inc/paginas-relacionadas.php');?>  

         <br class="clear" />  



         <?php include('inc/regioes.php');?>

         <?php include('inc/copyright.php');?>


     </section>

 </main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>