<?php
$h1    			= 'Saco hamper';
$title 			= 'Saco hamper';
$desc  			= 'Saco hamper é indicado para embalar e transportar roupas sujas que foram utilizadas em ambientes médico-hospitalares.';
$key   			= 'Sacos hamper, Saco, sacos, hamper';
$var 			= 'Sacos hamper';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section> 
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             
             <p><strong>Saco hamper</strong> é indicado para embalar e transportar roupas sujas que foram utilizadas em ambientes médico-hospitalares. Possui dispositivo de fechamento seguro e higiênico acoplado na boca do <strong>saco</strong>.</p>
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>

            <p>Fabricado com matéria-prima 100% virgem de polietileno de alta densidade (PEAD), o <strong>Saco hamper</strong> possui excelente resistência mecânica, e o PEAD por uma resina com aspecto fosco, proporciona a opacidade necessária aplicação.</p>
            <h2>Nosso saco hamper é próprio para embalar até 120 litros.</h2>
            <p>Nossas medidas padrões são:</p>
            <ul class="list">
                <li>90,0 cm de largura x 110,0 cm de comprimento</li>
                <li>90,0 cm de largura x 100,0 cm de comprimento</li>
            </ul>
            <h2>Os sacos hamper são confeccionados nos seguintes padrões:</h2>
            <ul class="list">
                <li><strong>Saco hamper verde</strong></li>
                <li><strong>Saco hamper vermelho</strong></li>
                <li><strong>Saco hamper amarelo</strong></li>
                <li><strong>Saco hamper azul</strong></li>
            </ul>
            <p>O <strong>saco hamper</strong> não é considerado um produto para a saúde pela ANVISA, portanto o produto se torna isento do registro da Anvisa.</p>
            <p>Para receber um orçamento de <strong>saco hamper</strong>, entre em contato com um de nossos consultores, e informe o modelo e a quantidade desejada.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>