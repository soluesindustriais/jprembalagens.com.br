<?php
$h1    			= 'Sacola personalizada';
$title 			= 'Sacola personalizada';
$desc  			= 'A sacola personalizada é amplamente utilizada em feiras, supermercados, lojas, shoppings, eventos, e em outros estabelecimentos que buscam oferecer um melhor serviço.';
$key   			= 'Sacola, personalizada, Sacolas personalizadas, Sacola personalizada impressa';
$var 			= 'Sacolas personalizadas';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>
<? include('inc/fancy.php');?>


<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>
                       <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     

             <br>   

             <p>A <strong>sacola personalizada</strong> é fabricada sob medida, de acordo com a necessidade de cada cliente.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>Pode ser lisa ou impressa em até 6 cores, além de serem feitas na cor natural ou pigmentada em diversas cores.</p>
             <p>A <strong>sacola personalizada</strong> é amplamente utilizada em feiras, supermercados, lojas, shoppings, eventos, e em outros estabelecimentos que buscam além de oferecer um melhor serviço, fornecer uma embalagem resistente e segura, que garanta a integridade do produto embalado.</p>
             <p>Geralmente, esta embalagem é fabricada em polietileno de alta ou baixa densidade. Uma forma de se reduzir custos com <strong>sacola personalizada</strong> são as fabricadas com matéria-prima reciclada. É uma forma ecologicamente correta de contribuirmos com o meio ambiente.</p>
             <h2>Veja abaixo os modelos que a JPR produz:</h2>
             <ul class="list">
                <li><strong>Sacola personalizada com alça vazada</strong>;</li>
                <li><strong>Sacola personalizada com alça camiseta</strong>;</li>
                <li><strong>Sacola personalizada com alça fita</strong>;</li>
                <li><strong>Sacola personalizada com ilhós</strong>;</li>
                <li><strong>Sacola personalizada com cordão</strong>;</li>
                <li>Entre outras.</li>
            </ul>	
            <p>Além de <strong>sacolas personalizadas</strong>, a JPR Embalagens possui uma ampla linha de embalagens como, <a href="<?=$url;?>saco-plastico-industrial" title="Saco Plastico Industrial"><strong>saco plastico industrial</strong></a>, <a href="<?=$url;?>envelope-sedex" title="Envelope Sedex"><strong>envelope sedex</strong></a>, <a href="<?=$url;?>envelope-plastico-mala-direta" title="Envelope Plástico Mala Direta"><strong>envelope plástico mala direta</strong></a>, filme e bobinas, entre outros.</p>
            <p>Nossa quantidade mínima de produção para essa linha de produtos é de 250kg e lisa 150 kg.</p>
            <p>Para receber um orçamento de <strong>sacola personalizada</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>
            
            
            

        </article>

        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  

        <br class="clear" />  
        


        <?php include('inc/regioes.php');?>

        <?php include('inc/copyright.php');?>


    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>