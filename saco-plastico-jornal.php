<?php
$h1    			= 'Saco plástico para jornal';
$title 			= 'Saco plástico para jornal';
$desc  			= 'O saco plástico para jornal é fabricado em polietileno de alta densidade e fica com o aspecto fosco e opaco, porem é transparente.';
$key   			= 'Sacos plásticos para jornais, Saco, sacos, plástico, jornal';
$var 			= 'Sacos plásticos para jornais';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             <p>Produzidos em diversos modelos e cores, o <strong>saco plástico para jornal</strong> pode ser liso ou impresso em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>São fabricados sob medida, de acordo com a necessidade de cada cliente.</p>
             <p>Geralmente o <strong>saco plástico para jornal</strong> é fabricado em polietileno de alta densidade, e fica com o aspecto fosco e opaco, porem é transparente sendo possível visualizar o que há dentro da embalagem.</p>
             <p>Também produzimos os <strong>sacos plásticos para jornal</strong> nas opções transparentes ou pigmentado em diversas cores.</p>
             <p>Para facilitar o manuseio do produto, utilize o <strong>saco plástico para jornal com aba adesiva</strong>, basta destacar o liner e dobrar a aba, pronto, a embalagem já estará lacrada e protegida.  </p>
             <p>Além de <strong>saco plástico para jornal</strong>, a JPR Embalagens trabalha com envelopes plásticos, sacolas, <strong>sacos comuns</strong>, embalagens especiais, filmes e bobinas.</p>
             <p>Para contribuir com o meio ambiente, utilize <strong>saco plástico para jornal oxi-biodegradavel</strong>. Enquanto o plástico comum leva mais de 100 anos para se decompor, o <strong>saco plástico para jornal</strong> com aditivo oxi-biodegradavel se degrada em curto espaço de tempo, em média 6 meses. A natureza agradece.</p>
             <p>Nossa quantidade mínima de produção de <strong>saco plástico para jornal</strong> são de 150kg liso e 300kg impresso.</p>
             <p>Para receber um orçamento de <strong>saco plástico para jornal</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>



             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>