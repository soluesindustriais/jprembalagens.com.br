<?php
$h1    			= 'Saco adesivado';
$title 			= 'Saco adesivado';
$desc  			= 'O saco adesivado, otimiza tempo e processo, assim é possível embalar mais produtos por minuto, dispensando o uso de seladoras, além de dar uma melhor segurança ao produto.';
$key   			= 'Sacos adesivados, Saco, Sacos, adesivado, adesivados';
$var 			= 'Sacos adesivados';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>      
            
            <p>Fabricamos <strong>saco adesivado</strong> em <a href="<?=$url;?>saco-pebd" title="Saco PEBD"><strong>PEBD</strong></a>, <a href="<?=$url;?>saco-pead" title="Saco PEAD"><strong>PEAD</strong></a>, <a href="<?=$url;?>saco-pp" title="Saco PP"><strong>PP</strong></a>, <a href="<?=$url;?>saco-bopp-transparente" title="Saco BOPP Transparente"><strong>BOPP</strong></a>, entre outros. Os <strong>sacos adesivados</strong> são indicados para embalar produtos que requerem segurança ao serem embalados.</p> 
            
            <p>Trabalhamos com dois sistemas de adesivo, o primeiro é o <strong>adesivo hotmelt</strong> (permanente), este adesivo tem a função de tornar a embalagem inviolável, e para abri-lo é necessário danificar a embalagem. O segundo adesivo muito utilizado nos <strong>sacos adesivados</strong> é o adesivo abre e fecha, este adesivo é indicado para embalagens que precisarão ser abertas diversas vezes. Este é muito utilizado por confecções.</p>
            <p>Por ser uma embalagem que possui um sistema de fechamento rápido, o <strong>saco adesivado</strong>, otimiza tempos e processos, assim é possível embalar mais produtos por minuto, dispensando o uso de seladoras, além de dar uma melhor segurança e acabamento ao seu produto.</p>
            
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>     
            
            <p>Os <strong>sacos adesivados</strong> também podem ser confeccionados com matéria-prima reciclada, esta é uma forma de reduzir seus custos com embalagem, além de ser uma forma de contribuir com o meio ambiente.</p>
            <p>São fabricados sob medida, de acordo com a necessidade de cada cliente, além disso, esse tipo de embalagem podem ser fabricados lisos ou impressos em até 6 cores.</p>
            <p>Nosso mínimo para fabricação de <strong>sacos adesivados</strong> são 250kg para impressos e 150kg para lisos.</p>
            
            
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>     
            
            
            
            <p>O <strong>saco adesivado</strong> também pode ser confeccionado com o aditivo oxi-biodegradavel, dessa forma, a embalagem quando entra em contato com o meio ambiente, se degrada em curto espaço de tempo, em média seis meses.</p>
            <p>Se você precisa de uma embalagem leve e funcional ao mesmo tempo, entre em contato com um de nossos consultores, e informe as medidas do <strong>saco adesivado</strong> (largura x comprimento x espessura) e a quantidade.</p>

            
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>