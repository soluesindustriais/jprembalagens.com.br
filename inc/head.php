<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
    
        
        <?php 
        include('inc/geral.php'); 
        include('inc/jquery.php'); 
        ?>
        
        <script src="<?=$url?>js/lazysizes.min.js" async></script>
        <script async src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
        <link rel="preconnect" type='text/css' href="css/normalize.css">
        <link rel="preload" href="css/style.css" as="style"> <link rel="stylesheet" href="css/style.css">
        
        <!-- MENU  MOBILE -->
        <script async type="text/javascript" src="<?=$url?>js/jquery.slicknav.js"></script>
        <!-- /MENU  MOBILE -->
        
        <title><?=$title." - ".$nomeSite?></title>
        <base href="<?=$url?>">
        <meta name="description" content="<?=$desc?>">
        <meta name="keywords" content="<?=$h1.", ".$key?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximun-scale=1.0">
    
        <meta name="geo.position" content="<?=$latitude.";".$longitude?>"> 
        <meta name="geo.placename" content="<?=$cidade."-".$UF?>"> 
        <meta name="geo.region" content="<?=$UF?>-BR">
        <meta name="ICBM" content="<?=$latitude.";".$longitude?>">
        <meta name="robots" content="index,follow">
        <meta name="rating" content="General">
        <meta name="revisit-after" content="7 days">
        <link rel="canonical" href="<?=$url.$urlPagina?>">
    <?php       
        if ( $author == '')
        {
        echo '<meta name="author" content="'.$nomeSite.'">';
        }
        else
        {
        echo '<link rel="author" href="'.$author.'">';
        }
    ?>
    
        <link rel="shortcut icon" href="<?=$url?>imagens/favicon.ico">
        
        <meta property="og:region" content="Brasil">
        <meta property="og:title" content="<?=$title." - ".$nomeSite?>">
        <meta property="og:type" content="article">
        <meta property="og:image" content="<?=$url.$pasta.$urlPagina?>-01.jpg">
        <meta property="og:url" content="<?=$url.$urlPagina?>">
        <meta property="og:description" content="<?=$desc?>">
        <meta property="og:site_name" content="<?=$nomeSite?>">
        <meta property="fb:admins" content="<?=$idFacebook?>">

        
        <!-- Desenvolvido por <?=$creditos." - ".$siteCreditos?> -->      
        
        <!-- Nivo Slider -->
        <link rel="stylesheet" href="<?=$url?>nivo-slider/nivo-slider.css" type="text/css" media="screen" />
        <script src="<?=$url?>nivo-slider/jquery.nivo.slider.js"></script>
        
        <script>
        $(window).load(function() {
                $('#slider').nivoSlider({
                effect: 'fade',               // Specify sets like: 'fold,fade,sliceDown'
                slices: 15,                     // For slice animations
                boxCols: 8,                     // For box animations
                boxRows: 4,                     // For box animations
                animSpeed: 500,                 // Slide transition speed
                pauseTime: 4000,                // How long each slide will show
                startSlide: 0,                  // Set starting Slide (0 index)
                directionNav: false,             // Next e Prev navigation
                controlNav: true,               // 1,2,3... navigation
                controlNavThumbs: false,        // Use thumbnails for Control Nav
                pauseOnHover: true,             // Stop animation while hovering		
            });
        });
        </script>
        <!-- fecha nivo -->
               
        <!-- acordeon menu lateral novo -->
        <script src="js/scriptbreaker-multiple-accordion-1.js"></script>
        <script>
        
        $(document).ready(function() {
            $(".cssmenu").accordion({
                accordion:false,
                speed: 500
            });
        });
        
        </script>

