<footer>

    <div class="wrapper">
        <div class="contact-footer">
            <address>
                <span><?= $nomeSite . " - " . $slogan ?></span>
                <?= $rua . " - " . $bairro ?> <br />
                <?= $cidade . " - " . $cidade . " - " . $UF . " - " . $cep ?>
            </address>
            <?= $ddd ?> <strong><?= $fone ?></strong>
        </div>

        <div class="menu-footer">
            <nav>
                <ul>

                    <li><a rel="nofollow" href="<?= $url; ?>" title="PÃ¡gina inicial">Home</a></li>
                    <li><a href="<?= $url; ?>empresa" title="Sobre a Empresa <?= $nomeSite; ?>">Empresa</a></li>
                    <li><a href="<?= $url; ?>missao" title="MissÃ£o <?= $nomeSite; ?>">MissÃ£o</a></li>
                    <li><a href="<?= $url; ?>produtos-categoria" title="Produtos<?= $nomeSite; ?>">Produtos</a></li>
                    <li><a href="<?= $url; ?>trabalhe-conosco" title="Trabalhe na <?= $nomeSite; ?>">Trabalhe conosco</a>
                    </li>
                    <!-- <li><a href="<?= $url; ?>contato" title="PrÃ© OrÃ§amento">PrÃ© OrÃ§amento</a></li> -->
                    <li style="border: none; margin:0px;"><a href="<?= $url; ?>mapa-site">Mapa do site</a></li>
                    <li>
                        <a class="botao-anuncio" id="btn-solucs-rodape" target="blanck"
                            href="https://faca-parte.solucoesindustriais.com.br"
                            title="Gostaria de Anunciar?">Gostaria de Anunciar?</a>
                    </li>

                </ul>
            </nav>
        </div>

        <?php include('inc/canais.php'); ?>

        <br class="clear" />

        <div class="copyright-footer">
            Copyright Â© <?= $nomeSite ?>. (Lei 9610 de 19/02/1998)
            <div class="selos">

                <a rel="nofollow" href="http://validator.w3.org/check?uri=<?= $url . $urlPagina ?>" target="_blank"
                    title="Site Desenvolvido em HTML5 nos padrÃµes internacionais W3C"><img
                        src="<?= $url ?>imagens/selo-w3c-html5.png"
                        alt="Site Desenvolvido em HTML5 nos padrÃµes internacionais W3C" /></a>
                <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?= $url . $urlPagina ?>"
                    target="_blank" title="Site Desenvolvido nos padrÃµes internacionais W3C"><img
                        src="<?= $url ?>imagens/selo-w3c-css.png" alt="Site Desenvolvido nos padrÃµes W3C" /></a>
                <img class="lazyload" data-src="<?= $url ?>imagens/selo-w3c-doutores-web.png" alt="<?= $creditos ?>"
                    title="Site desenvolvido por www.doutoresdaweb.com.br" />
            </div>
        </div>
    </div>
</footer>
<!-- ConteÃºdo do footer acima -->
<!-- Includes Detalhes -->
<script defer src="https://apis.google.com/js/plusone.js"></script>
<script defer src="<?= $url ?>js/geral.js"></script>
<? include "inc/readmore.php" ?>
<!-- Plugin facebook -->

<!-- API botao-cotar -->
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>


<!-- Script Aside-Launcher start -->
<script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
<script>
    const aside = document.querySelector('aside');
    const data = '<div data-sdk-ideallaunch data-segment="SoluÃ§Ãµes Industriais - Oficial"></div>';
    aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
</script>

<?php
foreach ($tagsanalytic as $analytics) {
    echo '<script async src="https://www.googletagmanager.com/gtag/js?id=' . $analytics . '"></script>' . '<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}' . "gtag('js', new Date()); gtag('config', '$analytics')</script>";
}
?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 40);
</script>