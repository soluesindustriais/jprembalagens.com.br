<?
$nomeSite			= 'JPR Embalagens';
$slogan				= 'Embalagens plásticas flexíveis';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
	$url = $http . "://" . $host . "/";
} else {
	$url = $http . "://" . $host . $dir["dirname"] . "/";
}

// $ddd				= '(11)';
// $fone				= '5518-3881';
// $fone2				= '';
// $fone3				= '5524–8109';
// $fone4				= '2365-5799';
// $emailContato		= 'contato@embalagemideal.com.br';
$rua				= 'Av. Guido Caloi Nº 1985 Galpão 18';
$bairro				= 'Jd. São Luiz';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 05802-140';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idCliente			= '5';
$idAnalytics		= 'UA-45914525-11';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br



$urlPagina = explode("/", $_SERVER['PHP_SELF']);
$urlPagina	= str_replace('.php', '', $urlPagina[sizeof($urlPagina) - 1]);
$urlPagina == "index" ? $urlPagina = "" : "";

$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
$tagsanalytic = ["G-HYLYH75TXE","G-D23WW3S4NC"];

$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]); 
//Breadcrumbs
$caminho 			= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoCategoria	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhoProdutosEnvelopes	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'envelopes" itemprop="item" title="Envelopes">
                      <span itemprop="name">Envelopes ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutosSacos	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'sacos" itemprop="item" title="Sacos">
                      <span itemprop="name">Sacos ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoSacolas	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'sacolas" itemprop="item" title="Sacolas">
                      <span itemprop="name"> Sacolas ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoBobinas	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'bobinas" itemprop="item" title="Bobinas">
                      <span itemprop="name">Bobinas ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoCapas	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'capas" itemprop="item" title="capas">
                      <span itemprop="name">Capas ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoCobertura	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'cobertura" itemprop="item" title="Cobertura">
                      <span itemprop="name">Cobertura ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoPlastico	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'plastico" itemprop="item" title="Plastico">
                      <span itemprop="name">Plastico ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoRolo	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'rolo" itemprop="item" title="rolo">
                      <span itemprop="name">Rolo ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoProdutoFilme	= '
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home ❱  </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos ❱  </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'filme" itemprop="item" title="Filme">
                      <span itemprop="name">Filme ❱  </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';


//Pasta de imagens, Galeria, url Facebook, etc.
$pasta 				= 'imagens/produtos/';

//Redes sociais
$idFacebook			= '238779496142098'; //Link para achar o ID da página do Facebook http://graph.facebook.com/Nome da página do Facebook

$idGooglePlus		= 'https://plus.google.com/+JprembalagensBrhome'; // ID da página da empresa no Google Plus

$paginaFacebook		= 'FacebookDevelopers'; //Nome da página do Facebook ex: https://www.facebook.com/FacebookDevelopers

$author = '' . $idGooglePlus . ''; // Link do perfil da empresa no g+ ou deixar em branco



//Reescrita dos links dos telefones
//$link_tel = str_replace('(', '', $ddd);
//$link_tel = str_replace(')', '', $link_tel);
//$link_tel = str_replace('11', '', $link_tel);
//$link_tel .= '5511'.$fone;
//$link_tel = str_replace('-', '', $link_tel);


$creditos			= 'Doutores da Web - Marketing Digital';
$siteCreditos		= 'www.doutoresdaweb.com.br';
