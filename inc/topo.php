<header>
    <div class="wrapper">

        <div class="logo"><a href="<?=$url;?>" title="Voltar a página Home"><?=$slogan;?> - <?=$nomeSite;?></a></div> 

        <div class="slider-wrapper">
            <div id="slider" class="nivoSlider">
                <img class="lazyload" data-src="<?=$url;?>imagens/slider/embalagens-plasticas-flexiveis-01.jpg" alt="Embalagens plásticas flexíveis, Sacos, sacolas, envelopes, bobinas, filme" />
                <img class="lazyload" data-src="<?=$url;?>imagens/slider/embalagens-plasticas-flexiveis-02.jpg" alt="Embalagens plásticas flexíveis, Sacos, sacolas, envelopes, bobinas, filme" />
                <img class="lazyload" data-src="<?=$url;?>imagens/slider/embalagens-plasticas-flexiveis-03.jpg" alt="Embalagens plásticas flexíveis, Sacos, sacolas, envelopes, bobinas, filme" />
                <img class="lazyload" data-src="<?=$url;?>imagens/slider/embalagens-plasticas-flexiveis-04.jpg" alt="Embalagens plásticas flexíveis, Sacos, sacolas, envelopes, bobinas, filme" />
                <img class="lazyload" data-src="<?=$url;?>imagens/slider/embalagens-plasticas-flexiveis-05.jpg" alt="Embalagens plásticas flexíveis, Sacos, sacolas, envelopes, bobinas, filme" />
            </div>
        </div>                                              
    </div>
    <nav id="menu">
<div class="wrapper">
        <ul>
            
                <?php include('inc/menu-top.php');?>
            
        </ul>
</div>
    </nav>

</header>
