<h2>Regiões onde a <?=$nomeSite?> atende <?=$h1?></h2>
<h3>Principais regiões da Grande São Paulo:</h3><br />

<div id="servicosTabsDois">

    <ul class="nav">
        <li class="nav-two"><a class="current" title="Selecione">Selecione</a></li>
        <li class="nav-two"><a title="Centro">Região Central</a></li>
        <li class="nav-two"><a title="Zona Norte">Zona Norte</a></li>
        <li class="nav-two"><a title="Zona Oeste">Zona Oeste</a></li>
        <li class="nav-two"><a title="Zona Sul">Zona Sul</a></li>
        <li class="nav-two"><a title="Zona Leste">Zona Leste</a></li>
        <li class="nav-two"><a title="Grande São Paulo">Grande São Paulo</a></li>
    </ul>

    <div class="list-wrap">
    	
        <ul id="selecione">
			<li><strong>Selecione a região de SP</strong></li> 
        </ul>

        <ul id="id1-1" class="hide">
            <li><strong>Aclimação</strong></li>
            <li><strong>Bela Vista</strong></li>
            <li><strong>Bom Retiro</strong></li>
            <li><strong>Brás</strong></li>
            <li><strong>Cambuci</strong></li>
            <li><strong>Centro</strong></li>
            <li><strong>Consolação</strong></li>
            <li><strong>Higienópolis</strong></li>
            <li><strong>Glicério</strong></li>
            <li><strong>Liberdade</strong></li>
            <li><strong>Luz</strong></li>
            <li><strong>Pari</strong></li>
            <li><strong>República</strong></li>
            <li><strong>Santa Cecília</strong></li>
            <li><strong>Santa Efigênia</strong></li>
            <li><strong>Sé</strong></li>
            <li><strong>Vila Buarque</strong></li>
        </ul>
        
        
        <ul id="id2-2" class="hide">
            <li><strong>Brasilândia</strong></li>
            <li><strong>Cachoeirinha</strong></li>
            <li><strong>Casa Verde</strong></li>
            <li><strong>Imirim</strong></li>
            <li><strong>Jaçanã</strong></li>
            <li><strong>Jardim São Paulo</strong></li>
            <li><strong>Lauzane Paulista</strong></li>
            <li><strong>Mandaqui</strong></li>
            <li><strong>Santana</strong></li>
            <li><strong>Tremembé</strong></li>
            <li><strong>Tucuruvi</strong></li>
            <li><strong>Vila Guilherme</strong></li>
            <li><strong>Vila Gustavo</strong></li>
            <li><strong>Vila Maria</strong></li>
            <li><strong>Vila Medeiros</strong></li>
        </ul>
        
        <ul id="id2-3" class="hide">
            <li><strong>Água Branca</strong></li>
            <li><strong>Bairro do Limão</strong></li>
            <li><strong>Barra Funda</strong></li>
            <li><strong>Alto da Lapa</strong></li>
            <li><strong>Alto de Pinheiros</strong></li>
            <li><strong>Butantã</strong></li>
            <li><strong>Freguesia do Ó</strong></li>
            <li><strong>Jaguaré</strong></li>
            <li><strong>Jaraguá</strong></li>
            <li><strong>Jardim Bonfiglioli</strong></li>
            <li><strong>Lapa</strong></li>
            <li><strong>Pacaembú</strong></li>
            <li><strong>Perdizes</strong></li>
            <li><strong>Perús</strong></li>
            <li><strong>Pinheiros</strong></li>
            <li><strong>Pirituba</strong></li>
            <li><strong>Raposo Tavares</strong></li>
            <li><strong>Rio Pequeno</strong></li>
            <li><strong>São Domingos</strong></li>
            <li><strong>Sumaré</strong></li>
            <li><strong>Vila Leopoldina</strong></li>
            <li><strong>Vila Sonia</strong></li>
        </ul>
        
        <ul id="id2-4" class="hide">
            <li><strong>Aeroporto</strong></li>
            <li><strong>Água Funda</strong></li>
            <li><strong>Brooklin</strong></li>
            <li><strong>Campo Belo</strong></li>
            <li><strong>Campo Grande</strong></li>
            <li><strong>Campo Limpo</strong></li>
            <li><strong>Capão Redondo</strong></li>
            <li><strong>Cidade Ademar</strong></li>
            <li><strong>Cidade Dutra</strong></li>
            <li><strong>Cidade Jardim</strong></li>
            <li><strong>Grajaú</strong></li>
            <li><strong>Ibirapuera</strong></li>
            <li><strong>Interlagos</strong></li>
            <li><strong>Ipiranga</strong></li>
            <li><strong>Itaim Bibi</strong></li>
            <li><strong>Jabaquara</strong></li>
            <li><strong>Jardim Ângela</strong></li>
            <li><strong>Jardim América</strong></li>
            <li><strong>Jardim Europa</strong></li>
            <li><strong>Jardim Paulista</strong></li>
            <li><strong>Jardim Paulistano</strong></li>
            <li><strong>Jardim São Luiz</strong></li>
            <li><strong>Jardins</strong></li>
            <li><strong>Jockey Club</strong></li>
            <li><strong>M'Boi Mirim</strong></li>
            <li><strong>Moema</strong></li>
            <li><strong>Morumbi</strong></li>
            <li><strong>Parelheiros</strong></li>
            <li><strong>Pedreira</strong></li>
            <li><strong>Sacomã</strong></li>
            <li><strong>Santo Amaro</strong></li>
            <li><strong>Saúde</strong></li>
            <li><strong>Socorro</strong></li>
            <li><strong>Vila Andrade</strong></li>
            <li><strong>Vila Mariana</strong></li>
        </ul>
        
        <ul id="id2-5" class="hide">
            <li><strong>Água Rasa</strong></li>
            <li><strong>Anália Franco</strong></li>
            <li><strong>Aricanduva</strong></li>
            <li><strong>Artur Alvim</strong></li>
            <li><strong>Belém</strong></li>
            <li><strong>Cidade Patriarca</strong></li>
            <li><strong>Cidade Tiradentes</strong></li>
            <li><strong>Engenheiro Goulart</strong></li>
            <li><strong>Ermelino Matarazzo</strong></li>
            <li><strong>Guianazes</strong></li>
            <li><strong>Itaim Paulista</strong></li>
            <li><strong>Itaquera</strong></li>
            <li><strong>Jardim Iguatemi</strong></li>
            <li><strong>José Bonifácio</strong></li>
            <li><strong>Moóca</strong></li>
            <li><strong>Parque do Carmo</strong></li>
            <li><strong>Parque São Lucas</strong></li>
            <li><strong>Parque São Rafael</strong></li>
            <li><strong>Penha</strong></li>
            <li><strong>Ponte Rasa</strong></li>
            <li><strong>São Mateus</strong></li>
            <li><strong>São Miguel Paulista</strong></li>
            <li><strong>Sapopemba</strong></li>
            <li><strong>Tatuapé</strong></li>
            <li><strong>Vila Carrão</strong></li>
            <li><strong>Vila Curuçá</strong></li>
            <li><strong>Vila Esperança</strong></li>
            <li><strong>Vila Formosa</strong></li>
            <li><strong>Vila Matilde</strong></li>
            <li><strong>Vila Prudente</strong></li>
        </ul>
        
        <ul id="id2-6" class="hide">
            <li><strong>São Caetano do sul</strong></li>
            <li><strong>São Bernardo do Campo</strong></li>
            <li><strong>Santo André</strong></li>
            <li><strong>Diadema</strong></li>
            <li><strong>Guarulhos</strong></li>
            <li><strong>Suzano</strong></li>
            <li><strong>Ribeirão Pires</strong></li>
            <li><strong>Mauá</strong></li>
            <li><strong>Embu</strong></li>
            <li><strong>Embu Guaçú</strong></li>
            <li><strong>Embu das Artes</strong></li>
            <li><strong>Itapecerica da Serra</strong></li>
            <li><strong>Osasco</strong></li>
            <li><strong>Barueri</strong></li>
            <li><strong>Jandira</strong></li>
            <li><strong>Cotia</strong></li>
            <li><strong>Itapevi</strong></li>
            <li><strong>Santana de Parnaíba</strong></li>
            <li><strong>Caierias</strong></li>
            <li><strong>Franco da Rocha</strong></li>
            <li><strong>Taboão da Serra</strong></li>
            <li><strong>Cajamar</strong></li>
            <li><strong>Arujá</strong></li>
            <li><strong>Alphaville</strong></li>
            <li><strong>Mairiporã</strong></li>
            <li><strong>ABC</strong></li>
            <li><strong>ABCD</strong></li>
        </ul>
    </div>
</div>

<h3>Principais cidades e regiões do Brasil:</h3><br />
<div id="servicosTabs">
    <div>
        <ul class="nav">
        
        	<li class="nav-two"><a class="current" title="Selecione">Selecione</a></li>
            
            <li class="nav-two"><a title="RJ - Rio de Janeiro">RJ - Rio de Janeiro</a></li>
            <li class="nav-two"><a title="MG - Minas Gerais">MG - Minas Gerais</a></li>
            <li class="nav-two"><a title="ES - Espírito Santo">ES - Espírito Santo</a></li>
            <li class="nav-two"><a title="SP - Litoral e Interior">SP -  Litoral e Interior</a></li>
            
            <li class="nav-two"><a title="PR - Paraná">PR - Paraná</a></li>
            <li class="nav-two"><a title="SC - Santa Catarina">SC - Santa Catarina</a></li>
            <li class="nav-two"><a title="RS - Rio Grande do Sul">RS - Rio Grande do Sul</a></li>
            
            <li class="nav-two"><a title="PE - Pernambuco">PE - Pernambuco</a></li>
            <li class="nav-two"><a title="BA - Bahia">BA - Bahia</a></li>
            <li class="nav-two"><a title="CE - Ceará">CE - Ceará</a></li>
            
            <li class="nav-two"><a title="GO e DF - Goiás e Distrito Federal">Goiás e Distrito Federal</a></li>
            
            <li class="nav-two"><a title="AM - Amazonas">AM - Amazonas</a></li>
            <li class="nav-two"><a title="PA - Pará">PA - Pará</a></li>

        </ul>
    </div>
    <div class="list-wrap">
    
    	<ul id="selecione2">
                        <li><strong>Selecione a região do Brasil</strong></li>
        </ul>

        <ul id="rj" class="hide">
            <li><strong>Rio de Janeiro </strong></li>
            <li><strong>São Gonçalo</strong></li>
            <li><strong>Duque de Caxias</strong></li>
            <li><strong>Nova Iguaçu</strong></li>
            <li><strong>Niterói</strong></li>
            <li><strong>Belford Roxo</strong></li>
            <li><strong>São João de Meriti</strong></li>
            <li><strong>Campos dos Goytacazes</strong></li>
            <li><strong>Petrópolis</strong></li>
            <li><strong>Volta Redonda</strong></li>
            <li><strong>Magé</strong></li>
            <li><strong>Itaboraí</strong></li>
            <li><strong>Mesquita</strong></li>
            <li><strong>Nova Friburgo</strong></li>
            <li><strong>Barra Mansa</strong></li>
            <li><strong>Macaé</strong></li>
            <li><strong>Cabo Frio</strong></li>
            <li><strong>Nilópolis</strong></li>
            <li><strong>Teresópolis</strong></li>
            <li><strong>Resende</strong></li>
        </ul>
        
        <ul id="mg" class="hide">
            <li><strong>Belo Horizonte</strong></li>
            <li><strong>Uberlândia</strong></li>
            <li><strong>Contagem</strong></li>
            <li><strong>Juiz de Fora</strong></li>
            <li><strong>Betim</strong></li>
            <li><strong>Montes Claros</strong></li>
            <li><strong>Ribeirão das Neves</strong></li>
            <li><strong>Uberaba</strong></li>
            <li><strong>Governador Valadares</strong></li>
            <li><strong>Ipatinga</strong></li>
            <li><strong>Santa Luzia</strong></li>
            <li><strong>Sete Lagoas</strong></li>
            <li><strong>Divinópolis</strong></li>
            <li><strong>Ibirité</strong></li>
            <li><strong>Poços de Caldas</strong></li>
            <li><strong>Patos de Minas</strong></li>
            <li><strong>Teófilo Otoni</strong></li>
            <li><strong>Sabará</strong></li>
            <li><strong>Pouso Alegre</strong></li>
            <li><strong>Barbacena</strong></li>
            <li><strong>Varginha</strong></li>
            <li><strong>Conselheiro Lafeiete</strong></li>
            <li><strong>Araguari</strong></li>
            <li><strong>Itabira</strong></li>
            <li><strong>Passos</strong></li>
        </ul>
        
        <ul id="es" class="hide">
                        <li><strong>Serra</strong></li>
            <li><strong>Vila Velha</strong></li>
            <li><strong>Cariacica</strong></li>
            <li><strong>Vitória</strong></li>
            <li><strong>Cachoeiro de Itapemirim</strong></li>
            <li><strong>Linhares</strong></li>
            <li><strong>São Mateus</strong></li>
            <li><strong>Colatina</strong></li>
            <li><strong>Guarapari</strong></li>
            <li><strong>Aracruz</strong></li>
            <li><strong>Viana</strong></li>
            <li><strong>Nova Venécia</strong></li>
            <li><strong>Barra de São Francisco</strong></li>
            <li><strong>Santa Maria de Jetibá</strong></li>
            <li><strong>Castelo</strong></li>
            <li><strong>Marataízes</strong></li>
            <li><strong>São Gabriel da Palha</strong></li>
            <li><strong>Domingos Martins</strong></li>
            <li><strong>Itapemirim</strong></li>
            <li><strong>Afonso Cláudio</strong></li>
            <li><strong>Alegre</strong></li>
            <li><strong>Baixo Guandu</strong></li>
            <li><strong>Conceição da Barra</strong></li>
            <li><strong>Guaçuí</strong></li>
            <li><strong>Iúna</strong></li>
            <li><strong>Jaguaré</strong></li>
            <li><strong>Mimoso do Sul</strong></li>
            <li><strong>Sooretama</strong></li>
            <li><strong>Anchieta</strong></li>
            <li><strong>Pinheiros</strong></li>
            <li><strong>Pedro Canário</strong></li>
        </ul>
        
        <ul id="sp" class="hide">
            <li><strong>Bertioga</strong></li>
            <li><strong>Caraguatatuba</strong></li>
            <li><strong>Cubatão</strong></li>
            <li><strong>Guarujá</strong></li>
            <li><strong>Ilhabela</strong></li>
            <li><strong>Itanhaém</strong></li>
            <li><strong>Mongaguá</strong></li>
            <li><strong>Riviera de São Lourenço</strong></li>
            <li><strong>Santos</strong></li>
            <li><strong>São Vicente</strong></li>
            <li><strong>Praia Grande</strong></li>
            <li><strong>Ubatuba</strong></li>
            <li><strong>São Sebastião</strong></li>
            <li><strong>Peruíbe</strong></li>
            <li><strong>São José dos campos</strong></li>
            <li><strong>Campinas</strong></li>
            <li><strong>Jundiaí</strong></li>
            <li><strong>Sorocaba</strong></li>
            <li><strong>Indaiatuba </strong></li>
            <li><strong>São José do Rio Preto </strong></li>
            <li><strong>Itatiba </strong></li>
            <li><strong>Amparo </strong></li>
            <li><strong>Barueri </strong></li>
            <li><strong>Ribeirão Preto</strong></li>
            <li><strong>Marília </strong></li>
            <li><strong>Louveira </strong></li>
            <li><strong>Paulínia </strong></li>
            <li><strong>Bauru </strong></li>
            <li><strong>Valinhos </strong></li>
            <li><strong>Bragança Paulista </strong></li>
            <li><strong>Araraquara</strong></li>
            <li><strong>Americana</strong></li>
            <li><strong>Atibaia </strong></li>
            <li><strong>Taubaté </strong></li>
            <li><strong>Araras </strong></li>
            <li><strong>São Carlos </strong></li>
            <li><strong>Itupeva </strong></li>
            <li><strong>Mendonça </strong></li>
            <li><strong>Itu </strong></li>
            <li><strong>Vinhedo </strong></li>
            <li><strong>Marapoama </strong></li>
            <li><strong>Votuporanga </strong></li>
            <li><strong>Hortolândia </strong></li>
            <li><strong>Araçatuba </strong></li>
            <li><strong>Jaboticabal </strong></li>
            <li><strong>Sertãozinho</strong></li>
        </ul>
        
        
        <ul id="pr" class="hide">
            <li><strong>Curitiba</strong></li>
            <li><strong>Londrina</strong></li>
            <li><strong>Maringá</strong></li>
            <li><strong>Ponta Grossa</strong></li>
            <li><strong>Cascavel</strong></li>
            <li><strong>São José dos Pinhais</strong></li>
            <li><strong>Foz do Iguaçu</strong></li>
            <li><strong>Colombo</strong></li>
            <li><strong>Guarapuava</strong></li>
            <li><strong>Paranaguá</strong></li>
            <li><strong>Araucária</strong></li>
            <li><strong>Toledo</strong></li>
            <li><strong>Apucarana</strong></li>
            <li><strong>Pinhais</strong></li>
            <li><strong>Campo Largo</strong></li>
            <li><strong>Almirante Tamandaré</strong></li>
            <li><strong>Umuarama</strong></li>
            <li><strong>Paranavaí</strong></li>
            <li><strong>Piraquara</strong></li>
            <li><strong>Cambé</strong></li>
            <li><strong>Sarandi</strong></li>
            <li><strong>Fazenda Rio Grande</strong></li>
            <li><strong>Paranavaí</strong></li>
            <li><strong>Francisco Beltrão</strong></li>
            <li><strong>Pato Branco</strong></li>
            <li><strong>Cianorte</strong></li>
            <li><strong>Telêmaco Borba</strong></li>
            <li><strong>Castro</strong></li>
            <li><strong>Rolândia</strong></li>

        </ul>
        
        <ul id="sc" class="hide">
            <li><strong>Joinville</strong></li>
            <li><strong>Florianópolis</strong></li>
            <li><strong>Blumenau</strong></li>
            <li><strong>Itajaí</strong></li>
            <li><strong>São José</strong></li>
            <li><strong>Chapecó</strong></li>
            <li><strong>Criciúma</strong></li>
            <li><strong>Jaraguá do sul</strong></li>
            <li><strong>Lages</strong></li>
            <li><strong>Palhoça</strong></li>
            <li><strong>Balneário Camboriú</strong></li>
            <li><strong>Brusque</strong></li>
            <li><strong>Tubarão</strong></li>
            <li><strong>São Bento do Sul</strong></li>
            <li><strong>Caçador</strong></li>
            <li><strong>Concórdia</strong></li>
            <li><strong>Camboriú</strong></li>
            <li><strong>Navegantes</strong></li>
            <li><strong>Rio do Sul</strong></li>
            <li><strong>Araranguá</strong></li>
            <li><strong>Gaspar</strong></li>
            <li><strong>Biguaçu</strong></li>
            <li><strong>Indaial</strong></li>
            <li><strong>Mafra</strong></li>
            <li><strong>Canoinhas</strong></li>
            <li><strong>Itapema</strong></li>

        </ul>
        
        <ul id="rs" class="hide">
            <li><strong>Porto Alegre</strong></li>
            <li><strong>Caxias do Sul</strong></li>
            <li><strong>Pelotas</strong></li>
            <li><strong>Canoas</strong></li>
            <li><strong>Santa Maria</strong></li>
            <li><strong>Gravataí</strong></li>
            <li><strong>Viamão</strong></li>
            <li><strong>Novo Hamburgo</strong></li>
            <li><strong>São Leopoldo</strong></li>
            <li><strong>Rio Grande</strong></li>
            <li><strong>Alvorada</strong></li>
            <li><strong>Passo Fundo</strong></li>
            <li><strong>Sapucaia do Sul</strong></li>
            <li><strong>Uruguaiana</strong></li>
            <li><strong>Santa Cruz do Sul</strong></li>
            <li><strong>Cachoeirinha</strong></li>
            <li><strong>Bagé</strong></li>
            <li><strong>Bento Gonçalves</strong></li>
            <li><strong>Erechim</strong></li>
            <li><strong>Guaíba</strong></li>
            <li><strong>Cachoeira do Sul</strong></li>
            <li><strong>Santana do Livramento</strong></li>
            <li><strong>Esteio</strong></li>
            <li><strong>Ijuí</strong></li>
            <li><strong>Alegrete</strong></li>
        </ul>
        
        
        <ul id="pe" class="hide">
            <li><strong>Recife</strong></li>
            <li><strong>Jaboatão dos Guararapes</strong></li>
            <li><strong>Olinda</strong></li>
            <li><strong>Bandeira caruaru.jpg Caruaru</strong></li>
            <li><strong>Petrolina</strong></li>
            <li><strong>Paulista</strong></li>
            <li><strong>Cabo de Santo Agostinho</strong></li>
            <li><strong>Camaragibe</strong></li>
            <li><strong>Garanhuns</strong></li>
            <li><strong>Vitória de Santo Antão</strong></li>
            <li><strong>Igarassu</strong></li>
            <li><strong>São Lourenço da Mata</strong></li>
            <li><strong>Abreu e Lima</strong></li>
            <li><strong>Santa Cruz do Capibaribe</strong></li>
            <li><strong>Ipojuca</strong></li>
            <li><strong>Serra Talhada</strong></li>
            <li><strong>Araripina</strong></li>
            <li><strong>Gravatá</strong></li>
            <li><strong>Carpina</strong></li>
            <li><strong>Goiana</strong></li>
            <li><strong>Belo Jardim</strong></li>
            <li><strong>Arcoverde</strong></li>
            <li><strong>Ouricuri</strong></li>
            <li><strong>Escada</strong></li>
            <li><strong>Pesqueira</strong></li>
            <li><strong>Surubim</strong></li>
            <li><strong>Palmares</strong></li>
            <li><strong>Bezerros</strong></li>

        </ul>
        
        <ul id="ba" class="hide">
            <li><strong>Salvador</strong></li>
            <li><strong>Feira de Santana</strong></li>
            <li><strong>Vitória da Conquista</strong></li>
            <li><strong>Camaçari</strong></li>
            <li><strong>Itabuna</strong></li>
            <li><strong>Juazeiro</strong></li>
            <li><strong>Lauro de Freitas</strong></li>
            <li><strong>Ilhéus</strong></li>
            <li><strong>Jequié</strong></li>
            <li><strong>Teixeira de Freitas</strong></li>
            <li><strong>Alagoinhas</strong></li>
            <li><strong>Barreiras</strong></li>
            <li><strong>Porto Seguro</strong></li>
            <li><strong>Simões Filho</strong></li>
            <li><strong>Paulo Afonso</strong></li>
            <li><strong>Eunápolis</strong></li>
            <li><strong>Santo Antônio de Jesus</strong></li>
            <li><strong>Valença</strong></li>
            <li><strong>Candeias</strong></li>
            <li><strong>Guanambi</strong></li>
            <li><strong>Jacobina</strong></li>
            <li><strong>Serrinha</strong></li>
            <li><strong>Senhor do Bonfim</strong></li>
            <li><strong>Dias d'Ávila</strong></li>
            <li><strong>Luís Eduardo Magalhães</strong></li>
            <li><strong>Itapetinga</strong></li>
            <li><strong>Irecê</strong></li>
            <li><strong>Campo Formoso</strong></li>
            <li><strong>Casa Nova</strong></li>
            <li><strong>Brumado</strong></li>
            <li><strong>Bom Jesus da Lapa</strong></li>
            <li><strong>Conceição do Coité</strong></li>
            <li><strong>Itamaraju</strong></li>
            <li><strong>Itaberaba</strong></li>
            <li><strong>Cruz das Almas</strong></li>
            <li><strong>Ipirá</strong></li>
            <li><strong>Santo Amaro</strong></li>
            <li><strong>Euclides da Cunha</strong></li>
        </ul>
        
        <ul id="ce" class="hide">
			<li><strong>Fortaleza</strong></li>
            <li><strong>caucacia</strong></li>
            <li><strong>Juazeiro do Norte</strong></li>
            <li><strong>Maracanaú</strong></li>
            <li><strong>Sobral</strong></li>
            <li><strong>Crato</strong></li>
            <li><strong>Itapipoca</strong></li>
            <li><strong>Maranguape</strong></li>
            <li><strong>Iguatu</strong></li>
            <li><strong>Quixadá</strong></li>
            <li><strong>Canindé</strong></li>
            <li><strong>Pacajus</strong></li>
            <li><strong>Crateús</strong></li>
            <li><strong>Aquiraz</strong></li>
            <li><strong>Pacatuba</strong></li>
            <li><strong>Quixeramobim</strong></li>
        </ul>
        
        <ul id="ma" class="hide">
			<li><strong>São Luís</strong></li>
            <li><strong>Imperatriz</strong></li>
            <li><strong>São José de Ribamar</strong></li>
            <li><strong>Timon</strong></li>
            <li><strong>Caxias</strong></li>
            <li><strong>Codó</strong></li>
            <li><strong>Paço do Lumiar</strong></li>
            <li><strong>Açailândia</strong></li>
            <li><strong>Bacabal</strong></li>
            <li><strong>Balsas</strong></li>
            <li><strong>Barra do Corda</strong></li>
        </ul>
        
         <ul id="pi" class="hide">
			<li><strong>Teresina</strong></li>
            <li><strong>São Raimundo Nonato</strong></li>
            <li><strong>Parnaíba</strong></li>
            <li><strong>Picos</strong></li>
            <li><strong>Uruçuí</strong></li>
            <li><strong>Floriano</strong></li>
            <li><strong>Piripiri</strong></li>
            <li><strong>Campo Maior</strong></li>
        </ul>
                
        <ul id="go" class="hide">
			<li><strong>Goiânia</strong></li>
            <li><strong>Aparecida de Goiânia</strong></li>
            <li><strong>Anápolis</strong></li>
            <li><strong>Rio Verde</strong></li>
            <li><strong>Luziânia</strong></li>
            <li><strong>Águas Lindas de Goiás</strong></li>
            <li><strong>Valparaíso de Goiás</strong></li>
            <li><strong>Trindade</strong></li>
            <li><strong>Formosa</strong></li>
            <li><strong>Novo Gama</strong></li>
            <li><strong>Itumbiara</strong></li>
            <li><strong>Senador Canedo</strong></li>
            <li><strong>Catalão</strong></li>
            <li><strong>Jataí</strong></li>
            <li><strong>Planaltina</strong></li>
            <li><strong>Caldas Novas</strong></li>
        </ul>
        
        <ul id="ms" class="hide">
			<li><strong>Campo Grande</strong></li>
            <li><strong>Dourados</strong></li>
            <li><strong>Três Lagoas</strong></li>
            <li><strong>Corumbá</strong></li>
            <li><strong>Ponta Porã</strong></li>
        </ul>
        
        <ul id="mt" class="hide">
			<li><strong>Cuiabá</strong></li>
            <li><strong>Várzea Grande</strong></li>
            <li><strong>Rondonópolis</strong></li>
            <li><strong>Sinop</strong></li>
            <li><strong>Tangará da Serra</strong></li>
            <li><strong>Cáceres</strong></li>
            <li><strong>Sorriso</strong></li>
        </ul>
        
        <ul id="am" class="hide">
			<li><strong>Manaus</strong></li>
            <li><strong>Parintins</strong></li>
            <li><strong>Itacoatiara</strong></li>
            <li><strong>Manacapuru</strong></li>
            <li><strong>Coari</strong></li>
            <li><strong>Centro Amazonense</strong></li>

        </ul>
        
        <ul id="pa" class="hide">
			<li><strong>Belém</strong></li>
            <li><strong>Ananindeua</strong></li>
            <li><strong>Santarém</strong></li>
            <li><strong>Marabá</strong></li>
            <li><strong>Castanhal</strong></li>
            <li><strong>Parauapebas</strong></li>
            <li><strong>Itaituba</strong></li>
            <li><strong>Cametá</strong></li>
            <li><strong>Bragança </strong></li>
            <li><strong>Abaetetuba</strong></li>
            <li><strong>Bragança</strong></li>
            <li><strong>Marituba</strong></li>
        </ul>

    </div>
</div>