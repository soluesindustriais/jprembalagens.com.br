<aside>

    <div class="banner-contato">
        <div style="display:flex;flex-wrap:wrap;flex-direction:column;">

            <button title="<?php echo $h1 ?>" class="btn-orcar botao-cotar"><i class="fa fa-envelope"></i>
                Solicite um Orçamento
            </button>

        </div>
    </div>
    <div class="sidebar">
        <h3><a href="<?= $url ?>produtos" title="Produtos <?= $nomeSite ?>"><span>Produtos</span></a></h3>
        <ul class="cssmenu">
            <?php include('inc/sub-menu.php'); ?>
        </ul>
    </div>

</aside>