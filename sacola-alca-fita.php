<?php
$h1    			= 'Sacola Alça Fita';
$title 			= 'Sacola Alça Fita';
$desc  			= 'A sacola alça fita pode ser fabricada com uma porcentagem de matéria-prima reciclada. Neste modelo, a sacola pode ser lisa ou impressa em até 6 cores.';
$key   			= 'Sacola, Alça, Fita, Sacolas Alça Fita, Sacola Alça Fita colorida, Sacola Alça Fita personalizada';
$var 			= 'Sacolas Alça Fita';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <h2>Fabricamos sacola alça fita em diversos tamanhos e cores. </h2>
             
             
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacolas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 

            <p>São próprias para divulgação em supermercados, feiras, shoppings, eventos, e para outros estabelecimentos que procuram, além de oferecer um melhor serviço, manter sua imagem sempre próxima a seus clientes.</p>
            <p>São fabricadas com a matéria-prima de polietileno de baixa densidade e polietileno de alta densidade.</p>
            <p>A <strong>sacola alça fita</strong> pode ser fabricada com uma porcentagem de matéria-prima reciclada. Neste modelo, a <strong>sacola</strong> pode ser lisa ou impressa em até 6 cores, além de ser produzida na cor transparente ou pigmentada em diversas cores.</p>
            
            <p>Esta é uma opção ecologicamente correta para se contribuir com o meio ambiente.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacolas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 

            <p>Também fabricamos a <strong>sacola alça fita com aditivo oxi-biodegradavel</strong>. Nesta opção, a <strong>sacola alça fita</strong> em contato com a natureza, se degrada em curto espaço de tempo, sem deixar resíduos nocivos ao meio ambiente.  A natureza agradece.</p>
            <p>Para obter uma embalagem moderna e inovadora, utilize a <strong>sacola com alça fita colorida</strong>, e de um melhor acabamento ao seu produto. Neste modelo a alça fita poder ser na cor da própria <strong>sacola</strong>, ou diferente, chamando mais atenção. Além da <strong>sacola alça fita</strong>, a JPR Embalagens trabalha com outros modelos de <strong>sacola</strong>, como <a href="<?=$url;?>sacola-boca-palhaco" title="Sacola Boca de Palhaço"><strong>sacola boca de palhaço</strong></a>, <a href="<?=$url;?>sacola-camiseta" title="Sacola Camiseta"><strong>sacola camiseta</strong></a>, <a href="<?=$url;?>sacola-alca-vazada" title="Sacola Alça Vazada"><strong>sacola vazada</strong></a>, entre outros modelos.</p>
            <p>Nossa quantidade mínima de produção de <strong>sacola alça fita impressa</strong> são de 250kg e liso 150 kg.</p>
            
            <div class="picture-legend picture-center">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacolas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 
            <p>Para receber um orçamento de <strong>sacola alça fita</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>