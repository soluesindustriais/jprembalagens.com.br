<?
$h1    		= 'Missão / Visão / valores';
$title 		= 'Missão';
$desc  		= 'Veja a Missão, Visão e os valores da JPR Embalagens.';
$key   		= 'Missão, Visão, valores, JPR Embalagens.';
$var 		= 'Missão';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

</head>
<body>

    <div class="wrapper-topo">
     
      <? include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
              
             
                <?=$caminho?>	
                
                <h1><?=$h1?></h1>   
                
                <img class="lazyload" data-src="<?=$url;?>imagens/missao-jpr-embalagens.jpg" alt="<?=$h1?>" title="<?=$var?>" class="picture-center" />
                
                <br class="clear" />  
                
                <article>
                
            </article>
            
            
            <? include('inc/coluna-lateral.php');?>
            
        </section>

    </main>

    
    
</div><!-- .wrapper -->



<? include('inc/footer.php');?>


</body>
</html>