<?php
$h1    			= 'Saco zip';
$title 			= 'Saco zip';
$desc  			= 'Produzimos saco zip em diversos tamanhos e cores. Além de poder ser liso ou impresso em até 6 cores, e também pode ser transparente ou pigmentado em diversas cores.';
$key   			= 'Sacos zip, Saco, sacos, zip';
$var 			= 'Sacos zip';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  
             
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Produzimos <strong>saco zip</strong> em diversos tamanhos e cores. Além de poder ser liso ou impresso em até 6 cores, e também pode ser transparente ou pigmentado em diversas cores.</p>   
            
            <p>É uma embalagem que possui um sistema de fechamento moderno, devido à praticidade que este fecho possui. Somente o zip continua protegendo e garantido a integridade do produto após sua abertura, pois basta fechar novamente e embalagem e ficará complemente lacrada.</p>
            <p>É fabricado sob medida, de acordo com a necessidade de cada cliente. Assim, crie e desenvolva o <strong>saco zip</strong> da maneira que desejar, possuímos equipamentos de ultima geração, e assim garantimos a qualidade que o seu produto merece.</p>
            <p>Essa embalagem é usada em diversos segmentos como indústrias alimentícias, confecção, laboratórios, gráficas, entre muitos outros.</p>
            <p>Uma embalagem inovadora, com proteção e segurança, faz parte dos diferencias de uma empresa, pois ela transmite a sua preocupação em inovar, oferecer sempre o melhor. Por isso, utilize <a href="<?=$url;?>saco-zip-personalizado" title="Saco Zip Personalizado"><strong>saco zip personalizado</strong></a> divulgando a sua marca e ganhando novos mercados. E esta é uma especialidade nossa.</p>
            
            <h2>O saco zip sustentável</h2>
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Uma alternativa ecológica que o <strong>saco zip</strong> pode ser fabricado é com a adição de aditivo oxibiodegradavel durante o seu processo de fabricação. Nesta opção, a embalagem em contato com o meio ambiente pode ser degradar em período de até 6 meses, sem deixar resíduos nocivos ao meio ambiente.</p>
            <p>A JPR Embalagens trabalha com diversos modelos de embalagem, além do <strong>saco zip</strong>, fabricamos também <a href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip"><strong>sacolas plásticas zip</strong></a>, <strong>envelopes com fecho zip</strong>, entre outros modelos. Nossa quantidade mínima de produção são de 150kg para <strong>sacos zip liso</strong> e 250kg impresso.</p>
            <p>Para receber um orçamento dessa embalagem, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>