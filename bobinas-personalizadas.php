<?php
	$h1    			= 'Bobinas personalizadas';
	$title 			= 'Bobinas personalizadas';
	$desc  			= 'As bobinas personalizadas podem ser amplamente utilizadas em diversos segmentos. Saiba mais sobre as vantagens desta embalagem';
	$key   			= 'bobinas, personalizadas, bobina personalizada';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina personalizada';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>As <strong>bobinas personalizadas</strong> podem ser amplamente utilizadas em diversos segmentos. Saiba mais sobre as vantagens desta embalagem.</p>
            
            <p>Uma embalagem de qualidade é um ponto fundamental para conservar os seus produtos e melhorar a imagem da sua marca no mercado e frente a seus consumidores. Por isso, uma opção versátil e com ótimo aspecto visual são as <strong>bobinas personalizadas</strong>.</p>
            
            <p>As <strong>bobinas personalizadas</strong> podem ser fabricadas tanto em polipropileno quanto em polietileno. No caso de fabricação em polipropileno, as bobinas são bastante utilizadas em indústrias da área de alimentos, em gráficas e confecções, por apresentar uma embalagem que se destaca pelo brilho e pela transparência.</p>

            <p>As <strong>bobinas personalizadas</strong> em polietileno podem ser usadas para embalar qualquer tipo de produto, por ser confeccionada com plástico atóxico e resistente, o que não causa nenhum tipo de alteração às características originais do que está sendo embalado. Por isso, é largamente empregada em alimentos, bebidas, leites e farináceos, além de também ser utilizada na área de metalurgia e automotrizes.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Uma opção para contribuir com o meio ambiente são as <strong>bobinas personalizadas</strong> feitas com materiais reciclados, como o reciclado canela e cristal. Neste caso, a embalagem mantém a qualidade elevada, continua com a transparência e representa redução de custos para a sua empresa.</p>
            
            <h2>Conheça as bobinas personalizadas da JPR Embalagens</h2>
            
            <p>A JPR Embalagens é uma empresa que está há mais de 15 anos atuando no mercado de embalagens flexíveis, oferecendo variada gama de produtos para seus clientes, inclusive as <strong>bobinas personalizadas</strong>.</p>
            
            <p>As bobinas são fabricadas de acordo com as necessidades e demandas de cada cliente, com impressão em até nove cores, em PEAD, PEBD, PEBDL e PP, com excelente qualidade de impressão e em várias medidas.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A equipe da JPR Embalagens disponibiliza um atendimento personalizado, que visa levar as melhores soluções para cada cliente, sempre com preços em conta e ótimas condições de pagamento. Os profissionais estão sempre atentos às novidades do mercado para garantir <strong>bobinas personalizadas</strong> com qualidade cada vez maior e com excelente relação custo-benefício.</p>
            
            <p>Saiba mais sobre as vantagens das <strong>bobinas personalizadas</strong> e conheça os benefícios da JPR Embalagens entrando em contato com um dos consultores. Solicite já o seu orçamento, informando medidas e quantidade que a sua empresa necessita.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>