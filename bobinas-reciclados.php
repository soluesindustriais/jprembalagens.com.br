<?php
	$h1    			= 'Bobinas reciclados';
	$title 			= 'Bobinas reciclados';
	$desc  			= 'As bobinas reciclados contribui para melhorar a imagem da sua empresa frente aos consumidores, por indicar a preocupação da sua marca com as questões de sustentabilidade';
	$key   			= 'bobinas, reciclados, bobina reciclado';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina reciclado';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>   
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
           	<p>As <strong>bobinas reciclados</strong> oferecem proteção para o seu produto e ainda colabora com as causas ambientais. Saiba mais.</p>

            <p>Investir na imagem da empresa é essencial para ampliar vendas, fidelizar clientes e aumentar a sua presença no mercado. Por isso, uma opção pode ser adquirir uma embalagem de qualidade, como é o caso das <strong>bobinas reciclados</strong>.</p>
            
            <p>As <strong>bobinas reciclados</strong> contribui para melhorar a imagem da sua empresa frente aos consumidores, por indicar a preocupação da sua marca com as questões de sustentabilidade. Além disso, ela é uma opção mais econômica, mas sem perder a qualidade da embalagem quando se compara a bobinas produzidas com matéria-prima virgem.</p>
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>As <strong>bobinas reciclados</strong> podem ser feitas em cristal. Neste caso, a fabricação acontece a partir de material virgem e embalagens recicladas. Neste caso, o processo de reciclagem faz com que a embalagem tenha aspecto amarelo claro, mas ainda com transparência.</p>
            
            <p>Outro tipo de fabricação das <strong>bobinas reciclados</strong> é o canela, feita com plásticos reciclados. Neste caso, a embalagem tem aspecto marrom claro, também com alguns pontos. Esta embalagem ainda mantém a transparência, além de continuar resistente, com resistência a rasgos e rupturas.</p>
            
            <p>As <strong>bobinas reciclados</strong> ainda pode ser feitas com reciclado colorido, que é fabricado a partir de mistura de embalagens recicladas, ficando sem padrão de cor e de transparência. Neste caso, a embalagem fica sem padrão de cor e de transparência, mas esta é uma das embalagens mais em conta disponíveis do mercado.</p>
            
            <h2>Bobinas reciclados com preço em conta</h2>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir as <strong>bobinas reciclados</strong>, conte com as vantagens da JPR Embalagens. A empresa atua no mercado há mais de 15 anos, levando até os clientes as melhores opções na área de embalagens flexíveis.</p>
            
            <p>A equipe da JPR Embalagens está sempre atualizada, de modo a identificar oportunidades de melhoria e de redução de custos e de perdas. Levamos até o cliente o que há de mais moderno neste segmento.</p>
            
            <p>O atendimento da JPR Embalagens é personalizado e totalmente voltado às necessidades e preferências de cada cliente.</p>
            
            <p>Por isso, aproveite para adquirir uma embalagem de qualidade e com preço em conta, resultando em ótima relação custo-benefício, além de contar com ótimas condições de pagamento. Entre em contato com um dos consultores e solicite já o seu orçamento.</p>
             
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>