<?php
$h1    			= 'Saco leitoso';
$title 			= 'Saco leitoso';
$desc  			= 'O saco leitoso pode ser fabricado em polietileno e polipropileno e são indicados para embalar produtos alimentícios';
$key   			= 'Sacos leitosos, Saco, sacos, leitoso, leitosos';
$var 			= 'Sacos leitosos';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>   
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  
             
             
             <p>O <strong>saco leitoso</strong> pode ser fabricado em polietileno e polipropileno e são indicados para embalar produtos alimentícios, objetos em geral, envio de correspondência entre outras aplicações. </p>
             
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>

             <p>Por ser uma embalagem moderna e segura, o <strong>saco leitoso</strong> pode ser fabricado com aba adesiva permanente ou abre e fecha. Para o <strong>saco leitoso</strong> com adesivo permanente, ele se tornar uma embalagem extremamente segura e inviolável, desta forma para violar a embalagem é necessário danificá-la. Já no caso do <strong>saco leitoso com adesivo abre e fecha</strong>, pode ser aberto várias vezes.</p>
             <p>O <strong>saco leitoso</strong> pode ser confeccionado com ou sem impressão, basta o cliente possui a arte que será impressa e assim transferimos esta arte para a embalagem.</p>
             <p>Para <strong>saco leitoso impresso</strong>, nosso mínimo de produção são de 250kg, e sem impressão 150kg.</p>
             <p>Se você deseja contribuir com a sustentabilidade do planeta, utilize <strong>saco leitoso oxi-biodegradavel</strong>. Neste formato, a embalagem em contato com a natureza se degrada em curto espaço de tempo, podem levar em média seis meses para que este processo ocorra.</p>
             <p>Trabalhamos com projetos de redução de custo, e se você utiliza <strong>saco leitoso</strong>, porém não embala produtos alimentícios e médico-hospitalares, utilize a embalagem reciclada. O <strong>saco leitoso reciclado</strong> é fabricado a partir de matéria-prima reciclada, proveniente das sobras de embalagens como sacarias em geral, embalagens de alimentos, <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacola plastica</strong></a>, entre outros, e dessa forma essas embalagens são reciclados, e assim fabricamos um novo material. Além de você reduzir seus custos com embalagens, você estará contribuindo com o meio ambiente. Este ainda pode ser um novo argumento de vendas para os seus produtos.</p>
             <p>Parar receber um orçamento de <strong>saco leitoso</strong> entre em contato com um de nossos consultores, e informar as medidas (largura x comprimento x espessura) e a quantidade desejada.</p>

             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>