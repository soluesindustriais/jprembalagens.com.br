<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 1;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <div class="page-title">
    <div class="title_left">
      <h3><i class="fa fa-list-ol"></i> Lista de categorias</h3>
    </div>
    <div class="clearfix"></div>
    <br/>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12"> 
        <div class="x_panel">

          <div class="x_content">
            <div class="x_title">
              <h2>Sessões e categorias.<small>Você também pode acessar, editar, remover e alterar status das sessões e categorias.</small></h2>                           
              <div class="clearfix"></div>                            
            </div>
            <br/>                       
          </div>
          <div class="clearfix"></div>
          <?php
          $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
          if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
            //COLOCAR ALERTA PERSONALIZADOS
            WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
            unset($_SESSION['Error']);
          endif;

          $ReadRecursos = new Read;
          $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE cat_parent IS NULL ORDER BY cat_title ASC");
          if (!$ReadRecursos->getResult()):
            WSErro("Desculpe mas não foi encontrado nenhuma categoria no sistema.", WS_INFOR, null, SITENAME);
          else:
            ?> 
            <div class="clearfix"></div>
            <!-- start accordion -->
            <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
              <?php
              foreach ($ReadRecursos->getResult() as $key):
                extract($key);
                ?>
                <div class="panel j_item" id="<?= $cat_id; ?>">
                  <a class="panel-heading" role="tab" id="CatHeading<?= $cat_id; ?>" data-toggle="collapse" data-parent="#accordion" href="#Cat-<?= $cat_id; ?>" aria-expanded="true" aria-controls="Cat-<?= $cat_id; ?>">
                    <h4 class="panel-title">Sessão: <strong><?= $cat_title; ?></strong></h4>
                  </a>
                  <div id="Cat-<?= $cat_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="CatHeading<?= $cat_id; ?>">
                    <div class="panel-body">
                      <div class="pull-left col-md-9">                                                
                        <p><?= $cat_content; ?></p>
                      </div>                                            
                      <div class="pull-right">                                                
                        <a class="btn btn-dark" href="../<?= $cat_name; ?>" target="_blank"><i class="fa fa-eye"></i></a>
                        <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=categorias/update&id=<?= $cat_id; ?>'"><i class="fa fa-pencil"></i></button>
                        <?php if($user_level > 1): ?>
                          <button type="button" class="btn btn-danger j_remove" rel="<?= $cat_id; ?>" action="RemoveCategoria"><i class="fa fa-trash"></i></button>
                        <?php endif; ?>
                        <button type="button" class="btn j_statusRecursos" rel="<?= $cat_id; ?>" tabindex="getStatusCateg" action="StatusCategoria" value="<?= $cat_status; ?>"><i class="fa fa-ban"></i></button>
                      </div>
                      <div class="clearfix"></div>
                      <hr>
                      <?php
                      $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE cat_parent = :cat ORDER BY cat_title ASC", "cat={$cat_id}");
                      if (!$ReadRecursos->getResult()):
                        WSErro("Nenhuma categoria foi encontrada para esta sessão.", WS_INFOR, NULL, SITENAME);
                      else:
                        ?>
                        <div class="table-responsive">
                          <table class="table table-striped dtr-inline dataTable">                                               
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>URL</th>
                                <th>Descrição</th>                                                            
                                <th>Status</th>
                                <th>Ações</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              foreach ($ReadRecursos->getResult() as $cont):
                                extract($cont);
                                ?>
                                <tr class="j_item" id="<?= $cat_id; ?>" style="background:#001B2C; color: #fff;">
                                  <th scope="row"><?= $cat_id; ?></th>
                                  <td><?= $cat_title; ?></td>
                                  <td><?= $cat_name; ?></td>
                                  <td><?= Check::Words($cat_content, 10); ?></td>                                                               
                                  <td class="j_GetStatusRecursos" rel="<?= $cat_id; ?>"></td>                                                               
                                  <td style="width: 110px;">
                                    <div class="btn-group btn-group-xs">                                                
                                      <a class="btn btn-dark" href="../<?= Check::CatByParent($cat_id); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                      <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=categorias/update&id=<?= $cat_id; ?>'"><i class="fa fa-pencil"></i></button>
                                      <?php if($user_level > 1): ?>
                                        <button type="button" class="btn btn-danger j_remove" rel="<?= $cat_id; ?>" action="RemoveCategoria"><i class="fa fa-trash"></i></button>
                                      <?php endif; ?>
                                      <button type="button" class="btn j_statusRecursos" rel="<?= $cat_id; ?>" tabindex="getStatusCateg" action="StatusCategoria" value="<?= $cat_status; ?>"><i class="fa fa-ban"></i></button>
                                    </div>                                                        
                                  </td>
                                </tr>
                                <?php
                                $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE cat_parent = :cat ORDER BY cat_title ASC", "cat={$cat_id}");
                                foreach ($ReadRecursos->getResult() as $cont):
                                  extract($cont);
                                  ?>
                                  <tr class="j_item" id="<?= $cat_id; ?>">
                                    <th scope="row"><?= $cat_id; ?></th>
                                    <td><?= $cat_title; ?></td>
                                    <td><?= $cat_name; ?></td>
                                    <td><?= Check::Words($cat_content, 10); ?></td>                                                               
                                    <td class="j_GetStatusRecursos" rel="<?= $cat_id; ?>"></td>                                                               
                                    <td style="width: 110px;">
                                      <div class="btn-group btn-group-xs">                                                
                                        <a class="btn btn-dark" href="../<?= Check::CatByParent($cat_id) . $cat_name; ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                        <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=categorias/update&id=<?= $cat_id; ?>'"><i class="fa fa-pencil"></i></button>
                                        <button type="button" class="btn btn-danger j_remove" rel="<?= $cat_id; ?>" action="RemoveCategoria"><i class="fa fa-trash"></i></button>
                                        <button type="button" class="btn j_statusRecursos" rel="<?= $cat_id; ?>" tabindex="getStatusCateg" action="StatusCategoria" value="<?= $cat_status; ?>"><i class="fa fa-ban"></i></button>
                                      </div>                                                        
                                    </td>
                                  </tr>
                                  <?php
                                endforeach;
                              endforeach;
                              ?>
                            </tbody>                                                
                          </table>
                        </div>
                      <?php endif; ?> 
                    </div>
                  </div>  
                </div>
              <?php endforeach; ?>
            </div>
            <!-- end of accordion -->                     
          <?php
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
