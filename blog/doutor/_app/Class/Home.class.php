<?php
/**
* home.class.php [CLASSE]
* <b>Home</b>
* Classe responsável por administrar as apresentações da home page e notificações ao carregar empresas
* @copyright (c) 2016, Rafael da Silva Lima Web Stylus & Inovedados
*/
class home {
    private $Result;
    private $Error;
    private $Data;
    private $Danger;
    private $Warning;
    private $Info;
    private $Notify;
    private $Documents;
    private $Condicion;
    private $Where;
    public function getDanger() {
        return $this->Danger;
    }
    public function getWarning() {
        return $this->Warning;
    }
    public function getInfo() {
        return $this->Info;
    }
    public function getNotify() {
        return $this->Notify;
    }
    public function getDocuments() {
        return $this->Documents;
    }
/**
* <b>Verifica se existem resultados</b>
* @return bolean
*/
public function getResult() {
    return $this->Result;
}
/**
* <b>Retorna as mensagens</b>
* Devolve um envolpe em array com 4 keys
* @return array
*/
public function getError() {
    return $this->Error;
}
/**
* <b>Entrada das Query full</b>
* string ou null com WHERE ou condição + AND no final.
* @param string $Condicion Entrada de condições da Query = WHERE empresa_id = $var
* @param string $Where Entrada de condições da Query = user_empresa = $var AND
*/
public function __construct($Condicion = null, $Where = null) {
    $this->Condicion = $Condicion;
    $this->Where = $Where;
}
/**
* <b>Pegar vencimentos</b>
* Valor int para os tipos de prioridades dos vencimentos
* 1 = danger
* 2 = warning
* 3 = info
* @param int $Tipo
*/
public function getVencimentos($Tipo) {
    $this->Data = $Tipo;
    if ($this->Data == 1):
        $this->setDanger();
    elseif ($this->Data == 2):
        $this->setWarning();
    elseif ($this->Data == 3):
        $this->setInfo();
    elseif ($this->Data == 4):
        $this->setNotify();
    elseif ($this->Data == 5):
        $this->setDocuments();
    else:
        $this->Result = false;
    endif;
}
private function setDocuments() {
    $Read = new Read;
    $Read->FullRead("SELECT *FROM " . TB_VW_NUMDOC . " {$this->Where}");
    if (!$Read->getResult()):
        $this->Documents = false;
    else:
        $this->Documents = $Read->getResult();
    endif;
}
private function setNotify() {
    $Read = new Read;
    $Read->FullRead("SELECT *FROM " . TB_VW_PEN . " {$this->Where}");
    if (!$Read->getResult()):
        $this->Notify = false;
    else:
        $this->Notify = $Read->getResult();
    endif;
}
private function setInfo() {
    $Read = new Read;
    $Read->FullRead("SELECT user_empresa, CONCAT('Outorga: ', out_processo) as Nome, out_validade as Validade, out_file as File, out_id as Id FROM " . TB_OUTORGAS . " WHERE {$this->Condicion} (out_validade BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL +150 DAY) and DATE_ADD(CURRENT_DATE, INTERVAL +240 DAY)) AND out_status NOT IN(1,2) UNION SELECT user_empresa, CONCAT('Regularização: ', reg_processo) as Nome, reg_validade as Validade, reg_file as File, reg_id as Id FROM " . TB_REGULARIZACAO . " WHERE {$this->Condicion} (reg_validade BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL +150 DAY) and DATE_ADD(CURRENT_DATE, INTERVAL +240 DAY)) AND reg_status NOT IN(1,2) ORDER BY Validade");
    if (!$Read->getResult()):
        $this->Info = false;
    else:
        $this->Info = $Read->getResult();
    endif;
}
private function setWarning() {
    $Read = new Read;
    $Read->FullRead("SELECT user_empresa, CONCAT('Outorga: ', out_processo) as Nome, out_validade as Validade, out_file as File, out_id as Id FROM " . TB_OUTORGAS . " WHERE {$this->Condicion} (out_validade BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL +130 DAY) AND DATE_ADD(CURRENT_DATE, INTERVAL +150 DAY)) AND out_status NOT IN(1,2) UNION SELECT user_empresa, CONCAT('Regularização: ', reg_processo) as Nome, reg_validade as Validade, reg_file as File, reg_id as Id FROM " . TB_REGULARIZACAO . " WHERE {$this->Condicion} (reg_validade BETWEEN DATE_ADD(CURRENT_DATE, INTERVAL +130 DAY) AND DATE_ADD(CURRENT_DATE, INTERVAL +150 DAY)) AND reg_status NOT IN(1,2) ORDER BY Validade");
    if (!$Read->getResult()):
        $this->Warning = false;
    else:
        $this->Warning = $Read->getResult();
    endif;
}
private function setDanger() {
    $Read = new Read;
    $Read->FullRead("SELECT user_empresa, CONCAT('Outorga: ', out_processo) as Nome, out_validade as Validade, out_file as File, out_id as Id FROM " . TB_OUTORGAS . " WHERE {$this->Condicion} (out_validade BETWEEN CURRENT_DATE AND DATE_ADD(CURRENT_DATE, INTERVAL +130 DAY)) AND out_status NOT IN(1,2) UNION SELECT user_empresa, CONCAT('Regularização: ', reg_processo) as Nome, reg_validade as Validade, reg_file as File, reg_id as Id FROM " . TB_REGULARIZACAO . " WHERE {$this->Condicion} (reg_validade BETWEEN CURRENT_DATE AND DATE_ADD(CURRENT_DATE, INTERVAL +130 DAY)) AND reg_status NOT IN(1,2) ORDER BY Validade");
    if (!$Read->getResult()):
        $this->Danger = false;
    else:
        $this->Danger = $Read->getResult();
    endif;
}
}