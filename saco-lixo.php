<?php
$h1    			= 'Saco de lixo';
$title 			= 'Saco de lixo';
$desc  			= 'O saco de lixo é indicado para embalar lixos comuns de diversos tipos e de toda a natureza, porem não é indicado para embalar lixo infectante';
$key   			= 'Sacos de lixo, Saco, lixo';
$var 			= 'Sacos de lixo';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <h2>Fabricamos sacos de lixo, em diversas medidas e cores.</h2>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>


             <p>O <strong>saco de lixo</strong> é indicado para embalar lixos comuns de diversos tipos e de toda a natureza, porem não é indicado para embalar lixo infectante, restos e cadáver ou lixo de hospitais.</p>
             <h3>Abaixo as medidas que fabricamos conforme a norma ABNT:</h3>
             <ul class="list">
                <li><strong>Saco de lixo preto 20 litros</strong></li>
                <li><strong>Saco de lixo preto 30 litros</strong></li>
                <li><strong>Saco de lixo preto 40 litros</strong></li>
                <li><strong>Saco de lixo preto 50 litros</strong></li>
                <li><strong>Saco de lixo preto 60 litros</strong></li>
                <li><strong>Saco de lixo preto 100 litros</strong></li>
                <li><strong>Saco de lixo preto 200 litros</strong></li>
            </ul>
            <p>São fabricados em polietileno virgem ou reciclado. Uma novidade que esta chegando no mercado, são os <strong>sacos de lixo oxi-biodegradaveis</strong>, que em contato com a natureza, se degradam em curto espaço de tempo.</p>
            <p>Também fabricamos <strong>sacos de lixo</strong> em outras medidas. Basta entrar em contato com um de nossos consultores e informar as medidas e quantidades desejadas.</p>
            
            <p>A JPR Embalagens trabalha com diversos modelos de embalagem, além do <strong>saco para lixo</strong>, fabricamos <a href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip"><strong>sacolas plásticas com fecho zip</strong></a>, <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plasticas</strong></a>, entre outros modelos.</p>
            
            <p>Para receber um orçamento de nossos produtos, entre em contato o nosso setor comercial, e tenha em mãos as medidas da embalagem (largura x comprimento x espessura) e a quantidade desejada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>