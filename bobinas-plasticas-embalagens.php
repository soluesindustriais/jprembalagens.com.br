<?php
	$h1    			= 'Bobinas plásticas para embalagens';
	$title 			= 'Bobinas plásticas para embalagens';
	$desc  			= 'As bobinas plásticas para embalagens podem ser fabricadas conforme a necessidade de cada cliente. Por isso, a fabricação pode ser feita em polietileno de alta ou baixa densidad';
	$key   			= 'bobinas, plásticas, embalagens, bobina plástica de embalagem';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina plástica de embalagem';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                <!--                  -->                
    			<?=$caminhoProdutoBobinas?>
                <article> <article>
                <h1><?=$h1?></h1>     
                	<br>   
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
           	<p>As <strong>bobinas plásticas para embalagens</strong> tem utilização bastante ampla por serem opções versáteis. Saiba mais sobre este material.</p>

            <p>A embalagem é um aspecto fundamental na hora de proteger um produto e fazer com que ele se torne mais atraente para os olhos dos consumidores. Por isso, invista em <strong>bobinas plásticas para embalagens</strong>.</p>
            
            <p>As <strong>bobinas plásticas para embalagens</strong> podem ser fabricadas conforme a necessidade de cada cliente. Por isso, a fabricação pode ser feita em polietileno de alta ou baixa densidade (PEAD ou PEBD), polipropileno (PP) ou BOPP.</p>
            
            <p>As <strong>bobinas plásticas para embalagens</strong> que são fabricadas em polipropileno (PP) são mais utilizadas em indústrias de confecções, gráficas e alimentos, graças ao brilho e à transparência que a embalagem possui.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Quando fabricadas em polietileno (PE), as <strong>bobinas plásticas para embalagens</strong> são muito usadas para alimentos e bebidas, além de setor automotivo e metalúrgico. Neste caso, a bobina pode ser utilizada para embalar qualquer tipo de produto, já que é um plástico atóxico, ou seja, que não altera as características originais de produtos como alimentos e bebidas, por exemplo.</p>
            
            <p>Outra opção são as <strong>bobinas plásticas para embalagens</strong> fabricadas em BOPP, que são bastante utilizadas pelo mercado de varejo para embalagem de presente, com opção de ser metalizado ou perolizado.</p>
            
            <h2>Bobinas plásticas para embalagens têm opções sustentáveis</h2>
            
            <p>Uma opção para contribuir com o meio ambiente são as <strong>bobinas plásticas para embalagens</strong> que são feitas com materiais reciclados. É possível fabricar a bobina em reciclado cristal que é feito a partir de aparas de material virgem e fica com aspecto amarelo claro, ou reciclado canela, feito a partir de aparas do reciclado cristal e que tem aspecto canela. Em ambos os casos, a transparência da embalagem é mantida, há resistência e a embalagem não perde em qualidade quando comparada às embalagens feitas com matéria-prima virgem.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para adquirir as bobinas, conte com o profissionalismo da JPR Embalagens, empresa que atua há mais de 15 anos no mercado de embalagens plásticas flexíveis.</p>
            
            <p>Na JPR Embalagens, o atendimento é totalmente personalizado e voltado para as suas necessidades. As <strong>bobinas plásticas para embalagens</strong> são feitas conforme a sua preferência, tanto em relação ao material quanto ao aspecto. É possível fabricar bobina lisa ou impressa em até seis cores.</p>
            
            <p>Entre em contato com um dos consultores para saber mais sobre os produtos. Aproveite os preços reduzidos e as ótimas condições de pagamento para solicitar já o seu orçamento.</p>
             
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>