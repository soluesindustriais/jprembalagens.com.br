<?php
$h1    			= 'Saco PP liso';
$title 			= 'Saco PP liso';
$desc  			= 'O saco PP liso com aba adesiva abre e fecha, pode ser aberto e fechado por varias vezes, como é no caso das embalagens para roupas.';
$key   			= 'Sacos PP lisos, Saco, sacos, PP, liso';
$var 			= 'Sacos PP lisos';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>

                       <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     

             <br> 

             <p>Se você precisa de uma embalagem que tenha uma excelente transparência e brilho, utilize o <strong>saco PP liso</strong>. É fabricado sob medida, de acordo com a necessidade de cada cliente. Além de serem fabricados com ou sem impressão.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>
             <p>Este tipo de embalagem é amplamente utilizado por gráficas, editoras, laboratórios, confecções, entre outros segmentos.</p>
             <p>Além de ser uma embalagem de fácil abertura, para facilitar o fechamento do <strong>saco PP liso</strong>, ele pode ser fabricado com aba adesiva abre e fecha ou permanente. No caso do <strong>sacos PP liso com aba adesiva permanente</strong>, para ser aberto é necessário danificar a embalagem. Já o <strong>saco PP liso com aba adesiva abre e fecha</strong>, pode ser aberto e fechado por varias vezes, como é no caso das embalagens para roupas.</p>

             <h2>Saco PP liso sustentável</h2>

             <p>Pensado em contribuir com o meio ambiente? Utilize o <strong>saco PP liso com aditivo oxi-biodegradavel</strong>. Durante o processo de produção da embalagem, é adicionado o aditivo na extrusão do filme. Com este aditivo, a embalagem se torna degradável em curto espaço de tempo. </p>
             <p>O <strong>saco PP liso</strong> também pode ser utilizado para embalar diversos alimentos de giro rápido, como é o caso da pipoca doce, pão para hot dog, entre muitos outros.</p>
             <p>Além de <strong>saco PP liso</strong>, a JPR Embalagens trabalha com <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plásticas</strong></a>, <a href="<?=$url;?>envelopes" title="Envelopes"><strong>envelopes</strong></a> em diversos modelos e cores, embalagens especiais, filmes bobinas, <a href="<?=$url;?>envelope-inviolavel" title="Envelope Inviolável"><strong>envelope inviolável</strong></a>, <a href="<?=$url;?>saco-lixo" title="Saco de Lixo"><strong>sacos de lixo</strong></a>, entre outros.</p>
             <p>Nossa quantidade mínima de produção são de 100kg para <strong>sacos PP liso</strong> e 250kg para o <a href="<?=$url;?>saco-pp-impresso" title="Saco PP Impresso">saco PP impresso</a>.</p>
             <p>Para receber um orçamento de <strong>sacos PP liso</strong> basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

             <?php include('inc/saiba-mais.php');?>



         </article>

         <?php include('inc/coluna-lateral-paginas.php');?>

         <?php include('inc/paginas-relacionadas.php');?>  

         <br class="clear" />  



         <?php include('inc/regioes.php');?>

         <?php include('inc/copyright.php');?>


     </section>

 </main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>