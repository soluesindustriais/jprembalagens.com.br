<?php
	$h1    			= 'Saco plástico industrial';
	$title 			= 'Saco plástico industrial';
	$desc  			= 'O saco plástico industrial é fabricado em polietileno de baixa densidade. É um plástico totalmente reforçado e resistente, próprios para suportar pesos de 10 a 30kg.';
	$key   			= 'Sacos plásticos industriais, Saco, sacos, plástico, industrial, saco plástico para industria';
	$var 			= 'Sacos plásticos industriais';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutosSacos?>                
                <!-- <article> <article> -->
                    	<h1><?=$h1?></h1>     
                	
                <br> 

            <p>O <strong>saco plástico industrial</strong> é fabricado em polietileno de baixa densidade. É um plástico totalmente reforçado e resistente, próprios para suportar pesos de 10 a 30kg.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div> 

            <p>Podem ser lisos ou impressos em até 6 cores, além do <strong>saco plástico industrial</strong>  pode ser transparente ou pigmentado em diversas cores.</p>
            <p>O <strong>saco plástico industrial</strong> é amplamente utilizado por indústrias metalúrgicas, automotivas, construção civil, química e têxtil.</p> 
            <p>São produzidos com espessura a partir de 0,200 mm (200 micras), desta forma obtemos uma embalagem resistente e homogênea. Os <strong>sacos plásticos industrial</strong> possuem alta resistência à tração, impacto, são flexíveis e resistentes ao frio.</p>

            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div> 

            <p>Para contribuir com o meio ambiente, fabricamos <strong>saco plástico industrial reciclado</strong>. Podem ser feitos em dois tipos de reciclados, o cristal e o canela. O <strong>saco plástico industrial reciclado cristal</strong> é produzido a partir das aparas do plástico virgem, e por ser reciclado a embalagem fica com alguns pontos na cor amarelo claro.</p>
            
            <p>Já o <strong>saco plástico industrial reciclado canela</strong>, é produzido a partir das aparas de outros reciclados, a embalagem fica na cor canela, porem, continuará transparente, ou seja, sendo possível visualizar o que há dentro da embalagem.</p>
            <p>Além de <strong>saco plástico industrial</strong>, trabalhamos com uma ampla linha de embalagens, como <a href="<?=$url;?>envelopes" title="Envelopes"><strong>envelopes</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, embalagens em geral, filmes e bobinas.</p>
            <p>Nossa quantidade mínima de produção são de 100kg para <strong>saco plástico industrial</strong> liso e 250kg impresso.</p>
            <p>Para receber um orçamento de <strong>saco plástico industrial</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>