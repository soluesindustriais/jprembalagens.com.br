<?php
	$h1    			= 'Envelope fronha';
	$title 			= 'Envelope fronha';
	$desc  			= 'O Envelope fronha é ideal para embalar revistas, remessa especial por correio de flyers, convites, malas diretas, entre outros';
	$key   			= 'Envelopes fronha, Envelopes, Envelope, fronha';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes fronha';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                                    
            <p>O <strong>Envelope fronha</strong> é ideal para embalar revistas, remessa especial por correio de flyers, convites, malas diretas, entre outros. São infinitas as aplicações dessa embalagem, além de  poder ser liso ou impresso em até 6 cores.</p> 
              
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 3; include('inc/gallery.php'); ?>
            
            <p>Com um sistema de fechamento prático, o <strong>Envelope fronha</strong> dispensa o uso de fitas adesivas ou durex, e assim de evita que o produto migre para fora da embalagem. Com foco em sustentabilidade, a JPR Embalagens produz <strong>envelopes fronha reciclado</strong>, assim contribuímos com o ambiente, além da redução de custos que a matéria-prima reciclada proporciona para a sua empresa.</p>
            
            <p>Além de ser uma embalagem totalmente moderna, o <strong>Envelope fronha</strong> pode ser transparente ou pigmentado em diversas cores. Nosso <strong>Envelope fronha</strong> é produzido de acordo com necessidade de cada cliente, além de poder ser fabricado em polietileno de baixa densidade (PEBD), polietileno de alta densidade (PEAD) e polipropileno (PP), todos com ou sem impressão.</p>
            
            <p>Para <strong>Envelope fronha impresso</strong>, nosso mínimo de produção são de 200kg e para liso 100kg. Os <strong>envelopes fronha</strong> possuem excelente desempenho, pois agiliza seu processo de manuseio, pois sua abertura é muito simples. Podem ser reutilizados, e além de ser um material totalmente reciclável.</p>
            
            <p>Trabalhamos com o desenvolvimento de <strong>Envelope fronha</strong>, basta fornecer aos nossos consultores as medidas (largura x comprimento x espessura) e a quantidade que você irá utilizar. Recomendamos o uso do <strong>Envelope fronha</strong> para envio de produtos simples, como convites ou revistas, pois através da luz é possível identificar o que há dentro da embalagem, porém ele é uma embalagem que protege o seu produto contra poeiras, quedas, etc.</p>
            <h2>Fronha de travesseiro</h2>
            
            <p>O nome <strong>Envelope fronha</strong> foi dado devido à embalagem possui um corte no verso, e lembrar uma “fronha de travesseiro”, pois a embalagem possui um sistema de fechamento automático, e para o produto ser retirado, basta virar o corte da fronha para cima e assim você terá acesso ao produto.</p> 
            <p>A JPR Embalagens trabalha com outras linhas de embalagens, como <a href="<?=$url;?>saco-zip" title="Saco Zip"><strong>Saco Zip</strong></a>, <a href="<?=$url;?>envelope-bolha" title="Envelope Bolha"><strong>Envelope Bolha</strong></a>, <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>Envelope de Segurança</strong></a>, <a href="<?=$url;?>envelope-adesivado" title="Envelope Adesivado"><strong>Envelope Adesivado</strong></a>, entre vários outros.</p>

        	 
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>