<?php
$h1    			= 'Envelope plástico laboratório';
$title 			= 'Envelope plástico laboratório';
$desc  			= 'O envelope plástico laboratório é amplamente utilizado para entrega de exames de diagnósticos, hospitais e por laboratório de analises clinicas.';
$key   			= 'Envelopes plásticos laboratório, Envelopes, Envelope, plástico, laboratório, Envelope para laboratório';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Envelopes plásticos laboratório';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>            
                			
                <?=$caminhoProdutosEnvelopes?>
                 <article>
                <h1><?=$h1?></h1> 
                <br> 
                
                <p>Fabricamos diversos modelos de envelopes plásticos laboratório. Podem ser lisos ou impressos em até 6 cores.</p>
                
                <div class="picture-legend picture-center">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                
                <p>Confeccionados em polietileno ou polipropileno, o <strong>envelope plástico laboratório</strong> é fabricado sob medida, assim você pode personalizar no formato que desejar. </p>
                <p>O <strong>envelope plástico laboratório</strong>, poder ser fabricado com diversos tipos de fechos, como aba adesiva, fecho zip, tala, botões, entre outros. </p>
                <p>Geralmente são utilizados na cor branco leitoso, e também fabricamos em outras cores, conforme a necessidade de cada cliente.</p>
                <p>O <strong>envelope plástico laboratório</strong> é amplamente utilizado para entrega de exames de diagnósticos, hospitais e por laboratório de analises clinicas.</p>
                
                <h2>Envelope plástico laboratório para o meio ambiente</h2>
                
                <div class="picture-legend picture-center">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>
                
                <p>Se você deseja contribuir com o meio ambiente, utilize <strong>envelope plástico laboratório oxi-biodegradavel</strong>. O aditivo oxi-biodegradavel é adicionado durante o processo de fabricação do material, e faz com que a embalagem se degrade em curto espaço de tempo. A natureza agradece.</p>
                <p>Além de <strong>envelope plástico laboratório</strong>, a JPR Embalagens fabrica <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plásticas</strong></a> com alça camiseta e alça vazada, <a href="<?=$url;?>envelope-plastico" title="Envelope Plástico"><strong>envelopes plásticos</strong></a>, <a href="<?=$url;?>envelope-plastico-inviolavel" title="Envelope Plástico Inviolável"><strong>Envelope Plástico Inviolável</strong></a>, <a href="<?=$url;?>envelopes-plasticos-especiais" title="Envelopes Plásticos Especiais"><strong>Envelopes Plásticos Especiais</strong></a>, filmes e bobinas, entre outras embalagens.</p>
                <p>Produzimos a partir de 250kg para <strong>envelope plástico laboratório impresso</strong> e 150kg para <strong>envelopes lisos</strong>.</p>
                <p>Para receber um orçamento de <strong>envelope plástico laboratório</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade que deseja utilizar.</p>

                <?php include('inc/saiba-mais.php');?>
                
                
                
            </article>
            
            <?php include('inc/coluna-lateral-paginas.php');?>
            
            <?php include('inc/paginas-relacionadas.php');?>  
            
            <br class="clear" />  
            

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        </section>

    </main>

    
    
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>