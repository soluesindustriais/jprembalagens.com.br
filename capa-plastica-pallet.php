<?php
$h1    			= 'Capa plástica para pallet';
$title 			= 'Capa plástica para pallet';
$desc  			= 'A Capa plástica para pallet é uma embalagem versátil e para isso é muito utilizada em indústrias químicas, fumageiras, fundição, alimentos, calçadistas e metalúrgicas.';
$key   			= 'capa, plástica, pallet, capas plásticas de pallet';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Capas plásticas de pallet';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            

             <?=$caminhoProdutoCapas?>
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>A <strong>Capa plástica para pallet</strong> é uma opção resistente e de qualidade para embalar mercadorias. Confira maiores informações.</p>

            <p>A embalagem é um ponto fundamental para conservar os seus produtos, fazendo com que eles estejam protegidos de influências externas. Por isso, uma opção altamente recomendável é a <strong>Capa plástica para pallet</strong>.</p>
            
            <p>A <strong>Capa plástica para pallet</strong> é uma embalagem versátil e para isso é muito utilizada em indústrias químicas, fumageiras, fundição, alimentos, calçadistas e metalúrgicas, por exemplo, entre outras. Ela é a opção para embalagem de produtos unitizados e para produtos que precisem de fácil inspeção e identificação.</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>Capa plástica para pallet</strong> é feita em polietileno transparente, que é um material que se caracteriza pela alta resistência mecânica e durabilidade prolongada, devido à exposição às intempéries.</p>
            
            <p>Além disso, o polietileno da <strong>Capa plástica para pallet</strong> proporciona em maior segurança e rigidez na hora de movimentar e transportar mercadorias e cargas, sem causar qualquer tipo de dano e fazendo a proteção impermeável contra influências externas como sujeiras e variações de temperatura. Outro benefício é a possibilidade de acondicionamento de cargas de disposições e tamanhos diferentes.</p>
            
            <h2>Capa plástica para pallet: preços em conta e ótimas condições de pagamento</h2>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir <strong>Capa plástica para pallet</strong> com preços em conta, ótimas condições de pagamento e a melhor relação custo-benefício do mercado, aproveite as vantagens da JPR Embalagens. A empresa está no mercado há mais de 15 anos, tendo vasta experiência na área de embalagens plásticas flexíveis.</p>
            
            <p>A JPR Embalagens conta com equipe de profissionais altamente especializados e constantemente atualizados, além de equipamentos de última geração, para proporcionar embalagem com baixo custo e qualidade comprovada.</p>
            
            <p>O objetivo da empresa é, em todos os seus produtos, identificar oportunidades de melhoria, resultando em redução de perdas e de custos. Além de <strong>Capa plástica para pallet</strong>, a JPR Embalagens disponibiliza sacos, sacolas, envelopes, filmes, bobinas e outros tipos de materiais.</p>
            
            <p>O atendimento da empresa é personalizado e voltado às necessidades específicas de cada cliente. Por isso, aproveite. Entre em contato com um dos consultores para maiores informações sobre o produto e sobre as condições e peça já seu orçamento. </p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>           
        <br class="clear" />                                                                                                    
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>