<?
	$h1    			= 'Bobina encolhível';
	$title 			= 'Bobina encolhível';
	$desc  			= 'A bobina encolhível é uma embalagem que se destaca pela versatilidade, podendo embalar uma série de produtos, desde produtos alimentícios, até de higiene e limpeza';
	$key   			= 'bobina, encolhível, bobinas encolhíveis';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas encolhíveis';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br>    
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
           	<p>Opção de embalagem resistente e ideal para economia de espaço. Confira maiores informações sobre a <strong>bobina encolhível</strong>.</p>

            <p>Na hora de embalar um produto, é preciso contar com soluções inteligentes. Afinal, a embalagem deve proteger o que está sendo embalado da melhor maneira possível e com custos baixos para o empreendedor. Por isso, conheça a <strong>bobina encolhível</strong>.</p>
            
            <p>A <strong>bobina encolhível</strong> é a solução para inúmeros problemas de embalagem, tanto para produtos de indústria quanto em varejo. Ou seja, objetos como ferramentas, brinquedos e utensílios domésticos, que têm formatos irregulares. A bobina se adéqua ao formato do produto, o que proporciona muito mais praticidade e poupa espaço.</p>

            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina encolhível</strong> é ideal para mercadorias que precisam de embalagens múltiplas e também para objetos de formatos irregulares ou que tenham bordas afiadas, que tenham muito manuseio no varejo.</p>
            
            <p>A <strong>bobina encolhível</strong> é uma embalagem que se destaca pela versatilidade, podendo embalar uma série de produtos, desde produtos alimentícios, até de higiene e limpeza. Este tipo de embalagem é altamente indicado para empresas de bebidas e alimentos, farmácias, cosméticos, brinquedos, eletrônicos, gráficas, editoras, entre outros. Outras características são a boa transparência e a alta resistência.</p>
            
            <p>A aplicação da <strong>bobina encolhível</strong> pode ser feita tanto em máquinas automáticas quanto semi automática. Por ser uma embalagem plástica flexível, pode ser fabricada com baixas taxas de encolhimento, conforme especificações técnicas.</p>
            
            <h2>Bobina encolhívelcom preço em conta e ótimas condições de pagamento</h2>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para adquirir a <strong>bobina encolhível</strong>, aproveite os benefícios e as vantagens da JPR Embalagens. A empresa atua há mais de 15 anos no mercado, levando aos consumidores as soluções mais inteligentes, práticas e econômicas na área de embalagens plásticas flexíveis.</p>
            
            <p>A JPR Embalagens conta com uma equipe técnica sempre atualizada com as novidades, já que a empresa tem como objetivo reduzir custos e perdas dos clientes e identificar oportunidades de melhoria.</p>
            
            <p>Na JPR Embalagens, o atendimento é totalmente personalizado e voltado para as necessidades e preferências de cada cliente. Entre em contato com os profissionais para saber mais sobre a <strong>bobina encolhível</strong> e os outros produtos que a empresa disponibiliza.</p>
            
            <p>Conheça as ótimas condições de pagamento e solicite já o seu orçamento, informando medidas e quantidade que a sua empresa necessita.</p>
             
            
            <? include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<? include('inc/coluna-lateral-paginas.php')?>
        
			<? include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>