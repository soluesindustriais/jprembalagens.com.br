<?php
$h1    			= 'Saco infectante';
$title 			= 'Saco infectante';
$desc  			= 'O saco infectante é amplamente utilizado na linha médico-hospitalar para embalar resíduos infectantes, e são confeccionados com solda reforçada.';
$key   			= 'Sacos infectantes, Saco, sacos, infectante, saco para materiais infectados, saco para lixo infectante';
$var 			= 'Sacos infectantes';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             
             <p>Trabalhamos com uma ampla linha de <strong>saco infectante</strong>. São fabricados conforme a norma ABNT, além de possuírem o registro da ANVISA.</p>
             
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            <p>São fabricados em polietileno de alta densidade (PEAD), na cor branco leitoso, com a impressão do símbolo infectante. </p>
            <p>O <strong>saco infectante</strong> é amplamente utilizado na linha médico-hospitalar para embalar resíduos infectantes, e são confeccionados com solda reforçada, contínua e homogênea no fundo da embalagem, proporcionando uma excelente vedação, evitando que o produto migre para fora da embalagem.</p>
            <p>Por ser direcionado a produtos infectantes, o <strong>saco infectante</strong> tem um aspecto opaco, devido à matéria-prima que é confeccionado, assim apresenta uma perfeita resistência mecânica e proporcionando a opacidade na embalagem. </p>
            <h2>Trabalhamos com os seguintes padrões de saco infectante:</h2>
            <ul class="list">
                <li><strong>Saco infectante para 15 litros</strong></li>
                <li><strong>Saco infectante para 30 litros</strong></li>
                <li><strong>Saco infectante para 50 litros</strong></li>
                <li><strong>Saco infectante para 100 litros</strong></li>
                <li><strong>Saco infectante para 200 litros</strong></li>
                <li><strong>Saco infectante para 240 litros</strong></li>
            </ul>
            <p>Além da linha de <strong>sacos infectantes</strong>, a JPR Embalagens trabalha com <a href="<?=$url;?>saco-plastico-personalizado" title="Saco Plastico Personalizado"><strong>sacos personalizados</strong></a>, <a href="<?=$url;?>saco-polietileno" title="Saco de Polietileno"><strong>sacos de polietileno</strong></a>, <a href="<?=$url;?>saco-polipropileno" title="Saco de Polipropileno"><strong>sacos de polipropileno</strong></a>, filmes e bobinas e embalagens especiais.</p>
            <p>Para receber um orçamento de <strong>saco infectante</strong>, entre em contato com os nossos consultores, e informe as medidas e quantidades desejadas.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>