<?php
	$h1    			= 'Bobinas de polipropileno';
	$title 			= 'Bobinas de polipropileno';
	$desc  			= 'As bobinas de polipropileno podem ser lisas ou impressas em até seis cores, com possibilidade de serem fabricadas também sob medida.';
	$key   			= 'bobinas, bobina de polipropileno, bobina de ';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina de polipropileno';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>   
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Embalagens de grande resistência, as <strong>bobinas de polipropileno</strong> são amplamente utilizadas em diversos segmentos. Confira maiores informações sobre o produto.</p>

            <p>Na hora de vender mais e conquistar novos consumidores e mercados, a embalagem é um ponto fundamental. Ela deve ser sempre resistente e conservar as características originais do produto, além de ter um aspecto visual agradável para chamar a atenção dos clientes. Neste caso, conte com as <strong>bobinas de polipropileno</strong>.</p>
            
            <p>As <strong>bobinas de polipropileno</strong> têm como característica o brilho elevado e a boa resistência a gases e ao vapor de água. Por isso, é um produto bastante utilizado em estruturas laminas, em alimentos, confecções e outros segmentos.</p>
            
            <p>O brilho das <strong>bobinas de polipropileno</strong> colaboram com o acabamento da embalagem e transmitem uma ótima impressão do produto, além de terem excelente transparência. Por isso, é amplamente adotado por várias empresas na hora de divulgar novos produtos ou a sua marca.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>As <strong>bobinas de polipropileno</strong> ainda contam com outras vantagens: são ideais para embalar alimentos de giro rápido, para embalagem roupas, e alimentos como macarrão, saches de ketchup e mostarda, entre outros. Outro ponto positivo neste tipo de embalagem é a resistência à tração e a possibilidade da colocação de aba adesiva para fechar a embalagem.</p>
            
            <h2>Bobinas de polipropileno podem ser personalizadas</h2>
            
            <p>As <strong>bobinas de polipropileno</strong> podem ser lisas ou impressas em até seis cores, com possibilidade de serem fabricadas também sob medida. Este modelo de embalagem é ideal para produtos promocionais, malas diretas, convites e outros, dando melhor apresentação e demonstração do produto.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para personalizar as <strong>bobinas de polipropileno</strong>, conte com a JPR Embalagens. A empresa atua há mais de 15 anos na área de embalagens plásticas flexíveis, levando até os clientes embalagens de alta tecnologia, qualidade elevada e preço reduzido.</p>
            
            <p>O atendimento da JPR Embalagens é totalmente personalizado confirme a preferência e necessidade dos consumidores. Além disso, a empresa oferece ótimas condições de pagamento, resultando em ótima relação custo-benefício para a sua empresa.</p>
            
            <p>Entre já em contato com um dos consultores para saber mais sobre as <strong>bobinas de polipropileno</strong> e conferir as condições da empresa. Aproveite e peça o seu orçamento, informando medidas e quantidade do produto que você necessita.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>