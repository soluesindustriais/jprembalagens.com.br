<?php
$h1              = 'Bobinas';
$title           = 'Bobinas';
$desc            = 'A JPR Embalagens traz para o mercado uma linha completa de bobinas. Em nossa linha de produtos você encontra diversos tipos de bobinas';
$key             = 'Bobinas, bobinas plásticas, fábrica de bobinas';
$var           = 'Bobinas';

include('inc/head.php');
?>


</head>

<body>

     <div class="wrapper-topo">

          <?php include('inc/topo.php'); ?>

     </div>

     <div class="wrapper">

          <main role="main">

               <section>


                    <?= $caminhoCategoria ?>
                    <article>

                         <h1><?= $h1 ?></h1>

                         <h2>A JPR Embalagens traz para o mercado uma linha completa de bobinas.</h2>

                         <p>Em nossa linha de produtos você encontra <strong>bobinas</strong>, de acordo com a necessidade da sua empresa.</p>

                         <p>Conheça nossa linha de <strong>bobinas</strong>:</p>

                         <ul class="thumbnails">
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-bopp" title="Bobina Bopp"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-bopp-01.jpg" alt="Bobina Bopp" /></a>
                                   <h2><a href="<?= $url; ?>bobina-bopp" title="Bobina Bopp">Bobina Bopp</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-canela" title="Bobina Canela"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-canela-01.jpg" alt="Bobina Canela" /></a>
                                   <h2><a href="<?= $url; ?>bobina-canela" title="Bobina Canela">Bobina Canela</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-cristal" title="Bobina Cristal"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-cristal-01.jpg" alt="Bobina Cristal" /></a>
                                   <h2><a href="<?= $url; ?>bobina-cristal" title="Bobina Cristal">Bobina Cristal</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-adesivo" title="Bobina de Adesivo"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-adesivo-01.jpg" alt="Bobina de Adesivo" /></a>
                                   <h2><a href="<?= $url; ?>bobina-adesivo" title="Bobina de Adesivo">Bobina de Adesivo</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-filme" title="Bobina de Filme"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-filme-01.jpg" alt="Bobina de Filme" /></a>
                                   <h2><a href="<?= $url; ?>bobina-filme" title="Bobina de Filme">Bobina de Filme</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-filme-plastico" title="Bobina de Filme Plástico"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-filme-plastico-01.jpg" alt="Bobina de Filme Plástico" /></a>
                                   <h2><a href="<?= $url; ?>bobina-filme-plastico" title="Bobina de Filme Plástico">Bobina de Filme Plástico</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-filme-pvc" title="Bobina de Filme Pvc"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-filme-pvc-01.jpg" alt="Bobina de Filme Pvc" /></a>
                                   <h2><a href="<?= $url; ?>bobina-filme-pvc" title="Bobina de Filme Pvc">Bobina de Filme Pvc</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-filme-stretch" title="Bobina de Filme Stretch"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-filme-stretch-01.jpg" alt="Bobina de Filme Stretch" /></a>
                                   <h2><a href="<?= $url; ?>bobina-filme-stretch" title="Bobina de Filme Stretch">Bobina de Filme Stretch</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-forracao" title="Bobina de Forração"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-forracao-01.jpg" alt="Bobina de Forração" /></a>
                                   <h2><a href="<?= $url; ?>bobina-forracao" title="Bobina de Forração">Bobina de Forração</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-lona-plastica" title="Bobina de Lona Plástica"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-lona-plastica-01.jpg" alt="Bobina de Lona Plástica" /></a>
                                   <h2><a href="<?= $url; ?>bobina-lona-plastica" title="Bobina de Lona Plástica">Bobina de Lona Plástica</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico" title="Bobina de Plástico"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-01.jpg" alt="Bobina de Plástico" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico" title="Bobina de Plástico">Bobina de Plástico</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico-bolha" title="Bobina de Plástico Bolha"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-bolha-01.jpg" alt="Bobina de Plástico Bolha" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico-bolha" title="Bobina de Plástico Bolha">Bobina de Plástico Bolha</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico-filme" title="Bobina de Plástico Filme"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-filme-01.jpg" alt="Bobina de Plástico Filme" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico-filme" title="Bobina de Plástico Filme">Bobina de Plástico Filme</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico-embalagem" title="Bobina de Plástico para Embalagem"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-embalagem-01.jpg" alt="Bobina de Plástico para Embalagem" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico-embalagem" title="Bobina de Plástico para Embalagem">Bobina de Plástico para Embalagem</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico-preto" title="Bobina de Plástico Preto"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-preto-01.jpg" alt="Bobina de Plástico Preto" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico-preto" title="Bobina de Plástico Preto">Bobina de Plástico Preto</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico-pvc" title="Bobina de Plástico Pvc"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-pvc-01.jpg" alt="Bobina de Plástico Pvc" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico-pvc" title="Bobina de Plástico Pvc">Bobina de Plástico Pvc</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastico-transparente" title="Bobina de Plástico Transparente"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastico-transparente-01.jpg" alt="Bobina de Plástico Transparente" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastico-transparente" title="Bobina de Plástico Transparente">Bobina de Plástico Transparente</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-polietileno" title="Bobina de Polietileno"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-polietileno-01.jpg" alt="Bobina de Polietileno" /></a>
                                   <h2><a href="<?= $url; ?>bobina-polietileno" title="Bobina de Polietileno">Bobina de Polietileno</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-polietileno-baixa-densidade" title="Bobina de Polietileno de Baixa Densidade"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-polietileno-baixa-densidade-01.jpg" alt="Bobina de Polietileno de Baixa Densidade" /></a>
                                   <h2><a href="<?= $url; ?>bobina-polietileno-baixa-densidade" title="Bobina de Polietileno de Baixa Densidade">Bobina de Polietileno de Baixa Densidade</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-pvc" title="Bobina de Pvc"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-pvc-01.jpg" alt="Bobina de Pvc" /></a>
                                   <h2><a href="<?= $url; ?>bobina-pvc" title="Bobina de Pvc">Bobina de Pvc</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-pvc-flexivel" title="Bobina de Pvc Flexível"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-pvc-flexivel-01.jpg" alt="Bobina de Pvc Flexível" /></a>
                                   <h2><a href="<?= $url; ?>bobina-pvc-flexivel" title="Bobina de Pvc Flexível">Bobina de Pvc Flexível</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-encolhivel" title="Bobina Encolhível"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-encolhivel-01.jpg" alt="Bobina Encolhível" /></a>
                                   <h2><a href="<?= $url; ?>bobina-encolhivel" title="Bobina Encolhível">Bobina Encolhível</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-impressa" title="Bobina Impressa"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-impressa-01.jpg" alt="Bobina Impressa" /></a>
                                   <h2><a href="<?= $url; ?>bobina-impressa" title="Bobina Impressa">Bobina Impressa</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-pebd" title="Bobina Pebd"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-pebd-01.jpg" alt="Bobina Pebd" /></a>
                                   <h2><a href="<?= $url; ?>bobina-pebd" title="Bobina Pebd">Bobina Pebd</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastica" title="Bobina Plástica"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastica-01.jpg" alt="Bobina Plástica" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastica" title="Bobina Plástica">Bobina Plástica</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastica-seladora" title="Bobina Plástica para Seladora"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastica-seladora-01.jpg" alt="Bobina Plástica para Seladora" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastica-seladora" title="Bobina Plástica para Seladora">Bobina Plástica para Seladora</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastica-reciclada" title="Bobina Plástica Reciclada"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastica-reciclada-01.jpg" alt="Bobina Plástica Reciclada" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastica-reciclada" title="Bobina Plástica Reciclada">Bobina Plástica Reciclada</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastica-tubular" title="Bobina Plástica Tubular"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-plastica-tubular-01.jpg" alt="Bobina Plástica Tubular" /></a>
                                   <h2><a href="<?= $url; ?>bobina-plastica-tubular" title="Bobina Plástica Tubular">Bobina Plástica Tubular</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-reciclada" title="Bobina Reciclada"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-reciclada-01.jpg" alt="Bobina Reciclada" /></a>
                                   <h2><a href="<?= $url; ?>bobina-reciclada" title="Bobina Reciclada">Bobina Reciclada</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-recuperada" title="Bobina Recuperada"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-recuperada-01.jpg" alt="Bobina Recuperada" /></a>
                                   <h2><a href="<?= $url; ?>bobina-recuperada" title="Bobina Recuperada">Bobina Recuperada</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-refilada" title="Bobina Refilada"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-refilada-01.jpg" alt="Bobina Refilada" /></a>
                                   <h2><a href="<?= $url; ?>bobina-refilada" title="Bobina Refilada">Bobina Refilada</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-sanfonada" title="Bobina Sanfonada"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-sanfonada-01.jpg" alt="Bobina Sanfonada" /></a>
                                   <h2><a href="<?= $url; ?>bobina-sanfonada" title="Bobina Sanfonada">Bobina Sanfonada</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-shrink" title="Bobina Shrink"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-shrink-01.jpg" alt="Bobina Shrink" /></a>
                                   <h2><a href="<?= $url; ?>bobina-shrink" title="Bobina Shrink">Bobina Shrink</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-tubular" title="Bobina Tubular"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-tubular-01.jpg" alt="Bobina Tubular" /></a>
                                   <h2><a href="<?= $url; ?>bobina-tubular" title="Bobina Tubular">Bobina Tubular</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-tubular-plastica" title="Bobina Tubular Plástica"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobina-tubular-plastica-01.jpg" alt="Bobina Tubular Plástica" /></a>
                                   <h2><a href="<?= $url; ?>bobina-tubular-plastica" title="Bobina Tubular Plástica">Bobina Tubular Plástica</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-eva" title="Bobinas de Eva"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-eva-01.jpg" alt="Bobinas de Eva" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-eva" title="Bobinas de Eva">Bobinas de Eva</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-polipropileno" title="Bobinas de Polipropileno"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-polipropileno-01.jpg" alt="Bobinas de Polipropileno" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-polipropileno" title="Bobinas de Polipropileno">Bobinas de Polipropileno</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-pp" title="Bobinas de Pp"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-pp-01.jpg" alt="Bobinas de Pp" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-pp" title="Bobinas de Pp">Bobinas de Pp</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-personalizadas" title="Bobinas Personalizadas"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-personalizadas-01.jpg" alt="Bobinas Personalizadas" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-personalizadas" title="Bobinas Personalizadas">Bobinas Personalizadas</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-plasticas" title="Bobinas Plásticas"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-plasticas-01.jpg" alt="Bobinas Plásticas" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-plasticas" title="Bobinas Plásticas">Bobinas Plásticas</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-plasticas-embalagens" title="Bobinas Plásticas para Embalagens"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-plasticas-embalagens-01.jpg" alt="Bobinas Plásticas para Embalagens" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-plasticas-embalagens" title="Bobinas Plásticas para Embalagens">Bobinas Plásticas para Embalagens</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-plasticas-personalizadas" title="Bobinas Plásticas Personalizadas"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-plasticas-personalizadas-01.jpg" alt="Bobinas Plásticas Personalizadas" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-plasticas-personalizadas" title="Bobinas Plásticas Personalizadas">Bobinas Plásticas Personalizadas</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-plasticas-recicladas" title="Bobinas Plásticas Recicladas"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-plasticas-recicladas-01.jpg" alt="Bobinas Plásticas Recicladas" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-plasticas-recicladas" title="Bobinas Plásticas Recicladas">Bobinas Plásticas Recicladas</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-plasticas-tecnicas" title="Bobinas Plásticas Técnicas"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-plasticas-tecnicas-01.jpg" alt="Bobinas Plásticas Técnicas" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-plasticas-tecnicas" title="Bobinas Plásticas Técnicas">Bobinas Plásticas Técnicas</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-reciclados" title="Bobinas Reciclados"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-reciclados-01.jpg" alt="Bobinas Reciclados" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-reciclados" title="Bobinas Reciclados">Bobinas Reciclados</a></h2>
                              </li>
                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobinas-tecnicas" title="Bobinas Técnicas"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-tecnicas-01.jpg" alt="Bobinas Técnicas" /></a>
                                   <h2><a href="<?= $url; ?>bobinas-tecnicas" title="Bobinas Técnicas">Bobinas Técnicas</a></h2>
                              </li>

                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>comprar-sacolas-plasticas" title="comprar sacolas plásticas"><img data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-0.jpg" alt="comprar sacolas plásticas" class="lazyload" title="comprar sacolas plásticas" /></a>
                                   <h4><a href="<?= $url; ?>comprar-sacolas-plasticas" title="comprar sacolas plásticas">comprar sacolas plásticas/a></h4>
                              </li>




                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>embalagem-plastica-flexivel-para-alimento-preco" title="embalagem plástica flexível para alimento preço"><img data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-2.jpg" alt="embalagem plástica flexível para alimento preço" class="lazyload" title="embalagem plástica flexível para alimento preço" /></a>
                                   <h4><a href="<?= $url; ?>embalagem-plastica-flexivel-para-alimento-preco" title="embalagem plástica flexível para alimento preço">embalagem plástica flexível para alimento preço/a></h4>
                              </li>




                              <li>
                                   <a rel="nofollow" href="<?= $url; ?>bobina-plastica-preco" title="bobina plástica preço"><img data-src="<?= $url; ?>imagens/produtos/bobinas/thumb/bobinas-7.jpg" alt="bobina plástica preço" class="lazyload" title="bobina plástica preço" /></a>
                                   <h4><a href="<?= $url; ?>bobina-plastica-preco" title="bobina plástica preço">bobina plástica preço/a></h4>
                              </li>


                         </ul>

                         <? include('inc/saiba-mais.php'); ?>

                    </article>

                    <? include('inc/coluna-lateral-paginas.php'); ?>

                    <br class="clear" />

                    <? include('inc/regioes.php'); ?>

               </section>

          </main>



     </div><!-- .wrapper -->



     <?php include('inc/footer.php'); ?>


</body>

</html>