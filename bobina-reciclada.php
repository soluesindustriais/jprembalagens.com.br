<?php
	$h1    			= 'Bobina reciclada';
	$title 			= 'Bobina reciclada';
	$desc  			= 'A bobina reciclada é uma das maneiras mais econômicas para fazer a embalagem de produtos diversos, sendo muito eficiente para empacotar automaticamente';
	$key   			= 'bobina, reciclada, bobinas recicladas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas recicladas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>A <strong>bobina reciclada</strong> é uma opção ideal para reduzir gastos sem comprometer a qualidade da embalagem. Confira maiores informações.</p>
            
            <p>A embalagem é um aspecto fundamental na hora de vender um produto. Isso porque a embalagem deve ser resistente e oferecer toda a proteção necessária para que o produto não sofra qualquer tipo de alteração e nem de danos. Por isso, conte com a <strong>bobina reciclada</strong>.</p>

            <p>A <strong>bobina reciclada</strong> é uma das maneiras mais econômicas para fazer a embalagem de produtos diversos, sendo muito eficiente para empacotar automaticamente ou embalar peças e produtos de medidas variadas, permitindo melhor administração de estoque e baixas quantidades de reserva.</p>
            

            
            <p>A <strong>bobina reciclada</strong>, assim como a bobina que é feita com matéria-prima virgem, garante a integridade do produto durante o transporte e armazenamento, sendo bastante resistente tanto a quedas quanto a rasgos e rupturas.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>  
                      
            <h2>Modelos de bobina reciclada</h2>
            
            <p>A <strong>bobina reciclada</strong> pode ser feita com diferentes tipos de material. A <strong>bobina reciclada</strong> cristal, por exemplo, é feita a partir de aparas de material virgem com embalagens já recicladas. Neste caso, a bobina tem alguns pontos devido ao processo de reciclagem, e o aspecto da embalagem é amarelo claro, mas sem que haja perda na transparência.</p>
            
            <p>Outra opção é a <strong>bobina reciclada</strong> canela, feita a partir de plásticos reciclados e que mantém a resistência da embalagem graças a uma solda reforçada, o que proporciona segurança contra rasgos e rupturas. Neste modelo, a embalagem tem aspecto marrom claro e mantem a transparência, sendo possível visualizar o conteúdo interno.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Também é possível fazer bobina colorida, que não tem padrão de cor e não há mais transparência. Esta é uma das opções mais baratas de embalagem plástica flexível. E para adquirir a bobina, conte com a JPR Embalagens. A empresa atua no segmento de embalagens plásticas flexíveis a mais de 15 anos e leva até o cliente as melhores opções, com materiais de qualidade e preço em conta, proporcionando redução de custos.</p>
            
            <p>Na JPR Embalagens o atendimento é personalizado e totalmente voltado às necessidades e demandas de cada tipo de cliente. Saiba mais sobre os modelos de <strong>bobina reciclada</strong> e conheça as vantagens da JPR Embalagens entrando em contado com os consultores. Aproveite as vantagens e solicite já o seu orçamento.</p>

            
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>