<?php
	$h1    			= 'Bobinas de eva';
	$title 			= 'Bobinas de eva';
	$desc  			= 'As bobinas de eva proporcionam muito mais praticidade se comparado a adquirir o EVA de maneira solta, já que a bobina facilita tanto o manuseio quanto o transporte do material';
	$key   			= 'bobinas, eva, bonina em eva';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina em eva';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>As <strong>bobinas de eva</strong> são a opção ideal para empresas que precisam de economia e de praticidade. Saiba mais sobre este tipo de material.</p>

            <p>A embalagem é um ponto fundamental para a proteção do produto. Afinal, ela precisa manter as características originais do que está sendo embalado e garantir a proteção durante o transporte e o armazenamento. E, neste caso, as <strong>bobinas de eva</strong> são altamente recomendadas para a sua empresa.</p>
            
            <p>As <strong>bobinas de eva</strong> proporcionam muito mais praticidade se comparado a adquirir o EVA de maneira solta, já que a bobina facilita tanto o manuseio quanto o transporte do material. O EVA é um material com alta maciez, proporcionando agradável contato.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>As <strong>bobinas de eva</strong> se caracterizam pela flexibilidade e a leveza. Outra grande vantagem deste material é a alta resiliência, ou seja, o material facilmente volta ao formato original, inclusive quando é pressionado ou passa por algum outro tipo de contato.</p>
            
            <p>As <strong>bobinas de eva</strong> são um material que proporcionam muito conforto e que tem fino acabamento, sendo a opção ideal para indústrias de calçados (podem ser usadas na confecção dos calçados ou como palmilhas, por exemplo), além de também serem utilizadas para outros segmentos.</p>
            
            <h2>Bobinas de eva: saiba onde adquirir com preços em conta e ótimas condições</h2>
            
            <p>E para adquirir as <strong>bobinas de eva</strong>, aproveite os benefícios da JPR Embalagens. A empresa atua no segmento de embalagens plásticas flexíveis há mais de 15 anos, disponibilizando <strong>bobinas de eva</strong> e de outros materiais, filmes, sacolas e outros tipos de embalagem.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A equipe técnica da empresa está constantemente atualizada sobre o que há de mais moderno no segmento de embalagens. O objetivo é fazer com que o cliente possa reduzir perdas e custos e identificar oportunidades de melhoria.</p>
            
            <p>O atendimento da JPR Embalagens é totalmente personalizado, conforme as preferências, demandas e necessidades de cada tipo de cliente. Além disso, a empresa disponibiliza preços em conta e ótimas condições de pagamento, proporcionando excelente relação custo-benefício.</p>
            
            <p>Saiba mais sobre as vantagens e aplicações das <strong>bobinas de eva</strong> entrando em contato com um dos consultores técnicos. Aproveite os benefícios para solicitar já o seu orçamento, informando medida e quantidade que a sua empresa necessita.</p>

            
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>