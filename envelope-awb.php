<?php
	$h1    			= 'Envelope AWB';
	$title 			= 'Envelope AWB';
	$desc  			= 'O envelope AWB é fabricado em polietileno ou em polipropileno, transparente com várias linhas de adesivo na frente ou verso';
	$key   			= 'Envelopes AWB, Envelopes, Envelope, AWB';
	$var 			= 'Envelopes AWB';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 
                
                
                <br>   
                    
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>
                              
            <p>O <strong>envelope AWB</strong> é fabricado em polietileno ou em polipropileno, transparente com várias linhas de adesivo na frente ou verso. Temos também a opção de fazer os <strong>envelopes AWB 100% adesivado</strong>.</p> 
            
            
            <p>Também conhecido como envelope janela adesivado, o <strong>envelope AWB</strong> pode ser liso ou impresso em até 6 cores. Devido a sua praticidade, o <strong>envelope AWB</strong> é amplamente utilizado por empresas de courrier, empresas aéreas, empresas de entrega rápida, transportadoras, empresas exportadoras, entre outras. </p>
            
            <p>Fabricamos <strong>envelopes AWB</strong> de acordo com a necessidade de cada cliente, assim você pode criar a embalagem da maneira que desejar.</p> 	

            
            <h2>O principal uso dos envelopes AWB estão em:</h2>
            
            <ul class="list">
            	<li>Caixas;</li>
            	<li>Conhecimento de embarque aéreo;</li>
            	<li>Faturas diversas, entre outras.</li>
            </ul>
            
            <p>O <strong>envelope AWB</strong> pode ser confeccionado nas opções transparentes ou pigmentados. </p>
            

            
            <p>A função do <strong>envelope AWB</strong>, é facilitar a identificação dos produtos que contém dentro de caixas ou <strong>envelopes plásticos</strong>, pois a nota fiscal é colocada dentro do envelope e após isso é colada em caixas ou <a href="<?=$url;?>envelope-plastico" title="Envelope Plástico"><strong>envelopes plásticos</strong></a>, assim você ganha tempo no envio e na identificação das mercadorias.</p>            
            <p>Além do <strong>envelope AWB</strong>, trabalhamos com outras linhas de embalagens como <a href="<?=$url;?>sacos" title="Sacos"><strong>sacos</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, bobinas, embalagens especiais, todos lisos ou impressos de acordo com a sua necessidade.</p>
            
            <? $pasta = "imagens/produtos/"; ?>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Outras opções que também fabricamos na linha de envelopes são o <strong>envelope com fecho zip</strong>, <strong>envelope tala</strong>, <a href="<?=$url;?>envelope-inviolavel" title="Envelope Inviolável"><strong>envelope inviolável</strong></a>, <a href="<?=$url;?>envelope-plastico-ilhos" title="Envelope Com Ilhós"><strong>envelope com ilhós</strong></a>, <strong>envelope com botão</strong>. Somos especialistas na produção de <strong>envelopes em geral</strong>.</p>
            
            <p>Possuímos equipamentos de ultima geração para desenvolvimento de novas linhas de <strong>envelopes</strong>. Consulte-nos. Para receber um orçamento de <strong>envelope AWB</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade que deseja utilizar.</p>
                        
             
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>