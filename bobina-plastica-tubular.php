<?php
	$h1    			= 'Bobina plástica tubular';
	$title 			= 'Bobina plástica tubular';
	$desc  			= 'A bobina plástica tubular é um tipo de embalagem bastante eficiente para empacotar automaticamente ou embalar peças de medidas variáveis';
	$key   			= 'bobina, plástica, tubular, bobina plástica tubular';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina plástica tubular';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Embalagem plástica e que permite melhor controle de estoque, a <strong>bobina plástica tubular</strong> é uma opção vantajosa para a sua empresa. Confira.</p>

            <p>É importante sempre investir em embalagens de qualidade comprovada, que proporcionem proteção para que seu produto mantenha as características visuais. Levando tudo isso em conta, a <strong>bobina plástica tubular</strong> se destaca como uma opção eficiente para a sua empresa.</p>
            
            <p>A <strong>bobina plástica tubular</strong> é um tipo de embalagem bastante eficiente para empacotar automaticamente ou embalar peças de medidas variáveis. Além disso, esta embalagem permite administração melhor do estoque, com quantidades reduzidas de reserva, o que resulta em melhor planejamento de custos.</p>

			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina plástica tubular</strong> é fabricada em diversas resinas, PEBD, PEAD, PEBDL e PP. Quando feita em polietileno, é largamente utilizada na área de bebidas, alimentos, leites e farinha, por ser um plástico atóxico e resistente, o que permite embalagem de qualquer tipo de produto.</p>
            
            <p>Já quanto feita em polipropileno, a <strong>bobina plástica tubular</strong> se destaca pelo brilho e pela transparência, sendo amplamente empregada nas indústrias alimentícias, além de gráficas e confecções. A <strong>bobina plástica tubular</strong> pode ser feita com matéria-prima virgem ou com material reciclado, que contribui com o meio ambiente e reduz custos.</p>
            
            <h2>Personalização da bobina plástica tubular</h2>
                        
            <p>A <strong>bobina plástica tubular</strong> ainda pode ser personalizada, sendo fabricada em diversos tamanhos e cores. Há opção de embalagem transparente ou pigmentadas, lisas ou impressas em diferentes cores.</p>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para fazer a personalização, conte com a JPR Embalagens. A empresa, que atua há mais de 15 anos na área de embalagens plásticas flexíveis, confecciona <strong>bobina plástica tubular</strong> com impressão de excelente qualidade e ótimo custos. A equipe da JPR Embalagens está sempre em busca de produtos que prezam pela excelência e pelo baixo custo, resultando em excelente relação custo-benefício para os clientes. O objetivo é identificar oportunidades de melhoria, redução de perdas e custos e garantir uma embalagem de qualidade.</p>
            
            <p>A JPR Embalagens disponibiliza um atendimento personalizado, conforme preferências e necessidades de cada cliente. Saiba mais entrando em contato com a equipe de profissionais e aproveite as vantagens para solicitar já o seu orçamento.</p>

            
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>