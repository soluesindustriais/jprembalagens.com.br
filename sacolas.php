<?
	$h1    		= 'Sacolas';
	$title 		= 'Sacolas';
	$desc  		= 'Na JPR Embalagens você encontra uma completa linha de Sacolas. Em nossa linha você encontra Sacolas personalizadas, fabricadas sob medida.';
	$key   		= 'Sacolas, Sacolas sob medida, fábrica de Sacolas, Sacolas Personalizadas';
	$var 		= 'Sacolas';
	
	include('inc/head.php');
?>

</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
               
               
                <?=$caminhoCategoria?>
				<article>
                
                <h1><?=$h1?></h1>   
                  
            <h2>A JPR Embalagens traz para o mercado uma completa linha de sacolas.</h2>
            
            <p>Em nossa linha de produtos você encontra <strong>sacolas fabricados sob medida</strong>, de acordo com a necessidade da sua empresa.</p><br/><br/>
            
            <p>Conheça nossa linha de <strong>sacolas</strong>:</p>
            <ul class="thumbnails">
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-alca-fita" title="Sacola Alça Fita"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-alca-fita-01.jpg" alt="Sacola Alça Fita" title="Sacola Alça Fita" /></a>
                      <h4><a href="<?=$url;?>sacola-alca-fita" title="Sacola Alça Fita">Sacola Alça Fita</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-alca-vazada" title="Sacola Alça Vazada"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-alca-vazada-01.jpg" alt="Sacola Alça Vazada" title="Sacola Alça Vazada" /></a>
                      <h4><a href="<?=$url;?>sacola-alca-vazada" title="Sacola Alça Vazada">Sacola Alça Vazada</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-boca-palhaco" title="Sacola Boca de Palhaço"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-boca-palhaco-01.jpg" alt="Sacola Boca de Palhaço" title="Sacola Boca de Palhaço" /></a>
                      <h4><a href="<?=$url;?>sacola-boca-palhaco" title="Sacola Boca de Palhaço">Sacola Boca de Palhaço</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-camiseta" title="Sacola Camiseta"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-camiseta-01.jpg" alt="Sacola Camiseta" title="Sacola Camiseta" /></a>
                      <h4><a href="<?=$url;?>sacola-camiseta" title="Sacola Camiseta">Sacola Camiseta</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-impressa" title="Sacola Impressa"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-impressa-01.jpg" alt="Sacola Impressa" title="Sacola Impressa" /></a>
                      <h4><a href="<?=$url;?>sacola-impressa" title="Sacola Impressa">Sacola Impressa</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-leitosa" title="Sacola Leitosa"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-leitosa-01.jpg" alt="Sacola Leitosa" title="Sacola Leitosa" /></a>
                      <h4><a href="<?=$url;?>sacola-leitosa" title="Sacola Leitosa">Sacola Leitosa</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-oxibiodegradavel" title="Sacola Oxibiodegradavel"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-oxibiodegradavel-01.jpg" alt="Sacola Oxibiodegradavel" title="Sacola Oxibiodegradavel" /></a>
                      <h4><a href="<?=$url;?>sacola-oxibiodegradavel" title="Sacola Oxibiodegradavel">Sacola Oxibiodegradavel</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-personalizada" title="Sacola Personalizada"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-personalizada-01.jpg" alt="Sacola Personalizada" title="Sacola Personalizada" /></a>
                      <h4><a href="<?=$url;?>sacola-personalizada" title="Sacola Personalizada">Sacola Personalizada</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-01.jpg" alt="Sacola Plastica" title="Sacola Plastica" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica" title="Sacola Plastica">Sacola Plastica</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-alta-densidade" title="Sacola Plastica Alta Densidade"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-alta-densidade-01.jpg" alt="Sacola Plastica Alta Densidade" title="Sacola Plastica Alta Densidade" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-alta-densidade" title="Sacola Plastica Alta Densidade">Sacola Plastica Alta Densidade</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-branca" title="Sacola Plastica Branca"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-branca-01.jpg" alt="Sacola Plastica Branca" title="Sacola Plastica Branca" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-branca" title="Sacola Plastica Branca">Sacola Plastica Branca</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-ziper" title="Sacola Plastica Com Ziper"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-ziper-01.jpg" alt="Sacola Plastica Com Ziper" title="Sacola Plastica Com Ziper" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-ziper" title="Sacola Plastica Com Ziper">Sacola Plastica Com Ziper</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-oxibiodegradavel" title="Sacola Plastica Oxibiodegradavel"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-oxibiodegradavel-01.jpg" alt="Sacola Plastica Oxibiodegradavel" title="Sacola Plastica Oxibiodegradavel" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-oxibiodegradavel" title="Sacola Plastica Oxibiodegradavel">Sacola Plastica Oxibiodegradavel</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-preta" title="Sacola Plastica Preta"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-preta-01.jpg" alt="Sacola Plastica Preta" title="Sacola Plastica Preta" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-preta" title="Sacola Plastica Preta">Sacola Plastica Preta</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-reciclada" title="Sacola Plastica Reciclada"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-reciclada-01.jpg" alt="Sacola Plastica Reciclada" title="Sacola Plastica Reciclada" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-reciclada" title="Sacola Plastica Reciclada">Sacola Plastica Reciclada</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-tipo-camiseta" title="Sacola Plastica Tipo Camiseta"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-tipo-camiseta-01.jpg" alt="Sacola Plastica Tipo Camiseta" title="Sacola Plastica Tipo Camiseta" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-tipo-camiseta" title="Sacola Plastica Tipo Camiseta">Sacola Plastica Tipo Camiseta</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-transparente" title="Sacola Plastica Transparente"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-transparente-01.jpg" alt="Sacola Plastica Transparente" title="Sacola Plastica Transparente" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-transparente" title="Sacola Plastica Transparente">Sacola Plastica Transparente</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-vazada" title="Sacola Plastica Vazada"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-vazada-01.jpg" alt="Sacola Plastica Vazada" title="Sacola Plastica Vazada" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-vazada" title="Sacola Plastica Vazada">Sacola Plastica Vazada</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-plastica-zip-01.jpg" alt="Sacola Plastica Zip" title="Sacola Plastica Zip" /></a>
                      <h4><a href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip">Sacola Plastica Zip</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-reciclada" title="Sacola Reciclada"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-reciclada-01.jpg" alt="Sacola Reciclada" title="Sacola Reciclada" /></a>
                      <h4><a href="<?=$url;?>sacola-reciclada" title="Sacola Reciclada">Sacola Reciclada</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>sacola-vazada" title="Sacola Vazada"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/sacola-vazada-01.jpg" alt="Sacola Vazada" title="Sacola Vazada" /></a>
                      <h4><a href="<?=$url;?>sacola-vazada" title="Sacola Vazada">Sacola Vazada</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>fabrica-sacolas-plasticas" title="Fábrica de Sacolas Plasticas"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/fabrica-sacolas-plasticas-01.jpg" alt="Fábrica de Sacolas Plasticas" title="Fábrica de Sacolas Plasticas" /></a>
                      <h4><a href="<?=$url;?>fabrica-sacolas-plasticas" title="Fábrica de Sacolas Plasticas">Fábrica de Sacolas Plasticas</a></h4>
                 </li>
                 <li>
                      <a rel="nofollow" href="<?=$url;?>industria-sacolas-plasticas" title="Industria Sacolas Plasticas"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacolas/thumb/industria-sacolas-plasticas-01.jpg" alt="Industria Sacolas Plasticas" title="Industria Sacolas Plasticas" /></a>
                      <h4><a href="<?=$url;?>industria-sacolas-plasticas" title="Industria Sacolas Plasticas">Industria Sacolas Plasticas</a></h4>
                 </li>
            </ul>
        	            	
			<? include('inc/saiba-mais.php');?>
            
            </article>
            	
			<? include('inc/coluna-lateral-paginas.php');?>   
               
           	<br class="clear" />  
                    
            <? include('inc/regioes.php');?>
            
        	</section>
            
        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>