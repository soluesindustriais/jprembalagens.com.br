<?php
$h1    			= 'Sacola camiseta';
$title 			= 'Sacola camiseta';
$desc  			= 'A sacola camiseta, é uma das sacolas mais usadas pelo mundo. Você pode utilizá-la como uma forma de divulgação da sua marca, levando a sua marca para novos mercados.';
$key   			= 'Sacola, camiseta, Sacolas camisetas, Sacola camiseta colorida, Sacola camiseta personalizada';
$var 			= 'Sacolas camisetas';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>   

             <p>Umas das <strong>sacolas</strong> mais usadas no mundo é a <strong>sacola camiseta</strong>. Com um sistema de fácil abertura, essa embalagem é amplamente utilizada em supermercados, lojas de brinquedos, lojas de roupas entre muitas outras.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>São super resistentes aos rasgo e a ruptura, devido ao sistema reforçado que é produzido a alça para transporte da <strong>sacola camiseta</strong>.</p>
             <p>Para ficar cada vez mais próximo dos seus clientes, utilize a <a href="<?=$url;?>sacola-personalizada" title="Sacola Personalizada"><strong>sacola camiseta personalizada</strong></a>, esta é uma forma de divulgar a sua marca, levando a sua empresa para novos mercados.</p>
             <h2>Sacolas camisetas biodegradaveis</h2>
             <p>Uma alternativa sustentável que esta sendo adotava por diversas empresas, são as <strong>sacolas camisetas oxi-biodegradaveis</strong>. Este é um aditivo adicionado no processo de produção que faz com que a sacola em contato com o meio ambiente se degrade em um curto espaço de tempo, podendo levar até 6 meses para se decompor, ao contrário das <strong>sacolas comuns</strong> que podem levar mais de 100 anos para se degradarem. Adote você também esta ideia, e contribua para um planeta mais sustentável. </p>
             <p>Além da <strong>sacola camiseta</strong>, a JPR Embalagens trabalha com outros modelos de <strong>sacola</strong> como, <a href="<?=$url;?>sacola-boca-palhaco" title="Sacola Boca de Palhaço"><strong>sacola boca de palhaço</strong></a>, <a href="<?=$url;?>sacola-alca-fita" title="Sacola Alça Fita"><strong>sacola alça fita</strong></a>, <a href="<?=$url;?>sacola-vazada" title="Sacola Vazada"><strong>sacola vazada</strong></a>, <strong>sacola com talas ou ilhós</strong>, entre outros modelos.</p>
             <p>Nossa quantidade mínima de produção de <strong>sacola camiseta impressa</strong> são de 250kg e lisa 150 kg.</p>
             <p>Para receber um orçamento de <strong>sacola camiseta</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>