<?php
$h1    			= 'Indústria sacolas plásticas';
$title 			= 'Indústria sacolas plásticas';
$desc  			= 'Localizada na Grande São Paulo, a indústria sacolas plásticas JPR Embalagens, fabrica os mais diversos modelos de sacolas, todas sob medida';
$key   			= 'Indústria, sacolas, plásticas, Indústria de sacola plástica';
$var 			= 'Indústria de sacola plástica';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>
<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>

                       <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     

             <br> 

             <h2>JPR Embalagens, Industria de sacolas plásticas sob medida</h2>
             <p>Localizada na Grande São Paulo, a <strong>indústria sacolas plásticas</strong> JPR Embalagens, fabrica os mais diversos modelos de <strong>sacolas</strong>, todas sob medida, de acordo com a necessidade de cada cliente.</p>
             <p>Para você que busca preço, qualidade e pontualidade na entrega, consulte a JPR Embalagens, esta é a nossa especialidade.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>

             <h2>Sustentabilidade</h2>
             <p>Para contribuir com o meio ambiente, a <strong>indústria sacolas plásticas</strong>, esta produzindo diversos modelos de embalagens com o aditivo oxi-biodegradavel. Nesta opção, os <a href="<?=$url;?>sacos" title="Sacos"><strong>sacos</strong></a>, sacolas ou bobinas plásticas, fabricados em PEBD, PEAD e PP, podem ser produzidos com este aditivo. Durante a produção dessas embalagens, que é realizada na <strong>indústria sacolas plásticas</strong>, é adicionado uma porcentagem do aditivo, e ele faz com que a embalagem plástica se degrade em curto espaço de tempo, em media seis meses, sem deixar resíduos nocivos ao meio ambiente. Esta é uma especialidade da <strong>indústria de sacolas plásticas</strong> comprometida com o meio ambiente.</p>
             <p>Além disso, a JPR Embalagens produz uma ampla linha de embalagens recicladas. As embalagens podem ser produzidas em três tipos de reciclados:</p>
             <ul class="list">
                <li><strong>Reciclado cristal</strong>: produzido a partir das aparas do material virgem, nesta opção, a embalagem fica com alguns pontos e com uma cor amarelada, devido ao próprio processo de reciclagem que a embalagem passa. Esse tipo de material é bastante empregado na <strong>indústria sacolas plásticas</strong>.</li>
                <li><strong>Reciclado canela</strong>: produzido a partir das aparadas do reciclado cristal, e nesta opção a embalagem também ficará com pontos, e com uma cor de canela (marrom claro), porem com transparência, ou seja, possível ver perfeitamente o produto dentro da embalagem.</li>
                <li><strong>Reciclado colorido</strong>: este reciclado é um dos mais em conta, e é o preferido na <strong>indústria de sacolas plásticas</strong>, por ser produzido através de vários tipos de embalagens, como sacos, sacolas, embalagens de alimentos em geral, e dessa forma a embalagem perde completamente a transparência. Geralmente a embalagem final fica na cor cinza, verde escuro, ou até mesmo preto. </li>
            </ul>
            <p>Esta linha de reciclados da <strong>indústria sacolas plásticas</strong>, também é uma forma ecologicamente correta de contribuir com o meio ambiente.</p>  
            <p>Para receber um orçamento da <strong>indústria de sacolas plásticas</strong>, basta possuir as medidas (largura x comprimento x espessura), e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            

        </article>

        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  

        <br class="clear" />  
        


        <?php include('inc/regioes.php');?>

        <?php include('inc/copyright.php');?>


    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>