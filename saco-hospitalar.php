<?php
$h1    			= 'Saco hospitalar';
$title 			= 'Saco hospitalar';
$desc  			= 'Trabalhamos com uma ampla linha de saco hospitalar. Além dessa linha hospitalar, a JPR Embalagens trabalha com diversos tipos de embalagem.';
$key   			= 'Sacos hospitalares, Saco, sacos, hospitalar, saco para hospitais, saco para lixo hospitalar';
$var 			= 'Sacos hospitalares';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             

             
             <p>Trabalhamos com uma ampla linha de <strong>saco hospitalar</strong>. Nossas embalagens são confeccionadas nos padrões da Anvisa e do Ministério da Saúde.</p>
             
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <h2>Abaixo relacionamos os modelos de saco hospitalar que fabricamos:</h2>
             <ul class="list">
                <li><strong>Saco hospitalar para lixo comum</strong>: São fabricamos com matéria-prima reciclada, disponível na cor preta. Caso deseje em outras cores, basta consultar o nosso departamento comercial.</li>
                <li><a href="<?=$url;?>saco-infectante" title="Saco Infectante"><strong>Saco hospitalar para lixo infectante</strong></a>: Constituído em polietileno de alta densidade virgem, com capacidades para 15, 30, 50, 100, 200 e 240 litros.</li>
                <li><a href="<?=$url;?>saco-hamper" title="Saco Hamper"><strong>Saco hospitalar hamper</strong></a>: Próprios para embalar e transportar roupas utilizadas e ambientes médico-hospitalares. Capacidade: 120 litros</li>
                <li><strong>Saco hospitalar</strong> – Capa para óbito: Fabricados em polietileno de baixa densidade, a capa para óbito é reforçada e acompanha etiqueta de identificação. Fabricadas nas medidas RN, P, M e G.</li>
            </ul>
            <p>Além da nossa linha de <strong>saco hospitalar</strong>, a JPR Embalagens trabalha com diversos tipos de embalagem, como <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plásticos</strong></a>, <a href="<?=$url;?>envelopes" title="Envelopes"><strong>envelopes</strong></a>, filmes e bobinas, reciclados, embalagens especiais, entre outras. Todas podem ser lisas ou impressas em até 6 cores.</p>
            <p>Para receber nosso orçamento de <strong>saco hospitalar</strong>, basta entrar em contato com um dos nossos consultores, e informar modelo de embalagem e a quantidade desejada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>