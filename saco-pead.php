<?php
$h1    			= 'Saco PEAD';
$title 			= 'Saco PEAD';
$desc  			= 'Fabricamos saco PEAD com matéria-prima virgem ou reciclada, são muito utilizados para envio de mala direta, revistas, entre outros';
$key   			= 'Sacos PEAD, Saco, sacos, PEAD, saco PEAD reciclado';
$var 			= 'Sacos PEAD';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             
             <p>Fabricamos <strong>saco PEAD</strong> com matéria-prima virgem ou reciclada, são muito utilizados para envio de mala direta, revistas, jornais, objetos leves, proteção de eletrodoméstico, entre outros.</p>
             
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>
             <p>Os <strong>sacos PEAD</strong> podem ser lisos ou personalizados em até 6 cores. Além disso, o <strong>PEAD</strong> é a matéria-prima com maior resistência a tração, por isso é amplamente utilizado para a fabricação de sacolas, bobinas picotadas, entre outros.</p>
             <p>Se você precisa de <strong>saco PEAD transparente</strong>, mas não armazena produtos alimentícios ou medicinais e deseja contribuir com a natureza, utilize o reciclado cristal que te garante total transparência sem o aspecto cristalino, mas com uma grande contribuição ao planeta. A grande vantagem em se utilizado <strong>saco PEAD reciclado</strong>, é que além de reduzir até 30% os seus custos em relação ao material virgem, você poderá ter um novo argumento de venda.</p>
             <p>Por ser uma embalagem versátil, o <strong>saco PEAD</strong> pode ser lacrado com uma seladora manual ou com aba adesiva.</p> 
             <p>São fabricados sob medida, desta forma você pode obter a embalagem na medida e com a impressão que desejar.</p>
             <p>Além de <strong>sacos PEAD</strong>, a JPR Embalagens fabrica outras linhas de embalagem, como <a href="<?=$url;?>saco-pebd" title="Saco PEBD"><strong>saco PEBD</strong></a>, <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança">envelopes de segurança</a>, <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plásticos</strong></a> em diversos modelos e cores, embalagens espciais, filme e bobinas.</p> 
             <p>Nossa quantidade mínima de fabricação para <strong>saco PEAD</strong> é de 250kg impresso e 150kg liso.</p>
             <p>Para receber um orçamento de <strong>saco PEAD</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade desejada.</p>

             
             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>