<?php
$h1    			= 'Saco plástico';
$title 			= 'Saco plástico';
$desc  			= 'Produzimos saco plástico de acordo com a necessidade de cada cliente, este é um produto amplamente utilizado em vários segmentos';
$key   			= 'Sacos plásticos, Saco, sacos, plástico, saco plástico reciclado';
$var 			= 'Sacos plásticos';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             <p>Produzimos <strong>saco plástico</strong> de acordo com a necessidade de cada cliente, este é um produto amplamente utilizado em vários segmentos. Pode ser fabricado em <a href="<?=$url;?>saco-pebd" title="Saco PEBD"><strong>PEBD</strong></a>, <a href="<?=$url;?>saco-pead" title="Saco PEAD"><strong>PEAD</strong></a>, <a href="<?=$url;?>saco-pp" title="Saco PP"><strong>PP</strong></a>, <a href="<?=$url;?>saco-bopp-transparente" title="Saco BOPP Transparente"><strong>BOPP</strong></a>, laminado, entre muitas outras opções.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>

             <p>Por ser um produto versátil, pode ser liso ou impresso em até 6 cores. </p>
             <h2>Veja abaixo alguns modelos e aplicações dos sacos plástico:</h2>
             <ul class="list">
                <li><a href="<?=$url;?>saco-pp" title="Saco PP"><strong>Saco PP</strong></a> (polipropileno) é muito empregado pelas indústrias alimentícias, gráficas e confecções, devido a brilho e transparente que a embalagem possui.</li>
                <li><a href="<?=$url;?>saco-plastico-pe" title="Saco Plastico Pe">Saco Plastico Pe</a> (polietileno), é uma das embalagens mais empregadas em alimentos, bebidas, leites e farináceos, e também para produtos metalúrgicos e automotivos. O polietileno é um plástico atóxico e resistente. Pode ser utilizado para embalar qualquer tipo de produto.</li>
                <li><a href="<?=$url;?>saco-bopp-transparente" title="Saco BOPP Transparente"><strong>Saco plástico BOPP</strong></a>, é amplamente utilizado no mercado de varejo, como embalagem de presente, podendo ser metalizado ou perolizado. </li>
            </ul>
            <p>Para contribuir com o meio ambiente, utilize <strong>saco plástico reciclado</strong>. Pode ser produzido em dois tipos, o reciclado cristal e o reciclado canela. O <strong>saco plástico reciclado cristal</strong>, é produzido a partir das aparas do material virgem, e também pode ser personalizado e por ser reciclado, a cor da embalagem altera para amarelo claro, porem a embalagem continuará transparente sendo possível visualizar o que há dentro. Já o <strong>saco plástico reciclado canela</strong> é produzido a partir das aparas do reciclado cristal, sua cor altera para canela, por isso o produto recebeu este nome, e mesmo assim a embalagem continuará tendo total transparência.</p>
            <p>Para transportes de produtos, ele pode ser lacrado através de uma seladora, ou para obter uma embalagem prática, poderá ser produzido com aba adesiva permanente ou tipo “abre e fecha”. A aba adesiva permanente é inviolável, e para se violar a embalagem será necessário danificá-la, e a aba adesiva abre e fecha, pode ser aberta diversas vezes, como é no caso das embalagens de roupas.</p>
            <p>Nosso lote mínimo de produção são de 100kg para <strong>saco plástico</strong> liso e 300kg impresso.</p>
            <p>Para receber um orçamento de <strong>saco plástico</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>