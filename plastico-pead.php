<?php
$h1    			= 'Plástico pead';
$title 			= 'Plástico pead';
$desc  			= 'O plástico pead é uma embalagem bastante versátil e que pode ter diferentes tipos de fabricação: com matéria prima ou reciclada, com aspecto visual liso ou personalizado.';
$key   			= 'plástico, pead, plásticos pead';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Plásticos pead';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoPlastico?>
              <article>
             <h1><?=$h1?></h1>     
             <br>   
             
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Embalagem versátil, é largamente usada em diversos segmentos. Confira maiores informações e conheça as vantagens.</p>
            
            <p>É importante oferecer para o cliente uma embalagem que se destaque pela resistência e pela qualidade, para que ela tenha um aspecto visual agradável e faça a proteção adequada do que está sendo embalado. Por isso, conheça o <strong>plástico pead</strong>.</p>
            
            <p>O <strong>plástico pead</strong> é uma embalagem bastante versátil e que pode ter diferentes tipos de fabricação: com matéria prima ou reciclada, com aspecto visual liso ou personalizado em até seis cores, o que contribui para melhor divulgação da sua marca e fixação para o cliente.</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O material usado é o PEAD, fazendo com que a embalagem tenha grande resistência à tração. Por causa desta característica, o <strong>plástico pead</strong> é amplamente usado para fabricar sacolas, bobinas picotadas, entre outros. A utilização do <strong>plástico pead</strong> é bastante ampla no mercado. Alguns exemplos são o uso para envio de mala direta, jornais e revistas, objetos leves e para proteção de eletrodomésticos, entre outros.</p>
            
            <p>Em caso de empresas que não trabalham com produtos alimentícios nem medicinais, uma opção é o <strong>plástico pead</strong> reciclado, que reduz os custos de embalagem em até 30% sem perder em qualidade e contribuindo com o meio ambiente. Aliás, o alinhamento da sua empresa com as causas ambientais é uma forma de melhorar ainda mais a imagem da sua marca no mercado.</p>
            
            <h2>Plástico pead com preços em conta</h2>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para adquirir o <strong>plástico pead</strong>, conte com os benefícios da JPR Embalagens. Com mais de 15 anos de experiência no mercado, a empresa tem como objetivo identificar oportunidades de melhoria que levem à redução de perdas e de custos.</p>
            
            <p>Para isso, conta com equipe sempre atualizada e equipamentos modernos, proporcionando embalagens plásticas flexíveis de qualidade e com preço em conta, resultando em excelente relação custo-benefício para o cliente.</p>
            
            <p>Outra vantagem é a possibilidade de personalização do produto, conforme as necessidades e as preferências de cada cliente e as ótimas condições de pagamento. A empresa disponibiliza um atendimento totalmente personalizado. Saiba mais e aproveite as condições entrando em contato com um dos consultores e solicitando já o seu orçamento, informando medidas e quantidade do produto que a sua empresa necessita.</p>
            
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>