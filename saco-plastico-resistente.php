<?php
$h1    			= 'Saco plástico resistente';
$title 			= 'Saco plástico resistente';
$desc  			= 'O saco plástico resistente possue alta resistência à tração, impacto, são flexíveis e resistentes ao frio. Podem ser lisos ou impressos em até 6 cores.';
$key   			= 'Sacos plásticos resistentes, Saco, sacos, plástico, resistente';
$var 			= 'Sacos plásticos resistentes';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             
             <p>Fabricamos <strong>saco plástico resistente</strong> em polietileno de baixa densidade (PEBD) ou em alta densidade (PEAD). São próprios para suportar pesos variados entre 10 e 30kg.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>Podem ser lisos ou impressos em até 6 cores, e fabricados na cor transparente ou pigmentado em diversas cores.</p>
             <p>Essa embalagem é amplamente utilizado por indústrias metalúrgicas, automotivas, construção civil, química, têxtil entre outras.</p> 
             <p>Geralmente, são produzidos com espessura a partir de 0,200 mm (200 micras), desta forma obtemos uma <strong>embalagem resistente</strong> e homogênea, além de obterem uma solda reforçando, impedindo que o produto migre para fora da embalagem. Os <strong>sacos plásticos resistentes</strong> possuem alta resistência à tração, impacto, são flexíveis e resistentes ao frio.</p>
             <p>Muitas empresas utilizam o <strong>saco plástico resistente</strong> produzido com matéria-prima reciclada. É uma forma ecológica de contribuirmos com o meio ambiente, além do plástico reciclado proporcionar redução de custos, pois a matéria-prima reciclada é mais em conta.</p>
             <h2>Veja algumas opções do saco plástico resistente reciclado:</h2>
             <ul class="list">
                <li><strong>saco plástico resistente reciclado cristal</strong>: produzido a partir das aparar do material virgem e também com embalagens que já foram recicladas, como embalagens de alimentos, sacolas plásticas e sacarias em geral. O material fica com alguns pontos, devido o processo de reciclagem que ele passa, e a cor da embalagem altera para amarelo claro, porem continua com transparência, sendo possível visualizar o produto dentro da embalagem.</li>
                <li><strong>saco plástico resistente reciclado canela</strong>: produzido com as aparas de plásticos reciclados, o reciclado canela continua resistente e proporciona segurança ao rasgo e a ruptura da embalagem, pois possui uma solda reforçada. Também fica com alguns pontos devido o processo de reciclagem que ele passa, e a cor da embalagem altera para marrom claro (cor de canela), porem continua com transparência, sendo possível visualizar o produto dentro da embalagem.</li>
                <li><strong>saco plástico resistente reciclado colorido</strong>: produzido com uma mistura de embalagens recicladas, o reciclado colorido fica sem padrão de cor e sem transparência. É uma das matérias-primas mais em conta que existe na linha das embalagens plásticas flexíveis.</li>
            </ul>
            <p>Além de <strong>saco plástico resistente</strong>, trabalhamos com uma ampla linha de embalagens, como <a href="<?=$url;?>envelopes" title="Envelopes"><strong>Envelopes</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>Sacolas</strong></a>, embalagens especiais, filmes e bobinas.</p>
            <p>Nossa quantidade mínima de produção são de 100kg para <strong>saco plástico resistente liso</strong> e 250kg impresso.</p>
            <p>Para receber um orçamento de saco <strong>plástico resistente</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>



            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>