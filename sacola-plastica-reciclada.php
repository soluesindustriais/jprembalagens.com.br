<?php
$h1    			= 'Sacola plástica reciclada';
$title 			= 'Sacola plástica reciclada';
$desc  			= 'Sacola plástica reciclada é uma das alternativas para reduzir seus custos com embalagens. São produzidas a partir do processo de reciclagem de outras embalagens.';
$key   			= 'Sacola, plástica, reciclada, Sacolas plásticas recicladas, sacola plástica reciclada sob medida';
$var 			= 'Sacolas plásticas recicladas';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>   
             
             <p>Fabricamos <strong>sacola plástica reciclada</strong> sob medida, de acordo com a necessidade de cada cliente.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>A <strong>sacola plástica reciclada</strong> é uma das alternativas para reduzir seus custos com embalagens. São produzidas a partir do processo de reciclagem de outras embalagens, como embalagens de alimentos, sacarias em geral, <strong>sacolas</strong>, entre outros. É uma forma ecologicamente correta para contribuir com o meio ambiente.</p>
             <p>Podem ser lisas ou impressas em até 6 cores, além da <strong>sacola plástica reciclada</strong> poder ser produzidas nas cores transparente ou pigmentadas em diversas cores.</p>
             <p>Outra alternativa para contribuirmos com o meio ambiente, são as <strong>sacolas plásticas recicladas</strong> com aditivo oxibiodegradavel. Nesta opção, a <strong>sacola plástica reciclada</strong> se degrada em curto espaço de tempo, podendo levar até 6 meses para se degradar completamente na natureza, sem deixar resíduos nocivos ao meio ambiente.</p>
             <p>São próprias para divulgação em feiras, muito utilizada também por supermercados, padarias, lojas de artigos em geral, além disso, são resistente ao rasgo e a ruptura, mantendo a integridade do produto até o seu destino final.</p>
             <p>Além de <strong>sacolas plásticas reciclada</strong>, a JPR Embalagens possui uma ampla linha de embalagens como, <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plásticos</strong></a>, <a href="<?=$url;?>envelope-plastico" title="Envelope Plástico"><strong>envelope plástico</strong></a>, embalagens especiais, filme e bobinas, entre outros.</p>
             <p>Para <strong>sacola plástica reciclada personalizada</strong>, nosso lote mínimo de produção são de 250kg e sem impressão 150kg.</p>
             <p>Para receber um orçamento de <strong>sacola plástica reciclada</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>