<?php
$h1    			= 'Sacola impressa';
$title 			= 'Sacola impressa';
$desc  			= 'A sacola impressa é própria para divulgação, feiras, supermercados, lojas e outros estabelecimentos que procuram manter sua imagem sempre próxima a seus clientes.';
$key   			= 'Sacola, impressa, Sacolas impressas, Sacola impressa colorida, Sacola impressa personalizada';
$var 			= 'Sacolas impressas';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             <p>Somos <strong>fabricante de sacola impressa</strong> em diversos formatos e cores. São produzidas em plásticos de baixa densidade ou alta densidade. </p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>A <strong>sacola impressa</strong> é uma forma de você divulgar a sua marca, te levando para novos mercados e novos clientes. Podem ser personalizadas em até 6 cores.</p>
             <h2> Veja alguns modelos que trabalhamos:</h2>
             <ul class="list">
                <li><strong>Sacola impressa com alça vazada</strong></li>
                <li><strong>Sacola impressa com alça camiseta</strong></li>
                <li><strong>Sacola impressa com alça fita</strong></li>
                <li><strong>Sacola impressa com cordão</strong></li>
            </ul>
            <h2>Sacola impressa na publicidade</h2>
            <p>A <strong>sacola impressa</strong> é própria para divulgação, feiras, supermercados, lojas e outros estabelecimentos que procuram, além de oferecer um melhor serviço, manter sua imagem sempre próxima a seus clientes.</p>
            <p>A vantagem em se utilizar esta embalagem produzida em plastico, é devido à durabilidade e resistência que o plástico possui, além de poder ser reutilizada varias vezes sem danificar e perder o formato. </p>
            <p>Nossa quantidade mínima de produção de <strong>sacola impressa</strong> são de 250kg e lisa 150 kg.</p>
            <p>Para receber um orçamento de <strong>sacola impressa</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>