<?php
	$h1    			= 'Envelope correios';
	$title 			= 'Envelope correios';
	$desc  			= 'O envelope correios são invioláveis por serem compostos de duas ou três camadas de polietileno, a fim de proporcionar maior resistência ao rasgo e ruptura';
	$key   			= 'Envelopes correios, Envelopes, Envelope, correios';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes correios';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                           
            <p><strong>Envelope correios</strong> feito em polietileno de alta resistência para transporte de produtos de valor, pode ser liso ou impresso em até 6 cores.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            

            <p>Também conhecido como <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelope de segurança</strong></a>, o <strong>Envelope correios</strong> é fabricado de acordo com a necessidade de cada cliente. O <strong>Envelope correios</strong> pode ser confeccionado com aba de segurança nível 1, 2, 3, 4, 5 e numeração sequencial.</p>
            
            <p>Os <strong>envelopes correios</strong> são invioláveis por serem compostos de duas ou três camadas de polietileno, a fim de proporcionar maior resistência ao rasgo e ruptura. Nossos <strong>envelopes correios</strong> são fornecidos sob encomenda, de acordo com a necessidade de cada cliente (proporcionando a divulgação da sua marca).</p>
                        
            <p>Nossos <strong>envelopes correios</strong> são mais seguros, devido ao nível de adesivo de segurança ser inviolável, dessa forma para você abrir o <strong>envelope</strong> é necessário danificá-lo, isso notifica o destinatário e dá a segurança de que o <strong>envelope</strong> nunca foi aberto antes de chegar a seu destino.</p>
            
            <p>O <strong>Envelope correios</strong> é amplamente utilizado por empresas de transportes ou para você que precisa enviar produtos que requerem uma embalagem segura e inviolável. Depois de violado, o <strong>Envelope correios</strong> não poder ser mais reutilizado, pois a embalagem estará completamente danificada.</p>
            
            <h2>Envelope correios sob medida</h2>
                                
			<? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>

            
            <p>A JPR Embalagens trabalha com diversas opções de <strong>envelopes correios</strong>, com duas ou mais linhas de cola para reforçar a inviolação da embalagem. Quando falamos em produtos que requerem uma segurança diferenciada, nos preocupamos em propor o melhor aos nossos clientes, esta é a nossa especialidade.</p>
            
            <p>Uma novidade que os nossos <strong>envelopes correios</strong> possuem, é a bolsa canguru, que fica na parte frontal da embalagem, e nela coloca-se a nota fiscal do produto embalado para facilitar a identificação. Desenvolvemos as melhores opções de embalagens, de acordo com a sua necessidade. Para receber um orçamento, basta possuir as medidas (largura x comprimento + aba x espessura) e a quantidade que você deseja utilizar. </p>
         
 
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>