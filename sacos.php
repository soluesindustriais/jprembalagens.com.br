<?php
	$h1    		= 'Sacos';
	$title 		= 'Sacos';
	$desc  		= 'A JPR Embalagens traz para o mercado uma completa linha de sacos. Em nossa linha você encontra sacos fabricados sob medida.';
	$key   		= 'Sacos, Sacos sob medida, fábrica de Sacos';
	$var 		= 'Sacos';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                  
                <?=$caminhoCategoria?>
				<article>
                
                <h1><?=$h1?></h1>    
                    
            <h2>A JPR Embalagens traz para o mercado uma completa linha de sacos.</h2>
            
            <p>Em nossa linha você encontra <strong>sacos fabricados sob medida</strong>, de acordo com a necessidade da sua empresa.</p>
            
            <p>Conheça nossa linha de <strong>sacos</strong>:</p>
                <ul class="thumbnails">
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-adesivado" title="Saco Adesivado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-adesivado-01.jpg" alt="Saco Adesivado" /></a>
                          <h2><a href="<?=$url;?>saco-adesivado" title="Saco Adesivado">Saco Adesivado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-adesivado-furo" title="Saco Adesivado com Furo"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-adesivado-furo-01.jpg" alt="Saco Adesivado com Furo" /></a>
                          <h2><a href="<?=$url;?>saco-adesivado-furo" title="Saco Adesivado com Furo">Saco Adesivado com Furo</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-adesivado-solapa" title="Saco Adesivado com Solapa"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-adesivado-solapa-01.jpg" alt="Saco Adesivado com Solapa" /></a>
                          <h2><a href="<?=$url;?>saco-adesivado-solapa" title="Saco Adesivado com Solapa">Saco Adesivado com Solapa</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-bolha" title="Saco Bolha"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-bolha-01.jpg" alt="Saco Bolha" /></a>
                          <h2><a href="<?=$url;?>saco-bolha" title="Saco Bolha">Saco Bolha</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-bolha-antiestatico" title="Saco Bolha Antiestático"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-bolha-antiestatico-01.jpg" alt="Saco Bolha Antiestático" /></a>
                          <h2><a href="<?=$url;?>saco-bolha-antiestatico" title="Saco Bolha Antiestático">Saco Bolha Antiestático</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-bopp-transparente" title="Saco BOPP Transparente"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-bopp-transparente-01.jpg" alt="Saco BOPP Transparente" /></a>
                          <h2><a href="<?=$url;?>saco-bopp-transparente" title="Saco BOPP Transparente">Saco BOPP Transparente</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-fecho-zip" title="Saco com Fecho Zip"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-fecho-zip-01.jpg" alt="Saco com Fecho Zip" /></a>
                          <h2><a href="<?=$url;?>saco-fecho-zip" title="Saco com Fecho Zip">Saco com Fecho Zip</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-lixo" title="Saco de Lixo"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-lixo-01.jpg" alt="Saco de Lixo" /></a>
                          <h2><a href="<?=$url;?>saco-lixo" title="Saco de Lixo">Saco de Lixo</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-obito" title="Saco de óbito"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-obito-01.jpg" alt="Saco de óbito" /></a>
                          <h2><a href="<?=$url;?>saco-obito" title="Saco de óbito">Saco de óbito</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-polietileno" title="Saco de Polietileno"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-polietileno-01.jpg" alt="Saco de Polietileno" /></a>
                          <h2><a href="<?=$url;?>saco-polietileno" title="Saco de Polietileno">Saco de Polietileno</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-polipropileno" title="Saco de Polipropileno"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-polipropileno-01.jpg" alt="Saco de Polipropileno" /></a>
                          <h2><a href="<?=$url;?>saco-polipropileno" title="Saco de Polipropileno">Saco de Polipropileno</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-fronha" title="Saco Fronha"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-fronha-01.jpg" alt="Saco Fronha" /></a>
                          <h2><a href="<?=$url;?>saco-fronha" title="Saco Fronha">Saco Fronha</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-fronha-revista" title="Saco Fronha para Revista"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-fronha-revista-01.jpg" alt="Saco Fronha para Revista" /></a>
                          <h2><a href="<?=$url;?>saco-fronha-revista" title="Saco Fronha para Revista">Saco Fronha para Revista</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-hamper" title="Saco Hamper"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-hamper-01.jpg" alt="Saco Hamper" /></a>
                          <h2><a href="<?=$url;?>saco-hamper" title="Saco Hamper">Saco Hamper</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-hospitalar" title="Saco Hospitalar"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-hospitalar-01.jpg" alt="Saco Hospitalar" /></a>
                          <h2><a href="<?=$url;?>saco-hospitalar" title="Saco Hospitalar">Saco Hospitalar</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-infectante" title="Saco Infectante"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-infectante-01.jpg" alt="Saco Infectante" /></a>
                          <h2><a href="<?=$url;?>saco-infectante" title="Saco Infectante">Saco Infectante</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-leitoso" title="Saco Leitoso"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-leitoso-01.jpg" alt="Saco Leitoso" /></a>
                          <h2><a href="<?=$url;?>saco-leitoso" title="Saco Leitoso">Saco Leitoso</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-metalizado" title="Saco Metalizado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-metalizado-01.jpg" alt="Saco Metalizado" /></a>
                          <h2><a href="<?=$url;?>saco-metalizado" title="Saco Metalizado">Saco Metalizado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-cadaver" title="Saco para Cadáver"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-cadaver-01.jpg" alt="Saco para Cadáver" /></a>
                          <h2><a href="<?=$url;?>saco-cadaver" title="Saco para Cadáver">Saco para Cadáver</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-pead" title="Saco PEAD"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-pead-01.jpg" alt="Saco PEAD" /></a>
                          <h2><a href="<?=$url;?>saco-pead" title="Saco PEAD">Saco PEAD</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-pebd" title="Saco PEBD"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-pebd-01.jpg" alt="Saco PEBD" /></a>
                          <h2><a href="<?=$url;?>saco-pebd" title="Saco PEBD">Saco PEBD</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico" title="Saco Plástico"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-01.jpg" alt="Saco Plástico" /></a>
                          <h2><a href="<?=$url;?>saco-plastico" title="Saco Plástico">Saco Plástico</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-bd" title="Saco Plástico Bd"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-bd-01.jpg" alt="Saco Plástico Bd" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-bd" title="Saco Plástico Bd">Saco Plástico Bd</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-alta-densidade" title="Saco Plástico de Alta Densidade"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-alta-densidade-01.jpg" alt="Saco Plástico de Alta Densidade" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-alta-densidade" title="Saco Plástico de Alta Densidade">Saco Plástico de Alta Densidade</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-grande" title="Saco Plástico Grande"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-grande-01.jpg" alt="Saco Plástico Grande" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-grande" title="Saco Plástico Grande">Saco Plástico Grande</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-industrial" title="Saco Plástico Industrial"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-industrial-01.jpg" alt="Saco Plástico Industrial" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-industrial" title="Saco Plástico Industrial">Saco Plástico Industrial</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-leitoso" title="Saco Plástico Leitoso"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-leitoso-01.jpg" alt="Saco Plástico Leitoso" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-leitoso" title="Saco Plástico Leitoso">Saco Plástico Leitoso</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-microperfurado" title="Saco Plástico Microperfurado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-microperfurado-01.jpg" alt="Saco Plástico Microperfurado" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-microperfurado" title="Saco Plástico Microperfurado">Saco Plástico Microperfurado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-oficio" title="Saco Plástico Oficio"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-oficio-01.jpg" alt="Saco Plástico Oficio" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-oficio" title="Saco Plástico Oficio">Saco Plástico Oficio</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-opaco" title="Saco Plástico Opaco"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-opaco-01.jpg" alt="Saco Plástico Opaco" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-opaco" title="Saco Plástico Opaco">Saco Plástico Opaco</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-jornal" title="Saco Plástico para Jornal"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-jornal-01.jpg" alt="Saco Plástico para Jornal" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-jornal" title="Saco Plástico para Jornal">Saco Plástico para Jornal</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-pe" title="Saco Plástico Pe"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-pe-01.jpg" alt="Saco Plástico Pe" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-pe" title="Saco Plástico Pe">Saco Plástico Pe</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-personalizado" title="Saco Plástico Personalizado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-personalizado-01.jpg" alt="Saco Plástico Personalizado" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-personalizado" title="Saco Plástico Personalizado">Saco Plástico Personalizado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-resistente" title="Saco Plástico Resistente"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-resistente-01.jpg" alt="Saco Plástico Resistente" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-resistente" title="Saco Plástico Resistente">Saco Plástico Resistente</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-sanfonado" title="Saco Plástico Sanfonado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-sanfonado-01.jpg" alt="Saco Plástico Sanfonado" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-sanfonado" title="Saco Plástico Sanfonado">Saco Plástico Sanfonado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-sp" title="Saco Plástico em SP"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-sp-01.jpg" alt="Saco Plástico em SP" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-sp" title="Saco Plástico em SP">Saco Plástico em SP</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente" title="Saco Plástico Transparente"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-transparente-01.jpg" alt="Saco Plástico Transparente" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-transparente" title="Saco Plástico Transparente">Saco Plástico Transparente</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-transparente-4-furos" title="Saco Plástico Transparente 4 Furos"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-transparente-4-furos-01.jpg" alt="Saco Plástico Transparente 4 Furos" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-transparente-4-furos" title="Saco Plástico Transparente 4 Furos">Saco Plástico Transparente 4 Furos</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-valvulado" title="Saco Plástico Valvulado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-valvulado-01.jpg" alt="Saco Plástico Valvulado" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-valvulado" title="Saco Plástico Valvulado">Saco Plástico Valvulado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-plastico-zip" title="Saco Plástico Zip"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-plastico-zip-01.jpg" alt="Saco Plástico Zip" /></a>
                          <h2><a href="<?=$url;?>saco-plastico-zip" title="Saco Plástico Zip">Saco Plástico Zip</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-pp" title="Saco PP"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-pp-01.jpg" alt="Saco PP" /></a>
                          <h2><a href="<?=$url;?>saco-pp" title="Saco PP">Saco PP</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-pp-adesivado" title="Saco PP Adesivado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-pp-adesivado-01.jpg" alt="Saco PP Adesivado" /></a>
                          <h2><a href="<?=$url;?>saco-pp-adesivado" title="Saco PP Adesivado">Saco PP Adesivado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-pp-impresso" title="Saco PP Impresso"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-pp-impresso-01.jpg" alt="Saco PP Impresso" /></a>
                          <h2><a href="<?=$url;?>saco-pp-impresso" title="Saco PP Impresso">Saco PP Impresso</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-pp-liso" title="Saco Pp Liso"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-pp-liso-01.jpg" alt="Saco Pp Liso" /></a>
                          <h2><a href="<?=$url;?>saco-pp-liso" title="Saco Pp Liso">Saco Pp Liso</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-reciclado" title="Saco Reciclado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-reciclado-01.jpg" alt="Saco Reciclado" /></a>
                          <h2><a href="<?=$url;?>saco-reciclado" title="Saco Reciclado">Saco Reciclado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-valvulado" title="Saco Valvulado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-valvulado-01.jpg" alt="Saco Valvulado" /></a>
                          <h2><a href="<?=$url;?>saco-valvulado" title="Saco Valvulado">Saco Valvulado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-zip" title="Saco Zip"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-zip-01.jpg" alt="Saco Zip" /></a>
                          <h2><a href="<?=$url;?>saco-zip" title="Saco Zip">Saco Zip</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-zip-personalizado" title="Saco Zip Personalizado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-zip-personalizado-01.jpg" alt="Saco Zip Personalizado" /></a>
                          <h2><a href="<?=$url;?>saco-zip-personalizado" title="Saco Zip Personalizado">Saco Zip Personalizado</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>saco-ziplock" title="Saco Ziplock"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/sacos/thumb/saco-ziplock-01.jpg" alt="Saco Ziplock" /></a>
                          <h2><a href="<?=$url;?>saco-ziplock" title="Saco Ziplock">Saco Ziplock</a></h2>
                     </li>
                     
        <li>
            <a rel="nofollow" href="<?=$url;?>embalagem-saco-plastico-transparente" title="embalagem saco plástico transparente"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-8.jpg" alt="embalagem saco plástico transparente" class="lazyload" title="embalagem saco plástico transparente" /></a>
            <h4><a href="<?=$url;?>embalagem-saco-plastico-transparente" title="embalagem saco plástico transparente">embalagem saco plástico transparente/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>saco-plasticos-adesivado-para-nota-fiscal" title="saco plásticos adesivado para nota fiscal"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-4.jpg" alt="saco plásticos adesivado para nota fiscal" class="lazyload" title="saco plásticos adesivado para nota fiscal" /></a>
            <h4><a href="<?=$url;?>saco-plasticos-adesivado-para-nota-fiscal" title="saco plásticos adesivado para nota fiscal">saco plásticos adesivado para nota fiscal/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>filme-plastico-stretch" title="filme plástico stretch"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-8.jpg" alt="filme plástico stretch" class="lazyload" title="filme plástico stretch" /></a>
            <h4><a href="<?=$url;?>filme-plastico-stretch" title="filme plástico stretch">filme plástico stretch/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelope-plastico-com-adesivo" title="envelope plástico com adesivo"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-2.jpg" alt="envelope plástico com adesivo" class="lazyload" title="envelope plástico com adesivo" /></a>
            <h4><a href="<?=$url;?>envelope-plastico-com-adesivo" title="envelope plástico com adesivo">envelope plástico com adesivo/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelope-saco-plastico-a4" title="envelope saco plástico a4"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-0.jpg" alt="envelope saco plástico a4" class="lazyload" title="envelope saco plástico a4" /></a>
            <h4><a href="<?=$url;?>envelope-saco-plastico-a4" title="envelope saco plástico a4">envelope saco plástico a4/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>filme-stretch-fabricante" title="filme stretch fabricante"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-9.jpg" alt="filme stretch fabricante" class="lazyload" title="filme stretch fabricante" /></a>
            <h4><a href="<?=$url;?>filme-stretch-fabricante" title="filme stretch fabricante">filme stretch fabricante/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelopes-de-seguranca-para-correspondencia" title="envelopes de segurança para correspondência"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-4.jpg" alt="envelopes de segurança para correspondência" class="lazyload" title="envelopes de segurança para correspondência" /></a>
            <h4><a href="<?=$url;?>envelopes-de-seguranca-para-correspondencia" title="envelopes de segurança para correspondência">envelopes de segurança para correspondência/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelope-saco-plastico-bolha" title="envelope saco plástico bolha"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-3.jpg" alt="envelope saco plástico bolha" class="lazyload" title="envelope saco plástico bolha" /></a>
            <h4><a href="<?=$url;?>envelope-saco-plastico-bolha" title="envelope saco plástico bolha">envelope saco plástico bolha/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelopes-de-seguranca-preco" title="envelopes de segurança preço"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-8.jpg" alt="envelopes de segurança preço" class="lazyload" title="envelopes de segurança preço" /></a>
            <h4><a href="<?=$url;?>envelopes-de-seguranca-preco" title="envelopes de segurança preço">envelopes de segurança preço/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>filme-termo-contratil" title="filme termo contratil"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-2.jpg" alt="filme termo contratil" class="lazyload" title="filme termo contratil" /></a>
            <h4><a href="<?=$url;?>filme-termo-contratil" title="filme termo contratil">filme termo contratil/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>filme-encolhivel-shrink" title="filme encolhível shrink"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-3.jpg" alt="filme encolhível shrink" class="lazyload" title="filme encolhível shrink" /></a>
            <h4><a href="<?=$url;?>filme-encolhivel-shrink" title="filme encolhível shrink">filme encolhível shrink/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>industria-de-sacos-plasticos" title="indústria de sacos plásticos"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-7.jpg" alt="indústria de sacos plásticos" class="lazyload" title="indústria de sacos plásticos" /></a>
            <h4><a href="<?=$url;?>industria-de-sacos-plasticos" title="indústria de sacos plásticos">indústria de sacos plásticos/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelopes-de-seguranca-com-fita-adesiva-permanente" title="envelopes de segurança com fita adesiva permanente"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-9.jpg" alt="envelopes de segurança com fita adesiva permanente" class="lazyload" title="envelopes de segurança com fita adesiva permanente" /></a>
            <h4><a href="<?=$url;?>envelopes-de-seguranca-com-fita-adesiva-permanente" title="envelopes de segurança com fita adesiva permanente">envelopes de segurança com fita adesiva permanente/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>fornecedor-bobinas-plasticas" title="fornecedor bobinas plásticas"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-2.jpg" alt="fornecedor bobinas plásticas" class="lazyload" title="fornecedor bobinas plásticas" /></a>
            <h4><a href="<?=$url;?>fornecedor-bobinas-plasticas" title="fornecedor bobinas plásticas">fornecedor bobinas plásticas/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelopes-de-seguranca-onde-comprar" title="envelopes de segurança onde comprar"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-7.jpg" alt="envelopes de segurança onde comprar" class="lazyload" title="envelopes de segurança onde comprar" /></a>
            <h4><a href="<?=$url;?>envelopes-de-seguranca-onde-comprar" title="envelopes de segurança onde comprar">envelopes de segurança onde comprar/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>industria-de-saco-de-lixo" title="indústria de saco de lixo"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-5.jpg" alt="indústria de saco de lixo" class="lazyload" title="indústria de saco de lixo" /></a>
            <h4><a href="<?=$url;?>industria-de-saco-de-lixo" title="indústria de saco de lixo">indústria de saco de lixo/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>saco-plastico-liso" title="saco plástico liso"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-0.jpg" alt="saco plástico liso" class="lazyload" title="saco plástico liso" /></a>
            <h4><a href="<?=$url;?>saco-plastico-liso" title="saco plástico liso">saco plástico liso/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>plastico-filme-stretch" title="plástico filme stretch"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-8.jpg" alt="plástico filme stretch" class="lazyload" title="plástico filme stretch" /></a>
            <h4><a href="<?=$url;?>plastico-filme-stretch" title="plástico filme stretch">plástico filme stretch/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>plastico-filme-stretch-preco" title="plástico filme stretch preço"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-6.jpg" alt="plástico filme stretch preço" class="lazyload" title="plástico filme stretch preço" /></a>
            <h4><a href="<?=$url;?>plastico-filme-stretch-preco" title="plástico filme stretch preço">plástico filme stretch preço/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>envelopes-de-seguranca-para-laboratorios" title="envelopes de segurança para laboratórios"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-9.jpg" alt="envelopes de segurança para laboratórios" class="lazyload" title="envelopes de segurança para laboratórios" /></a>
            <h4><a href="<?=$url;?>envelopes-de-seguranca-para-laboratorios" title="envelopes de segurança para laboratórios">envelopes de segurança para laboratórios/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>plastico-filme-stretch-preco" title="plástico filme stretch preço"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-3.jpg" alt="plástico filme stretch preço" class="lazyload" title="plástico filme stretch preço" /></a>
            <h4><a href="<?=$url;?>plastico-filme-stretch-preco" title="plástico filme stretch preço">plástico filme stretch preço/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>plastico-termo-contratil" title="plástico termo contrátil"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-5.jpg" alt="plástico termo contrátil" class="lazyload" title="plástico termo contrátil" /></a>
            <h4><a href="<?=$url;?>plastico-termo-contratil" title="plástico termo contrátil">plástico termo contrátil/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>plastico-termo-contratil" title="plástico termo contrátil"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-4.jpg" alt="plástico termo contrátil" class="lazyload" title="plástico termo contrátil" /></a>
            <h4><a href="<?=$url;?>plastico-termo-contratil" title="plástico termo contrátil">plástico termo contrátil/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>rolo-de-filme-stretch" title="rolo de filme stretch"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-9.jpg" alt="rolo de filme stretch" class="lazyload" title="rolo de filme stretch" /></a>
            <h4><a href="<?=$url;?>rolo-de-filme-stretch" title="rolo de filme stretch">rolo de filme stretch/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>saco-de-lixo-biodegradavel" title="saco de lixo biodegradável"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-7.jpg" alt="saco de lixo biodegradável" class="lazyload" title="saco de lixo biodegradável" /></a>
            <h4><a href="<?=$url;?>saco-de-lixo-biodegradavel" title="saco de lixo biodegradável">saco de lixo biodegradável/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>saco-de-lixo-preco" title="saco de lixo preço"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-9.jpg" alt="saco de lixo preço" class="lazyload" title="saco de lixo preço" /></a>
            <h4><a href="<?=$url;?>saco-de-lixo-preco" title="saco de lixo preço">saco de lixo preço/a></h4>
        </li>

        
 

        <li>
            <a rel="nofollow" href="<?=$url;?>saco-de-lixo-transparente" title="saco de lixo transparente"><img data-src="<?=$url;?>imagens/produtos/sacos/thumb/sacos-5.jpg" alt="saco de lixo transparente" class="lazyload" title="saco de lixo transparente" /></a>
            <h4><a href="<?=$url;?>saco-de-lixo-transparente" title="saco de lixo transparente">saco de lixo transparente/a></h4>
        </li>

        

    			       </ul>
            
              <? include('inc/saiba-mais.php');?>
            
            </article>
            	
			<? include('inc/coluna-lateral-paginas.php');?>   
               
           	<br class="clear" />  
                    
            <? include('inc/regioes.php');?>
            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>