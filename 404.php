<?php
	$h1    		= 'Página não encontrada';
	$title 		= 'Página não encontrada';
	$desc  		= 'Ops! Escolha abaixo a página que deseja visualizar.';
	$key   		= '';
	$var   		= 'Página não encontrada';
	
	include('inc/head.php');
?>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
        <section class="full">
        	<?=$caminho;?>      
            <h1><?=$h1?></h1>
                                      
            <p class="msg-404">Ops! Página não encontrada.<br /><br />
            
                Navegue pelo site da <?=$nomeSite?> e encontre o que está procurando, escolha abaixo a página que deseja visualizar.
            </p>
            
            <div class="menu-404">
                <h2>O que deseja fazer?</h2>
                <br>
                <a rel="nofollow" title="Voltar a página inicial" href="<?=$url;?>" >Voltar a página inicial</a>
                <br><br>
                <a rel="nofollow" title="Ver O Mapa do site" href="<?=$url;?>mapa-site.php" >Ver O Mapa do site</a>
                <br><br>
            </div>
            
            <script type="text/javascript">
			  var GOOG_FIXURL_LANG = 'pt-BR';
			  var GOOG_FIXURL_SITE = '<?=$url?>'
			</script>
			<script type="text/javascript"
			  src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js;
			"></script>
            
        </section>
     </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>