<?php
	$h1    			= 'Bobina sanfonada';
	$title 			= 'Bobina sanfonada';
	$desc  			= 'A bobina sanfonada é um tipo de embalagem que pode ser feita em polietileno de baixa ou de alta densidade. Ela pode ser lisa ou impressa em até seis cores diferentes';
	$key   			= 'bobina, sanfonada, bobinas sanfonadas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas sanfonadas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Ideal para produtos grandes, a <strong>bobina sanfonada</strong> é uma opção de qualidade para a sua empresa. Saiba mais sobre esta embalagem.</p>

            <p>A embalagem é um aspecto fundamental na hora de vender mais um produto e garantir a proteção adequada. Por isso, uma ótima indicação, especialmente para empresas que trabalham com produtos grandes, é a <strong>bobina sanfonada</strong>. A <strong>bobina sanfonada</strong> é um tipo de embalagem que pode ser feita em polietileno de baixa ou de alta densidade. Ela pode ser lisa ou impressa em até seis cores diferentes, o que possibilita personalização.</p>
            
            <p>A <strong>bobina sanfonada</strong> é a indicação ideal para quem deseja embalar produtos com medidas grandes, que precisam de uma embalagem que faça a proteção contra poeira, umidade e, em alguns casos, até contra a passagem do tempo. Outra utilização frequente é forrar caixas de papelão.</p>
            
            <p>A <strong>bobina sanfonada</strong>, quando aberta, faz com que o fundo da embalagem tenha um formato quadrado, ocupando o fundo e toda a área útil da caixa.</p>
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <h2>Bobina sanfonada pode ser feita com material reciclado</h2>
                       
            <p>Uma opção para reduzir custos e ainda colaborar com as questões ambientais é a <strong>bobina sanfonada</strong> fabricada com material reciclado. Neste caso, ela pode ser feita de duas maneiras: cristal ou canela.</p>
            
            <p>A <strong>bobina sanfonada</strong> cristal é fabricada a partir de material virgem e outras embalagens. Por causa do processo de reciclagem, a bobina fica com aspecto amarelado e com alguns pontos, mas mantém a transparência, possibilitando visualização interna.</p>
            
            <p>Já a <strong>bobina sanfonada</strong> canela é feita a partir de reciclado cristal e fica com um aspecto cor de canela (marrom claro), também com alguns pontos e ainda com a transparência sendo mantida.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir a bobina, conte com a JPR Embalagens. Com mais de 15 anos de experiência na área de embalagens plásticas flexíveis, a empresa leva até o cliente as opções mais modernas, de qualidade e com preço em conta, resultando em ótima relação custo-benefício.</p>
            
            <p>O objetivo é que o cliente possa reduzir perdas e custos e identificar oportunidades de melhoria.</p>
            
            <p>Além disso, o atendimento é personalizado conforme a necessidade dos clientes. Saiba mais sobre as vantagens da empresa entrando em contato com um dos consultores e aproveite para solicitar já o seu orçamento.</p>
            
                        
          <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>