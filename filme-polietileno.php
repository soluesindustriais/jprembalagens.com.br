<?php
$h1    			= 'Filme de polietileno';
$title 			= 'Filme de polietileno';
$desc  			= 'O filme de polietileno é estirado e tem utilização frequente tanto em ambientes domésticos quanto na indústria';
$key   			= 'filme, polietileno, filmes em polietileno';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Filmes em polietileno';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoFilme?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>filme/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Proteja os seus produtos contra agentes externos. Conheça as vantagens do <strong>filme de polietileno</strong> e conte com uma embalagem de qualidade.</p>
            
            <p>A embalagem é um ponto de bastante importância na hora de vender um produto. Afinal, é ela quem garante a proteção do que está embalado, com a função de manter as características originais do produto e fazer a proteção contra agentes externos. Por isso, uma opção altamente recomendável é o <strong>filme de polietileno</strong>, também conhecido pelo nome de filme stretch.</p>
            
            <p>O <strong>filme de polietileno</strong> é estirado e tem utilização frequente tanto em ambientes domésticos quanto na indústria. O objetivo principal desta embalagem é o de proteger cargas paletizadas e garantir a movimentação e o transporte, além de proteger o que está sendo embalado contra umidade, poeira e rasgos.</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>filme/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Na indústria, a utilização do <strong>filme de polietileno</strong> é bastante ampla: nos segmentos de papel, celulose, em fraldas, refrigerantes, tampas e latas, frascos e pisos cerâmicos. Também se usa em agricultura e para movimentação de cargas tanto por terra quanto por ar.</p>
            
            <h2>Características do filme de polietileno</h2>

            <p>O <strong>filme de polietileno</strong> se caracteriza por ter uma alta aderência e grande resistência mecânica, tanto para alongamento quanto para atração.  Também se caracteriza por ser resistente à perfuração e por ter grande brilho e transparência, o que resulta em excelente aspecto visual.</p>
            
            <p>O <strong>filme de polietileno</strong> é um plástico atóxico. Justamente por isso, pode ser utilizado para embalar qualquer tipo de produto, inclusive alimentos e bebidas, sem causar qualquer tipo de alteração às propriedades do produto. Além disso, é uma embalagem reciclada, o que contribui para redução de custos e proporciona ótima relação custo-benefício.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>filme/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir o <strong>filme de polietileno</strong>, aproveite os benefícios da JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa conta com uma equipe profissional e sempre atualizada, que está atenta às novidades do mercado e sempre que sempre busca o que há de mais moderno em embalagens, sempre prezando pela excelência dos produtos e pelo preço em conta. O objetivo da JPR Embalagens é identificar oportunidades de melhoria para reduzir perdas e custos. O atendimento da JPR Embalagens é personalizado de acordo com as necessidades e preferências do cliente. O <strong>filme de polietileno</strong> pode ser personalizado de diversas maneiras para melhor atender aos diferentes tipos de demanda. A empresa disponibiliza preços em conta e ótimas condições de pagamento. Entre em contato com um dos consultores para saber mais sobre as vantagens deste produto e para conhecer as vantagens da JPR Embalagens. Solicite já o seu orçamento. </p>

            
            
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>