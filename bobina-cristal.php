<?
	$h1    			= 'Bobina cristal';
	$title 			= 'Bobina cristal';
	$desc  			= 'A bobina cristal é um tipo de embalagem plástica flexível feita com material reciclado. A produção é a partir de aparas de material virgem ou de embalagens já recicladas.';
	$key   			= 'bobina, cristal, bobinas de cristal';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de cristal';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br>
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
      
            <p>Embalagem resistente e barata, a <strong>bobina cristal</strong> se destaca pela sua qualidade. Saiba mais sobre este tipo de embalagem. </p>
            
            <p>Empresários estão sempre em busca de maneiras de melhorar vendas para alcançar novos mercados. Para isso, melhoram produtos e contam com embalagens de qualidade, já que muitas vezes o primeiro contato que o cliente tem com a marca é justamente através da embalagem. Por isso, conheça a <strong>bobina cristal</strong>.</p>
            
            <p>A <strong>bobina cristal</strong> é um tipo de embalagem plástica flexível feita com material reciclado. A produção acontece a partir de aparas de material virgem e também a partir de embalagens que já fora, recicladas, tais como embalagens de alimentos, sacolas e sacarias. </p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Devido ao processo de reciclagem, a embalagem fica com alguns pontos e tem cor da embalagem alterada para amarelo claro. A <strong>bobina cristal</strong> permanece com transparência, sendo possível a visualização interna da embalagem. </p>
            
            <p>A <strong>bobina cristal</strong> é uma das formas mais econômicas de embalar produtos com segurança. Afinal, a embalagem mantém a mesma qualidade da bobina que é fabricada com matéria-prima virgem. A <strong>bobina cristal</strong> garante a integridade dos produtos durante o processo de transporte e de armazenamento, e é bastante resistente a quedas, rupturas, rasgos e demais fatores externos. Outra vantagem da <strong>bobina cristal</strong> é a possibilidade de personalização da aparência da embalagem, mantendo-a lisa ou com impressão em várias cores, o que colabora para a divulgação da sua marca no mercado. </p>
            
            <h2>Bobina cristal com preço em conta e condições vantajosas de pagamento</h2>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir a <strong>bobina cristal</strong>, conte com as vantagens da JPR Embalagens. A empresa está no mercado há mais de 15 anos, sendo especializada na fabricação e venda de embalagens plásticas flexíveis. </p>
            
            <p>No caso de embalagens recicladas, a empresa também disponibiliza opções como bobina canela e bobina colorida. Além de bobinas, também há filmes, sacos, sacolas, envelopes, etc. </p>
            
            <p>A equipe da JPR Embalagens está sempre atenta para levar ao cliente o que há de mais moderno no segmento de embalagens flexíveis. O objetivo da equipe é fazer com que os clientes reduzam perdas e custos e tenham embalagens que se destacam pela excelência e pelos preços em conta, resultando em ótima relação custo-benefício. </p>
            
            <p>Aproveite as condições e esclareça dúvidas sobre o produto entrando em contato com a nossa equipe. Solicite já o seu orçamento. </p>

            
            <? include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<? include('inc/coluna-lateral-paginas.php')?>
        
			<? include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>