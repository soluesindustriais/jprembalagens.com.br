<?php
$h1    			= 'Envelope bolha';
$title 			= 'Envelope bolha';
$desc  			= 'O envelope bolha é muito utilizado para resolver problemas de proteção de diversos produtos, de forma econômica e prática';
$key   			= 'Envelopes bolha, Envelopes, Envelope, bolha';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Envelopes bolha';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
        
       
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>            
                			
                <?=$caminhoProdutosEnvelopes?>
                 <article>
                <h1><?=$h1?></h1> 
                <br> 
                
                <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>  
                
                <p>O <strong>envelope bolha</strong> é muito utilizado para resolver problemas de proteção de diversos produtos, de forma econômica e prática, além proteger o produto de impactos e da umidade. </p>
                
                <p>Se você busca redução de custos no <strong>plástico bolha</strong>, e não embala produtos médico-hospitalares, utilize o <strong>envelope bolha reciclado</strong>. Além de você reduzir custos com embalagens, você estará contribuindo com o meio ambiente.</p><br/>
                
                <h2>Trabalhamos com uma diversificada linha de envelopes bolha, como:</h2>
                
                <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div> 
                
                <ul class="list">
                   <li><strong>envelope bolha colorido</strong>; </li>
                   <li><strong>envelope bolha com aba adesiva</strong>;</li>
                   <li><strong>envelope bolha simples</strong>;</li>
                   <li><strong>envelope bolha reciclado</strong>;</li>
                   <li><strong>envelope bolha reforçado</strong>;</li>
               </ul>
               
               <p>Os <strong>envelopes bolha</strong> são fabricados em polietileno de baixa densidade, porem sem impressão. Produzimos a partir de 2.000 peças os sacos bolha simples e com aba adesiva.</p>
               
               <p>A vantagem de se utilizar o <strong>envelope bolha</strong>, é que ele protege os objetos contra arranhões, batidas e quedas, envolvendo o produto tanto como embalagem interna quanto externa.</p> 
               
               <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div> 
            
            <p>Além de <strong>envelopes bolha</strong>, trabalhamos com outras linhas de <strong>embalagens como bobina bolha</strong>, <strong>envelope kraft com bolha interna</strong> com ou sem personalização, <a href="<?=$url;?>saco-bolha" title="Saco Bolha"><strong>saco bolha</strong></a>, <a href="<?=$url;?>saco-bolha-antiestatico" title="Saco Bolha antiestático"><strong>saco bolha antiestático</strong></a>, entre outros.</p>
            
            <p>A JPR Embalagens possui mais de 15 anos de experiência na comercialização de <strong>embalagens flexíveis</strong>. Além do <strong>envelope bolha</strong> fabricamos <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelope de segurança</strong></a>, <strong>envelope comuns</strong>, <a href="<?=$url;?>envelope-plastico-mala-direta" title="Envelope Plástico Mala Direta"><strong>envelope mala direta</strong></a>, entre muitos outros.</p>
            
            <p>Para receber um orçamento de <strong>envelope bolha</strong>, bastar ter as medidas (largura x comprimento) e quantidades que deseja utilizar.</p>
            
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>           
        <br class="clear" />                                                                                                    
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>