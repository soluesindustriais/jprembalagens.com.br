<?php
$h1    			= 'Sacola plástica oxibiodegradável';
$title 			= 'Sacola plástica oxibiodegradável';
$desc  			= 'Para se identificar uma sacola plástica oxibiodegradável, basta verificar se a embalagem possui o símbolo de uma gota d’agua, escrito D2W.';
$key   			= 'Sacola, plástica, oxibiodegradável, Sacolas plásticas oxibiodegradáveis';
$var 			= 'Sacolas plásticas oxibiodegradáveis';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             <p>Uma novidade que chegou ao mercado brasileiro é a <strong>sacola plástica oxibiodegradável</strong>, enquanto o plástico convencional pode levar mais de 100 anos para se decompor, a embalagem com o aditivo oxibiodegradável se degrada em curto espaço de tempo, em média 6 meses. O <strong>oxibiodegradável</strong> é um aditivo granulado, que é adicionado durante o processo de fabricação da <strong>embalagem plástica</strong>, e torna esta embalagem degradável em um tempo significamente menor quando comparado ao plástico convencional. Adote esta ideia, e contribua com o meio ambiente. A natureza agradece.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 2; include('inc/gallery.php'); ?>
             
             <p>A <strong>sacola plástica oxibiodegradável</strong> pode ser lisa ou impressa em até 6 cores. São produzidas em polietileno de alta ou baixa densidade.</p>
             <p>Para se identificar uma <strong>sacola plástica oxibiodegradável</strong>, basta verificar se a embalagem possui o símbolo de uma gota d’agua, escrito D2W. De acordo com a Resbrasil (fornecedora exclusiva do aditivo no Brasil), esta norma deve ser adotada para todas as embalagens que possuem o aditivo.</p>
             <p>Fabricamos a <strong>sacola plástica oxibiodegradável</strong> com diversos modelos de alças e corte, tais como:</p>
             <ul class="list">
                <li><strong>Sacola plástica oxibiodegradável com alça vazada</strong>;</li>
                <li><strong>Sacola plástica oxibiodegradável com alça camiseta</strong>;</li>
                <li><strong>Sacola plástica oxibiodegradável com alça boca de palhaço</strong>;</li>
                <li><strong>Sacola plástica oxibiodegradável com alça fita</strong>;</li>
                <li><strong>Sacola plástica oxibiodegradável com ilhós</strong>;</li>
                <li><strong>Sacola plástica oxibiodegradável com cordão</strong>;</li>
                <li>Entre outras.</li>
            </ul>
            <p>Para receber um orçamento de <strong>sacola plástica oxibiodegradável</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>