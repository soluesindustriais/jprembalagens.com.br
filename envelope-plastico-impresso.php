<?php
	$h1    			= 'Envelope plástico impresso';
	$title 			= 'Envelope plástico impresso';
	$desc  			= 'O envelope plástico impresso pode ser fabricado em diversos modelos, com aba adesiva, fecho zip, ilhós, furos, botão, entre outros.';
	$key   			= 'Envelopes plásticos impressos, Envelopes, Envelope, plástico, impresso';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos impressos';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                      
            <p>Trabalhamos com <strong>envelope plástico impresso</strong> em até 6 cores. Nossos <strong>envelopes</strong> são produzidos com matéria-prima de alta qualidade, assim obtemos um excelente acabamento na embalagem.</p>
            
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 3; include('inc/gallery.php'); ?>
            
            <h2>Modelos de envelope plástico impresso</h2>
            <p>Os <strong>envelopes plásticos impressos</strong> podem ser fabricamos em diversos modelos, com aba adesiva, fecho zip, ilhós, furos, botão, entre outros. Além disso, podem ser feitos nas cores transparentes ou pigmentados.</p>
            <p>Fabricados sob medida, esta embalagem também pode ser produzido a partir de matéria-prima reciclada, esta é uma das formas para você obter redução de custo com embalagens. Nossos <strong>envelopes plásticos impressos</strong>, podem ser feitos em polietileno de baixa densidade, polipropileno, BOPP, entre muitos outros.</p>
            <p>Produzimos também <strong>envelopes plásticos impressos</strong> para presente, e personalizamos de acordo com necessidade de cada cliente. Por ser um produto totalmente versátil, o <strong>envelope plástico impresso</strong> pode ser utilizado para diversas aplicações, como envio de documentos, embalar presentes, transportar objetos de valores, exames, e etc.</p>
            <p>Outra opção que temos, é o <strong>envelope plástico impresso para correios</strong>, este modelo é indicado para envio de produtos que requerem segurança e não podem ser violados. Eles são produzidos em duas cores, geralmente preto na parte interna e branco na parte externa. Nosso pedido mínimo são de 300kg para <strong>envelope plástico impresso</strong> e 150kg para <strong>envelopes lisos</strong>.</p>
            <p>Para receber um orçamento desse produto, ou outros produtos como, <a href="<?=$url;?>envelope-sedex" title="Envelope Sedex"><strong>Envelope Sedex</strong></a>, <a href="<?=$url;?>envelope-transparente" title="Envelope Transparente"><strong>Envelope Transparente</strong></a>, <a href="<?=$url;?>saco-metalizado" title="Saco Metalizado"><strong>Saco Metalizado</strong></a>, <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>Saco Plastico</strong></a>, <a href="<?=$url;?>sacola-impressa" title="Sacola Impressa"><strong>Sacola Impressa</strong></a>, entre outros, basta possuir as medidas (largura x comprimento x espessura), e a quantidade que você deseja utilizar.</p>
            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>