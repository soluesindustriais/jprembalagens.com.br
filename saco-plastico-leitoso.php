<?php
$h1    			= 'Saco plástico leitoso';
$title 			= 'Saco plástico leitoso';
$desc  			= 'O saco plástico leitoso pode ser fabricado em polietileno ou polipropileno, sob medida, de acordo com a necessidade de cada cliente.';
$key   			= 'Sacos plásticos industriais, Saco, sacos, plástico, industrial, saco plástico para industria';
$var 			= 'Sacos plásticos leitosos';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <p>O <strong>saco plástico leitoso</strong> pode ser fabricado em polietileno ou polipropileno, sob medida, de acordo com a necessidade de cada cliente. Também podem ser produzidos em outras cores.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>É amplamente utilizado em laboratórios, gráficas, editoras e empresas em geral.</p>  
             <p>O <strong>saco plástico leitoso de polietileno</strong> pode ser produzido com matéria-prima 100% reciclada. Nesta opção, utilizamos as aparas do material virgem e outras embalagens que foram reprocessadas. Além de você obter uma redução de custos com embalagens, estará contribuindo com o meio ambiente.</p>
             <p>Por ser uma embalagem versátil, o <strong>saco plástico leitoso</strong> pode ser produzido com aba adesiva permanente ou abre e fecha. No caso do adesivo permanente, o <strong>saco plástico leitoso</strong> se torna inviolável, e para violar a embalagem é necessário danificá-la. </p>
             <p>Se você deseja ter uma embalagem sustentável, utilize o <strong>saco plástico leitoso com aditivo oxi-biodegradavel</strong>. Nesta opção, a embalagem em contato com a natureza se degrada em curto espaço de tempo. </p>
             <h2>Abaixo algumas alternativas do saco plástico leitoso:</h2>
             <ul class="list">
                <li><strong>Saco plástico leitoso com fecho zip</strong>;</li>
                <li><strong>Saco plástico leitoso vai-e-vem</strong>;</li>
                <li><strong>Saco plástico leitoso com aba adesiva</strong>;</li>
                <li><strong>Saco plástico leitoso com tala</strong>.</li>
            </ul>
            <p>Nossa quantidade mínima de produção de <strong>saco plástico leitoso</strong> são de 150kg liso e 300kg impresso.</p>
            <p>Para receber um orçamento de <strong>saco plástico leitoso</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>