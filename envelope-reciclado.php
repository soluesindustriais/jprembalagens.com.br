<?php
	$h1    			= 'Envelope reciclado';
	$title 			= 'Envelope reciclado';
	$desc  			= 'Utilize o envelope reciclado, que te garante total transparência sem o aspecto cristalino, mas com uma grande contribuição ao planeta.';
	$key   			= 'Envelopes reciclados, Envelopes, Envelope, reciclado';
	$var 			= 'Envelopes reciclados';
	$legendaImagem 	= ''.$var.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br>   
                    
            <h2>Fabricamos envelope reciclado em plástico cristal ou canela</h2>
            
                      
            <p>O <strong>envelope reciclado cristal</strong>, é produzido a partir das aparas do material virgem, neste modelo o <strong>envelope natural</strong> fica com um aspecto visual amarelo claro, e pode ser liso ou impresso em até 6 cores, além de pode ser pigmentado.</p>
            <p>Já o <strong>envelope reciclado canela</strong>, é produzido a partir das aparas do material reciclado cristal, neste modelo o <strong>envelope</strong> fica com um aspecto visual na cor canela, porem transparente, ou seja, é possível ver perfeitamente o que há dentro da embalagem. Também pode ser liso ou impresso até 6 cores e também pigmentado em outras cores.</p>
            
            <div class="picture-legend picture-center">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Se você precisa de uma embalagem plástica transparente, mas não armazena produtos alimentícios ou medicinais e deseja contribuir com o meio ambiente, utilize o <strong>envelope reciclado</strong>, que te garante total transparência sem o aspecto cristalino, mas com uma grande contribuição ao planeta.</p>
            <p>Se você possui uma grande escala de produtos para embalar, sugerimos que utilize <strong>envelope reciclado</strong> com aba adesiva, este formato otimiza tempo para fechar a embalagem, basta destacar o liner que protege o adesivo, e dobrar a aba para aderir ao plástico. Caso o volume seja em escala menor, indicamos fechar as embalagens através de uma seladora manual.</p>
            <p>Para <strong>envelope reciclado</strong> impresso, produzimos a partir de 300kg e para os lisos 200kg.</p>
            <p>Para receber um orçamento de <strong>envelope reciclado</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento + aba x espessura) e a quantidade que deseja utilizar.</p>

        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>