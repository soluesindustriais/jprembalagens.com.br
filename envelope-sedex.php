<?php
	$h1    			= 'Envelope Sedex';
	$title 			= 'Envelope Sedex';
	$desc  			= 'O envelope sedex foi especialmente desenvolvido para envio de documentos sigilosos, talões de cheque, documentos bancários, produtos de e-commerce de uma maneira geral.';
	$key   			= 'Envelopes Sedex, Envelopes, Envelope, Sedex, Envelope de segurança sedex';
	$var 			= 'Envelopes Sedex';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
            
                            
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                                    
            <p>Fabricado com três camadas de plástico coextrusado, o <strong>envelope sedex</strong> é uma das embalagens mais resistente que existem na linha de <strong>envelopes</strong>.</p>
            
                        
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>

            <h2>Mais segurança para enviar seus produtos</h2>
            
            <p>Também conhecido como <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelope de segurança</strong></a>, o <strong>envelope sedex</strong> foi especialmente desenvolvido para envio de documentos sigilosos, talões de cheque, documentos bancários, produtos de e-commerce de uma maneira geral, todo e qualquer produto/documento que exija proteção e segurança.</p>
            
            <p>Para contribuir com o meio ambiente, estamos fabricando este produto com matéria prima oxi-biodegradavel, neste formato a embalagem se decompõe em curto espaço de tempo, em média 6 meses, enquanto a embalagem comum pode levar até 100 anos. </p> 
             
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Nossos lotes mínimo de produção são de 200kg para <strong>envelopes sedex liso</strong> e 300kg para os impressos.</p>
            <p>Este tipo de embalagem flexível é fabricado com duas ou mais camadas, para se tornar resistente ao rasgo e rupturas.</p>
            <p>Para produtos que exigem extrema segurança, este produto pode ser confeccionado com adesivos de nível de segurança 1,2,3,4 e 5. Também podemos colocar números sequenciais e código de barras nos <strong>envelopes sedex</strong>.</p>
            <p>Por ser uma embalagem prática, o esta embalagem pode ser fabricado com a bolsa canguru na parte frontal do <strong>envelope</strong>, assim você pode colocar a nota fiscal para facilitar a identificação e envio do produto.</p>

            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Além de <strong>envelope sedex</strong>, a JPR Embalagens fabrica outras linhas de embalagens, como <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plástico</strong></a>, <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plásticas</strong></a> com alça vazada e camiseta, bobinas plásticas, embalagens especiais, entre outras.</p>
            <p>Fabricamos <strong>envelope sedex</strong> de acordo com a necessidade de cada cliente, basta entrar em contato com a nossa área comercial, e informar as medidas (largura x comprimento x espessura) e a quantidade.</p>

            
			<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>