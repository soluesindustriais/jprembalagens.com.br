<?php
$h1    			= 'Sacola leitosa';
$title 			= 'Sacola leitosa';
$desc  			= 'Fabricamos a sacola leitosa em diversos formatos e também em cores. São produzidas em polietileno de alta ou baixa densidade.';
$key   			= 'Sacola, leitosa, Sacolas leitosas, Sacola leitosa impressa, Sacola leitosa personalizada';
$var 			= 'Sacolas leitosas';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>

                       <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     

             <br>  

             <p>Fabricamos a <strong>sacola leitosa</strong> em diversos formatos e cores. São produzidas em polietileno de alta ou baixa densidade.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>Esta embalagem pode ser lisa ou impressa em até 6 cores, além de poder receber diversos modelos de alças, formatos e acabamentos.</p>
             <h2>Veja alguns modelos que fabricamos:</h2>
             <ul class="list">
                <li><strong>Sacola leitosa com alça vazada</strong>;</li>
                <li><strong>Sacola leitosa com alça camiseta</strong>;</li>
                <li><strong>Sacola leitosa com alça fita</strong>;</li>
                <li><strong>Sacola leitosa com ilhós</strong>;</li>
                <li><strong>Sacola leitosa com cordão</strong>;</li>
                <li>Entre outras.</li>
            </ul>
            <p>Para se reduzir custos com embalagens, utilize a <strong>sacola leitosa</strong> produzida com uma porcentagem de matéria-prima reciclada. Além de reduzir custos, você estará contribuindo com o meio ambiente.</p>
            <p>Outra forma de contribuirmos com a natureza, é utilizar a <strong>sacola leitosa oxibiodegradavel</strong>. Nessa opção, a <strong>sacola</strong> em contato com o meio ambiente se degrada em curto espaço de tempo, sem deixar resíduos nocivos ao meio ambiente. Para identificar uma <strong>sacola leitosa oxibiodegradavel</strong>, ela deve ter impressa a logo de uma gota d’agua escrita D2W, esta norma serve para todas as embalagens plásticas flexíveis.</p>
            <p>Nossa quantidade mínima de produção são de 250kg e lisa 150 kg.</p>
            <p>Para receber um orçamento de <strong>sacola leitosa</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>
            
            <?php include('inc/saiba-mais.php');?>
            
            

        </article>

        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  

        <br class="clear" />  
        


        <?php include('inc/regioes.php');?>

        <?php include('inc/copyright.php');?>


    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>