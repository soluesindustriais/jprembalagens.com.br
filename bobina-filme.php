<?
	$h1    			= 'Bobina de filme';
	$title 			= 'Bobina de filme';
	$desc  			= 'A bobina de filme em polipropileno é bastante usada em indústrias de confecções, gráficas e alimentícias, já que a embalagem proporciona brilho e transparência';
	$key   			= 'Bobinas de filme, Bobina, filme';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de filme';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br>    
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Conheça opção de embalagem que proporciona variedade de aparência e graus de resistência. Saiba mais sobre a <strong>bobina de filme</strong>.</p>
            
            <p>Além da qualidade do produto que a sua empresa tem para oferecer, um ponto muito importante também é a qualidade da embalagem que envolve este produto. Por isso, conheça a <strong>bobina de filme</strong>.</p>

            <p>A bobina é um produto que pode ser produzido em PEBD, PEAD, PBDL e polipropileno. O produto tem características técnicas que possibilitam variações no que se refere tanto à aparência, quanto à resistência e a temperatura. Além disso, a <strong>bobina de filme</strong> plástico possui alto rendimento e excelente soldabilidade.</p>
            
            <p>A <strong>bobina de filme</strong> em polipropileno é bastante usada em indústrias de confecções, gráficas e alimentícias, já que a embalagem proporciona brilho e transparência. Já quando é feita em polietileno, é bastante empregada para alimentos, bebidas, leites e farináceos, por ser um plástico atóxico e resistente, usado para embalar qualquer tipo de produto. A bobina também é utilizada para embalar produtos metalúrgicos e automotivos.</p>
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina de filme</strong> é uma embalagem que pode ser feita natural ou pigmentada e lisa ou impressa em até nove cores, o que permite personalização de acordo com as cores da marca do cliente, por exemplo.</p>
            
            <p>Além disso, a fabricação da <strong>bobina de filme</strong> pode ser feita com matéria-prima totalmente virgem ou reciclada nas modalidades de cristal, canela ou colorido. As três últimas opções, além de contribuir com o meio ambiente, tem um custo inferior para a sua empresa.</p>
            
            <h2>Bobina de filme na JPR Embalagens</h2>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <p>E para adquirir sua <strong>bobina de filme</strong>, conte com os benefícios da JPR Embalagens. A empresa atua há mais de 15 anos no mercado de embalagens flexíveis, trabalhando com uma ampla linha de bobinas plásticas, e tem como característica produtos de excelente qualidade e baixo custo.</p>

            <p>A equipe técnica da empresa esclarece todas as dúvidas e tem o objetivo de fazer com que o cliente tenha aumento na produtividade e redução de perdas. A empresa disponibiliza um atendimento totalmente personalizado e voltado para as necessidades e preferências dos clientes. Entre em contato com a equipe técnica da JPR Embalagens para esclarecer eventuais dúvidas e saber mais sobre os produtos e as ótimas condições de pagamento. Aproveite e solicite já o seu orçamento de <strong>bobina de filme</strong>.</p>

            
            
            <? include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<? include('inc/coluna-lateral-paginas.php')?>
        
			<? include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>