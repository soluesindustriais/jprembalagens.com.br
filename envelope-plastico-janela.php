<?php
	$h1    			= 'Envelope plástico janela';
	$title 			= 'Envelope plástico janela';
	$desc  			= 'O envelope plástico janela pode ser transparente ou pigmentado de acordo com a necessidade de cada cliente e é uma embalagem prática, funcional e ao mesmo tempo protetora.';
	$key   			= 'Envelopes plásticos janela, Envelopes, Envelope, plástico, janela, envelope janela de plástico, envelope para nota fiscal';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos janela';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 

            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>                                    
            <p>Fabricados com 3 ou mais fitas adesivas, o <strong>envelope plástico janela</strong> é uma embalagem própria para transportar notas fiscais.</p>
            
            <p>Por ser uma embalagem prática, funcional e ao mesmo tempo protetora, o <strong>envelope plástico janela</strong> pode ser transparentes ou pigmentados de acordo com a necessidade de cada cliente.</p>
            <h2>Funcionalidade do envelope plástico janela</h2>
            <p>Geralmente, o <strong>envelope plástico janela</strong>, é colado na parte externa da caixa de papelão ou <strong>envelopes plásticos</strong> comuns para identificar a nota fiscal, assim evita que a caixa ou o <strong>envelope</strong> seja aberto e agiliza o despacho da mercadoria.</p>
            

            
            <p>Também conhecido como <a href="<?=$url;?>envelope-awb" title="Envelope AWB"><strong>envelope AWB</strong></a>, o <strong>envelope plástico janela</strong> pode ser fabricado com matéria-prima reciclada, assim você obterá uma embalagem 100% sustentável.</p>
            <p>Por ser uma embalagem super versátil, o <strong>envelope plástico janela</strong> é amplamente utilizado por empresas de courrier, empresas aéreas, empresas de entrega rápida, transportadoras, empresas exportadoras, entre outras. </p>           
            
            <? $pasta = "imagens/produtos/"; ?>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>Outras opções que também fabricamos na linha de <strong>envelopes</strong> são o <strong>envelope com fecho zip</strong>, <strong>envelope tala</strong>, <a href="<?=$url;?>envelope-plastico-ilhos" title="Envelope Plástico Com Ilhós"><strong>Envelope Plástico Com Ilhós</strong></a>, <strong>envelope com botão</strong>. Somos especialistas na produção de <strong>envelopes em geral</strong>. </p>
            <p>Produzimos a partir de 250kg para <strong>envelope plástico janela</strong> impresso  e 150kg para envelopes lisos.</p>
            <p>Para receber um orçamento de <strong>envelope plástico janela</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade que deseja utilizar.</p>

            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>