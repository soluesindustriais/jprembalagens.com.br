<?php
$h1    			= 'Sacola alça vazada';
$title 			= 'Sacola alça vazada';
$desc  			= 'Para obter uma sacola alça vazada mais resistente, podemos produzir com reforço na alça, desta forma ela poderá suportar pesos maiores, garantindo integridade do produto.';
$key   			= 'Sacola, Alça, vazada, Sacolas alças vazadas, Sacola Alça vazada colorida, Sacola Alça vazada personalizada';
$var 			= 'Sacolas alça vazada';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             <p>Produzidas em polietileno de alta ou baixa densidade, a <strong>sacola alça vazada</strong> pode ser lisa ou impressa em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>A <strong>sacola alça vazada</strong>, também conhecida como <a href="<?=$url;?>sacola-boca-palhaco" title="Sacola Boca de Palhaço"><strong>sacola boca de palhaço</strong></a> ou <strong>sacola boca triste</strong>, pode ser produzida na cor transparente ou pigmentada em diversas cores.</p>
             <p>Possuem a resistência necessária para garantir a integridade do produto e o acabamento que valoriza a sua marca, além da <strong>sacola alça vazada</strong> pode ser produzida com uma porcentagem de matéria-prima reciclada, pois esta é uma opção para você reduzir seus custos com embalagem.</p>
             <p>Para obter uma <strong>sacola alça vazada</strong> mais resistente, podemos produzir a <strong>sacola com reforço na alça</strong>, desta forma ela poderá suportar pesos maiores, garantindo a integridade do produto, e proporcionando a segurança que o seu produto merece.</p>
             <h2>Sacola alça vazada com aditivo D2W</h2>
             <p>Também fabricamos a <strong>sacola alça vazada com aditivo D2W</strong>, oxi-biodegradavel. O aditivo é adicionado durante o processo de fabricação da <strong>sacola</strong>, e proporciona uma degradação em curto espaço de tempo. Se você deseja contribuir com o meio ambiente, utilize <strong>sacola alça vazada oxi-biodegradavel</strong>. Adote esta ideia e ajude-nos a ter um planeta mais sustentável.</p>
             <p>São produzidas sob medida, de acordo com a necessidade de cada cliente. </p>
             <p>Trabalhamos com <a href="<?=$url;?>sacola-alca-fita" title="Sacola Alça Fita"><strong>sacola alça fita</strong></a>, <a href="<?=$url;?>sacola-camiseta" title="Sacola Camiseta"><strong>sacola alça camiseta</strong></a>, <strong>sacola com cordão</strong> todas podendo ser lisas ou impressas, sob medida.</p>
             <p>Para receber um orçamento basta enviar as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>
             <p>Nossa quantidade mínima de produção de <strong>sacola alça vazada impressa</strong> são de 250kg e lisa 150 kg.</p>


             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>