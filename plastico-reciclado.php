<?php
$h1    			= 'Plástico reciclado';
$title 			= 'Plástico reciclado';
$desc  			= 'O plástico reciclado é uma opção resistente, sustentável e barata de embalagem. A embalagem é um ponto fundamental na hora de vender um produto';
$key   			= 'plástico, reciclado, plásticos reciclados';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Plásticos reciclados';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoPlastico?>
              <article>
             <h1><?=$h1?></h1>     
             <br>   
             
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O <strong>plástico reciclado</strong> é uma opção resistente, sustentável e barata de embalagem. Confira maiores informações.</p>

            <p>A embalagem é um ponto fundamental na hora de vender um produto. Ela precisa ter qualidade e ter aspecto visual atraente para o consumidor. Por isso, uma opção altamente recomendada é o <strong>plástico reciclado</strong>.</p>
            
            <p>O <strong>plástico reciclado</strong> pode ser fabricado em diversos modelos e cores. Esta é uma das soluções mais econômicas para embalar diferentes tipos de produto, mantendo a qualidade, já que o plástico é resistente a quedas, rasgos, rupturas e outros tipos de danos.</p>
            
            <h2>Plástico reciclado pode ser feito em três modelos diferentes</h2>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O <strong>plástico reciclado</strong> pode ser feito de três maneiras distintas. Uma delas é a fabricação a partir de material virgem e de embalagens que foram recicladas, tais como sacolas e embalagens de alimentos, por exemplo. Neste caso, a embalagem fica com coloração amarelo claro e com alguns pontos, devido ao processo de reciclagem, mas mantém a transparência da embalagem.</p>
            
            <p>Outra opção é o <strong>plástico reciclado</strong> canela, feito com aparas de plásticos reciclados. Neste caso, a resistência e a segurança da embalagem são mantidos, graças à presença de uma solda reforçada. A coloração fica marrom claro com alguns pontos, mas a transparência é mantida. A terceira opção é o <strong>plástico reciclado</strong> colorido, que é fabricado a partir de mistura de embalagens recicladas, o que resulta em falta de padrão de cor e sem transparência. Esta é uma das matérias-primas com menores custos que existem no ramo de embalagens.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>plastico/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para adquirir o <strong>plástico reciclado</strong>, conte com as vantagens da JPR Embalagens. A empresa atua no mercado de embalagens plásticas flexíveis há mais de 15 anos, tendo como objetivo principal reduzir perdas e custos do cliente através da identificação de oportunidades de melhoria.</p>
            
            <p>Por isso, a JPR Embalagens conta com equipamentos modernos e equipe técnica sempre atualizada, para levar até o cliente embalagens de qualidade e com preços reduzidos, resultando em ótima relação custo-benefício.</p>
            
            <p>O atendimento da empresa é personalizado e voltado às necessidades e demandas dos clientes. Por isso, aproveite os benefícios e entre já em contato com a equipe, solicitando seu orçamento a partir da quantidade e medidas do produto que você precisa.</p>

            
            
            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>