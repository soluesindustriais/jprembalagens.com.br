<?php
$h1    			= 'Fábrica de sacolas plásticas';
$title 			= 'Fábrica de sacolas plásticas';
$desc  			= 'Se você procura por fábrica de sacolas plásticas, entre em contato com a JPR Embalagens, pois nossa especialidade é a embalagens plásticas flexíveis';
$key   			= 'Fabrica, sacola, plástica, Fabrica de sacola plástica';
$var 			= 'Fabrica de sacola plástica';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>
<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <p>Nossa <strong>fábrica de sacolas plásticas</strong> se localiza em São Paulo, Capital, na Zona Sul. Se você procura por <strong>fábrica de sacolas plásticas</strong>, entre em contato com a JPR Embalagens, pois nossa especialidade é a embalagens plásticas flexíveis.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>

             <p>Como a <strong>sacola plástica</strong> é muito versátil, pode ser <strong>fabricada</strong> com diversos cortes e soldas.</p> 
             <h2>Modelos produzidos em nossa fábrica de sacolas plásticas</h2>
             <ul class="list">
                <li><strong>Sacola plástica camiseta</strong></li>
                <li><strong>Sacola plástica alça vazada</strong></li>
                <li><strong>Sacola plástica com cordão</strong></li>
                <li><strong>Sacola plástica alça boca de palhaço</strong></li>
            </ul>
            <p>Além das <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plásticas</strong></a>, trabalhamos com <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>sacos plásticos</strong></a>, <a href="<?=$url;?>envelope-plastico" title="Envelope Plástico"><strong>envelope plástico</strong></a>, embalagens especiais, filmes e bobinas.</p>
            <p>Por sermos <strong>fabrica de sacolas plásticas</strong>, produzimos as <strong>sacolas sob medida</strong>, de acordo com a sua necessidade. Podem ser lisas ou impressas em até 6 cores.</p>
            <p>A fábrica de sacolas plásticas JPR Embalagens, pensando em sustentabilidade, está produzindo as sacolas plásticas com aditivo oxi-biodegradavel. Este aditivo proporciona a rápida degradação da sacola em contato com o meio ambiente, sem deixar resíduos nocivos ao meio ambiente. Adote esta ideia, e ajude-nos a ter um planeta mais sustentável.</p>
            <p>Possuímos equipamentos modernizados, desta forma é possível desenvolver e produzir diversos modelos de embalagens. </p>
            <p>Para receber um orçamento da nossa <strong>fabrica de sacolas plásticas</strong>, basta possuir as medidas (largura x comprimento x espessura), e a quantidade estimada.</p>


            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>