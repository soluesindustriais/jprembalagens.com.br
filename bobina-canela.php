<?
	$h1    			= 'Bobina canela';
	$title 			= 'Bobina canela';
	$desc  			= 'A bobina canela é um tipo de embalagem reciclada, mas não por isso perde qualidade em relação à bobina feita com matéria-prima virgem';
	$key   			= 'bobina, canela, bobinas canelas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas canelas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br>    
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            

            <p>A <strong>bobina canela</strong> é uma opção de qualidade e com preço em conta para embalar o seu produto. Confira maiores informações sobre este tipo de embalagem. </p>
            
            <p>Para conquistar novos mercados, fidelizar clientes e aumentar a quantidade de consumidores do seu produto, é necessário ter um produto de qualidade comprovada. Mas, além disso, também é necessário contar com uma embalagem resistente e que faça a proteção adequada do produto. E uma opção que atende a estes requisitos é a <strong>bobina canela</strong>. </p>
            
            <p>A <strong>bobina canela</strong> é um tipo de embalagem reciclada, mas não por isso perde qualidade em relação à bobina feita com matéria-prima virgem.</p>
            
            <p>A <strong>bobina canela</strong> é uma das formas mais econômicas do mercado para embalar grande variedade de produtos, com resistência a quedas, rasgos e rupturas, garantindo integridade da embalagem durante o transporte. </p>

            
            <h2>Entenda como é a bobina canela</h2>
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <p>A <strong>bobina canela</strong> é confeccionada com aparas de plásticos reciclados. Por isso, a embalagem fica com alguns pontos e com coloração marrom claro, por isso o nome canela. Apesar da coloração, a transparência é mantida, sendo possível visualizar o conteúdo interno da embalagem. </p>
            
            <p>Vale destacar que a <strong>bobina canela</strong> é um material que continua bastante resistente a danos e rupturas, já que a embalagem conta com uma solda reforçada para garantir a segurança. </p>
            
            <p>E para adquirir a <strong>bobina canela</strong>, conte com os benefícios da JPR Embalagens. A empresa está há mais de 15 anos no mercado de embalagens plásticas flexíveis, e tem como objetivo identificar oportunidades de melhoria e fazer com que o cliente possa reduzir custos e preços, sempre utilizando embalagens que se caracterizam pela excelência. </p>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Para isso, a JPR Embalagens conta com equipe atenta às novidades, de modo a proporcionar o que há de mais moderno neste segmento, tudo com preço em conta e condições vantajosas de pagamento. </p>
            
            <p>Na JPR Embalagens o atendimento é personalizado de acordo com os diferentes tipos de demanda dos clientes, confirme necessidade e preferências. É possível personalizar a <strong>bobina canela</strong> com impressão em várias cores, e a empresa também disponibiliza outros tipos de bobinas de material reciclado, como cristal e colorida, por exemplo. </p>
            
            <p>Invista em uma embalagem de qualidade, com preço em conta e que também está alinhada com as questões ambientais. Entre já em contato com os profissionais da JPR Embalagens para saber mais sobre os produtos e as vantagens que a empresa proporciona. Peça já o seu orçamento. </p>

            
            <? include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<? include('inc/coluna-lateral-paginas.php')?>
        
			<? include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>