<?php
$h1    			= 'Saco PP impresso';
$title 			= 'Saco PP impresso';
$desc  			= 'O saco PP impresso é fabricado sob medida, de acordo com a necessidade de cada cliente e pode ser liso ou personalizado em até 6 cores';
$key   			= 'Sacos PP impressos, Saco, sacos, PP, impresso, saco polipropileno impresso';
$var 			= 'Sacos PP impressos';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             <p>O <strong>saco PP impresso</strong> é fabricado sob medida, de acordo com a necessidade de cada cliente e pode ser liso ou personalizado em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>É amplamente utilizado para envio de produtos promocionais, malas diretas, convites e outros, além dar uma melhor apresentação, demonstração e proteção do seu produto.</p>
             <h2>É uma embalagem muito versátil, e pode receber diversos acessórios e fechos como:</h2>
             <ul class="list">
                <li><strong>Saco PP impresso com aba adesiva</strong></li>
                <li><strong>Saco PP impresso com solapa</strong></li>
                <li><strong>Saco PP impresso com botão</strong></li>
                <li><strong>Saco PP impresso com solapa e aba adesiva</strong></li>
            </ul>	
            <p>A principal característica do <strong>saco PP impresso</strong>, é o brilho intenso e a transparência que a embalagem possui. </p>
            <p>A resistência à tração desse modelo de embalagem é maior que a do <strong>saco de polietileno</strong>, iniciando o rompimento da embalagem logo após o seu estiramento máximo.</p>
            <p>Uma novidade que o mercado esta adotando, é o <strong>saco PP impresso com aditivo oxibiodegradavel</strong>. Neste formato, a embalagem se degrada em curto espaço de tempo, sem deixar resíduos nocivos ao meio ambiente.</p>
            <p>Nossa quantidade mínima de produção são de 250kg para <strong>sacos PP impresso</strong> e 150kg liso.</p>
            <p>Para receber um orçamento de <strong>sacos PP impresso</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>