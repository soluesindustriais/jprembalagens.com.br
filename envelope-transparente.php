<?php
	$h1    			= 'Envelope transparente';
	$title 			= 'Envelope transparente';
	$desc  			= 'O envelope transparente pode ser personalizado de acordo com a sua necessidade, basta ter uma arte desenvolvida, e assim é possível imprimir na embalagem plástica.';
	$key   			= 'Envelopes, Envelope, transparente, Envelopes transparentes, envelope translucido, envelope incolor';
	$var 			= 'Envelopes transparentes';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <h2>A versatilidade do envelope transparente</h2>                      
            <p>Para você que busca uma embalagem simples e para produtos não sigilosos, indicamos trabalhar com <strong>envelope transparente</strong>. Este modelo de embalagem flexívél pode ser fabricado com matéria-prima virgem ou reciclado, além de poder ser natural ou pigmentado.</p>
            
            <p>O <strong>envelope transparente</strong> pode ser personalizado de acordo com a sua necessidade, basta ter uma arte desenvolvida, e assim é possível imprimir na embalagem plástica.</p>
            
            <p>Por ser uma embalagem moderna e ao mesmo tempo simples, pois esse produto pode trabalhar com diversos tipos de fecho, como aba adesiva permanente, indicada para produtos que precisam ser acessado diversas vezes, e adesivo permanente, que para se acessar o produto, é necessário violar e danificar a embalagem. Devido à versatilidade que o <strong>envelope transparente</strong> possui, ele poder ser usado para embalar diversos produtos, como roupas, bijuterias, alimentos, revistas, convitea, jornais, entre muitos outros. </p>
            
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Se você procura por redução de custos, utilize <strong>envelope transparente reciclado</strong>. Além de ser uma grande economia, este produto na versão reciclado também pode ser personalizado, e é produzido a partir da reciclagem de outras embalagens, como embalagens alimentícias, <a href="<?=$url;?>sacos" title="Sacos"><strong>sacos</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, entre outros.</p>
            <p>Além de fechos adesivados, o <strong>envelope transparente</strong> também pode ser fabricado com fecho zip, que é um fecho super moderno e ideal para inovar a sua embalagem. O fecho zip é composto por dois trilhos de plástico (macho/fêmea), poucas embalagens protegem o produto como o <strong>envelope transparente com fecho zip</strong>.</p>
            
            <p>Mas, diferentemente de qualquer outra, só o zip continua protegendo após a abertura.</p>
            
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>


            <p>Além de <strong>envelope transparente</strong>, a JPR Embalagens trabalha com outras linhas de embalagem, como <a href="<?=$url;?>sacola-plastica" title="Sacola Plastica"><strong>sacolas plásticas</strong></a>, <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelopes de segurança</strong></a>, bobinas plásticas e embalagens especiais.</p>
            <p>Para <strong>envelope transparente impresso</strong>, produzimos a partir de 300kg e para os lisos 200kg.</p>
            <p>Para receber um orçamento de nossos produtos basta entrar em contato com um de nossos consultores, e informe as medidas (largura x comprimento + aba x espessura) e a quantidade que deseja utilizar.</p>

            
        
			<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>