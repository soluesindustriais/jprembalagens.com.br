<?php
$h1    			= 'Sacola plástica tipo camiseta';
$title 			= 'Sacola plástica tipo camiseta';
$desc  			= 'A sacola plástica tipo camiseta , é fabricada em polietileno de alta (PEAD) ou baixa densidade (PEBD), podendo ser liso ou impressa em até 6 cores.';
$key   			= 'Sacola, plástica, tipo, camiseta, Sacolas plásticas tipo camiseta, sacola plástica tipo camiseta reciclada';
$var 			= 'Sacolas plásticas tipo camiseta';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 

             <p>A <strong>sacola plástica tipo camiseta</strong>, é fabricada em polietileno de alta (PEAD) ou baixa densidade (PEBD), podendo ser liso ou impressa em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>São infinitas as aplicações da <strong>sacola plástica tipo camiseta</strong>, é um produto super versátil, desde transportes de produtos em geral, e em ultimo caso, a <strong>sacola</strong> também é utilizada para transportar lixos.</p>
             <h2>Sacola plástica tipo camiseta, um produto ecologicamente correto</h2>
             <p>Oxibiodegradavel ou reciclada, são alternativas para obtermos um produto sustentável e ecologicamente correto.</p>
             <p>Na opção da <strong>sacola plástica tipo camiseta oxibiodegradavel</strong>, quando a <strong>sacola</strong> entre em contato com o meio ambiente, se degrada em um curto espaço de tempo, em média seis meses.</p>
             <p>Já a <strong>sacola plástica tipo camiseta reciclada</strong>, é produzida a partir das aparas do material virgem, e também com embalagens que já foram recicladas, como embalagens de alimento, <a href="<?=$url;?>sacos" title="Sacos"><strong>sacos</strong></a>, <a href="<?=$url;?>envelope-plastico" title="Envelope Plástico"><strong>envelopes plásticos</strong></a>, entre outros. Além de você contribuir com o meio ambiente, estará reduzindo custos com embalagens, pois a matéria-prima reciclada é bem mais em conta que a matéria-prima virgem. O produto continuará com a mesma resistência e qualidade que o material virgem, porém a embalagem fica com alguns pontos, devido possuir uma porcentagem de matéria-prima reciclada.</p>
             <p>Para <strong>sacola plástica tipo camiseta personalizada</strong>, nosso lote mínimo de produção são de 250kg e sem impressão 150kg.</p>
             <p>Para receber um orçamento de <strong>sacola plástica tipo camiseta</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>