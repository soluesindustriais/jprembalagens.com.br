<?php
	$h1    			= 'Envelope de segurança';
	$title 			= 'Envelope de segurança';
	$desc  			= 'O envelope de segurança protegem o seu produto desde o embarque até a chegada ao destinatário final, por ser um envelope inviolável';
	$key   			= 'Envelopes de segurança, Envelopes, Envelope, segurança';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes de segurança';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
            
            <p>Trabalhamos com <strong>envelope de segurança</strong> em diversos formatos e cores. Os <strong>envelopes de segurança</strong> protegem o seu produto desde o embarque até a chegada ao destinatário final, por ser um <a href="<?=$url;?>envelope-inviolavel" title="Envelope Inviolável"><strong>envelope inviolável</strong></a>.</p>
            
            
            <p>Cada vez vem que uma encomenda é enviada por correio, courrier ou transportadoras, quase sempre ela vai protegida por um dos nossos <strong>envelopes de segurança</strong>. Nossos <strong>envelopes de segurança</strong> podem ser lisos ou personalizados de acordo com a necessidade de cada cliente.</p> 
            
            <p>O <strong>envelope de segurança</strong> com bolha interna foi desenvolvido especialmente para transportes de produtos frágeis, e dessa forma protege o produto contra quedas e arranhões. Os <strong>envelopes de segurança</strong> podem receber abas adesivas de nível 1 a 5, inclusive com a impressão de números sequenciais.</p>
            
            <p>Fabricados em polietileno coextrudado, em 2 cores diferentes, nas partes interna e externa, ou monocamada (uma só cor) são opções do <strong>envelope de segurança</strong>.  Ou seja, atendimento específico das suas necessidades. Para <strong>envelopes de segurança</strong> impresso fabricamos a partir de 300kg e sem personalização 200kg.</p>
            
            
             <? $pasta = "imagens/produtos/envelopes/"; $quantia = 3; include('inc/gallery.php'); ?>
            
            <h2>Envelopes de segurança sustentáveis</h2>
            
            <p>Os <strong>envelopes de segurança</strong> podem ser confeccionados com aditivo oxi-biodegradavel, e neste formato, a embalagem em contato com a natureza se degrada em um período de até 6 meses. Esta uma opção para você contribuir com o meio ambiente. A natureza agradece.</p> 
            
            <p>Por ser um produto versátil, o <strong>envelope de segurança</strong> protege seu produto contra a luz, ou seja, não é possível visualizar o que há dentro da embalagem. São infinitas as aplicações do <strong>envelope de segurança</strong>, como envio de cheques, transporte de valores, produtos de e-commerce, entre outros.</p>
            
            <p>Realizamos projetos de redução de custo, para isto, é necessário recebermos uma amostra da sua embalagem e os detalhes do produto a ser embalado. A JPR Embalagens fabrica diversos modelos e medidas de <strong>envelopes de segurança</strong>, basta você cliente ter as medidas (largura x comprimento + aba x espessura) e a quantidade que será utilizada.</p>
          
			<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>