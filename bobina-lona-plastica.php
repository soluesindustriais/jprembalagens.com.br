<?
	$h1    			= 'Bobina de lona plástica';
	$title 			= 'Bobina de lona plástica';
	$desc  			= 'A bobina de lona plástica é um tipo de bobina que pode ser confeccionado com polietileno de baixa densidade, com coloração negra ou de outra cor em toda a sua extensão';
	$key   			= 'bobina, lona, plástica, bobina plástica, lona plástica, bobinas de lonas plásticas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de lonas plásticas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
           	<p>Saiba mais sobre a <strong>bobina de lona plástica</strong>, material versátil e que pode ser empregado para as mais diversas situações.</p>

            <p>Para vender, conquistar novos mercados e fidelizar clientes, uma empresa deve investir na qualidade dos seus produtos. Mas, além disso, também é preciso levar em consideração outros elementos, como a embalagem que protege este produto. A embalagem precisa ser resistente para garantir a proteção durante todas as etapas. Por isso, conte com a <strong>bobina de lona plástica</strong>.</p>
            
            <p>A <strong>bobina de lona plástica</strong> é um tipo de bobina que pode ser confeccionado com polietileno de baixa densidade, com coloração negra ou de outra cor em toda a sua extensão.</p>
            
            <p>A durabilidade do produto varia conforme o meio de fabricação, ou seja, varia devido a diferenças de aditivação e de espessura.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>No caso de lonas pretas, uma das grandes vantagens é a proteção total do que está sendo envolto contra os raios solares, que muitas vezes podem causar danos ou alterações em produtos.</p>
            
            <p>As formas de utilização mais comuns da <strong>bobina de lona plástica</strong>, além do uso para a proteção contra luz, são forrações, transporte de produtos e coberturas diversas, como cobertura de pisos para fazer pinturas, por exemplo.</p>
            
            <h2>Bobina de lona plástica com preço em conta e ótimas condições de pagamento</h2>

            
            <p>Para adquirir a <strong>bobina de lona plástica</strong>, aproveite as vantagens da JPR Embalagens. A empresa está no mercado há mais de 15 anos, com equipe técnica com vasta experiência na área de embalagens plásticas flexíveis.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>  
                      
            <p>Além de <strong>bobina de lona plástica</strong>, a empresa disponibiliza bobinas de outros materiais, além de envelopes, sacos, sacolas, filmes, entre outros tipos de material. Tudo isso sempre levando ao consumidor o que há de mais moderno e com qualidade comprovada, mas com preço em conta, proporcionando ótima relação custo-benefício. Com isso, sua empresa pode reduzir custos e melhorar processos.</p>
            
            <p>O atendimento na JPR Embalagens é personalizado conforme as necessidades e preferências de cada cliente. Saiba mais sobre as condições da empresa e esclareça dúvidas sobre a <strong>bobina de lona plástica</strong> entrando em contato com um dos consultores. Aproveite e solicite já o seu orçamento, informando medidas e quantidade que a sua empresa necessita.</p>
 
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>