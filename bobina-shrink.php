<?php
	$h1    			= 'Bobina shrink';
	$title 			= 'Bobina shrink';
	$desc  			= 'A bobina shrink se destaca no mercado por ser uma solução inteligente para uma série de problemas de embalagem no varejo e na indústri';
	$key   			= 'bobina, shrik, bobinas shrink';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas shrink';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Confira opção de embalagem que se adéqua ao formato do produto. Confira maiores informações sobre a <strong>bobina shrink</strong>.</p>

            <p>Uma embalagem de qualidade é um ponto fundamental para passar uma imagem melhor da sua empresa no mercado e para garantir a proteção adequada do seu produto. Por isso, uma ótima indicação é a <strong>bobina shrink</strong>.</p>
            
            <p>A <strong>bobina shrink</strong> se destaca no mercado por ser uma solução inteligente para uma série de problemas de embalagem no varejo e na indústria, por se adequar ao formato do produto. Por isso, é a opção ideal para embalar alguns objetos tais como brinquedos, ferramentas e utensílios domésticos.</p>
            
            <p>A <strong>bobina shrink</strong> proporciona praticidade e economiza espaço, facilitando o transporte das mercadorias. Além disso, outra utilização muito frequente deste tipo de embalagem é para mercadorias que demandam embalagens múltiplas, objetos de formatos irregulares ou que tenham bordas afiadas.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
             
            <p>Por toda a versatilidade que a <strong>bobina shrink</strong> oferece, ela tem ampla utilização no mercado, sendo bastante usada em segmentos como: alimentos e bebidas, editoras, farmácias, cosméticos, gráficas, eletrônico, brinquedos, higiene e limpeza.</p>
            
            <p>Outras características da <strong>bobina shrink</strong> são a alta resistência, ideal para proteger o que está sendo embalado e a boa transparência, proporcionando destaque ao produto. Além disso, possibilitam maior facilidade de manipulação e tem impermeabilização.</p>
            
            <p>A aplicação desta embalagem pode ser feita em máquinas automáticas e semiautomáticas e, por ser uma embalagem plástica com flexibilidade, é possível fabricá-la também com baixas taxas de encolhimento.</p>
            
            <h2>Confira onde adquirir bobina shrink</h2>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para adquirir a <strong>bobina shrink</strong>, conte com a JPR Embalagens. A empresa está no mercado há mais de 15 anos e é especialista em embalagens plásticas flexíveis, levando até os clientes opções de qualidade e com preço em conta. O objetivo principal da empresa é proporcionar ótima relação custo-benefício, reduzindo custos e perdas dos clientes e identificando oportunidades de melhoria. O atendimento na JPR Embalagens é personalizado e voltado às necessidades, demandas e preferências de cada consumidor. Por isso, aproveite. Entre em contato com um dos consultores para esclarecer eventuais dúvidas sobre a <strong>bobina shrink</strong> e conhecer nossos outros produtos. Além de bobinas, também temos filmes, envelopes, sacos, sacolas, entre outros. Aproveite as ótimas condições e solicite já o seu orçamento, informando medidas e quantidade que a sua empresa necessita.</p>

            
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>