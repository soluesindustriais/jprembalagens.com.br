<?
$h1            = 'Rolo de Plástico';
$title         = 'Rolo';
$desc          = 'A JPR Embalagens traz para o mercado uma linha completa de rolo de plástico. Em nossa linha de produtos você encontra rodo de plástico fabricados sob medida.';
$key           = 'rolo de plástico, plástico, fábrica de plástico';
$var         = 'Rolo de Plástico';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php'); ?>

<!-- Função Regiões -->
<script src="<?= $url; ?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>

<body>

    <div class="wrapper-topo">

        <? include('inc/topo.php'); ?>

    </div>

    <div class="wrapper">

        <main role="main">

            <section>


                <?= $caminhoCategoria ?>
                <article>

                    <h1><?= $h1 ?></h1>

                    <h2>A JPR Embalagens traz para o mercado uma linha completa de envelopes.</h2>

                    <p>Em nossa linha de produtos você encontra <strong>Rolos de plástico</strong>, de acordo com a necessidade da sua empresa.</p>

                    <p>Conheça nossa linha de <strong>rolos de plástico</strong>:</p>
                    <ul class="thumbnails">
                        <li>
                            <a rel="nofollow" href="<?= $url; ?>rolo-plastico-preto" title="Rolo de Plástico Preto"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/rolo/thumb/rolo-plastico-preto-01.jpg" alt="Rolo de Plástico Preto" /></a>
                            <h2><a href="<?= $url; ?>rolo-plastico-preto" title="Rolo de Plástico Preto">Rolo de Plástico Preto</a></h2>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?= $url; ?>rolo-plastico-transparente" title="Rolo de Plástico Transparente"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/rolo/thumb/rolo-plastico-transparente-01.jpg" alt="Rolo de Plástico Transparente" /></a>
                            <h2><a href="<?= $url; ?>rolo-plastico-transparente" title="Rolo de Plástico Transparente">Rolo Plástico Transparente</a></h2>
                        </li>
                        <li>
                            <a rel="nofollow" href="<?= $url; ?>saco-plastico-transparente-200-litros" title="saco plástico transparente 200 litros"><img data-src="<?= $url; ?>imagens/produtos/rolo-de-plastico/thumb/rolo-de-plastico-4.jpg" alt="saco plástico transparente 200 litros" class="lazyload" title="saco plástico transparente 200 litros" /></a>
                            <h4><a href="<?= $url; ?>saco-plastico-transparente-200-litros" title="saco plástico transparente 200 litros">saco plástico transparente 200 litros/a></h4>
                        </li>


                    </ul>

                    <? include('inc/saiba-mais.php'); ?>

                </article>

                <? include('inc/coluna-lateral-paginas.php'); ?>

                <br class="clear" />

                <? include('inc/regioes.php'); ?>

            </section>

        </main>



    </div><!-- .wrapper -->



    <? include('inc/footer.php'); ?>


</body>

</html>