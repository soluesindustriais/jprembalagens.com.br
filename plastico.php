<?
$h1    		= 'Plástico';
$title 		= 'Plástico';
$desc  		= 'A JPR Embalagens traz para o mercado uma linha completa de envelopes. Em nossa linha de produtos você encontra plásticos para diversas finalidades.';
$key   		= 'Plástico, Plástico reciclado, Plástico encolhível';
$var 		= 'Plásticos';

include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>


  <? include('inc/topo.php');?> 

  
  <div class="wrapper">
    
    <main role="main">
      
      <section>
       
          
          <?=$caminhoCategoria?>
          <article>
          
          <h1><?=$h1?></h1>    
          
          <h2>A JPR Embalagens traz para o mercado uma linha completa de plásticos.</h2>
          
          <p>Em nossa linha de produtos você encontra <strong>plásticos para várias finalidades</strong>, de acordo com a necessidade da sua empresa.</p>
          
          <p>Conheça nossa linha de <strong>plásticos</strong>:</p>
          
          <ul class="thumbnails">
           <li>
            <a rel="nofollow" href="<?=$url;?>plastico-bolha" title="Plástico Bolha"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-bolha-01.jpg" alt="Plástico Bolha" /></a>
            <h2><a href="<?=$url;?>plastico-bolha" title="Plástico Bolha">Plástico Bolha</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>plastico-encolhivel" title="Plástico Encolhível"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-encolhivel-01.jpg" alt="Plástico Encolhível" /></a>
            <h2><a href="<?=$url;?>plastico-encolhivel" title="Plástico Encolhível">Plástico Encolhível</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>plastico-filme" title="Plástico Filme"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-filme-01.jpg" alt="Plástico Filme" /></a>
            <h2><a href="<?=$url;?>plastico-filme" title="Plástico Filme">Plástico Filme</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>plastico-pead" title="Plástico Pead"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-pead-01.jpg" alt="Plástico Pead" /></a>
            <h2><a href="<?=$url;?>plastico-pead" title="Plástico Pead">Plástico Pead</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>plastico-preto-rolo" title="Plástico Preto Rolo"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-preto-rolo-01.jpg" alt="Plástico Preto Rolo" /></a>
            <h2><a href="<?=$url;?>plastico-preto-rolo" title="Plástico Preto Rolo">Plástico Preto Rolo</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>plastico-reciclado" title="Plástico Reciclado"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-reciclado-01.jpg" alt="Plástico Reciclado" /></a>
            <h2><a href="<?=$url;?>plastico-reciclado" title="Plástico Reciclado">Plástico Reciclado</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url;?>plastico-termoencolhivel" title="Plástico Termoencolhível"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/plastico/thumb/plastico-termoencolhivel-01.jpg" alt="Plástico Termoencolhível" /></a>
            <h2><a href="<?=$url;?>plastico-termoencolhivel" title="Plástico Termoencolhível">Plástico Termoencolhível</a></h2>
          </li>
        </ul>

        
        <? include('inc/saiba-mais.php');?>
        
      </article>
      
      <? include('inc/coluna-lateral-paginas.php');?>   
      
      <br class="clear" />  
      
      <? include('inc/regioes.php');?>
      
    </section>
  </main>

  
  
</div><!-- .wrapper -->



<? include('inc/footer.php');?>


</body>
</html>