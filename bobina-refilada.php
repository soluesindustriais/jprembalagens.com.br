<?php
	$h1    			= 'Bobina refilada';
	$title 			= 'Bobina refilada';
	$desc  			= 'A bobina refilada pode ser produzida em polietileno de baixa ou alta densidade. Ela é aberta de um lado e pode ser produzida em diferentes tamanhos e cores.';
	$key   			= 'bobina, refilada, bobinas refiladas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas refiladas';
	
	include('inc/head.php');
?>


<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Confira opção de embalagem resistente e com preço em conta. Saiba mais sobre as vantagens da <strong>bobina refilada</strong>.</p>

            <p>Na hora de divulgar a sua marca para atrair a atenção de novos consumidores e alcançar novos mercados, cada detalhe é fundamental. Por isso, a indicação é investir em uma embalagem de qualidade e com bom aspecto visual, como é o caso da bobina refinada.</p>
            
            <p>A <strong>bobina refilada</strong> pode ser produzido em polietileno de baixa ou alta densidade. Ela é aberta de um lado e pode ser produzida em diferentes tamanhos e cores, sempre mantendo a qualidade do produto.</p>
            
            <p>A <strong>bobina refilada</strong> é um tipo de embalagem muito comum nos mercados de construção civil, farmacêutico, cosmético, metalúrgico e alimentício. A aplicação mais comum da <strong>bobina refilada</strong> é para embalagens de peças, sachês de sal e açúcar, condimentos diversos, medicamentos, etc.</p>

            
            <h2>Personalização da bobina refilada na JPR Embalagens: uma maneira de divulgar a sua marca</h2>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <p>A <strong>bobina refilada</strong> é bastante usada em lojas, estabelecimentos de um modo geral, além de feiras e supermercado, graças à possibilidade de personalização do produto, com impressão em até seis cores diferentes. Com isso, além de proporcionar melhor serviço, a <strong>bobina refilada</strong> faz com que sua empresa possa manter a imagem sempre próxima dos clientes.</p>
            
            <p>E para adquirir a <strong>bobina refilada</strong>, conte com os benefícios da JPR Embalagens. A empresa atua no segmento de embalagens plásticas flexíveis há mais de 15 anos e tem como objetivo principal identificar oportunidades de melhoria e fazer com que a sua empresa reduza perdas e, consequentemente, custos.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A empresa conta com equipe sempre atenta às novidades e ao que há de mais moderno neste segmento, oferecendo embalagens que se destacam pela qualidade e pelos preços reduzidos, resultando em ótima relação custo-benefício. Além de <strong>bobina refilada</strong>, temos outros tipos de bobina</p>
            
            <p>Além disso, o atendimento da JPR Embalagens é totalmente personalizado, conforme necessidades e preferências dos clientes. Por isso, aproveite. Entre em contato com um dos nossos consultores e saiba mais sobre os produtos, além de conhecer as ótimas condições de pagamento e os preços em conta que a empresa disponibiliza. Solicite já o seu orçamento, informando quantidade e medidas que você necessita.</p>
            
                        
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>