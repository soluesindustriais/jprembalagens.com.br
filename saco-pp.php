<?php
$h1    			= 'Saco PP';
$title 			= 'Saco PP';
$desc  			= 'Devido ao brilho intenso que o saco PP possui, acaba sendo adotado por varias empresas, pois ajuda no acabamento da embalagem, além de dar ótima impressão ao seu produto.';
$key   			= 'Sacos PP, Saco, sacos, PP, saco polipropileno';
$var 			= 'Sacos PP';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             <p>Umas das embalagens com maior brilho e transparência são os <strong>sacos PP</strong> (polipropileno), além de possuírem boa resistência a gases e ao vapor d’agua.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>Devido ao brilho intenso que o <strong>saco PP</strong> possui, acaba sendo adotado por varias empresas, pois ajuda no acabamento da embalagem e além de dar uma ótima impressão do seu produto por ser brilhoso e possuir uma excelente transparência.</p>
             <p>Fabricados sob medida, o <strong>saco PP</strong> pode ser liso ou impresso em até 6 cores. </p>
             <p>É empregado em produtos alimentícios de rápido consumo, como a pipoca doce, e também é muito utilizado para embalar revistas, convites, roupas, entre vários outros.</p>
             <p>O <strong>saco PP</strong> pode ser lacrado e duas formas, com a seladora manual ou com aba adesiva.</p>
             <p>Se você possui uma grande escala de produção e manuseio de produtos, utilize o <strong>saco PP</strong> com aba adesiva, pois basta destacar o liner e dobrar a aba, pronto, já esta lacrada.</p>
             <p>Além de <strong>sacos PP</strong>, trabalhamos também com sacos de polietileno, sacos para presentes, sacos para envio de revistas, envelopes plásticos, sacolas plásticas em geral e embalagens especiais.</p>
             <p>Nossa quantidade mínima de produção são de 100kg para <strong>sacos PP</strong>  liso e 250kg impresso.</p>
             <p>Para receber um orçamento de <strong>sacos PP</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>