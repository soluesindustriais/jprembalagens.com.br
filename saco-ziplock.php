<?php
$h1    			= 'Saco ziplock';
$title 			= 'Saco ziplock';
$desc  			= 'O saco ziplock pode ser aberto infinitas vezes, sem perder a aderência, facilitando a vida do consumidor. E ótimo para você que ganha um novo argumento de venda.';
$key   			= 'Sacos ziplock, Saco, sacos, ziplock';
$var 			= 'Sacos ziplock';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  
             
             <p>Se você procura uma embalagem segura e moderna, utilize o <strong>saco ziplock</strong>. Fabricado em polietileno de baixa densidade, o <strong>saco ziplock</strong> pode ser liso e impresso em até 6 cores.</p>
             
             
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <h2>A versatilidade do saco ziplock</h2>
             
             <p>São infinitas as aplicações do <strong>saco ziplock</strong>, é uma embalagem resistente, atóxica, e protege o seu produto contra umidade e poeira.</p> 
             <p>Uma forma de você e sua empresa contribuírem com o meio ambiente, é utilizando o <strong>saco ziplock com aditivo oxibiodegradavel</strong>. Neste modelo, a embalagem se degrada em curto espaço de tempo, evitando a poluição do nosso meio ambiente. A natureza agradece.</p>
             <p>É uma embalagem super moderna, pois o fecho zip é produzido em polietileno em formato de dois trilhos (macho e fêmea), e são aplicados na parte superior da embalagem.  Assim o <strong>saco ziplock</strong> pode ser aberto infinitas vezes, sem perder a aderência, facilitando a vida do consumidor. E ótimo para você que ganha um novo argumento de venda.</p>
             <p>É fabricado sob medida, de acordo com a necessidade de cada cliente. Assim, crie e desenvolva o <strong>saco ziplock</strong> da maneira que desejar. Possuímos equipamentos de ultima geração, e assim garantimos a qualidade que o seu produto merece.</p>
             <p>A JPR Embalagens trabalha com diversos modelos de embalagens, além do <strong>saco ziplock</strong>, fabricamos também <a href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip"><strong>sacola plastica zip</strong></a>, <strong>envelopes com fecho zip</strong>, <a href="<?=$url;?>sacola-plastica-ziper" title="Sacola Plastica Com Ziper"><strong>sacola plastica com ziper</strong></a>, <a href="<?=$url;?>saco-zip-personalizado" title="Saco Zip Personalizado"><strong>saco zip personalizado</strong></a>, entre outros modelos.</p>
             <p>Nossa quantidade mínima de produção são de 150kg para <strong>sacos ziplock liso</strong> e 250kg para <strong>sacos ziplock impresso</strong>.</p>
             <p>Para receber um orçamento de <strong>sacos ziplock</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>