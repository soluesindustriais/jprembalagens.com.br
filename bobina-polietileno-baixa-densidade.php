<?php
	$h1    			= 'Bobina de polietileno de baixa densidade';
	$title 			= 'Bobina de polietileno de baixa densidade';
	$desc  			= 'A bobina de polietileno de baixa densidade, também conhecida pela sigla PEBD, é uma embalagem que se caracteriza pela voa resistência a rasgo, tração e selagem.';
	$key   			= 'bobina, polietileno, baixa densidade, bobinas de baixa densidade, bobinas de polietileno em baixas densidade';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de polietileno em baixas densidades';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>   
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
          	<p>Embalagem resistente, a <strong>bobina de polietileno de baixa densidade</strong> tem ampla utilização. Saiba mais sobre este tipo de material.</p>

            <p>Além de investir na qualidade do produto, outro aspecto fundamental é a qualidade da embalagem que o envolve, que precisa ser resistente. Por isso, uma opção altamente recomendada é a <strong>bobina de polietileno de baixa densidade</strong>.</p>
            
            <p>A <strong>bobina de polietileno de baixa densidade</strong>, também conhecida pela sigla PEBD, é uma embalagem que se caracteriza pela voa resistência a rasgo e tração, além de ter uma ótima selagem. Por isso, é utilizada para embalar uma enorme gama de produtos.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <h2>Bobina de polietileno de baixa densidade pode ser feita com material reciclado</h2>

            
            <p>Uma opção para contribuir com o meio ambiente é utilizar a <strong>bobina de polietileno de baixa densidade</strong>. Além desta vantagens, há outras mais: a melhoria da imagem da sua empresa no mercado por indicar a preocupação com a sustentabilidade e a redução nos custos com embalagem.</p>
            
            <p>No caso de <strong>bobina de polietileno de baixa densidade</strong> feita com material reciclado, existem três formas de produção: cristal, canela e colorido. No cristal, a fabricação é feita a partir de aparas de material virgem e o aspecto da embalagem é amarelo claro.</p>
            
            <p>No caso de fabricação com reciclado canela, são utilizadas aparas de material reciclado cristal, fazendo com que a embalagem tenha aspecto marrom claro. Em ambos os casos, a transparência é mantida. Também há a opção em reciclado colorido, feito a partir da mistura de vários plásticos, que resulta em uma embalagem sem padrão de cor definido e sem transparência.</p>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E na hora de solicitar a <strong>bobina de polietileno de baixa densidade</strong>, conte com a JPR Embalagens. A empresa está no mercado há mais de 15 anos, levando até os clientes as melhores soluções na área de embalagens plásticas flexíveis, sempre buscando reduzir custo e perdas e identificar oportunidades de melhoria.</p>
            
            <p>A JPR Embalagens tem um atendimento totalmente personalizado, além de preços em conta e ótimas condições de pagamento, resultando em excelente relação custo-benefício para o consumidor.</p>
            
            <p>Por isso, aproveite. Entre em contato com um dos consultores para saber mais sobre as bobinas e os outros produtos disponibilizados pela empresa, aproveite as vantagens e solicite já o seu orçamento.</p>

            
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>