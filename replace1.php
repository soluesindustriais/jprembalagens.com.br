
<?php
set_time_limit (0);
$de = array('package-es2015.js');

$para = array("package-es2015.js?v=<?= date_timestamp_get(date_create()); ?>");

if ((float) phpversion() >= 5.3) {
	foreach(glob(__DIR__.'/*') as $arquivo){

		if(is_file($arquivo) && mime_content_type($arquivo) == 'text/html' && strpos($arquivo,"index.php") === false && strpos($arquivo,"replace.php") === false){

			$dados = file_get_contents($arquivo);

			$dados = str_replace($de, $para, $dados);

			file_put_contents($arquivo, $dados);
		}
	} 
} else {	
	foreach(glob($_SERVER['DOCUMENT_ROOT'].'/*') as $arquivo){

		$type = explode(".", $arquivo);
		if(is_file($arquivo) && $type[1] != 'php' && strpos($arquivo,"index.php") === false && strpos($arquivo,"replace.php") === false){

			$dados = file_get_contents($arquivo);

			$dados = str_replace($de, $para, $dados);

			file_put_contents($arquivo, $dados);
		}
	} 
}


if(file_exists('replace.php')){
	unlink('replace.php');
}
exit;


