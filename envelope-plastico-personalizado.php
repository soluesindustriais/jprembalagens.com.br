<?php
	$h1    			= 'Envelope plástico personalizado';
	$title 			= 'Envelope plástico personalizado';
	$desc  			= 'O envelope plástico personalizado pode ser usado para embalar diversos produtos por ser uma embalagem segura e resistente.';
	$key   			= 'Envelopes plásticos personalizados, Envelopes, Envelope, plástico, personalizado, envelope personalizado de plástico';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos personalizados';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br>   
                    
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>
                                    
            <p>O <strong>envelope plástico personalizado</strong> pode ser fabricado em polietileno (PE) ou polipropileno (PP). A impressão pode ser feita em até 6 cores.</p>
            

            <p>O <strong>envelope plástico personalizado</strong> pode ser usado para embalar diversos produtos por ser uma embalagem segura e resistente. É amplamente utilizado por transportadora, courriers, laboratórios, editoras, gráficas, entre outros. Além disso, este produto pode ser produzido com aba adesiva, assim facilita durante o manuseio e dá uma melhor apresentação do seu produto, dispensando o uso de seladoras. </p>
            <p>Se você precisa de uma embalagem inviolável, utilize nosso <strong>envelope plástico personalizado</strong> com hotmelt, este é uma adesivo permanente, e para ser violado é necessário danificar a embalagem. Nosso lote mínimo de produção de <strong>envelope plástico personalizado</strong>, são de 250kg e sem personalização 150kg.</p> 
            <p>Por sermos fabrica, não trabalhamos com medidas padrão, assim os <strong>envelopes plásticos personalizados</strong> são fabricados de acordo com a necessidade de cada cliente.</p>
            <p>Com o intuito de contribuir com o meio ambiente, estamos produzindo <strong>envelope plástico personalizado</strong> com matéria-prima reciclada, assim reciclamos varias outras embalagens, e fazemos um novo produto. É uma forma de contribuirmos com o nosso meio ambiente, além do material reciclado proporcionar uma ótima redução no custo da embalagem.</p> 
            <p>Além dos <strong>envelopes personalizados</strong> temos com uma linha completa de embalagens personalizadas como, <a href="<?=$url;?>saco-plastico-personalizado" title="Saco Plastico Personalizado"><strong>Saco Plastico Personalizado</strong></a>, <a href="<?=$url;?>saco-zip-personalizado" title="Saco Zip Personalizado"><strong>Saco Zip Personalizado</strong></a>, <a href="<?=$url;?>sacola-personalizada" title="Sacola Personalizada"><strong>Sacola Personalizada</strong></a>, entre outros.</p>
            <p>Para receber um orçamento de <strong>envelope plástico personalizado</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade que deseja utilizar.</p>

            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>