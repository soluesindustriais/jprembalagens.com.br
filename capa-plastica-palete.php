<?php
$h1    			= 'Capa plástica para palete';
$title 			= 'Capa plástica para palete';
$desc  			= 'A capa plástica para palete é fabricada em polietileno transparente. Este é um material que tem como destaque a resistência mecânica e a durabilidade.';
$key   			= 'capa plástica, capa para palete, palete, capas plásticas para paletes';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Capas plásticas para paletes';

include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
           
                            

             <?=$caminhoProdutoCapas?>
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  
             
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>A <strong>Capa plástica para palete</strong> proporciona segurança para a carga. Confira maiores informações sobre este modelo de embalagem.</p>

            <p>Uma embalagem deve garantir a proteção e as características originais dos produtos, protegendo-os contra qualquer tipo de influências externas. Por isso, uma opção de qualidade é a <strong>Capa plástica para palete</strong>.</p>
            
            <p>A <strong>Capa plástica para palete</strong> é fabricada em polietileno transparente. Este é um material que tem como destaque a resistência mecânica e a durabilidade, graças à exposição às intempéries.</p>
            
            <p>Por causa destas características, a <strong>Capa plástica para palete</strong> proporciona impermeabilidade contra sujeira, exposições a variações de temperatura e qualquer outro tipo de dano, além de proporcionar maior rigidez e segurança na movimentação de cargas e no transporte de produtos.</p>
            
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Além disso, a <strong>Capa plástica para palete</strong> faz com que o processo de inspeção e de identificação da carga seja mais simples e se destaca pela flexibilidade, fazendo com que seja possível acondicionar cargas de tamanhos e disposições diferentes.</p>
            
            <p>Por causa de todas estas vantagens, a <strong>Capa plástica para palete</strong> tem utilização ampla, em indústrias como: químicas, calçadistas, fundição, fumageiras, metalúrgicas e de alimentos, por exemplo, entre outras.</p>
            
            <h2>Capa plástica para palete com qualidade e preços em conta</h2>
            
            <p>Para adquirir a <strong>Capa plástica para palete</strong>, aproveite todas as vantagens e benefícios da JPR Embalagens. A empresa atua na área de embalagens plásticas flexíveis há mais de 15 anos, e por isso conta com uma vasta experiência neste segmento.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>    
            
            <p>A JPR Embalagens tem equipe que é constantemente atualizada e equipamentos modernos e eficientes, levando até o cliente as melhores opções de <strong>Capa plástica para palete</strong>. O objetivo principal da empresa é a identificação de oportunidades de melhoria, o que resulta em redução de perdas, e, consequentemente, de custos.</p>
            
            <p>Além de capa plástica, a JPR Embalagens também disponibiliza sacos, sacolas, envelopes, filmes, bobinas e outros tipos de material, sempre com preços em conta e com condições vantajosas de pagamento.</p>
            
            <p>Além disso, outra vantagem é o atendimento totalmente personalizado. Por isso, aproveite. Entre em contato com um dos profissionais para saber mais sobre os produtos e solicite já o seu orçamento, informando medidas e quantidade que você precisa.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>           
        <br class="clear" />                                                                                                    
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>