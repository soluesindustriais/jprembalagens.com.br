<?php
$h1    			= 'Envelope Adesivado';
$title 			= 'Envelope Adesivado';
$desc  			= 'O envelope adesivado, pode ser fabricado em plástico virgem ou reciclado, pode ser liso ou impresso em até 6 cores';
$key   			= 'Envelopes Adesivados, Envelopes, Adesivados, Envelope com Adesivo';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Envelopes Adesivados';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>            
                			
                <?=$caminhoProdutosEnvelopes?>
                 <article>
                <h1><?=$h1?></h1> 
                <br> 


                <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>      	

                <p>O <strong>envelope adesivado</strong>, pode ser fabricado em plástico virgem ou reciclado, pode ser liso ou impresso em até 6 cores, o este produto possui um sistema prático de fechamento através do adesivo que é aplicado na aba da embalagem. </p>

                <p>Para produtos que não podem ser violados, podemos trabalhar com o <strong>envelope adesivado permanente</strong>, ou seja, para violar a embalagem, será necessário danificá-la para ter acesso ao produto. </p>

                <p>Em outras aplicações, esta embalagem também pode ser fabricado com adesivo abre e fecha, e assim o produto poderá ser acessado por diversas vezes. </p>

                <p>O <strong>envelope adesivado</strong> possui ampla utilização, como envio de revistas, jornais, mala direta, arquivar documentos, embalar roupas, entre muitas outras.  </p>



                <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>  

                <p>Com a praticidade do <strong>envelope adesivado</strong>, ele ajuda a otimizar tempos e processos para embalar seus produtos, dispensa o uso de seladoras, além de dar um acabamento melhor em seu produto.</p>

                <p>Nosso lote mínimo de produção é de 150kg para <strong>envelopes sem impressão</strong> e 250kg para <strong>envelopes com impressão</strong>.</p>

                <p>Para você se diferenciar da concorrência, podemos fabrica-lo em formatos e cores especiais, pois a modernidade da embalagem fala muito do produto que ela contém.</p>

                <h2>Poucas embalagens protegem o produto como o envelope adesivado.</h2>



                <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
                </div>  

                <p>Nossos <strong>envelopes adesivados</strong>, podem ser transparentes ou pigmentados em diversas cores, possui como características praticidade e segurança.</p>

                <p>O fechamento do <strong>envelope adesivado</strong> é feito a partir de um <strong>adesivo termo-plástico</strong>. Por isso, o <strong>envelope adesivado</strong> é ideal para embalar produtos pequenos e objetos em geral.</p>

                <p>Polietileno coextrusado, em 2 cores diferentes, nas partes interna e externa, ou monocamada (uma só cor) são alternativas do material do envelope.</p>

                <p>Fabricamos <strong>envelope adesivado</strong> de acordo com a necessidade de cada cliente. Basta entrar em contato com um de nossos consultores, e informar as medidas (largura x comprimento x espessura) e quantidades que você deseja utilizar. </p>


                <?php include('inc/saiba-mais.php');?>



            </article>

            <?php include('inc/coluna-lateral-paginas.php');?>

            <?php include('inc/paginas-relacionadas.php');?>

            <br class="clear" />                                                                                                    
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        </section>

    </main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>