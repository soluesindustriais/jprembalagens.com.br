<?php
	$h1    			= 'Bobina plástica';
	$title 			= 'Bobina plástica';
	$desc  			= 'A bobina plástica é um produto com aplicações bastante diversas e que pode ser personalizado conforme necessidade do cliente. Confira maiores informações sobre esta embalagem';
	$key   			= 'bobina, plástica, bobinas plásticas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas plásticas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina plástica</strong> é um produto com aplicações bastante diversas e que pode ser personalizado conforme necessidade do cliente. Confira maiores informações sobre esta embalagem.</p>

            <p>Muitas vezes, o primeiro contato que o cliente tem com a sua marca é através da embalagem. Por isso, é necessário que ela tenha um bom aspecto visual e seja de qualidade, apresentando as propriedades e resistência suficientes para garantir total proteção para o seu produto. Neste caso, a <strong>bobina plástica</strong> é uma opção altamente recomendada.</p>

            
            <p>A <strong>bobina plástica</strong> pode ser feita em PEBD, PEAD ou PEBDL. Quando feita em polietileno, é bastante utilizada para alimentos, bebidas, leites e farináceos, por ser um plástico resistente e atóxico, o que permite utilização para embalar qualquer tipo de produto.</p>
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>    
                    
            <p>A <strong>bobina plástica</strong> também pode ser feita em polipropileno, sendo bastante utilizada em confecções, gráficas, além de indústrias alimentícias, devido ao brilho e à transparência que a embalagem possui.</p>
            
            <p>Outra opção é a <strong>bobina plástica</strong> em BOPP, largamente utilizada no mercado de varejo como embalagem de presente, podendo ser fabricada metalizada ou perolizada. A bobina também pode ser feita em material reciclado, o que resulta em maior preservação do meio ambiente e redução de custos para a sua empresa.</p>
            
            <h2>Bobina plástica pode ser personalizada</h2>
            
           	<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Uma vantagem da <strong>bobina plástica</strong> é a personalização. É possível fabricá-las com impressão em até oito cores (flexografia) ou nove cores (rotogravura), lamunação com e sem solventes. É possível confeccionar este tipo de embalagem em vários tamanhos, transparente ou pigmentada, lisa ou impressa, sempre com ótima qualidade.</p>
            
            <p>E para adquirir a <strong>bobina plástica</strong>, conte com os benefícios da JPR Embalagens. Com mais de 15 anos de atuação no mercado, a empresa é especializada na área de embalagens plásticas flexíveis, com equipe técnica sempre atenta para aumentar cada vez mais a qualidade de seus produtos, ao mesmo tempo em que reduz custos e repassa esta redução para os consumidores. A JPR Embalagens disponibiliza um atendimento personalizado, voltado às necessidades específicas e preferências de cada cliente, sempre proporcionando ótima relação custo-benefício.</p>
            
            <p>Saiba mais e aproveite as condições entrando em contato com um dos consultores e peça já o seu orçamento de <strong>bobina plástica</strong>. Aproveite.</p>
            
                        
              <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>