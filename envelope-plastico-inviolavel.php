<?php
	$h1    			= 'Envelope plástico inviolável';
	$title 			= 'Envelope plástico inviolável';
	$desc  			= 'O envelope plástico inviolável é indicado para produtos que requerem segurança desde envio até a chegada em seu destino.';
	$key   			= 'Envelopes plásticos invioláveis, Envelopes, Envelope, plástico, inviolável, envelope de plastico com lacre';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos invioláveis';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                    
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O <strong>envelope plástico inviolável</strong> é indicado para produtos que requerem segurança desde envio até a chegada em seu destino. Fabricados em polietileno coextrusado, essa embalagem flexível possui um adesivo especial em sua aba que o torna <strong>inviolável</strong>, e para se violar a embalagem, é necessário danificá-la.</p>
            <p>O polietileno coextrusado, é a matéria-prima utilizada para confeccionar o <strong>envelope plástico inviolável</strong>, pois o processo de coextrusão une dois ou mais tipos de plástico, e assim torna a embalagem mais resistente ao rasgo e a ruptura.</p>
            <p>É muito empregado em transportadora, bancos, empresas de courrier, correios, transporte de valores, entre outras aplicações.</p>

            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <h2>Fabricamos envelope plástico inviolável personalizado</h2>
            <p>Dependendo das exigências de cada cliente, o <strong>envelope plástico inviolável</strong> pode ser confeccionado com adesivos especiais, que vão do nível de segurança 1 a 5, cada nível possui um sistema de segurança diferente. Geralmente para envio de <a href="<?=$url;?>envelope-plastico-revista" title="Envelope Plástico Revista"><strong>revistas</strong></a> ou <a href="<?=$url;?>envelope-plastico-mala-direta" title="Envelope Plástico Mala Direta"><strong>malas direta</strong></a>, utiliza-se o nível de segurança 1, já para transportes de valores, pode-se utilizar o nível de segurança 3, 4 ou 5.</p>
            <p>Além de ser uma embalagem segura, esse produto pode ser liso ou personalizado em até 6 cores, assim permite o cliente divulgar a sua marca. </p>

            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Outras opções são os <strong>envelopes plásticos inviolável transparentes ou pigmentados</strong>, porem fabricados em monocamada, ou seja, um único filme. Esta é uma forma mais econômica para produzir o <strong>envelope plástico inviolável</strong>.</p>
            <p>Produzimos a partir de 300kg para <strong>envelope plástico inviolável impresso</strong> e 150kg para envelopes lisos.</p>
            <p>Para receber um orçamento de nossos produtos, basta possuir as medidas (largura x comprimento x espessura), e a quantidade que você deseja utilizar.</p>

            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>