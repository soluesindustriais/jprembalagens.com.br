<?php session_start(); ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<link rel="preconnect" href="https://fonts.googleapis.com" />
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
	<? include "$linkminisite" . "inc/geral.php"; ?>
	<? include "$linkminisite" . "inc/jquery.php"; ?>
	<? include "$linkminisite" . "config.php";?>
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" />
	<link href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet"/>
	<script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
	<script src="<?= $linkminisite ?>js/lazysizes.min.js"></script>
	<script src="<?= $linkminisite ?>js/header-create.js"></script>

	<script src="<?= $linkminisite ?>js/readmore.min.js"></script>

	

	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
	<script>
		<?php
		include "$linkminisite" . "auto-breadcrumb.js";
		include "$linkminisite" . "js/vendor/modernizr-2.6.2.min.js";
		include "$linkminisite" . "js/jquery.slicknav.js";
		
		?>
		<?php
		function remove_acentos($string)
		{
			return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U c C"), $string);
		}
		?>
	</script>
	<title><?= $title . " - " . $nomeSite ?></title>
	<base href="<?= $url ?>">
	<?php
	$desc = strip_tags($desc);
	$desc = str_replace('  ', ' ', $desc);
	$desc = str_replace(' ,', ',', $desc);
	$desc = str_replace(' .', '.', $desc);
	if (mb_strlen($desc, "UTF-8") > 160) {
		$desc = mb_substr($desc, 0, 159);
		$finalSpace = strrpos($desc, " ");
		$desc = substr($desc, 0, $finalSpace);
		$desc .= ".";
	} else if (mb_strlen($desc, "UTF-8") < 140 && mb_strlen($desc, "UTF-8") > 130) {
		$desc .= "... Saiba mais.";
	}
	?>
	<meta name="description" content="<?= ucfirst($desc) ?>">
	<meta name="keywords" content="<?= str_replace($prepos, ', ', $h1) . ', ' . $nomeSite ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="geo.position" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>">
	<meta name="geo.region" content="<?= $UF ?>-BR">
	<meta name="ICBM" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?= $url . $urlPagina ?>">
	<?php
	if ($author == '') {
		echo '<meta name="author" content="' . $nomeSite . '">';
	} else {
		echo '<link rel="author" href="' . $author . '">';
	}
	?>

<link rel="shortcut icon" href="<?= $linkminisite ?>imagens/logo-cliente-2.webp">

	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
	<meta property="og:type" content="article">
	<?php
	if (file_exists($url . $pasta . $urlPagina . "-01.jpg")) {
	?>
		<meta property="og:image" content="<?= $url . $pasta . $urlPagina ?>-01.jpg">
	<?php
	}
	?>
	<meta property="og:url" content="<?= $url . $urlPagina ?>">
	<meta property="og:description" content="<?= $desc ?>">
	<meta property="og:site_name" content="<?= $nomeSite ?>">
	<!-- Desenvolvido por <?= $creditos . " - " . $siteCreditos ?> -->
	<? include "$linkminisite" . "inc/header-scroll.php;" ?>

	<link rel="preload" as="style" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" onload="this.rel='stylesheet'">
	<script>
		initReadMore('.ReadMore', {
			collapsedHeight: 200,
			heightMargin: 50,
		});
	</script>





		