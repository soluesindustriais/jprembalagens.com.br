<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Acomex Embalagens';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Distribuição de embalagens industriais';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);


//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "fita-adesiva-para-embalagem-marrom",
    "selo-metalico-para-fita-de-arquear",
    "rolo-de-fita-crepe-para-pintura",
    "cantoneira-de-papelao-para-paletizacao",
    "fita-de-arquear-16mm",
    "papel-semi-kraft-bobina",
    "fita-adesiva-para-embalagem",
    "fita-de-arquear-13mm",
    "medidor-de-espessura-digital",
    "estacao-meteorologica-convencional",
];

//Criar página de Serviço
$VetPalavrasInformacoes = [
];


// Numero do formulario de cotação
$formCotar = 270;

// Informações Geral.php


$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Acomex Embalagens';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Distribuição de embalagens industriais';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Contato';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : '11 94081-9167 / 91480-3434';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : '';
$UF = ($clienteAtivo == 'inativo') ? "SP" : '';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : '';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';

?>

<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #3c3950;
        --cor-secundariadocliente: #4876b1;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>