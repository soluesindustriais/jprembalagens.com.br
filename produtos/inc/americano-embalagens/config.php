<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Americano Embalagens';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Sacos Valvulados';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);




//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "saco-valvulado-25-kg",
    "saco-plastico-auto-vedante-para-embalagem",
    "sacaria-valvulada-polietileno",
    "embalagens-plasticas-valvuladas-e-transparentes",
    "saco-plastico-transparente-auto-vedante",
    "sacos-plasticos-de-polietileno-valvulados",
    "sacos-de-polietileno-valvulado",
    "embalagens-sacos-valvulados",
    "sacos-valvulados-para-argamassa",
    "sacos-valvulados-para-fertilizantes",
];

//Criar página de Serviço
$VetPalavrasInformacoes = [

];



// Numero do formulario de cotação
$formCotar = 232;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Americano Embalagens';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Sacos Valvulados';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Rua Henrique Wiezel 940';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Cidade Industrial';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Santa Barbara D´Oeste';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '13456-165';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'contato@icaterm.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #2172b3;
        --cor-secundariadocliente: #4876b1;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>