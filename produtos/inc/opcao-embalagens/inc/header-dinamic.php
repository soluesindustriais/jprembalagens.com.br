<?php
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
$hostnw = preg_replace('/^www\./', '', $host);
$linksubdominio = $http . '://'. $subdominio.'.'. $hostnw .'/';

$parts = explode('.', $host);
$desiredPart = $parts[count($parts) - 3];
$linkdominio = "$http"."://www.$desiredPart.com.br/produtos/";
$hostName = parse_url($url, PHP_URL_HOST);

if ($hostName === 'localhost') {
    $linkdominio = $url; 
}


    // if (strpos($linksubdominio, "localhost") !== false) {
    //     $linksubdominio = $url;
    // }

if($clienteAtivo == "inativo"){
    $linksubdominio = $url;
}

$items = [
    ["text" => "Inicio", "link" => $linksubdominio],
    ["text" => "Sobre nós", "url" => "sobre-nos", "link" => $linksubdominio . "sobre-nos"],
    ["text" => "Contato", "url" => "contato", "link" => $linksubdominio . "contato"],
    ["text" => "Produtos", "url" => "informacoes", "dropdown" => true, "link" => $linksubdominio . "informacoes"]
];



// Mapeia os títulos e links conforme os arrays anteriores
$menus = [];

// Adicionando o menu de Serviços apenas se houver itens
if (!empty($VetPalavrasInformacoes)) {
    $menus[] = [
        "title" => "Serviços",
        "items" => array_map(function ($item) use ($linkdominio) {
            $finalLink = (strpos($item, 'http') !== false ? $item : $linkdominio . $item);
            return [
                "title" => str_replace('-', ' ', ucwords($item)),
                "link" => $finalLink
            ];
        }, $VetPalavrasInformacoes)
    ];
}

// Adicionando o menu de Produtos apenas se houver itens
if (!empty($VetPalavrasOriginais)) {
    $menus[] = [
        "title" => "Produtos",
        "items" => array_map(function ($item) use ($linkdominio) {
            // Criando o link sem caracteres especiais e em minúsculas
            $link = strtolower($item);
            $link = iconv('UTF-8', 'ASCII//TRANSLIT', $link);
            $link = preg_replace('/[^a-z0-9\-]/', '', $link);

            $finalLink = (strpos($item, 'http') !== false ? $item : $linkdominio . $link);

            return [
                "title" => str_replace('-', ' ', ucwords($item)),
                "link" => $finalLink
            ];
        }, $VetPalavrasOriginais)
    ];
};
?>

<header id="nav-menu" aria-label="navigation bar">
    <div class="container-wrapper wrapper">
        <div class="nav-start-header">
            <a class="logo-header" href="<?= $linksubdominio ?>">
                <img src="<?= $linkminisite ?>imagens/logo-cliente-2.webp" width="80" alt="Logo Empresa">
            </a>
            <nav class="menu-header">
                <ul class="menu-bar-header">
                    <?php foreach ($items as $item) : ?>
                        <li>
                            <?php if (!isset($item['dropdown'])) : ?>
                                <a class="nav-link-header" href="<?= $item['link'] ?>" title="<?= $item['text'] ?>"><?= $item['text'] ?></a>
                            <?php else : ?>
                                <a class="nav-link-header dropdown-btn-header button-link-header" data-dropdown="dropdown2" aria-haspopup="true" aria-expanded="false" aria-label="<?= strtolower($item['text']) ?>" href="<?= $item["link"] ?>">
                                    <?= $item['text'] ?> <i class="fa-solid fa-chevron-right" aria-hidden="true"></i>
                                </a>
                                <div id="dropdown2" class="dropdown-header">
                                    <?php foreach ($menus as $menu2) : ?>
                                        <ul role="menu">
                                            <!-- <span class="dropdown-link-title-header"><?= $menu2['title'] ?></span> -->
                                            <?php foreach ($menu2['items'] as $subitem) : ?>
                                                <li role="menuitem">
                                                    <a class="dropdown-link-header" href="<?= $subitem['link'] ?>"><?= $subitem['title'] ?></a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
        </div>
        <div class="nav-end-header">
            <div class="right-container-header">
                <? include "$linkminisite" . "inc/pesquisa-inc.php"; ?>
            </div>
            <button class="hamburguer-mobile" id="hamburger" aria-label="hamburger" aria-haspopup="true" aria-expanded="false">
            <i class="fa-solid fa-bars" aria-hidden="true"></i>
            <i class="fa-solid fa-xmark" aria-hidden="true"></i>
            </button>
        </div>
    </div>

    <div class="clear"></div>
</header>

<div id="header-block"></div>

<script>

const iconHamburguer = document.querySelector('.fa-bars');
const iconExit = document.querySelector('.fa-xmark');

iconHamburguer.addEventListener('click', function(){
    iconHamburguer.style.display = "none";
    iconExit.style.display = "block";
});

iconExit.addEventListener('click', function(){
    iconExit.style.display = "none";
    iconHamburguer.style.display = "block";
});


</script>


<script>
    document.addEventListener("DOMContentLoaded", function() {
        const header = document.getElementById('nav-menu');

        // Função que adiciona a classe 'fixed-header' ao cabeçalho baseado na posição de rolagem
        function handleScroll() {
            // Verifica se a página foi rolada mais do que 50 pixels
            if (window.scrollY > 50) {
                header.classList.add('fixed-header');


            } else {
                header.classList.remove('fixed-header');

            }
        }

        // Adiciona o listener ao evento de rolagem da página
        window.addEventListener('scroll', handleScroll);
    });
</script>

