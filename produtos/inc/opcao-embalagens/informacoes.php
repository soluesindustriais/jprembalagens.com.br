<? $h1 = "Informações";
$title  = "Informações";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php");
include("inc/informacoes/informacoes-vetPalavras.php"); ?>

<style>
  body {
    scroll-behavior: smooth;
  }

  <?
  include('css/header-script.css');
  include("$linkminisite" . "css/style.css");
  ?>
</style>
</head>

<body> <? include("inc/header-dinamic.php"); ?><main role="main">
    <div class="content">
      <section> <?= $caminho ?> <div class="wrapper-produtos"> <br class="clear">
          <h1 style="text-align: center;  "><?= $h1 ?></h1>
          <article class="full">
            <div class="article-content">
              <p>Além da qualidade dos produtos, valorizamos a pontualidade. Cumprimos rigorosamente os prazos de distribuição acordados, garantindo que você receba seus pedidos no momento certo. Nosso compromisso com a excelência também se reflete em nosso atendimento ao cliente, sempre pronto para ajudar e oferecer as melhores soluções.</p>
            </div>
            <ul class="thumbnails-main"> <?php include_once("inc/informacoes/informacoes-categoria.php"); ?> </ul>
          </article>
      </section>
    </div>
  </main>
  </div>
  <!-- .wrapper --> <? include("inc/footer.php"); ?> </body>

</html>