<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Opcao Embalagens';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'Site especializado em entregar o melhor para o cliente';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);

//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "envelope-bolha",
    "envelope-canguru",
    "envelope-de-plastico",
    "envelope-de-polietileno",
    "envelope-de-polipropileno",
    "envelope-reciclado",
    "saco-de-seguranca",
    "saco-polietileno",
    "sacos-pe",
    "saco-bolha",
];

$VetPalavrasOriginais = [
    "envelope-bolha",
    "envelope-canguru",
    "envelope-de-plástico",
    "envelope-de-polietileno",
    "envelope-de-polipropileno",
    "envelope-reciclado",
    "saco-de-segurança",
    "saco-polietileno",
    "sacos-pe",
    "saco-bolha",
];

//Criar página de Serviço
/* $VetPalavrasInformacoes = [

]; */


// Numero do formulario de cotação
$formCotar = 301; // Colocar aqui;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Opção Embalagens';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'Empresa de embalagens';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'R. Quinze de Agosto, 86';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Canhema';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Diadema';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'SP';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : 'CEP: 09941-520';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-2.webp';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'contato2@opcaoembalagens.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';
$telefoneCliente = ($clienteAtivo == 'inativo') ? '' : '(11) 2677-9050';


?>

<!-- VARIAVEIS DE CORES -->

<style>
    :root {
        --azul-solucs: rgba(27, 39, 50, 1);
        --second-color: rgba(241, 174, 21, 1);
        --cor-dourada: #2d2d2f;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>