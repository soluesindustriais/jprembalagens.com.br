<?

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") {
    $url = $http . "://" . $host . "/";
} else {
    $url = $http . "://" . $host . $dir["dirname"] . "/";
}

$minisite = "americano-embalagens";
$subdominio = "americano-embalagens";
$linkminisite = "inc/$minisite/";
$linkminisitenb = substr($linkminisite, 0, -1);
$clienteAtivo = "ativo";
$nomeArquivo = pathinfo(basename(__FILE__), PATHINFO_FILENAME);
$nomeVariavel = ucwords(str_replace('-', ' ', $nomeArquivo));
$h1 = 'Categorias';
$title = 'Categorias';
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
$newUrl = str_replace('/produtos', '', $url);

include("$linkminisite" . "inc/head.php");
include("$linkminisite" . "inc/informacoes/informacoes-vetPalavras.php"); ?>



<?
$caminhoIndex = '<div class="background-bread-produtos">
  <div class="breadcrumb"">
      <div class="wrapper bread-content">
          <div class="bread__row">
          <h1 class="bread__title">' . $h1 . '</h1>
  <nav aria-label="breadcrumb" class="bread-list">
    <ol id="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
      <div class="item-breadcrumb">
        <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <a href="' . $newUrl . '" itemprop="item" title="Home">
            <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Início ❱ </span>
          </a>
          <meta itemprop="position" content="1" />
        </li>
        <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
          <span itemprop="name">' . $h1 . '</span>
          <meta itemprop="position" content="2" />
        </li>
      </div>
    </ol>
  </nav>
  </div>
  </div>
  </div>
</div>';
?>

<style>
    body {
        scroll-behavior: smooth;
    }

    <?
    include("$linkminisite" . "css/header-script.css");
    include("$linkminisite" . "css/style.css");
    ?>
</style>

</head>

<header id="nav-menu" aria-label="navigation bar">
    <div class="container-wrapper wrapper">
        <div class="nav-start-header">
            <a class="logo-header" href="<?= $newUrl ?>">
                <img src="<?= $newUrl ?>imagens/logo.jpg" width="100" alt="Logo Empresa">
                <!-- achar logo do satalte -->
            </a>
            <nav class="menu-header">
                <ul class="menu-bar-header">
                    <li>
                        <a class="nav-link-header" title="Home" href="<?= $newUrl ?>">Home</a>
                    </li>
                    <li>
                        <a class="nav-link-header" title="Sobre Nós" href="<?= $newUrl ?>sobre-nos">Sobre Nós</a>
                    </li>
                    <li>
                        <a class="nav-link-header" title="Contato" href="<?= $newUrl ?>sobre-nos">Contato</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="nav-end-header">
            <button class="hamburguer-mobile" id="hamburger" aria-label="hamburger" aria-haspopup="true" aria-expanded="false">
                <i class="fa-solid fa-bars" aria-hidden="true"></i>
            </button>
        </div>
    </div>
</header>



<main role="main">
    <section> <?= $caminhoIndex ?> <div class="wrapper-produtos"> <br class="clear">
            <h1 style="text-align: center;  "><?= $h1 ?></h1>
            <article class="full">

                <?

                if (is_dir("inc")) {
                    $conteudo = scandir("inc");
                    foreach ($conteudo as $item) {

                        if ($item != '.' && $item != '..') {
                            if (is_dir("inc" . '/' . $item) && $item != 'informacoes') {
                                include "inc" . '/' . $item . '/config.php';

                                echo '<ul class="thumbnails-main">';
                                $i = 1;

                                foreach ($VetPalavrasProdutos as $produto) {

                                    echo "
            <li>
            <div class=\"overflow-hidden\"> <a rel=\"nofollow\" href=\"" . $produto . "\" title=\"$produto\"><img src=\"" . "inc/$item/imagens/informacoes/$produto-1.webp" . "\" alt=\"$produto\" title=\"$produto\"></a> </div>
            <div class=\"title-produtos\">
            <a href=\"" . $produto . "\">$produto</a>
            </div>  
          </li>";
                                    $i++;
                                }
                                echo '</ul>';
                            }
                        }
                    }
                } else {
                    echo "O caminho especificado não é um diretório válido.";
                }

                ?>
            </article>
    </section>
</main>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        const header = document.getElementById('nav-menu');

        // Função que adiciona a classe 'fixed-header' ao cabeçalho baseado na posição de rolagem
        function handleScroll() {
            // Verifica se a página foi rolada mais do que 50 pixels
            if (window.scrollY > 1) {
                header.classList.add('fixed-header');


            } else {
                header.classList.remove('fixed-header');

            }
        }
        // Adiciona o listener ao evento de rolagem da página
        window.addEventListener('scroll', handleScroll);
    });
</script>

</div>
<!-- .wrapper -->
<div class="clear"></div>
<footer>
    <div class="wrapper">
        <div class="contact-footer">

            <?php
            // Obtém o caminho do diretório atual
            $currentDirectory = __DIR__;

            // Obtém o caminho do diretório pai da pasta 'produtos'
            $parentDirectory = dirname($currentDirectory);

            // Extrai o nome da pasta pai
            $parentFolderName = basename($parentDirectory);

            // Remove a extensão .com.br do nome da pasta pai, se estiver presente
            $cleanParentFolderName = str_replace(".com.br", "", $parentFolderName);

            // Imprime o nome da pasta pai limpo em HTML
            echo '<address><span>' . $cleanParentFolderName . '</span></address>';
            ?>
            <br>
        </div>
        <div class="menu-footer">

            <!-- quando tiver no minisite -->
            <!-- <nav>
				<ul>
					<?php

                    foreach ($menu as $key => $value) {
                        if ($sigMenuPosition !== false && $key == $sigMenuPosition) include('inc/menu-footer-sig.php');
                        echo '
								<li>
									<a rel="nofollow" href="' . (strpos($value[0], 'http') !== false ? $value[0] : $linksubdominio . $value[0]) . '" title="' . ($value[1] == 'Home' ? 'Página inicial' : $value[1]) . '" ' . (strpos($value[0], 'http') !== false ? 'target="_blank"' : "") . '>' . $value[1] . '</a>
								</li>
								';
                    }

                    ?>
					<li><a href="<?= $linksubdominio ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
				</ul>
			</nav> -->


            <nav>
                <ul>
                    <?php

                    foreach ($menu as $key => $value) {
                        if ($sigMenuPosition !== false && $key == $sigMenuPosition) include('inc/menu-footer-sig.php');
                        echo '
								<li>
									<a rel="nofollow" href="' . (strpos($value[0], 'http') !== false ? $value[0] : $newUrl . $value[0]) . '" title="' . ($value[1] == 'Home' ? 'Página inicial' : $value[1]) . '" ' . (strpos($value[0], 'http') !== false ? 'target="_blank"' : "") . '>' . $value[1] . '</a>
								</li>
								';
                    }

                    ?>
                    <li><a href="<?= $newUrl ?>mapa-site" title="Mapa do site <?= $nomeSite ?>">Mapa do site</a></li>
                </ul>
            </nav>
        </div>
        <br class="clear">
    </div>
</footer>
<div class="copyright-footer">
    <div class="wrapper">
        <div class="selos">
            <a rel="nofollow" href="https://validator.w3.org/nu/?showsource=yes&doc=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank" title="HTML5 W3C"><i class="fa fa-html5"></i> <strong>W3C</strong></a>
            <a rel="nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?= $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&lang=pt-BR" target="_blank" title="CSS W3C"><i class="fa fa-css3"></i> <strong>W3C</strong></a>
        </div>
    </div>
</div>
<?
include "$linkminisite" . "inc/header-scroll.php";
include "$linkminisite" . "inc/header-fade.php";
?>


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

<!-- fonts -->
<link href="https://fonts.googleapis.com/css2?family=Catamaran:wght@100..900&family=Dosis:wght@200..800&display=swap" rel="stylesheet">
<!-- <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">  -->
<!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;1,400;1,600&family=Oswald:wght@200;300;700&display=swap" rel="stylesheet"> -->
<!-- fim fonts -->

<!-- ANALYTICS -->
<!-- Código gtag.js global e local -->
<script defer src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script defer>
    // Configuração específica do site
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    // ID da propriedade específica do site
    gtag('config', '<?= $tagManager ?>');
</script>
<!-- fim ANALYTICS -->
<? include "$linkminisite" . "inc/fancy.php" ?>
<? include "$linkminisite" . "inc/LAB.php" ?>
<script>
    initReadMore('.ReadMore', {
        collapsedHeight: 200,
        heightMargin: 20
    });
</script>
</body>

<script>
    document.title = "Produtos";
</script>

<style>
    .fa-bars {
        color: #2d2d2f;
    }
</style>

</html>