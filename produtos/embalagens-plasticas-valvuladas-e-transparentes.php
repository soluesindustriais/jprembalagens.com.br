<?php
$minisite = "americano-embalagens";
$subdominio = "americano-embalagens";
$linkminisite = "inc/$minisite/";


$linkminisitenb = substr($linkminisite, 0, -1);
$clienteAtivo = "ativo";

// Obtém o nome do arquivo atual sem a extensão
$nomeArquivo = pathinfo(basename(__FILE__), PATHINFO_FILENAME);

// Remove os hífens e transforma a primeira letra de cada palavra em maiúscula
$nomeVariavel = ucwords(str_replace('-', ' ', $nomeArquivo));

$h1 = $nomeVariavel;
$title = $nomeVariavel;
$desc = $metadescription;
$key = "mpi,sample,lorem,ipsum";
$legendaImagem = "Foto ilustrativa de Armário de aço tipo roupeiro preço";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
$nomeurl = pathinfo(basename($_SERVER["PHP_SELF"]), PATHINFO_FILENAME);
include ("$linkminisite"."inc/head-mpi.php");
include ("$linkminisite"."inc/fancy.php");
?>
<style>
  body {
    scroll-behavior: smooth;
  }
  <?php
  include("$linkminisite". "css/header-script.css");
  include ("$linkminisite"."css/mpi-product.css");
  include ("$linkminisite"."css/mpi.css");
  include ("$linkminisite"."css/aside.css");
  include ("$linkminisite"."css/style-mpi.css");
  ?>
</style>
</head>
<body>
  <?php include ("$linkminisite"."inc/header-dinamic.php"); ?>
  <main>
    <div class="content" itemscope itemtype="https://schema.org/Article">
      <?= $caminhocateg ?>
      <div class="main-tag-content">
        <div class="container-main-produtos">
          <section class="anuncio-principal wrapper flex-wrap-mobile light-box-shadow">
            <section class="content-anuncio-principal">
              <div class="imagens-anuncio">
                <?php include ("$linkminisite"."inc/gallery-product.php") ?>
              </div>
              <div class="description">
                <hr class="mobile-line">
                <div class="article-content">
                  <div class="ReadMore"
                    style="overflow: hidden; height: auto; transition: height 100ms ease-in-out 0s;">
                    <h2 class="h2-description">Descrição</h2>
                    <article class="p-description">
                    <?= $conteudo4[0] ?>
                    </article>
                    <span id="readmore-open">Continuar Lendo...</span>
                    <span id="readmore-close">Fechar <i class="fa-solid fa-turn-up"
                        style="color: var(--cor-pretopantone);"></i></span>
                  </div>
                </div>
              </div>
            </section>
            <aside class="especificacoes-anuncio-principal">
              <h1 class="h1-title-anuncio">
                <?= $title ?>
              </h1>
              <p class="p-nivel-de-procura">
                Nivel de Procura:
                <span id="span-nivel-de-procura">alta</span>
              </p>

              <button class="botao-cotar botao-cotar-mobile btn-solicitar-orcamento btn-cotar" title="<?= $h1 ?>">Solicite um
                Orçamento</button>
              <? include "$linkminisite"."inc/btn-cotar.php"; ?>
              <div class="icons-anuncio">
                <div class="icon-aside">
                  <img src="<?=$linkminisite?>imagens/icons/regiao-atendimento.png"
                    alt=" ICONE REGIÃO DE ATENDIMENTO" />
                  <div class="text-icon">
                    <h2>Região de Atendimento</h2>
                    <p>São Paulo</p>
                  </div>
                </div>
                <div class="icon-categoria icon-aside">
                  <img src="<?=$linkminisite?>imagens/icons/icon-categoria.png" alt=" ICONE CATEGORIA" />
                  <div class="text-icon">
                    <h2>Categoria</h2>
                    <p>Produtos</p>
                  </div>
                </div>
                <div class="icon-sub-categoria icon-aside">
                  <img src="<?=$linkminisite?>imagens/icons/sub-categoria.png" alt=" ICONE CATEGORIA" />
                  <div class="text-icon">
                    <h2>Sub Categoria</h2>
                    <p>
                      <?= $nomeurl ?>
                    </p>
                  </div>
                </div>
              </div>
            </aside>
          </section>
          <div class="wrapper content-produtos">
            <? include "$linkminisite"."inc/produtos-populares.php"; ?>
            <? include "$linkminisite"."inc/aside-produtos.php"; ?>
            <!-- <? include "$linkminisite"."inc/regioes.php"; ?> -->
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="<?= $linkminisite ?>js/img-alternate-product.js"></script>
  <? include "$linkminisite"."inc/footer.php"; ?>
  <script src="<?=$linkminisite?>js/organictabs.jquery.js" async></script>
</body>
<script>
  document.addEventListener('DOMContentLoaded', function () {
    const pSideElements = document.querySelectorAll('.p-side');
    pSideElements.forEach(element => {
      element.addEventListener('click', function () {
        pSideElements.forEach(el => {
          el.classList.remove('p-side-active');
        });
        element.classList.add('p-side-active');
      });
    });
  });
</script>
<script>
  const btnCotar = document.querySelector('.btn-cotar');
  const modal = document.querySelector('.modal-btn');
  const closeBtn = document.querySelector('.modal-btn .close-btn');
  btnCotar.addEventListener('click', () => {
    modal.style.display = 'flex';
  });
  closeBtn.addEventListener('click', () => {
    modal.style.display = 'none';
  });
  window.addEventListener('click', (e) => {
    if (e.target === modal) {
      modal.style.display = 'none';
    }
  });
</script>
</html>
