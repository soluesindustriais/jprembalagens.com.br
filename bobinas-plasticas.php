<?php
	$h1    			= 'Bobinas plásticas';
	$title 			= 'Bobinas plásticas';
	$desc  			= 'As bobinas plásticas são uma embalagem versátil, que podem ter diferentes tipos de aspectos e materiais de fabricação. Elas podem ser fabricadas, por exemplo, em polietileno';
	$key   			= 'bobinas, plásticos, bobina plástica';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobina plástica';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p><strong>Bobinas plásticas</strong> podem ser feitas em medidas personalizada e com materiais diversos, de acordo com a necessidade do cliente. Confira maiores informações.</p>

            <p>Para que um produto tenha êxito no mercado, é preciso que ele tenha qualidade e que agrade os consumidores. Mas outro ponto fundamental é a embalagem, que precisa ter um bom aspecto visual e garantir a proteção necessária para o que está embalado. É por isso que as <strong>bobinas plásticas</strong> tem utilização bastante ampla. As <strong>bobinas plásticas</strong> são uma embalagem versátil, que podem ter diferentes tipos de aspectos e materiais de fabricação.</p>
            
            <p>Elas podem ser fabricadas, por exemplo, em polietileno, que faz com que a embalagem tenha como características a resistência e a atoxicidade. Neste caso, a bobina pode embalar qualquer tipo de produto e é bastante usada no segmento de alimentos, tais como leites e farinhas, além de bebidas e outras áreas.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div> 
            
            <p>Já as <strong>bobinas plásticas</strong> em polipropileno são usadas em confecções, também em indústrias alimentícias e em gráficas, e se destacam pelo brilho e pela transparência. É possível fabricá-las ainda em BOPP, com possibilidade de ser metalizada ou perolizada, sendo largamente usada em mercado de varejo.</p>
            
            <p>Uma opção barata para a empresa e que ainda contribui com o meio ambiente são as <strong>bobinas plásticas</strong> feitas com material reciclado.</p>
            
            <h2>Personalização das bobinas plásticas</h2>
            
            <p>Outra vantagem das <strong>bobinas plásticas</strong> é que elas podem ser personalizadas conforme a preferência do cliente. É possível fabricá-las com impressão, transparente, pigmentada ou lisa, além de também ter variedade de tamanhos.</p>
         
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>E para fazer a personalização, conte com a JPR Embalagens. A empresa atua na área de embalagens plásticas flexíveis há mais de 15 anos, levando até o consumidor embalagens modernas, que se destacam pela excelência e pelos preços reduzidos, resultando em melhorias, redução de perdas e de custos para o cliente.</p>
            
            <p>A JPR Embalagens tem um atendimento completamente voltado às necessidades e demandas de cada cliente, sempre oferecendo a melhor relação custo-benefício do mercado. Saiba mais sobre como as <strong>bobinas plásticas</strong> podem ser personalizadas entrando em contato com a equipe e informe-se também sobre valores e condições de pagamento. Aproveite e solicite já o seu orçamento, informando medidas e quantidade que a sua empresa necessita.</p>
            
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>