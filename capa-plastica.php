<?php
	$h1    			= 'Capa plástica';
	$title 			= 'Capa plástica';
	$desc  			= 'A capa plástica facilita a inspeção e identificação da carga, além de se destacar por proporcionar maior segurança e rigidez durante a movimentação e o transporte de mercadorias';
	$key   			= 'capa, plática, capas plásticas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Capas plásticas';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                

    			<?=$caminhoProdutoCapas?>
                 <article>
				<h1><?=$h1?></h1>     
                	
<br>  
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>Capa plástica</strong> se destaca pela resistência e pela durabilidade. Saiba maiores informações sobre este tipo de embalagem.</p>

            <p>Na hora de embalar um produto, vários aspectos devem ser levados em consideração. O mais importante é a qualidade do material para proteger o que está sendo embalado sem alterar as características originais do produto. Por isso, uma opção de qualidade é a <strong>Capa plástica</strong>.</p>
            
            <p>Confeccionada com polietileno transparente, a <strong>Capa plástica</strong> é uma embalagem que se caracteriza por ter grande resistência mecânica, além de durabilidade, graças à exposição às intempéries.</p>
            
            <p>A <strong>Capa plástica</strong> facilita a inspeção e identificação da carga, além de se destacar por proporcionar maior segurança e rigidez durante a movimentação e o transporte de mercadorias.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div> 
            
            <p>Com este tipo de embalagem, as mercadorias ficam impermeabilizada contra sujeiras, exposições a diferentes temperaturas e outros tipos de danos. Outro diferencial é que a <strong>Capa plástica</strong> tem alta flexibilidade, o que permite o acondicionamento de cargas de diferentes tamanhos e disposições.</p>
            
            <p>A <strong>Capa plástica</strong> é ideal para a embalagem de produtos unitizados. As indústrias que mais usam este tipo de material são as seguintes: químicas, fumageiras, calçadistas, metalúrgicas, fundição e alimentos.</p>
            
            <h2>Capa plástica com preços reduzidos e ótimas condições de pagamento</h2>
            
            <p>Na hora de adquirir a <strong>Capa plástica</strong>, conte com as vantagens proporcionadas pela JPR Embalagens. A empresa atua no segmento de embalagens plásticas flexíveis há mais de 15 anos, levando até o cliente as melhores soluções neste setor.</p>
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Com o objetivo principal de reduzir perdas e custos a partir da identificação de oportunidades de melhoria, a JPR Embalagens dispõe de equipe atualizada e com ampla experiência, além de equipamentos modernos e eficientes. O resultado não poderia ser outro: <strong>Capa plástica</strong> com qualidade e resistência garantidas, sempre com valores em conta e ótimas condições de pagamento. O mesmo é válido para os outros produtos da empresa, que também confecciona sacos, sacolas, filmes, envelopes, bobinas, entre outros materiais.</p>
            
            <p>O atendimento da JPR Embalagens é personalizado, voltado para os diferentes tipos de necessidade. Por isso, aproveite. Saiba mais entrando em contato com a equipe para esclarecer eventuais dúvidas e peça já o seu orçamento, informando medidas e quantidade do produto que você necessita.</p>

            
           <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>           
            <br class="clear" />                                                                                                    
			<?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>