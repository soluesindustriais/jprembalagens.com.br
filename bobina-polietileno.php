<?php
	$h1    			= 'Bobina de polietileno';
	$title 			= 'Bobina de polietileno';
	$desc  			= 'A bobina de polietileno pode ser feita em PEAD, PEBD ou PEDBL e é bastante empregada em indústrias de farináceos, laticínios, alimentos e bebidas';
	$key   			= 'bobina, polietileno, bobinas em polietileno';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas em polietileno';
	
	include('inc/head.php');
?>
<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br> 
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
          	<p>A <strong>bobina de polietileno</strong> é uma opção ideal para grande variedade de produtos, inclusive alimentares. Confira maiores detalhes sobre este tipo de embalagem.</p>

            <p>A embalagem precisa ter um bom aspecto visual e deve ser de qualidade para que o produto que está envolvido seja sempre protegido. Isso combinado com a excelência do produto são aspectos essenciais para que uma empresa possa vender mais. Neste caso, uma ótima opção para a sua empresa é a <strong>bobina de polietileno</strong>.</p>
            
            <p>A <strong>bobina de polietileno</strong> pode ser feita em PEAD, PEBD ou PEDBL e é bastante empregada em indústrias de farináceos, laticínios, alimentos e bebidas. Isso porque esta bobina é um plástico resistente e atóxico, e por isso permite que qualquer tipo de produto seja embalado, sem que haja qualquer tipo de alteração nas características originais.</p>
            
			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Além da <strong>bobina de polietileno</strong>, também há possibilidade de fabricar com outros materiais, como polipropileno e BOPP. Outra opção é a bobina feia em material reciclado, que está alinhada com as questões ambientais e ainda resultam em redução de custos para a empresa, mantendo a mesma qualidade do produto que é fabricado com matéria-prima virgem.</p>
            
            <h2>Bobina de polietileno da JPR Embalagens</h2>
            
            <p>E para adquirir a <strong>bobina de polietileno</strong>, conte com as vantagens da JPR Embalagens. Com mais de 15 anos de presença no mercado de embalagens plásticas flexíveis, a JPR Embalagens conta com profissionais sempre atentos e atualizados.</p>
                        
            <p>Por isso, a <strong>bobina de polietileno</strong> e os outros produtos da empresa se destacam por ser o que há de mais moderno, com qualidade e preços em conta. Com isso, o cliente pode reduzir perdas, controlar melhor o estoque e reduzir seus custos mensais com embalagens.</p>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Na JPR Embalagens, é possível personalizar a <strong>bobina de polietileno</strong>, mantendo a embalagem lisa ou com impressão com diversas opções de cores. Também é possível personalizar as medidas do produto.</p>
            
            <p>Saiba mais sobre a <strong>bobina de polietileno</strong> e suas aplicações entrando em contato com um dos nossos consultores. O atendimento disponibilizado pela JPR Embalagens é totalmente voltado às necessidades dos clientes e personalizado. Aproveite também para conhecer os preços e as condições de pagamento e solicite já o seu orçamento.</p>
            
                        
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>