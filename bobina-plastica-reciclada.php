<?php
	$h1    			= 'Bobina plástica reciclada';
	$title 			= 'Bobina plástica reciclada';
	$desc  			= 'A bobina plástica reciclada é uma maneira de melhorar a imagem da sua marca no mercado por indicar a preocupação da sua empresa com as causas ambientais';
	$key   			= 'bobina, plástica, reciclada, bobinas plásticas recicladas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas plásticas recicladas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
                
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>   
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Invista em uma opção de qualidade e que está alinhada com as questões ambientais para embalar os seus produtos. Conheça a <strong>bobina plástica reciclada</strong>.</p>

            <p>A imagem da sua empresa precisa ser trabalhada sempre que possível para atrair novos consumidores e fidelizar aqueles que já compraram produtos da sua marca. Por isso, conte com a <strong>bobina plástica reciclada</strong>.</p>
            
            <p>A <strong>bobina plástica reciclada</strong> é uma maneira de melhorar a imagem da sua marca no mercado por indicar a preocupação da sua empresa com as causas ambientais. Além disso, ela tem vantagens econômicas: até 30% de redução de custos com embalagem.</p>
            
            <p>Vale destacar que esta economia é feita sem perder a qualidade do material, que segue sendo resistentes a quedas, rasgos e rupturas e, por isso, este tipo de embalagem é amplamente utilizado.</p>
                        
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina plástica reciclada</strong> pode ser feita no modo cristal. Este processo de fabricação combina material virgem e embalagens que já foram recicladas. Pelo processo de reciclagem, a embalagem fica com cor amarelo claro e com alguns pontos, mas mantendo a transparência.</p>
            
            <p>Outra opção é a <strong>bobina plástica reciclada</strong> canela, que é produzida a partir de aparas de plasticos reciclados. Neste caso, a embalagem continua resistente e proporcionando segurança devido à solda reforçada. O aspecto da embalagem é com alguns pontos e com cor marrom claro, mas a transparência é mantida.</p>
            
            <h2>Personalização da bobina plástica reciclada</h2>
            
                        
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Além destas opções de fabricação, a <strong>bobina plástica reciclada</strong> também pode ser personalizada de acordo com a necessidade do cliente. A fabricação pode ser feita sob medida, com material totalmente liso ou impresso em até seis cores distintas.</p>
            
            <p>Para adquirir a <strong>bobina plástica reciclada</strong>, conte com as vantagens da JPR Embalagens. A empresa está no mercado há mais de 15 anos, levando até os clientes as melhores opções na área de embalagens plásticas flexíveis.</p>
            
            <p>O objetivo da JPR Embalagens é proporcionar ao cliente o que há de mais moderno neste segmento, mas sempre com valores em conta e ótimas condições de pagamento. Desta forma, o cliente reduz perda e custos e aproveita as oportunidades de melhoria.</p>
            
            <p>O atendimento é totalmente personalizado. Por isso, aproveite. Entre em contato com a empresa e aproveite as ótimas condições para solicitar já o seu orçamento.</p>
            
                        
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>