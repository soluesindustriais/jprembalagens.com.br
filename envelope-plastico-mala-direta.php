<?php
	$h1    			= 'Envelope plástico mala direta';
	$title 			= 'Envelope plástico mala direta';
	$desc  			= 'O envelope plástico mala direta é muito utilizado por editoras, gráficas, courier, laboratórios, entre outros segmentos.';
	$key   			= 'Envelopes plásticos malas diretas, Envelopes, Envelope, plástico, mala, direta';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plásticos malas diretas';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 

                                    
            <h2>Fabricamos envelope plástico mala direta em diversos modelos e cores.</h2> 
            
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>
            <p>Os <strong>envelopes plásticos mala direta</strong> são muitos utilizados por editoras, gráficas, courier, laboratórios, entre outros segmentos. Pode ser fabricado com aba adesiva, fecho tipo fronha, tala, fecho zip, botões, entre outros modelos.</p>
            <p>Pensando em sustentabilidade, a JPR Embalagens passou a produzir <strong>envelope plástico mala direta</strong> oxi-biodegradavel, e nesta opção a embalagem se degrada em curto espaço de tempo, assim contribuindo com o ambiente, pois o plástico comum pode levar mais de 100 anos para de decompor. </p>
            <p>A modernidade da embalagem fala muito do produto que ela contém, dessa forma, nosso produto é fabricado de acordo com a necessidade de cada cliente, e se você deseja uma embalagem especial, diferente do que já existe no mercado, entre em contato conosco. Possuímos equipamentos de ultima geração, para assim atender as solicitações dos nossos clientes, assim garantimos total qualidade e pontualidade na entrega de nossos produtos.</p>
            <h3>Modelos do envelope plástico mala direta</h3>
            <p>O envelope plástico mala direta, pode ser liso ou personalizado em até 6 cores. </p> 
            <p>Produzimos a partir de 250kg para <strong>envelope plástico mala direta</strong> impresso  e 150kg para <strong>envelopes lisos</strong>.</p>
            <p>Além deste produto, a JPR Embalagens trabalha com uma ampla linha de embalagens como, <a href="<?=$url;?>sacos" title="Sacos"><strong>Sacos</strong></a>, <a href="<?=$url;?>sacolas" title="Sacolas"><strong>Sacolas</strong></a>, <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelopes de segurança</strong></a>, <a href="<?=$url;?>envelopes-plasticos-especiais" title="Envelopes Plásticos Especiais">Envelopes Plásticos Especiais</a>, filmes e bobinas.</p>
            <p>Para receber um orçamento de <strong>envelope plástico mala direta</strong>, entre em contato com um de nossos consultores, e informe as medidas (largura x comprimento x espessura) e a quantidade que deseja utilizar.</p>

            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>