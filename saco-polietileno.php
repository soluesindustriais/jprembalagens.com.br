<?php
$h1    			= 'Saco de polietileno';
$title 			= 'Saco de polietileno';
$desc  			= 'O saco de polietileno pode ser liso ou impresso em até 6 cores. Além de transparente, também fabricamos o este produto pigmentado em diversas cores.';
$key   			= 'Sacos de polietileno, Saco, sacos, polietileno';
$var 			= 'Sacos de polietileno';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <p>Fabricado sob medida, o <strong>saco de polietileno</strong> pode ser liso ou impresso em até 6 cores. Além de transparente, também fabricamos o este produto pigmentado em diversas cores.</p>
             
             
             <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>

            <h2>Versatilidade do saco de polietileno</h2>
            <p>O <strong>saco de polietileno</strong> é uma embalagem super versátil, pode embalar diversos tipos de produtos, desde a linha de higiene e limpeza, até a linha de produtos alimentícios.</p>
            <p>Com foco em sustentabilidade, esta é uma das únicas embalagens que podem ser produzidas com matéria-prima 100% reciclada. Desta forma, reutilizamos várias outras embalagens, como <a href="<?=$url;?>sacolas" title="Sacolas"><strong>sacolas</strong></a>, sacarias em geral, embalagens de alimentos, que vão para reciclagem, e partir desta nova matéria-prima confeccionamos uma nova embalagem. Por ser um material de mil e uma utilidades, o <strong>saco de polietileno industrial</strong> é indicado para embalagens que precisam suportar pesos elevados, com 20, 25 ou 30kg. Por ser um material totalmente flexível, podemos produzi-los com 450 micras ou 0,450 mm.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>           

            <p>Outra funcionalidade do <strong>saco de polietileno</strong>, é que ele pode ser termo-encolhivel. O plástico termo-encolhivel é muito utilizado em indústrias alimentícias e moveleiras, pois podem ser encolhidos através de sopradores térmicos ou tuneis de encolhimento, e através deste processo o plástico é aquecido e encolhe ficando no formato do produto.</p> 
            <p>Para produtos ou objetos mais leves, podemos utilizar o <strong>saco de polietileno convencional</strong>, pois este é fabricado com espessuras mais finas, e assim e adequam a necessidade de cada cliente.</p>
            <p>Se você precisa de uma embalagem resistente e inviolável, esta embalagem é uma ótima alternativa, pois ele pode ser fabricado com uma aba adesiva permanente, e assim torna e embalagem inviolável, de tal forma que para violar é necessário danificar a embalagem.</p>
            <h2>Fabricamos os seguntes modelos de saco de polietileno:</h2>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>

            <ul class="list">
                <li><strong>Saco de polietileno com fecho zip</strong>;</li>
                <li><strong>Saco de polietileno reforçado</strong>;</li>
                <li><strong>Saco de polietileno reciclado cristal</strong>;</li>
                <li><strong>Saco de polietileno reciclado canela</strong>;</li>
                <li><strong>Saco de polietileno para brindes</strong>;</li>
                <li>E muito mais.</li>
            </ul>
            <p>Além deste produto, a JPR Embalagens fabrica outros modelos de embalagem como <a href="<?=$url;?>sacola-impressa" title="Sacola Impressa"><strong>Sacolas Impressas</strong></a>, <a href="<?=$url;?>envelope-plastico-personalizado" title="Envelope Plástico Personalizado"><strong>Envelope Plástico Personalizado</strong></a>, filme e bobinas e embalagens especiais.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>