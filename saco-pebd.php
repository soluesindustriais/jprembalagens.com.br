<?php
$h1    			= 'Saco PEBD';
$title 			= 'Saco PEBD';
$desc  			= 'O saco PEBD (polietileno de baixa densidade) ou PE, como é conhecido, é utilizada para embalar uma enorme gama de produtos.';
$key   			= 'Sacos PEBD, Saco, sacos, PEBD, saco PEBD reciclado';
$var 			= 'Sacos PEBD';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>     
            

            <p>O <strong>saco PEBD</strong> (polietileno de baixa densidade) ou PE, como é conhecido, é utilizada para embalar uma enorme gama de produtos. É uma embalagem que tem como característica uma ótima selagem, boa resistência ao rasgo e a tração. </p>
            <p>Pode ser confeccionado com ou sem impressão. Além de poder receber diversos acessórios como aba adesiva, fecho zip, talas e alças. Este produto pode se tornar uma embalagem inviolável, basta fabricarmos a embalagem com fecho adesivo hotmelt, assim, para abrir a embalagem é necessário danificá-la.</p>
            <p>Invista e sustentabilidade contribuía para a manutenção da vida. Para isso, utilize <strong>sacos PEBD reciclados</strong> ou oxi-biodegradavel. O modelo reciclado é fabricado com matéria-prima 100% reciclada feitas a partir de embalagens de alimentos, sacolas plásticas de uma maneira geral, e após serem recicladas, compramos esta nova matéria-prima e fabricamos a embalagem final.</p> 
            <h2>O saco PEBD pode ser reciclado em três formas:</h2>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>    
            
            <ul class="list">
                <li><strong>Saco PEBD reciclado cristal</strong>: fabricado a partir das aparas do material virgem.</li>
                <li><strong>Saco PEBD reciclado canela</strong>: fabricado a partir das aparas do material reciclado cristal.</li>
                <li><strong>Saco PEBD reciclado colorido</strong>: fabricado a partir da mistura de varias plásticos transparente ou coloridos. Este material fica sem transparência e sem padrão de cor.</li>
            </ul> 
            <p>Com o produto reciclado, além de você contribuir com o nosso planeta, poderá reduzir até 30% dos custos com embalagem.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>sacos/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>    
            
            <p>Enquanto os plásticos comuns levam mais de 100 anos para se decompor, o <strong>saco PEBD oxiodegradavel</strong>  se decompõe em curto espaço de tempo, em média 6 meses.
                
                <p>O aditivo oxi-biodegradavel acelera o processo de degradação da embalagem quando ela entra em contato com o meio ambiente. A natureza agradece.</p>

                
                <p>Nosso lote mínimo de fabricação é de 250kg <strong>saco PEBD impresso</strong> e 150kg sem impressão.</p>
                <p>Para receber um orçamento, entre em contato com um dos nossos consultores, e informe as medidas e quantidades desejadas.</p>

                
                
                <?php include('inc/saiba-mais.php');?>
                
                
                
            </article>
            
            <?php include('inc/coluna-lateral-paginas.php');?>
            
            <?php include('inc/paginas-relacionadas.php');?>  
            
            <br class="clear" />  
            

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        </section>

    </main>

    
    
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>