<?php
$h1    			= 'Saco plástico zip';
$title 			= 'Saco plástico zip';
$desc  			= 'O saco plástico zip é fabricado sob medida, de acordo com a necessidade de cada cliente. Pode ser liso ou impresso em até 6 cores.';
$key   			= 'Sacos plásticos zip, Saco, sacos, plástico, zip';
$var 			= 'Sacos plásticos zip';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  
             <p>A modernidade que o <strong>fecho zip</strong> traz a embalagem mostra a sua preocupação em inovar e sempre oferecer o melhor.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>
             
             <p>O <strong>saco plástico zip</strong> é fabricado sob medida, de acordo com a necessidade de cada cliente. Pode ser liso ou impresso em até 6 cores, além de serem fabricados na cor transparente ou pigmentado em diversas cores.</p>
             <p>Por ser uma embalagem resistente a pratica, o <strong>saco plástico zip</strong> é muito empregado em indústrias alimentícias, confecção, laboratórios, gráficas, entre outros.</p>
             <p>A praticidade do <strong>saco plástico zip</strong> faz com que ele seja cada vez mais utilizado nas embalagens para frutas. No caso das uvas, com uma característica a mais: furos ou trefilados tipo colmeia para ventilação adequada. E esta é uma especialidade nossa.</p>
             <p>Os <strong>sacos plásticos zip</strong> são indicados para diversos produtos, oferecem durabilidade e resistência ao rasgo e à perfuração.</p>
             <p>Uma alternativa ecológica que o <strong>saco plástico zip</strong> pode ser fabricado é com a adição de aditivo oxibiodegradavel durante o seu processo de fabricação. Nesta opção, a embalagem em contato com o meio ambiente pode ser degradar em período de até 6 meses, sem deixar resíduos nocivos ao meio ambiente.</p>
             <p>A JPR Embalagens trabalha com diversos modelos de embalagem, além do <strong>saco plástico zip</strong>, fabricamos também <a href="<?=$url;?>sacola-plastica-zip" title="Sacola Plastica Zip"><strong>sacolas plásticas com fecho zip</strong></a>, <strong>envelopes com fecho zip</strong>, entre outros modelos.</p>
             <p>Nossa quantidade mínima de produção são de 100kg para <strong>sacos plástico zip</strong> liso e 250kg impresso.</p>
             <p>Para receber um orçamento de <strong>sacos plástico zip</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

             <?php include('inc/saiba-mais.php');?>
             
             
             
         </article>
         
         <?php include('inc/coluna-lateral-paginas.php');?>
         
         <?php include('inc/paginas-relacionadas.php');?>  
         
         <br class="clear" />  
         

         
         <?php include('inc/regioes.php');?>
         
         <?php include('inc/copyright.php');?>

         
     </section>

 </main>

 
 
</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>