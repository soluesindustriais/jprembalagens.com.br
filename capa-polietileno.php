<?php
$h1    			= 'Capa em polietileno';
$title 			= 'Capa em polietileno';
$desc  			= 'A capa em polietileno é a opção ideal para proteger a sua mercadoria contra qualquer dano. Ela faz a proteção contra sujeiras, água, poeira, variações de temperatura...';
$key   			= 'capa, polietileno, capas de polietileno';
$legendaImagem 	= ''.$h1.'';
$var 			= 'Capas de polietileno';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoCapas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>   
             
             
             <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>Capa em polietileno</strong> se destaca pela resistência mecânica e pela durabilidade. Conheça mais sobre este tipo de embalagem.</p>

            <p>Investir na qualidade de um produto durante o processo de fabricação é apenas uma das etapas para garantir a qualidade da mercadoria. É necessário também investir em embalagens de qualidade, para que as características originais sejam mantidas. Por isso, uma opção altamente recomendada é a <strong>Capa em polietileno</strong>.</p>
            
            <p>O polietileno é um material de grande resistência mecânica e alta durabilidade, devido à exposição às intempéries. Por isso, a <strong>Capa em polietileno</strong> é a opção ideal para proteger a sua mercadoria contra qualquer dano. Ela faz a proteção contra sujeiras, água, poeira, variações de temperatura e qualquer outro tipo de influência externa.</p>
            
            <div class="picture-legend picture-right">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>Capa em polietileno</strong> é ideal para facilitar a inspeção e identificação de cargas, para a embalagem de produtos unitizados e também para a movimentação e transporte de mercadorias, por proporcionar maior segurança e rigidez.</p>
            
            <p>Outro diferencial da <strong>Capa em polietileno</strong> é a flexibilidade, permitindo o acondicionamento de cargas com disposições e tamanhos diferentes. Com tantas vantagens, a <strong>Capa em polietileno</strong> tem sido cada vez mais adotada em diversos segmentos, tais como indústrias químicas, fumageiras, calçadistas, metalúrgicas, de fundição e de alimentos.</p>
            
            <h2>Preços reduzidos para adquirir Capa em polietileno</h2>
            
            <p>Na hora de comprar <strong>Capa em polietileno</strong>, conte com todos os benefícios da JPR Embalagens. A empresa atua na área de embalagens plásticas flexíveis há mais de 15 anos, levando até o cliente as soluções mais inteligentes e econômicas do segmento.</p>
            
            <div class="picture-legend picture-left">
                <img class="lazyload" data-src="<?=$url.$pasta?>capas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A JPR Embalagens identifica oportunidades de melhoria, o que resulta em melhoria da qualidade dos produtos, redução de perdas e de custos. Para isso, faz investimentos constantes na atualização de seus profissionais e em equipamentos modernos, de última geração.</p>
            
            <p>O resultado disso é uma <strong>Capa em polietileno</strong> que se destaca pela qualidade e pela resistência, além de ter preço em conta e condições vantajosas de pagamento.</p>
            
            <p>Outra vantagem é o atendimento personalizado, voltado às necessidades de cada tipo de cliente. Por isso, aproveite. Entre em contato com um dos consultores para saber mais sobre o produto e as ótimas condições da empresa e solicite já seu orçamento, informando medidas e quantidade que você necessita.</p>

            
            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>           
        <br class="clear" />                                                                                                    
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>