<?php
	$h1    			= 'Bobina tubular plástica';
	$title 			= 'Bobina tubular plástica';
	$desc  			= 'A bobina tubular plástica pode ser feita conforme a necessidade de cada cliente, com possibilidade de fabricação liso ou impresso em até seis cores';
	$key   			= 'bobina, tubular, plástica, bobina tubular, bobina plástica, bobinas tubulares plásticas';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas tubulares plásticas';
	
	include('inc/head.php');
?>



<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
            
                                
    			<?=$caminhoProdutoBobinas?>
                 <article>
                <h1><?=$h1?></h1>     
                	<br>  
                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            
            <p>A versatilidade da <strong>bobina tubular plástica</strong> faz com que ela seja aplicada em diversos segmentos. Confira maiores informações.</p>

            <p>Na hora de vender um produto, é necessário investir na qualidade deste produto e também em outras questões, como é o caso da embalagem, por exemplo. Por isso, conheça a <strong>bobina tubular plástica</strong>.</p>
            
            <p>A <strong>bobina tubular plástica</strong> pode ser feita conforme a necessidade de cada cliente, com possibilidade de fabricação liso ou impresso em até seis cores. A impressão é uma forma de divulgar a sua marca no mercado.</p>
            
            <p>O material da bobina tubular plástica também pode variar. Ela pode ser feita em polipropileno, que se destaca por brilho e transparência e, por isso, tem utilização ampla na área de alimentos, gráficas e em confecções.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>Outra opção é a <strong>bobina tubular plástica</strong> de polietileno, usada em alimentos, leites, farináceos e bebidas, além de produtos metalúrgicos e automotivos. Por ser um material atóxico e resistente, pode ser usado para embalar qualquer tipo de produto, já que não altera as propriedades e características do que está sendo embalado.</p>
            
            <h2>Reduza custos com a bobina tubular plástica de material reciclado</h2>
            
         	<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <p>Uma opção para colaborar com o meio ambiente e ainda reduzir custos é a <strong>bobina tubular plástica</strong> feita com material reciclado.</p>
            
            <p>A fabricação pode ser feita em material reciclado cristal, a partir de aparas de material virgem e embalagens recicladas, resultando em embalagem de coloração amarelo claro, mas mantendo a transparência da embalagem. Outra opção é o reciclado canela, que tem coloração marrom clara e ainda mantém a transparência da embalagem.</p>

            
            <p>Para adquirir a <strong>bobina tubular plástica</strong>, conte com a JPR Embalagens. A empresa está no mercado de embalagens plásticas flexíveis há mais de 15 anos, com o objetivo de identificar oportunidades de melhoria e promover a redução de perdas e de custos.</p>
            
            <p>Para isso, a JPR Embalagens conta com uma equipe profissional, que está atualizada em relação às novidades do mercado, para levar até o cliente uma embalagem de qualidade elevada e com preço em conta, proporcionando a melhor relação custo-benefício do mercado.</p>
            
            <p>Aproveite. Entre já em contato com um dos nossos consultores para saber mais sobre os produtos e esclarecer eventuais dúvidas. Solicite já o seu orçamento, informando medidas e quantidade que você necessita.</p>
            
                                                    
            <?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>