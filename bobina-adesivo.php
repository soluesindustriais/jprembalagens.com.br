<?
	$h1    			= 'Bobina de adesivo';
	$title 			= 'Bobina de adesivo';
	$desc  			= 'A bobina de adesivo é um tipo de embalagem confeccionado em polietileno de baixa densidade, material que também é conhecido pela sigla PEBD';
	$key   			= 'bobina, adesivo, bobinas de adesivos';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Bobinas de adesivos';
	
	include('inc/head.php');
?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<? include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
             
               
                
    			<?=$caminhoProdutoBobinas?>      
                <h1><?=$h1?></h1>     
                	<article>
                <br>

                    
            
            <div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-01.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>A <strong>bobina de adesivo</strong> é um produto com ampla utilização em diversos segmentos. Confira maiores informações sobre este modelo de embalagem.</p>

            <p>A embalagem é um ponto fundamental na hora de confeccionar algum tipo de produto. Por isso, conte com opções de qualidade, como é o caso da <strong>bobina de adesivo</strong>.</p>
            
            <p>A <strong>bobina de adesivo</strong> é um tipo de embalagem confeccionado em polietileno de baixa densidade, material que também é conhecido pela sigla PEBD. Ela possui alta resistência ao rasgo e a rupturas, oferecendo maior proteção ao produto que está embalado.</p>

            <p>Justamente por ter estas características, a <strong>bobina de adesivo</strong> é amplamente utilizada em indústrias alimentícias, de confecções, metalúrgicas, entre outros segmentos. Por isso, é utilizada para a proteção de superfícies de cerâmico, alumínio e inclusive mármore, evitando problemas tais como arranhões ou danos causados por agentes externos.</p>
            
  			<div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-02.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
                        
            <h2>Bobina de adesivo na JPR Embalagens</h2>
            
            <p>E na JPR Embalagens você pode adquirir <strong>bobina de adesivo</strong> em diversas medidas e inclusive personalizadas, conforme necessidades e preferências de cada um dos nossos clientes. Disponibilizamos opção de <strong>bobina de adesivo</strong> lisa ou pigmentada.</p>
            
            
            <p>A JPR Embalagens é uma empresa que está no mercado há mais de 15 anos, sempre levando aos clientes a melhor solução em termos de embalagem. Além da <strong>bobina de adesivo</strong> de PEBD, a JPR Embalagens também disponibiliza outras opções de bobina, fabricadas em PEAD, PP, BOPP, PET, estrutura laminadas, entre outros, com possibilidade de serem fabricadas com matéria-prima virgem ou recicladas cristal, canela e colorido.</p>
            
			<div class="picture-legend picture-left">
                    <img class="lazyload" data-src="<?=$url.$pasta?>bobinas/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <p>O atendimento disponibilizado pela empresa é sempre personalizado, para garantir o atendimento ideal para o cliente, além de busca sempre por oportunidades de melhoria, de redução de perdas e de custos e de materiais de qualidade cada vez maior. Isso se reflete em um custo menor para a empresa, e esta redução é repassada para o nosso consumidor, que tem um produto de qualidade com um preço em conta.</p>
            
            <p>Saiba mais sobre a <strong>bobina de adesivo</strong> entrando em contato com os nossos profissionais e esclareça suas dúvidas. Aproveite as ótimas condições da JPR Embalagens e solicite já o seu orçamento, informando medida e quantidade que você necessita.</p>

            <? include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<? include('inc/coluna-lateral-paginas.php')?>
        
			<? include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <? include('inc/regioes.php');?>
            
            <? include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<? include('inc/footer.php');?>


</body>
</html>