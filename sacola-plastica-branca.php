<?php
$h1    			= 'Sacola plástica branca';
$title 			= 'Sacola plástica branca';
$desc  			= 'Fabricada em polietileno (PE), a sacola plástica branca é amplamente utilizada por gráficas, livrarias, editoras, padarias, supermercados, entre vários outros.';
$key   			= 'Sacola, plástica, branca, Sacolas plásticas brancas';
$var 			= 'Sacolas plásticas brancas';
$legendaImagem 	= ''.$var.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutoSacolas?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>   

             <p>Fabricada em polietileno (PE), a <strong>sacola plástica branca</strong> é amplamente utilizada por gráficas, livrarias, editoras, padarias, supermercados, entre vários outros.</p>
             <? $pasta = "imagens/produtos/sacolas/"; $quantia = 3; include('inc/gallery.php'); ?>
             <p>Por ser um produto super versátil, esse tipo de sacola pode receber vários tipos de alça, como: </p>
             <ul class="list">
                <li><strong>Sacola plástica branca com alça vazada</strong>;</li>
                <li><strong>Sacola plástica branca com alça camiseta</strong>;</li>
                <li><strong>Sacola plástica branca com alça boca de palhaço</strong>;</li>
                <li><strong>Sacola plástica branca com alça fita</strong>;</li>
                <li><strong>Sacola plástica branca com ilhós</strong>;</li>
                <li><strong>Sacola plástica branca com cordão</strong>;</li>
                <li>Entre outras.</li>
            </ul> 	
            <p>Podem ser lisas ou impressas em até 6 cores. A <strong>sacola plástica branca com personalização</strong> é um excelente canal para divulgar a sua marca, levando sua empresa para novos mercados.</p>
            <p>Para contribuir como meio ambiente, utilize a <strong>sacola plástica branca oxi-biodegradavel</strong>. É uma forma ecologicamente correta, além de você ter um novo argumento de vendas. Adote esta ideia.</p>
            <p>Além da <strong>sacola plástica</strong>, trabalhamos com uma ampla linha de embalagens, como <a href="<?=$url;?>envelope-seguranca" title="Envelope de Segurança"><strong>envelopes de segurança</strong></a>, <a href="<?=$url;?>saco-zip" title="Saco Zip"><strong>sacos com fecho zip</strong></a>, <a href="<?=$url;?>sacola-reciclada" title="Sacola Reciclada"><strong>sacola reciclada</strong></a>, embalagens especiais, filme e bobinas.</p>
            <p>Para <strong>sacola plástica branca com impressão</strong>, nosso lote mínimo de produção são de 250kg e sem impressão 150kg.</p>
            <p>Para receber um orçamento de <strong>sacola plástica branca</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>