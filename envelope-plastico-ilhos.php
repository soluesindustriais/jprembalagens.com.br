<?php
	$h1    			= 'Envelope plástico com ilhós';
	$title 			= 'Envelope plástico com ilhós';
	$desc  			= 'O envelope plástico com ilhós é uma novidade que chegou ao mercado brasileiro, trazendo mais praticidade e apresentando uma embalagem moderna e melhor ao seu consumidor.';
	$key   			= 'Envelopes plástico com ilhós, Envelopes, Envelope, plástico, ilhós';
	$legendaImagem 	= ''.$h1.'';
	$var 			= 'Envelopes plástico com ilhós';
	
	include('inc/head.php');
?>


<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>            
                    			
				<?=$caminhoProdutosEnvelopes?>
                 <article>
				<h1><?=$h1?></h1> 
                 <br> 
                                    
            <p>O <strong>envelope plástico com ilhós</strong> é uma novidade que chegou ao mercado brasileiro, trazendo mais praticidade e apresentando uma embalagem moderna e melhor ao seu consumidor.</p>
            
            <div class="picture-legend picture-right">
                    <img class="lazyload" data-src="<?=$url.$pasta?>envelopes/<?=$urlPagina?>-03.jpg" alt="<?=$h1?>" title="<?=$var?>" />
                    <strong><?=$legendaImagem?></strong>
            </div>
            
            <h2>Vantagens do envelope plástico com ilhós</h2>
            <p>A vantagem em se utilizar o <strong>envelope plástico com ilhós</strong>, é que você pode obter uma embalagem mais fina, assim reduzindo custos, e garantindo a mesma resistência e segurança que os sacos comuns com furos. Os <strong>envelopes plásticos com ilhós</strong>, são próprios para embalar acessórios para banheiros, cozinhas, metais/fundição, entre outros produtos.</p>
     
            <h2>O ilhós</h2>
            
            <p>O <strong>ilhós</strong> é uma argola de aço própria para ser utilizada em <strong>embalagens plásticas</strong>, é indicado para produtos que serão colocados em displays ou para produtos que vão para exposição, pois todo o peso do produto embalado é segurado pelo <strong>ilhós</strong>, pois ao contrário dos <a href="<?=$url;?>sacos" title="Sacos"><strong>Sacos</strong></a> comuns, que na maioria das vezes o furo é vazado na embalagem, dependendo do peso embalado, o furo pode ser danificado, e impossibilita o produto de ser colocado novamente no display.</p>
            <p>Geralmente o <strong>ilhós</strong> é aplicado em <strong>envelopes</strong> de polipropileno ou polipropileno, além disso, a modernidade da embalagem fala muito do produto que ela contém.</p>
            <p>Fabricamos <strong>envelope plástico com ilhós</strong> lisos ou impressos em até 6 cores, de acordo com a necessidade de cada cliente. Além disso, o <strong>envelope plástico com ilhós</strong> pode ser confeccionado com aba adesiva para facilitar o fechamento da embalagem, e produzido nas opções transparentes ou pigmentados em diversas cores.</p>
            
            <? $pasta = "imagens/produtos/envelopes/"; $quantia = 2; include('inc/gallery.php'); ?>
            
            <p>Devido à praticidade que muitos processos exigem, o <strong>envelope plástico com ilhós</strong> pode receber uma aba adesiva no final da embalagem, e assim dispensa o uso de seladoras, dando um acabamento melhor na embalagem.</p>
            <p> Para receber um orçamento do <strong>envelope plástico com ilhós</strong>, basta possuir as medidas (largura x comprimento x espessura), e a quantidade que você deseja utilizar.</p>
 
            
        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>