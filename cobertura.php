<?
	$h1    		= 'Cobertura';
	$title 		= 'Cobertura';
	$desc  		= 'A JPR Embalagens traz para o mercado uma linha completa de envelopes. Em nossa linha de produtos você encontra cobertura para pallet.';
	$key   		= 'Cobertura, Cobertura pallet, cobertura palete';
	$var 		= 'Cobertura';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
    
              
                <?=$caminhoCategoria?>
				<article>
                
                <h1><?=$h1?></h1>     
                    
            <h2>A JPR Embalagens traz para o mercado uma linha completa de envelopes.</h2>
            
            <p>Em nossa linha de produtos você encontra <strong>cobertura para pallet</strong>, de acordo com a necessidade da sua empresa.</p>
            
            <p>Conheça nossa linha de <strong>cobertura</strong>:</p>
            
                <ul class="thumbnails">
                     <li>
                          <a rel="nofollow" href="<?=$url;?>cobertura-palete" title="Cobertura para Palete"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/cobertura/thumb/cobertura-palete-01.jpg" alt="Cobertura para Palete" /></a>
                          <h2><a href="<?=$url;?>cobertura-palete" title="Cobertura para Palete">Cobertura para Palete</a></h2>
                     </li>
                     <li>
                          <a rel="nofollow" href="<?=$url;?>cobertura-pallet" title="Cobertura para Pallet"><img class="lazyload" data-src="<?=$url;?>imagens/produtos/cobertura/thumb/cobertura-pallet-01.jpg" alt="Cobertura para Pallet" /></a>
                          <h2><a href="<?=$url;?>cobertura-pallet" title="Cobertura para Pallet">Cobertura para Pallet</a></h2>
                     </li>
    			</ul>          
                
            <? include('inc/saiba-mais.php');?>
            
            </article>
            	
			<? include('inc/coluna-lateral-paginas.php');?>   
               
           	<br class="clear" />  
                    
            <? include('inc/regioes.php');?>
            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>