<?php
$h1    			= 'Saco reciclado';
$title 			= 'Saco reciclado';
$desc  			= 'Fabricamos saco reciclado em diversos modelos e cores, essa é uma das formas mais econômicas para embalar diversos tipos de produto';
$key   			= 'Sacos reciclados, Saco, sacos, reciclado, Saco reciclado cristal, Saco reciclado canela, Saco reciclado colorido';
$var 			= 'Sacos reciclados';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br>  

             <p>Fabricamos <strong>saco reciclado</strong> em diversos modelos e cores, essa é uma das formas mais econômicas para embalar diversos tipos de produto, garantindo a integridade da embalagem durante o transporte, e sendo resistentes a quedas e ao rasgo e ruptura.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 2; include('inc/gallery.php'); ?>
             <h2>Trabalhamos com três modelos de saco reciclado, sendo eles, cristal, canela e colorido.</h2>
             <ul class="list">
                <li><strong>Saco reciclado cristal</strong>: produzido a partir das aparar do material virgem e também com embalagens que já foram recicladas, como embalagens de alimentos, sacolas plásticas e sacarias em geral. O material fica com alguns pontos, devido o processo de reciclagem que ele passa, e a cor da embalagem altera para amarelo claro, porem continua com transparência, sendo possível visualizar o produto dentro da embalagem.</li>
                <li><strong>Saco reciclado canela</strong>: produzido com as aparas de plásticos reciclados, o reciclado canela continua resistente e proporciona segurança ao rasgo e a ruptura da embalagem, pois possui uma solda reforçada. Também fica com alguns pontos, devido ao processo de reciclagem que ele passa, e a cor da embalagem altera para marrom claro (cor de canela), porem continua com transparência, sendo possível visualizar o produto dentro da embalagem.</li>
                <li><strong>Saco reciclado colorido</strong>: produzido com uma mistura de embalagens recicladas, o reciclado colorido fica sem padrão de cor e sem transparência. É uma das matérias-primas mais em conta que existe na linha das embalagens plásticas flexíveis.</li>
            </ul> 
            <p>São fabricados sob medida, de acordo com a necessidade de cada cliente. Os <strong>sacos reciclados</strong> podem ser lisos impressos em até 6 cores.</p>
            <p>Nossa quantidade mínima de produção são de 250kg para <strong>saco reciclado impresso</strong> e 150kg liso.</p>
            <p>Para receber um orçamento de <strong>saco reciclado</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>