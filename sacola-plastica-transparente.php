<?php
	$h1    			= 'Sacola plástica transparente';
	$title 			= 'Sacola plástica transparente';
	$desc  			= 'A sacola plástica transparente, pode ser lisa ou impressa em até 6 cores. São infinitas as aplicações do produto, é um produto simples e pratico.';
	$key   			= 'Sacola, plástica, transparente, Sacolas plásticas transparentes, sacola plástica transparente personalizada';
	$var 			= 'Sacolas plásticas transparentes';
	$legendaImagem 	= ''.$h1.'';
	
	include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
   
		<?php include('inc/topo.php');?> 
        
    </div>
            
    <div class="wrapper">
          
        <main role="main">
    
            <section>
               
                
    			<?=$caminhoProdutoSacolas?>                
                <!-- <article> <article> -->
	            <h1><?=$h1?></h1>     
                	
                <br>   

            <p>São fabricadas sob medida, de acordo com a necessidade de cada cliente.</p>
            <? $pasta = "imagens/produtos/sacolas/"; $quantia = 2; include('inc/gallery.php'); ?>
    
            <p>A <strong>sacola plástica transparente</strong>, pode ser lisa ou impressa em até 6 cores. São infinitas as aplicações do produto, é um produto simples e pratico, além de ser leve e resistente ao mesmo tempo.</p>
            <h2>Modelos de cortes, soldas e alças</h2>
            <p>Por ser versátil, a <strong>sacola plástica transparente</strong> pode receber diversos modelos de cortes, soldas e alças. Veja alguns dos nossos modelos:</p>
            <ul class="list">
                <li><strong>Com alça vazada</strong>;</li>
                <li><strong>Com alça camiseta</strong>;</li>
                <li><strong>Com alça boca de palhaço</strong>;</li>
                <li><strong>Com alça fita</strong>;</li>
                <li><strong>Com ilhós</strong>;</li>
                <li><strong>Com zíper</strong>;</li>
                <li><strong>Com cordão</strong>;</li>
                <li>Entre muitas outras.</li>
            </ul> 
            <h2>Redução de custos com a sacola plástica transparente</h2>
            <p>Se deseja reduzir custos com embalagens, utilize a <strong>sacola plástica transparente</strong> produzidas a partir de matérias-primas recicladas. Nesta opção, além de você ter uma grande economia, também está contribuindo com o nosso meio ambiente.</p>
            <p>Podem ser produzidas em diversas cores, e as <strong>sacolas plásticas transparentes</strong> são próprias para divulgação em feiras, eventos, lojas, shoppings, e para outros estabelecimentos que buscam sempre estar próximos aos seus clientes, fornecendo um meio de transporte seguro e que garanta a integridade do produto até o seu destino final.</p>
            <p>Para <strong>sacola plástica transparente personalizada</strong>, nosso lote mínimo de produção são de 250kg e sem impressão 150kg.</p>
            <p>Para receber um orçamento de <strong>sacola plástica transparente</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>


        	<?php include('inc/saiba-mais.php');?>
            
            
			
            </article>
            	
          	<?php include('inc/coluna-lateral-paginas.php');?>
        
			<?php include('inc/paginas-relacionadas.php');?>  
               
            	<br class="clear" />  
        

            
            <?php include('inc/regioes.php');?>
            
            <?php include('inc/copyright.php');?>

            
        	</section>

        </main>

    	
	
    </div><!-- .wrapper -->
    

    
	<?php include('inc/footer.php');?>


</body>
</html>