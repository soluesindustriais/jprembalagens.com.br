<?
$h1            = 'JPR Embalagens';
$title         = 'Home';
$desc          = 'A JPR Embalagens atua na fabricação de embalagens plásticas flexíveis como, sacolas, sacos, envelopes, sacos para lixo, envelopes de segurança, sacolas biodegradáveis, entre outros produtos.';
$key           = 'Embalagens, envelopes, sacos, sacolas, embalagens, plásticas, flexíveis, embalagens flexíveis';
$var         = 'Home';

include('inc/head.php');
?>

</head>

<body>

    <? include('inc/topo.php'); ?>

    <div class="wrapper">

        <main role="main">

            <section>


                <div class="content-index">

                    <h1><?= $h1 ?></h1>

                    <ul class="thumbnailsHome">

                        <li>
                            <h2><a href="<?= $url; ?>envelopes" title="Envelopes"><span class="category">Envelopes</span></a></h2>

                            <a rel="nofollow" href="<?= $url; ?>envelopes" title="Envelopes"><img class="lazyload" data-src="<?= $url; ?>imagens/envelopes-index.jpg" alt="Envelopes" title="Envelopes" /></a>


                            <p class="description">Envelopes personalizados, Envelope Adesivado, Envelope Inviolável, Envelope Reciclado, entre outros.</p>

                            <a rel="nofollow" href="<?= $url; ?>envelopes" title="Veja mais sobre Envelopes">Saiba mais</a>
                        </li>

                        <li>
                            <h2><a href="<?= $url; ?>sacos" title="Sacos"><span class="category">Sacos</span></a></h2>
                            <a rel="nofollow" href="<?= $url; ?>sacos" title="Sacos"><img class="lazyload" data-src="<?= $url; ?>imagens/sacos-index.jpg" alt="Sacos" title="Sacos" /></a>


                            <p class="description">Saco Adesivado ,Saco Adesivado Com Furo, Saco Bolha, Saco de Lixo, Saco Hospitalar, entre outros.</p>

                            <a rel="nofollow" href="<?= $url; ?>sacos" title="Veja mais sobre Sacos">Saiba mais</a>

                        </li>

                        <li>
                            <h2><a href="<?= $url; ?>sacolas" title="Sacolas"><span class="category">Sacolas</span></a></h2>
                            <a rel="nofollow" href="<?= $url; ?>sacolas" title="Sacolas"><img class="lazyload" data-src="<?= $url; ?>imagens/sacolas-index.jpg" alt="Sacolas" title="Sacolas" /></a>

                            <p class="description">Sacola Alça Fita, Sacola Alça Vazada, Sacola Camiseta, Sacola Impressa, Sacola Personalizada, entre outras.</p>

                            <a rel="nofollow" href="<?= $url; ?>sacolas" title="Veja mais sobre Sacolas">Saiba mais</a>

                        </li>

                        <li>
                            <h2><a href="<?= $url; ?>bobinas" title="Bobinas"><span class="category">Bobinas</span></a></h2>


                            <a rel="nofollow" href="<?= $url; ?>bobinas" title="Bobinas"><img class="lazyload" data-src="<?= $url; ?>imagens/bobinas-index.jpg" alt="Bobinas" title="Bobinas" /></a>



                            <p class="description">A embalagem precisa ter um bom aspecto visual e deve ser de qualidade. Conheça nossas opções</p>

                            <a rel="nofollow" href="<?= $url; ?>bobinas" title="Veja mais sobre Bobinas">Saiba mais</a>

                        </li>

                        <li>
                            <h2><a href="<?= $url; ?>capas" title="Capas"><span class="category">Capas</span></a></h2>
                            <a rel="nofollow" href="<?= $url; ?>capas" title="Capas"><img class="lazyload" data-src="<?= $url; ?>imagens/capas-index.jpg" alt="Capas" title="Capas" /></a>


                            <p class="description">A embalagem é um ponto fundamental para conservar os seus produtos, fazendo com que eles estejam protegidos de influências externas.</p>

                            <a rel="nofollow" href="<?= $url; ?>capas" title="Veja mais sobre Capas">Saiba mais</a>

                        </li>

                        <li>
                            <h2><a href="<?= $url; ?>cobertura" title="Cobertura"><span class="category">Cobertura</span></a></h2>
                            <a rel="nofollow" href="<?= $url; ?>cobertura" title="Cobertura"><img class="lazyload" data-src="<?= $url; ?>imagens/cobertura-index.jpg" alt="Cobertura" title="Cobertura" /></a>


                            <p class="description">A cobertura ainda proporciona segurança e rigidez na movimentação de cargas, além de facilidade na hora de inspecionar e identificar a carga.</p>

                            <a rel="nofollow" href="<?= $url; ?>cobertura" title="Veja mais sobre Cobertura">Saiba mais</a>

                        </li>
                        <li>
                            <h2><a href="<?= $url; ?>plastico" title="Plásticos"><span class="category">Plásticos</span></a></h2>

                            <a rel="nofollow" href="<?= $url; ?>plastico" title="Plásticos"><img class="lazyload" data-src="<?= $url; ?>imagens/plasticos-index.jpg" alt="Plásticos" title="Plásticos" /></a>


                            <p class="description">A JPR Embalagens traz para o mercado uma linha completa de plásticos.</p>

                            <a rel="nofollow" href="<?= $url; ?>plastico" title="Veja mais sobre Plástico">Saiba mais</a>

                        </li>
                        <li>
                            <h2><a href="<?= $url; ?>rolo" title="Rolo"><span class="category">Rolo</span></a></h2>


                            <a rel="nofollow" href="<?= $url; ?>rolo" title="Rolo"><img class="lazyload" data-src="<?= $url; ?>imagens/rolo-index.jpg" alt="Rolo" title="Rolo" /></a>



                            <p class="description">A embalagem de um determinado produto precisa ser de qualidade para que haja a proteção adequada do que está sendo embalado.</p>

                            <a rel="nofollow" href="<?= $url; ?>rolo" title="Rolo">Saiba mais</a>

                        </li>
                        <li>
                            <h2><a href="<?= $url; ?>filme" title="Filme"><span class="category">Filme</span></a></h2>

                            <a rel="nofollow" href="<?= $url; ?>filme" title="Filme"><img class="lazyload" data-src="<?= $url; ?>imagens/filme-index.jpg" alt="Filme" title="Filme" /></a>


                            <p class="description">Para embalar produtos com formatos irregulares, uma ótima opção é o filme. Saiba mais sobre este tipo de embalagem.</p>

                            <a rel="nofollow" href="<?= $url; ?>filme" title="Veja mais sobre Filme">Saiba mais</a>

                        </li>
                    </ul>


                </div>
            </section>


        </main>



    </div><!-- wrapper -->



    <? include('inc/footer.php'); ?>


</body>

</html>