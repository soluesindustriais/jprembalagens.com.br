<?php
$h1    			= 'Saco plástico BD';
$title 			= 'Saco plástico BD';
$desc  			= 'O saco plástico BD é uma embalagem muito utilizada para embalar roupas, tecidos, alimentos, produtos gráficos, jornais, produtos médico-hospitalares, entre muitos outros.';
$key   			= 'Sacos plásticos BD, Saco, sacos, plástico, BD, saco plástico polietileno de baixa densidade';
$var 			= 'Sacos plásticos BD';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">
     
      <?php include('inc/topo.php');?> 
      
  </div>
  
  <div class="wrapper">
      
    <main role="main">
        
        <section>
            
                            
             <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     
             
             <br> 
             
             
             <p>Trabalhamos com uma ampla linha de <strong>sacos BD</strong> (polietileno de baixa densidade) podendo ser lisos ou impressos em até 6 cores.</p>
             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>
             
             <p>O <strong>saco plástico BD</strong> é uma embalagem muito utilizada para embalar roupas, tecidos, alimentos, produtos gráficos, jornais, produtos médico-hospitalares, entre muitos outros. </p>
             <p>São fabricados sob medida, de acordo com a necessidade de cada cliente, além de poderem ser produzidos na cor transparente ou pigmentado em diversas cores.</p>
             <h2>Veja abaixo alguns modelos e formatos que o saco plástico BD é fabricado:</h2>
             <ul class="list">
                <li><strong>saco plástico BD com aba adesiva</strong> é um sistema pratico para fechamento da embalagem. Pode ser feito em dois formatos, o permanente e o abre e fecha. O adesivo permanente é indicado para produtos que não poder ser violados, assim para acessar o produto é necessário danificar a embalagem, e o adesivo abre e fecha pode ser aberto e fechado novamente várias vezes.</li>
                <li><strong>saco plástico BD com fecho zip ou ziplock</strong> é uma das embalagens mais pratica e moderna do mercado. Uma embalagem com fecho zip expressa a sua preocupação em inovar, em oferecer sempre o melhor. Poucas embalagens protegem o produto como o <strong>saco com fecho zip</strong>. Mas, diferentemente de qualquer outra, só o zip continua protegendo após a abertura. Basta fechar novamente que a proteção continua.</li>
            </ul>
            <p>Além disso, o <strong>saco plástico BD</strong> pode ser 100% reciclável, é uma forma ecologicamente correta de contribuirmos com a natureza. Se você deseja reduzir seus custos com embalagem, utilize <strong>saco plástico BD</strong> reciclado. </p>
            <p>Nossa quantidade mínima de produção são de 100kg para <strong>saco plástico BD</strong> liso e 300kg impresso.</p>
            <p>Para receber um orçamento de <strong>saco plástico BD</strong>, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            
            
        </article>
        
        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  
        
        <br class="clear" />  
        

        
        <?php include('inc/regioes.php');?>
        
        <?php include('inc/copyright.php');?>

        
    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>