<?php
$h1    			= 'Saco plástico valvulado';
$title 			= 'Saco plástico valvulado';
$desc  			= 'O Saco plástico valvulado é fabricado em polietileno natural ou pigmentado em diversas cores, podem ser lisos ou impressos em até 6 cores';
$key   			= 'Sacos plásticos valvulados, Saco, sacos, plástico, valvulado, Saco plástico com valvula';
$var 			= 'Sacos plásticos valvulados';
$legendaImagem 	= ''.$h1.'';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php');?>

<!-- Função Regiões -->
<script src="<?=$url;?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>
<body>

    <div class="wrapper-topo">

      <?php include('inc/topo.php');?> 

  </div>

  <div class="wrapper">

    <main role="main">

        <section>

                       <?=$caminhoProdutosSacos?>                
              <article>
             <h1><?=$h1?></h1>     

             <br> 

             <p>O <strong>Saco plástico valvulado</strong> é fabricado em polietileno natural ou pigmentado em diversas cores, podem ser lisos ou impressos em até 6 cores.</p>

             <? $pasta = "imagens/produtos/sacos/"; $quantia = 3; include('inc/gallery.php'); ?>


             <p>É um <a href="<?=$url;?>saco-plastico" title="Saco Plastico"><strong>saco plastico</strong></a> super moderno, e possui um sistema de fechamento prático e eficiente. A válvula se fecha automaticamente através da compressão do produto ensacado.</p>
             <p>Fabricados com um sistema avançado de filamentos selados, nossos <strong>sacos plástico valvulados</strong> são isentos de costuras e facilitam os processos de enchimento automático, paletização, transporte e armazenagem.</p>
             <p>Esse tipo de embalagem flexível é amplamente utilizado por indústrias de alimentos, adubos, produtos químicos e minérios.</p>
             <p>Também podem ser produzidos com matéria-prima reciclada. É uma opção para reduzir seus custos com embalagens.</p>
             <p>Nossos <strong>sacos plásticos valvulados</strong> possuem alta resistência à tração, são flexíveis, são resistentes ao frio e inodoro. O polímero utilizado é qualificado para produtos alimentares.</p>
             <h2>Há vários formatos que o saco plástico valvulado pode ser produzido, veja alguns modelos:</h2>
             <ul class="list">
                <li><strong>Saco plástico valvulado com válvula topo e fundo quadrado</strong>.</li>
                <li><strong>Saco plástico valvulado com válvula topo e fundo sanfonado</strong>.</li>
                <li><strong>Saco plástico valvulado com válvula topo com fundo reto</strong>.</li>
                <li><strong>Saco plástico valvulado com válvula traseira, próprio para argamassa</strong>. (Este modelo não fabricamos)</li>
            </ul>	
            <p>Nossa quantidade mínima de produção são de 100kg para <strong>sacos plásticos valvulados liso</strong> e 250kg impresso.</p>
            <p>Para receber um orçamento desse produto, basta possuir as medidas (largura x comprimento x espessura) e a quantidade estimada.</p>

            <?php include('inc/saiba-mais.php');?>
            
            

        </article>

        <?php include('inc/coluna-lateral-paginas.php');?>
        
        <?php include('inc/paginas-relacionadas.php');?>  

        <br class="clear" />  
        


        <?php include('inc/regioes.php');?>

        <?php include('inc/copyright.php');?>


    </section>

</main>



</div><!-- .wrapper -->



<?php include('inc/footer.php');?>


</body>
</html>