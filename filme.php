<?
$h1        = 'Filme';
$title     = 'Filme';
$desc      = 'A JPR Embalagens traz para o mercado uma linha completa de envelopes. Em nossa linha de produtos você encontra filme de polietileno e encolhíveis.';
$key       = 'Filme, polietileno, Filme encolhível';
$var     = 'Filme';

include('inc/head.php');
?>

<!-- Fancy Lightbox -->
<? include('inc/fancy.php'); ?>

<!-- Função Regiões -->
<script src="<?= $url; ?>js/organictabs.jquery.js" type="text/javascript"></script>
</head>

<body>

  <div class="wrapper-topo">

    <? include('inc/topo.php'); ?>

  </div>

  <div class="wrapper">

    <main role="main">

      <section>


        <?= $caminhoCategoria ?>
        <article>

          <h1><?= $h1 ?></h1>

          <h2>A JPR Embalagens traz para o mercado uma linha completa de envelopes.</h2>

          <p>Em nossa linha de produtos você encontra <strong>filmes de polietileno e encolhíveis</strong>, de acordo com a necessidade da sua empresa.</p>

          <p>Conheça nossa linha de <strong>filmes</strong>:</p>

          <ul class="thumbnails">
            <li>
              <a rel="nofollow" href="<?= $url; ?>filme-polietileno" title="Filme de Polietileno"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-polietileno-01.jpg" alt="Filme de Polietileno" /></a>
              <h2><a href="<?= $url; ?>filme-polietileno" title="Filme de Polietileno">Filme de Polietileno</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>filme-termo-encolhivel" title="Filme Termo Encolhível"><img class="lazyload" data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-termo-encolhivel-01.jpg" alt="Filme Termo Encolhível" /></a>
              <h2><a href="<?= $url; ?>filme-termo-encolhivel" title="Filme Termo Encolhível">Filme Termo Encolhível</a></h2>
            </li>
            <li>
              <a rel="nofollow" href="<?= $url; ?>filme-termo-encolhivel-shrink" title="filme termo encolhível shrink"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-0.jpg" alt="filme termo encolhível shrink" class="lazyload" title="filme termo encolhível shrink" /></a>
              <h4><a href="<?= $url; ?>filme-termo-encolhivel-shrink" title="filme termo encolhível shrink">filme termo encolhível shrink/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>industria-de-envelopes-de-seguranca" title="indústria de envelopes de segurança"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-8.jpg" alt="indústria de envelopes de segurança" class="lazyload" title="indústria de envelopes de segurança" /></a>
              <h4><a href="<?= $url; ?>industria-de-envelopes-de-seguranca" title="indústria de envelopes de segurança">indústria de envelopes de segurança/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>onde-comprar-saco-de-lixo" title="onde comprar saco de lixo"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-0.jpg" alt="onde comprar saco de lixo" class="lazyload" title="onde comprar saco de lixo" /></a>
              <h4><a href="<?= $url; ?>onde-comprar-saco-de-lixo" title="onde comprar saco de lixo">onde comprar saco de lixo/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>bobina-de-saco-plastico-para-alimentos" title="bobina de saco plástico para alimentos"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-9.jpg" alt="bobina de saco plástico para alimentos" class="lazyload" title="bobina de saco plástico para alimentos" /></a>
              <h4><a href="<?= $url; ?>bobina-de-saco-plastico-para-alimentos" title="bobina de saco plástico para alimentos">bobina de saco plástico para alimentos/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>bobina-de-saco-plastico-transparente" title="bobina de saco plástico transparente"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-4.jpg" alt="bobina de saco plástico transparente" class="lazyload" title="bobina de saco plástico transparente" /></a>
              <h4><a href="<?= $url; ?>bobina-de-saco-plastico-transparente" title="bobina de saco plástico transparente">bobina de saco plástico transparente/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>bobina-filme-stretch-a-venda" title="bobina filme stretch a venda"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-4.jpg" alt="bobina filme stretch a venda" class="lazyload" title="bobina filme stretch a venda" /></a>
              <h4><a href="<?= $url; ?>bobina-filme-stretch-a-venda" title="bobina filme stretch a venda">bobina filme stretch a venda/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>rolo-de-plastico-filme-preco" title="rolo de plástico filme preço"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-7.jpg" alt="rolo de plástico filme preço" class="lazyload" title="rolo de plástico filme preço" /></a>
              <h4><a href="<?= $url; ?>rolo-de-plastico-filme-preco" title="rolo de plástico filme preço">rolo de plástico filme preço/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>embalagem-plastica-flexivel-para-industria" title="embalagem plástica flexível para indústria"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-5.jpg" alt="embalagem plástica flexível para indústria" class="lazyload" title="embalagem plástica flexível para indústria" /></a>
              <h4><a href="<?= $url; ?>embalagem-plastica-flexivel-para-industria" title="embalagem plástica flexível para indústria">embalagem plástica flexível para indústria/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>saco-plastico-com-lacre-de-seguranca" title="saco plástico com lacre de segurança"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-3.jpg" alt="saco plástico com lacre de segurança" class="lazyload" title="saco plástico com lacre de segurança" /></a>
              <h4><a href="<?= $url; ?>saco-plastico-com-lacre-de-seguranca" title="saco plástico com lacre de segurança">saco plástico com lacre de segurança/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>embalagem-plastica-pp" title="embalagem plástica pp"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-7.jpg" alt="embalagem plástica pp" class="lazyload" title="embalagem plástica pp" /></a>
              <h4><a href="<?= $url; ?>embalagem-plastica-pp" title="embalagem plástica pp">embalagem plástica pp/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>embalagem-plastica-preco" title="embalagem plástica preço"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-4.jpg" alt="embalagem plástica preço" class="lazyload" title="embalagem plástica preço" /></a>
              <h4><a href="<?= $url; ?>embalagem-plastica-preco" title="embalagem plástica preço">embalagem plástica preço/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>bobina-filme-stretch-comprar" title="bobina filme stretch comprar"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-8.jpg" alt="bobina filme stretch comprar" class="lazyload" title="bobina filme stretch comprar" /></a>
              <h4><a href="<?= $url; ?>bobina-filme-stretch-comprar" title="bobina filme stretch comprar">bobina filme stretch comprar/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>bobina-filme-stretch-distribuidor" title="bobina filme stretch distribuidor"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-3.jpg" alt="bobina filme stretch distribuidor" class="lazyload" title="bobina filme stretch distribuidor" /></a>
              <h4><a href="<?= $url; ?>bobina-filme-stretch-distribuidor" title="bobina filme stretch distribuidor">bobina filme stretch distribuidor/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>envelope-plastico-com-adesivo-correio" title="envelope plástico com adesivo correio"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-3.jpg" alt="envelope plástico com adesivo correio" class="lazyload" title="envelope plástico com adesivo correio" /></a>
              <h4><a href="<?= $url; ?>envelope-plastico-com-adesivo-correio" title="envelope plástico com adesivo correio">envelope plástico com adesivo correio/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>embalagem-saco-plastico" title="embalagem saco plástico"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-8.jpg" alt="embalagem saco plástico" class="lazyload" title="embalagem saco plástico" /></a>
              <h4><a href="<?= $url; ?>embalagem-saco-plastico" title="embalagem saco plástico">embalagem saco plástico/a></h4>
            </li>




            <li>
              <a rel="nofollow" href="<?= $url; ?>bobina-filme-stretch-fabricante" title="bobina filme stretch fabricante"><img data-src="<?= $url; ?>imagens/produtos/filme/thumb/filme-6.jpg" alt="bobina filme stretch fabricante" class="lazyload" title="bobina filme stretch fabricante" /></a>
              <h4><a href="<?= $url; ?>bobina-filme-stretch-fabricante" title="bobina filme stretch fabricante">bobina filme stretch fabricante/a></h4>
            </li>

          </ul>

          <? include('inc/saiba-mais.php'); ?>

        </article>

        <? include('inc/coluna-lateral-paginas.php'); ?>

        <br class="clear" />

        <? include('inc/regioes.php'); ?>

      </section>
    </main>



  </div><!-- .wrapper -->



  <? include('inc/footer.php'); ?>


</body>

</html>